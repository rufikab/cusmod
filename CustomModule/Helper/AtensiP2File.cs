﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using CustomModule;

namespace CustomModule.Helper
{
    public static class AtensiP2File
    {
        //Entities entities = new Entities();
        public static string base_path = ConfigurationManager.AppSettings["AtensiP2FilePath"];

        public static string GetFilePathById(int id)
        {
            Entities entities = new Entities();
            return (from p in entities.P2FILE where p.ID == id select p.DOCPATH).FirstOrDefault();
        }

        public static string generateFileName(int id,string ext,string action="Hold")
        {
            string basePath = AtensiP2File.base_path;
            if (!Directory.Exists(basePath))
                Directory.CreateDirectory(basePath);
            string save_path = basePath + "//" + id.ToString();
            if (!Directory.Exists(save_path))
                Directory.CreateDirectory(save_path);
            save_path = save_path + "//" + action;
            if (!Directory.Exists(save_path))
                Directory.CreateDirectory(save_path);
            System.Threading.Thread.Sleep(30);
            return save_path + "//P2" + ext;
        }
    }
}