﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace CustomModule.Helper
{
    public class SoapEnvelope
    {
        private string XMLResponse = string.Empty;

        public string post(string destinationUrl, string requestXml)
        {
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                requestStream.Dispose();
            }
            request.BeginGetResponse(new AsyncCallback(onFinishRequest), request);
            return XMLResponse;
        }
        
        void onFinishRequest(IAsyncResult result)
        {
            using (HttpWebResponse response = (result.AsyncState as HttpWebRequest).EndGetResponse(result) as HttpWebResponse)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        string responseStr = new StreamReader(responseStream).ReadToEnd();
                        XMLResponse = responseStr;
                        responseStream.Close();
                        responseStream.Dispose();
                    }
                }
                else
                    XMLResponse = null;
                response.Close();
            }
        }
    }
}
