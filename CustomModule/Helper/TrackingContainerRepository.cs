﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Xml;

namespace CustomModule.Helper
{
    public class TrackingContainerRepository
    {
        private static string _url = System.Configuration.ConfigurationManager.AppSettings["NPCT1"];
        private static string _xmlDir = System.Configuration.ConfigurationManager.AppSettings["messageXMLDirName"];

        public List<TrackingModel> trackingContainer(string inout,string voyage,string container,bool dummy=false)
        {
            var xmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _xmlDir, "TrackingContainer.xml");
            string xmlString = File.ReadAllText(xmlPath);
            StringBuilder xmlRequest = new StringBuilder(xmlString);
            xmlRequest.Replace("[inout]", inout.Trim());
            xmlRequest.Replace("[voyage]", voyage.Trim());
            xmlRequest.Replace("[container]", container.Trim());
            string stringResponse = string.Empty;
            if (dummy)
            {
                var xmlPathDummy = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _xmlDir, "TrackingContainerDummy.xml");
                stringResponse = File.ReadAllText(xmlPathDummy);
            }
            else
            {
                SoapEnvelope soap = new SoapEnvelope();
                stringResponse = soap.post(_url, xmlRequest.ToString());
            }
            return parseTrackingContainer(stringResponse);
        }

        private List<TrackingModel> parseTrackingContainer(string xmlString)
        {
            List<TrackingModel> result = new List<TrackingModel>();
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(xmlString);
            XmlNamespaceManager xNSMgr = new XmlNamespaceManager(xDoc.NameTable);
            xNSMgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            xNSMgr.AddNamespace("urn", "urn:tracking_container");
            XmlNodeList xNodes = xDoc.SelectNodes("//soap:Envelope/soap:Body/urn:tracking_containerResponse ", xNSMgr);
            if (xNodes.Count > 0)
            {
                XmlNodeList nodes = xNodes[0].ChildNodes;
                if (!string.IsNullOrWhiteSpace(nodes[0].InnerText))
                {
                    if (!nodes[0].InnerText.Contains("Records not found"))
                    {
                        string data = nodes[0].InnerText;
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(data);
                        XmlNodeList xmlnode = xmlDoc.GetElementsByTagName("loop"); 
                        for (var i = 0; i < xmlnode.Count; i++)
                        {
                            TrackingModel model = new TrackingModel();
                            model.response_no = xmlnode[i].ChildNodes.Item(9).ChildNodes.Item(4).InnerText;
                            model.response_date = xmlnode[i].ChildNodes.Item(9).ChildNodes.Item(5).InnerText;
                            model.no_container = xmlnode[i].ChildNodes.Item(10).ChildNodes.Item(0).InnerText;
                            model.status = xmlnode[i].ChildNodes.Item(10).ChildNodes.Item(10).InnerText;
                            result.Add(model);
                        }
                    }
                }
            }
            return result;
        }
    }

    public class TrackingModel
    {
        public string response_no { get; set; }
        public string response_date { get; set; }
        public DateTime response_date_parsed
        {
            get
            {
                DateTime result;
                DateTime.TryParseExact(this.response_date, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out result);
                return result;
            }
        }
        public string no_container { get; set; }
        public string status { get; set; }
    }
}