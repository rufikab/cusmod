﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CustomModule.Helper
{
    public class FileLogger
    {
        private const long LOGFILE_MAX_BYTES = 50000000; //50MB

        public void Trace(string message)
        {
            LogMessage("Trace", message, null, null);
        }

        public void Debug(string message)
        {
            LogMessage("Debug", message, null, null);
        }

        public void Info(string message)
        {
            LogMessage("Info", message, null, null);
        }

        public void Warn(string message)
        {
            LogMessage("Warn", message, null, null);
        }

        public void Error(string message)
        {
            LogMessage("Error", message, null, null);
        }

        public void Error(string message, Exception ex)
        {
            string technicalMessage = string.Empty;

            if (ex.InnerException != null)
                technicalMessage = ex.InnerException.ToString();

            LogMessage("Error", message, technicalMessage, ex.StackTrace);
        }

        public void Fatal(string message)
        {
            LogMessage("Fatal", message, null, null);
        }

        public void Fatal(string message, Exception ex)
        {
            string technicalMessage = string.Empty;

            if (ex.InnerException != null)
                technicalMessage = ex.InnerException.ToString();

            LogMessage("Fatal", message, technicalMessage, ex.StackTrace);
        }

        private static bool LogMessage(string loggerType, string errorMsg, string technicalMsg, string stackTrace)
        {

            try
            {
                string dirPath = System.Configuration.ConfigurationManager.AppSettings["LogPath"];
                if (string.IsNullOrEmpty(dirPath))
                {
                    dirPath = "C:\\LogCustomsModule";
                }
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }

                StringBuilder msg = new StringBuilder();
                string logFilePath = dirPath + "\\CustomsModule" + DateTime.Today.ToString("yyyyMMddHH") + ".log";

                long fileSize = 0;

                if (File.Exists(logFilePath))
                {
                    //msg.AppendLine("");
                    FileStream fileStream = File.OpenRead(logFilePath);
                    fileSize = fileStream.Length;
                    fileStream.Close();
                }

                if (fileSize < LOGFILE_MAX_BYTES)
                {
                    //Date : Type - Message
                    string template = "{0} : {1} - {2}";
                    string logDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fffffffK");
                    string logText = string.Format(template, logDate, loggerType, errorMsg);
                    msg.AppendLine(logText);

                    if (!string.IsNullOrEmpty(stackTrace))
                        msg.AppendLine("  Stack trace: " + stackTrace);

                    if (!string.IsNullOrEmpty(technicalMsg))
                        msg.AppendLine("  Inner exception: " + technicalMsg);

                    //msg.Append("\n======================================");
                    //msg.Append("");
                    if (fileSize + msg.Length >= LOGFILE_MAX_BYTES)
                    {
                        msg.AppendLine("====== MAXIMUM FILESIZE REACHED ======");
                    }

                    File.AppendAllText(logFilePath, msg.ToString());
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return false;
        }

        private static bool MaxFileSizeExceeded()
        {
            string folderPath = AppDomain.CurrentDomain.SetupInformation.PrivateBinPath;
            string logFilePath = folderPath.Replace("\\", "/") + "/ErrorLog" + DateTime.Today.ToString("yyyyMMdd") + ".log";
            return (File.Exists(logFilePath) && File.OpenRead(logFilePath).Length > LOGFILE_MAX_BYTES);
        }
    }
}
