﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using CustomModule.Models;
using System.Text;
using System.Xml;

namespace CustomModule.Helper
{

    /// <summary>
    /// Summary description for Class1
    /// </summary>
    public static class CustomsBlockPost
    {
        public static string SUCCESS_MSG = "Success";
        public static string ALREADY_BLOCKED_MSG = "Container already block";

        private static string _xmlDir = System.Configuration.ConfigurationManager.AppSettings["messageXMLDirName"];

        public static MessageResponse Post(string exportimport, string containernumber,string call_sign,string voyage,string status="HOLD")
        {
            MessageResponse response = new MessageResponse();
            StringBuilder xmlRequest = new StringBuilder();
            string xmlString = "";
            string result = "";

            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _xmlDir, "messagehold.xml");
            xmlString = File.ReadAllText(path);
            xmlRequest = new StringBuilder(xmlString);


            #region "NPCT1 hold message"
            xmlRequest.Replace("[status]", status);
            string exim = (exportimport == "E") ? "EXPORT" : "IMPORT";
            xmlRequest.Replace("[transaction]", exim);
            xmlRequest.Replace("[container]", containernumber);
            xmlRequest.Replace("[path_document]", "path/document.pdf");
            xmlRequest.Replace("[call_sign]", call_sign);
            xmlRequest.Replace("[voyage]", voyage);
            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(xmlRequest.ToString());
                NPCT1Service.Server serv = new NPCT1Service.Server();
                XmlNamespaceManager xNSMgr = new XmlNamespaceManager(xDoc.NameTable);
                xNSMgr.AddNamespace("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
                xNSMgr.AddNamespace("urn", "urn:customs_block");

                XmlNodeList xNodes = xDoc.SelectNodes("//soapenv:Envelope/soapenv:Body/urn:customs_block", xNSMgr);
                if (xNodes.Count > 0)
                {
                    XmlNodeList nodes = xNodes[0].ChildNodes;
                    string user = nodes[0].InnerText; ;
                    string password = nodes[1].InnerText;
                    string strData = nodes[2].InnerText;

                    result = serv.customs_block(user, password, strData);
                    xDoc.LoadXml(result);

                    if (xDoc.InnerText != null)
                    {
                        MessageResponse resultItem = new MessageResponse();
                        response.Result = xDoc.InnerText;
                    }
                    else
                    {
                        MessageException obj = new MessageException();
                        obj.errorCode = "404";
                        obj.errorMessage = "NotFound";
                        response.messageException = obj;
                    }

                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
                response.messageException = new MessageException()
                {
                    errorMessage = result
                };
            }
            #endregion

            return response;
        }

        public static MessageResponse Request(CONTAINER container, ATENSI_P2 atensi, string call_sign, string status = "HOLD")
        {
            FileLogger logger = new FileLogger();
            MessageResponse response = new MessageResponse();


            #region "NPCT1 hold message"
            string exim = (container.EXPORTIMPORT == "E") ? "EXPORT" : "IMPORT";
            string remark = "";
            if (status == "HOLD")
            {
                remark = atensi.HOLDNOTE;
            }
            else
            {
                remark = atensi.RELEASENOTE;
            }
            string result = "";
            try
            {
                PostXml postXml = new PostXml();
                //result = postXml.postXMLData(npctUri, xmlRequest.ToString());
                NPCT1Gateway.Service1Client client = new NPCT1Gateway.Service1Client();
                logger.Debug(result);

                Entities entities = new Entities();
                var p2File = (from p in entities.P2FILE
                              where p.P2ID == atensi.ID
                              orderby p.ID descending
                              select p).FirstOrDefault();

                string path = "";
                if (p2File != null)
                {
                    if (String.IsNullOrEmpty(p2File.DOCPATH))
                    {
                        path = p2File.DOCPATH;
                    }
                    else
                    {
                        path = p2File.RELEASEPATH;
                    }
                }
                if (!String.IsNullOrEmpty(path))
                {
                    path = "/AMS/P2File/Get/"+atensi.ID;
                }
                result = client.P2HoldRelease(
                    status, container.CONTAINERNUMBER, exim, atensi.DOCUMENTNUMBER,
                     atensi.DOCUMENTDATE.GetValueOrDefault().ToString("yyyyMMdd"), path, remark);
                logger.Debug(container.CONTAINERNUMBER+"-"+status+" : "+result);
                try
                {
                    AutogateEntities _autogateEntities = new AutogateEntities();
                    AG_LOG log = new AG_LOG();
                    log.STARTTIME = DateTime.Now;
                    log.TAR = container.TRANSACTION_ID;
                    log.WEBMETHODNAME = "Customs Block : " + status;
                    log.RESPONSE = result;
                    log.IPADDRESS = "localhost";
                    log.ENDTIME = DateTime.Now;
                    _autogateEntities.AG_LOG.AddObject(log);
                    _autogateEntities.SaveChanges();
                }
                catch (Exception logEx)
                {

                }

                response.Result = result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                result = ex.Message;
                response.messageException = new MessageException()
                {
                    errorMessage = result
                };
            }
            #endregion

            return response;
        }
    }
}
