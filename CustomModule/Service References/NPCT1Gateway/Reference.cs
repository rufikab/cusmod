﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CustomModule.NPCT1Gateway {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CompositeType", Namespace="http://schemas.datacontract.org/2004/07/MessageNPCTHold")]
    [System.SerializableAttribute()]
    public partial class CompositeType : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool BoolValueField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StringValueField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool BoolValue {
            get {
                return this.BoolValueField;
            }
            set {
                if ((this.BoolValueField.Equals(value) != true)) {
                    this.BoolValueField = value;
                    this.RaisePropertyChanged("BoolValue");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StringValue {
            get {
                return this.StringValueField;
            }
            set {
                if ((object.ReferenceEquals(this.StringValueField, value) != true)) {
                    this.StringValueField = value;
                    this.RaisePropertyChanged("StringValue");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="MessageResponse", Namespace="http://schemas.datacontract.org/2004/07/MessageNPCTHold")]
    [System.SerializableAttribute()]
    public partial class MessageResponse : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AppStatusField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ErrorCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ResultField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TARField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private CustomModule.NPCT1Gateway.MessageException messageExceptionField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AppStatus {
            get {
                return this.AppStatusField;
            }
            set {
                if ((object.ReferenceEquals(this.AppStatusField, value) != true)) {
                    this.AppStatusField = value;
                    this.RaisePropertyChanged("AppStatus");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ErrorCode {
            get {
                return this.ErrorCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.ErrorCodeField, value) != true)) {
                    this.ErrorCodeField = value;
                    this.RaisePropertyChanged("ErrorCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Result {
            get {
                return this.ResultField;
            }
            set {
                if ((object.ReferenceEquals(this.ResultField, value) != true)) {
                    this.ResultField = value;
                    this.RaisePropertyChanged("Result");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string TAR {
            get {
                return this.TARField;
            }
            set {
                if ((object.ReferenceEquals(this.TARField, value) != true)) {
                    this.TARField = value;
                    this.RaisePropertyChanged("TAR");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public CustomModule.NPCT1Gateway.MessageException messageException {
            get {
                return this.messageExceptionField;
            }
            set {
                if ((object.ReferenceEquals(this.messageExceptionField, value) != true)) {
                    this.messageExceptionField = value;
                    this.RaisePropertyChanged("messageException");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="MessageException", Namespace="http://schemas.datacontract.org/2004/07/MessageNPCTHold")]
    [System.SerializableAttribute()]
    public partial class MessageException : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string errorCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string errorMessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string errorParam1Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string errorParam2Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string errorParam3Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string tagField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string errorCode {
            get {
                return this.errorCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.errorCodeField, value) != true)) {
                    this.errorCodeField = value;
                    this.RaisePropertyChanged("errorCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string errorMessage {
            get {
                return this.errorMessageField;
            }
            set {
                if ((object.ReferenceEquals(this.errorMessageField, value) != true)) {
                    this.errorMessageField = value;
                    this.RaisePropertyChanged("errorMessage");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string errorParam1 {
            get {
                return this.errorParam1Field;
            }
            set {
                if ((object.ReferenceEquals(this.errorParam1Field, value) != true)) {
                    this.errorParam1Field = value;
                    this.RaisePropertyChanged("errorParam1");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string errorParam2 {
            get {
                return this.errorParam2Field;
            }
            set {
                if ((object.ReferenceEquals(this.errorParam2Field, value) != true)) {
                    this.errorParam2Field = value;
                    this.RaisePropertyChanged("errorParam2");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string errorParam3 {
            get {
                return this.errorParam3Field;
            }
            set {
                if ((object.ReferenceEquals(this.errorParam3Field, value) != true)) {
                    this.errorParam3Field = value;
                    this.RaisePropertyChanged("errorParam3");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string tag {
            get {
                return this.tagField;
            }
            set {
                if ((object.ReferenceEquals(this.tagField, value) != true)) {
                    this.tagField = value;
                    this.RaisePropertyChanged("tag");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="NPCT1Gateway.IService1")]
    public interface IService1 {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/GetData", ReplyAction="http://tempuri.org/IService1/GetDataResponse")]
        string GetData(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/P2HoldRelease", ReplyAction="http://tempuri.org/IService1/P2HoldReleaseResponse")]
        string P2HoldRelease(string status, string container, string exim, string docno, string docdate, string docpath, string remark);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/GetDataUsingDataContract", ReplyAction="http://tempuri.org/IService1/GetDataUsingDataContractResponse")]
        CustomModule.NPCT1Gateway.CompositeType GetDataUsingDataContract(CustomModule.NPCT1Gateway.CompositeType composite);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/Message1", ReplyAction="http://tempuri.org/IService1/Message1Response")]
        CustomModule.NPCT1Gateway.MessageResponse Message1(string type, string container);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/Message6", ReplyAction="http://tempuri.org/IService1/Message6Response")]
        CustomModule.NPCT1Gateway.MessageResponse Message6(string tar, string noContainer, decimal point, string idves, string ei);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IService1Channel : CustomModule.NPCT1Gateway.IService1, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class Service1Client : System.ServiceModel.ClientBase<CustomModule.NPCT1Gateway.IService1>, CustomModule.NPCT1Gateway.IService1 {
        
        public Service1Client() {
        }
        
        public Service1Client(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public Service1Client(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public Service1Client(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public Service1Client(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string GetData(int value) {
            return base.Channel.GetData(value);
        }
        
        public string P2HoldRelease(string status, string container, string exim, string docno, string docdate, string docpath, string remark) {
            return base.Channel.P2HoldRelease(status, container, exim, docno, docdate, docpath, remark);
        }
        
        public CustomModule.NPCT1Gateway.CompositeType GetDataUsingDataContract(CustomModule.NPCT1Gateway.CompositeType composite) {
            return base.Channel.GetDataUsingDataContract(composite);
        }
        
        public CustomModule.NPCT1Gateway.MessageResponse Message1(string type, string container) {
            return base.Channel.Message1(type, container);
        }
        
        public CustomModule.NPCT1Gateway.MessageResponse Message6(string tar, string noContainer, decimal point, string idves, string ei) {
            return base.Channel.Message6(tar, noContainer, point, idves, ei);
        }
    }
}
