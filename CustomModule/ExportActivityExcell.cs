﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using CustomModule.Models;

namespace CustomModule
{
    public class ExportActivityExcell
    {
        /// <summary>
        /// Generates the report.
        /// </summary>
        public static Byte[] GenerateReport(List<UserActivityModel> model)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                //set the workbook properties and add a default sheet in it
                SetWorkbookProperties(p);
                //Create a sheet
                ExcelWorksheet ws = CreateSheet(p,"Export Aktivitas Userr");
                DataTable dt = CreateDataTable(model); //My Function which generates DataTable

                //Merging cells and create a center heading for out table
                ws.Cells[1, 1].Value = "Export Aktivitas User";
                ws.Cells[1, 1, 1, dt.Columns.Count].Merge = true;
                ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                ws.Cells[1, 1, 1, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                int rowIndex = 2;

                CreateHeader(ws, ref rowIndex, dt);
                CreateData(ws, ref rowIndex, dt);

                //Generate A File with Random name
                Byte[] bin = p.GetAsByteArray();
                string file = Guid.NewGuid().ToString() + ".xlsx";
                
                return bin;
            }
        }

        private static ExcelWorksheet CreateSheet(ExcelPackage p, string sheetName)
        {
            p.Workbook.Worksheets.Add(sheetName);
            ExcelWorksheet ws = p.Workbook.Worksheets[1];
            ws.Name = sheetName; //Setting Sheet's name
            ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
            ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet

            return ws;
        }

        /// <summary>
        /// Sets the workbook properties and adds a default sheet.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns></returns>
        private static void SetWorkbookProperties(ExcelPackage p)
        {
            //Here setting some document properties
            p.Workbook.Properties.Author = "Halotec-Indonesia";
            p.Workbook.Properties.Title = "Export Ativitas User";
        }

        private static void CreateHeader(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {
            int colIndex = 1;
            foreach (DataColumn dc in dt.Columns) //Creating Headings
            {
                var cell = ws.Cells[rowIndex, colIndex];

                //Setting the background color of header cells to Orange
                var fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(Color.Orange);

                //Setting Top/left,right/bottom borders.
                var border = cell.Style.Border;
                border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                //Setting Value in cell
                cell.Value = dc.ColumnName;

                colIndex++;
            }
        }

        private static void CreateData(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {
            int colIndex=0;
            foreach (DataRow dr in dt.Rows) // Adding Data into rows
            {
                colIndex = 1;
                rowIndex++;

                foreach (DataColumn dc in dt.Columns)
                {
                    var cell = ws.Cells[rowIndex, colIndex];

                    //Setting Value in cell
                    cell.Value = dr[dc.ColumnName];

                    //Setting borders of cell
                    var border = cell.Style.Border;
                    border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                    colIndex++;
                }
            }
        }

        /// <summary>
        /// Creates the data table 
        /// </summary>
        /// <returns>DataTable</returns>
        private static DataTable CreateDataTable(List<UserActivityModel> model)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Username");
            dt.Columns.Add("Jlh. Hold");
            dt.Columns.Add("Jlh. Release");
            dt.Columns.Add("Jlh. Cek Container");
            dt.Columns.Add("Jlh. Tambah Atensi");
            dt.Columns.Add("Jlh. Edit Atensi");
            dt.Columns.Add("Jlh. Confirm P2");
            dt.Columns.Add("Jlh. Tambah Putusan");
            dt.Columns.Add("Jlh. Hapus Transaksi");

            foreach (UserActivityModel user in model)
            {
                DataRow dr = dt.NewRow();
                dr["Username"] = user.USERNAME;
                dr["Jlh. Hold"] = user.HOLD;
                dr["Jlh. Release"] = user.RELEASE;
                dr["Jlh. Cek Container"] = user.CHECKCONTAINER;
                dr["Jlh. Tambah Atensi"] = user.ADDATENSI;
                dr["Jlh. Edit Atensi"] = user.EDITATENSI;
                dr["Jlh. Confirm P2"] = user.CONFIRMP2;
                dr["Jlh. Tambah Putusan"] = user.ADDPUTUSAN;
                dr["Jlh. Hapus Transaksi"] = user.DELETETRX;
                dt.Rows.Add(dr);
            }

            return dt;
        }

        public static Byte[] GenerateReportUser(List<UserActivityModel> model)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                //set the workbook properties and add a default sheet in it
                SetWorkbookProperties(p);
                //Create a sheet
                ExcelWorksheet ws = CreateSheet(p, "Export Aktivitas Userr");
                DataTable dt = CreateDataTableReport(model); //My Function which generates DataTable

                //Merging cells and create a center heading for out table
                ws.Cells[1, 1].Value = "Export Aktivitas User";
                ws.Cells[1, 1, 1, dt.Columns.Count].Merge = true;
                ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                ws.Cells[1, 1, 1, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                int rowIndex = 2;

                CreateHeader(ws, ref rowIndex, dt);
                CreateData(ws, ref rowIndex, dt);

                //Generate A File with Random name
                Byte[] bin = p.GetAsByteArray();
                string file = Guid.NewGuid().ToString() + ".xlsx";

                return bin;
            }


        }
        private static DataTable CreateDataTableReport(List<UserActivityModel> model)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Username");
            dt.Columns.Add("Aktivitas");
            dt.Columns.Add("Tanggal");

            foreach (UserActivityModel user in model)
            {
                DataRow dr = dt.NewRow();
                dr["Username"] = user.USERNAME;
                dr["Aktivitas"] = user.ACTIVITYDESCR;
                dr["Tanggal"] = user.ACTIVITYDATE.ToString("dd-MM-yyyy hh:mm");
                dt.Rows.Add(dr);
            }

            return dt;
        }
    }
}