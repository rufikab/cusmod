﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Drawing;

namespace CustomModule.Controllers
{
    [Authorize]
    public class IPCameraController : Controller
    {
        [HttpGet]
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Index(string ip,string user,string pass, string w, string h, string model)
        {
            byte[] data = null;
            {
                string url = model.Equals("axis") ? 
                    System.Configuration.ConfigurationManager.AppSettings["axisURL"].ToString() :
                    System.Configuration.ConfigurationManager.AppSettings["sonyURL"].ToString();
                url = url.Replace("[ip]", ip);
                url = url.Replace("[w]", w);
                url = url.Replace("[h]", h);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = 5000;
                request.ReadWriteTimeout = 5000;
                if (model.Equals("axis"))
                {
                    request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
                    request.Credentials = new NetworkCredential(user, pass);
                    request.PreAuthenticate = true;
                }
                Task<WebResponse> responseTask = Task.Factory.FromAsync<WebResponse>(request.BeginGetResponse, request.EndGetResponse, null);
                using (HttpWebResponse response = responseTask.Result as HttpWebResponse)
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            responseStream.CopyTo(ms);
                            data = ms.ToArray();
                        }
                    }
                }
            }

            return File(data, System.Net.Mime.MediaTypeNames.Image.Jpeg, DateTime.Now.ToFileTime().ToString() + ".jpg");
        }
    }
}
