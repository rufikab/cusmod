﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomModule.Models;
using Telerik.Web.Mvc;
using System.Data;
using System.IO;
using OfficeOpenXml;
using System.Threading.Tasks;

namespace CustomModule.Controllers
{
    public class AjaxFilterActivityBindingModel
    {
        public string from { get; set; }
        public string to { get; set; }

    }
    [Authorize(Roles="Administrator,ReportUserTask")]
    public class UserActivityTaskController : Controller
    {
        //-------------------- USER COUNTER ----------------------------------//
        UserActivityRepository activityrepo = new UserActivityRepository();
        ContainerRepository containerRepo = new ContainerRepository();

        public ActionResult Index()
        {
            var HasBroken = Task.Factory.StartNew<bool>(() => containerRepo.hasBrokenContainer(User.Identity.Name));
            ViewData["HasBroken"] = HasBroken.Result ;
            return View();
        }

        public ActionResult IndexAjax()
        {
            var model = Task.Factory.StartNew<List<UserActivityModel>>(() => activityrepo.CountUser());
            return View(model.Result );
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxBinding()
        {
            var model = Task.Factory.StartNew<List<UserActivityModel>>(() => activityrepo.CountUser());
            return View("IndexAjax", new GridModel(model.Result ));
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxFilterBinding(AjaxFilterActivityBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");
            var model = Task.Factory.StartNew<List<UserActivityModel>>(() => activityrepo.CountUser(filter.from, filter.to));            
            return View("IndexAjax", new GridModel(model.Result ));
        }

        public ActionResult ExportCSV(AjaxFilterActivityBindingModel filter)
        {
            var result = Task.Factory.StartNew<byte[]>(() =>
            {
                List<UserActivityModel> model = Export(filter);
                return Export2CSV(model);
            });
            return File(result.Result , "text/csv", "Export" + string.Join("-", Request.QueryString.AllKeys) + Guid.NewGuid().ToString() + ".csv"); // adjust content type appropriately
        }

        public ActionResult ExportExcell(AjaxFilterActivityBindingModel filter)
        {
            var result = Task.Factory.StartNew<byte[]>(() =>
            {
                List<UserActivityModel> model = Export(filter);
                return ExportActivityExcell.GenerateReport(model);
            });
            return File(result.Result , "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Export" + string.Join("-", Request.QueryString.AllKeys) + Guid.NewGuid().ToString() + ".xlsx"); // adjust content type appropriately
        }

        #region Non Action Counter
        [NonAction]
        private List<UserActivityModel> Export(AjaxFilterActivityBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");
            List<UserActivityModel> model = activityrepo.CountUser(filter.from, filter.to);
           
            return model;
        }

        [NonAction]
        private byte[] Export2CSV(List<UserActivityModel> model)
        {
            CsvExport myExport = new CsvExport();
            foreach (UserActivityModel activity in model)
            {
                myExport.AddRow();
                myExport["Username"] = activity.USERNAME;
                myExport["Jlh. Hold"] = activity.HOLD;
                myExport["Jlh. Release"] = activity.RELEASE ;
                myExport["Jlh. Cek Container"] = activity.CHECKCONTAINER;
                myExport["Jlh. Tambah Atensi"] = activity.ADDATENSI;
                myExport["Jlh. Edit Atensi"] = activity.EDITATENSI;
                myExport["Jlh. Confirm P2"] = activity.CONFIRMP2;
                myExport["Jlh. Tambah Putusan"] = activity.ADDPUTUSAN;
                myExport["Jlh. Hapus Transaksi"] = activity.DELETETRX;
            }
            return myExport.ExportToBytes();
        }
        #endregion

        //------------------ END OF USER COUNTER ---------------------//


        // REPORT ACTIVITY USER --- MORE DETAIL ------ //

        public ActionResult IndexReport()
        {
            var HasBroken = Task.Factory.StartNew<bool>(() => containerRepo.hasBrokenContainer(User.Identity.Name));
            ViewData["HasBroken"] = HasBroken.Result ;
            return View();
        }

        public ActionResult IndexReportAjax()
        {
            var model = Task.Factory.StartNew<List<UserActivityModel>>(() => activityrepo.ReportActivity());
            return View(model.Result );
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxBindingReport()
        {
            var model = Task.Factory.StartNew<List<UserActivityModel>>(() => activityrepo.ReportActivity());
            return View("IndexReportAjax", new GridModel(model.Result ));
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxFilterBindingReport(AjaxFilterActivityBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");
            var model = Task.Factory.StartNew<List<UserActivityModel>>(() => activityrepo.ReportActivity(filter.from, filter.to));
            return View("IndexReportAjax", new GridModel(model.Result ));
        }


        public ActionResult ExportExcellReport(AjaxFilterActivityBindingModel filter)
        {
            var result = Task.Factory.StartNew<byte[]>(() =>
            {
                List<UserActivityModel> model = ExportReport(filter);
                return ExportActivityExcell.GenerateReportUser(model);
            });
            return File(result.Result , "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Export" + string.Join("-", Request.QueryString.AllKeys) + Guid.NewGuid().ToString() + ".xlsx"); // adjust content type appropriately
        }

        #region Non Action
        [NonAction]
        private List<UserActivityModel> ExportReport(AjaxFilterActivityBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");
            List<UserActivityModel> model = activityrepo.ReportActivity(filter.from, filter.to);
            return model;
        }
        #endregion
    }
}
