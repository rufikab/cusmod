﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.IO;
using CustomModule.Models;
using Telerik.Web.Mvc;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.Web.Security;

namespace CustomModule.Controllers
{
    public class TaskController : Controller
    {

        ContainerRepository containerRepo = new ContainerRepository();

        public ActionResult UpdatePosition(int diff=8)
        {
            return Content(containerRepo.updatePosition(-diff));
        }
    }
}
