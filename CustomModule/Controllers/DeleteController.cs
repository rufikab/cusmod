﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomModule.Models;
using Telerik.Web.Mvc;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace CustomModule.Controllers
{
    public class AjaxFilterDeleteBindingModel
    {
        public string cn { get; set; }
        public string voyage { get; set; }
    }

    [Authorize(Roles = "Administrator,Monitor,ControlRoom,DeletionOperator")]
    public class DeleteController : Controller
    {
        ContainerRepository containerRepo = new ContainerRepository();
        string basepath = "/";
        public ActionResult Index()
        {
            var HasBroken = Task.Factory.StartNew<bool>(() => containerRepo.hasBrokenContainer(User.Identity.Name));
            ViewData["HasBroken"] = HasBroken.Result ;
            return View();
        }

        public ActionResult IndexAjax()
        {
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetDeleteList(mappath: basepath));
            return View(model.Result );
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxBinding()
        {
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetDeleteList(mappath: basepath));
            return View("IndexAjax",new GridModel(model.Result ));
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxFilterBinding(AjaxFilterDeleteBindingModel filter)
        {
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetDeleteList(filter.cn, filter.voyage, basepath));
            return View("IndexAjax", new GridModel(model.Result ));
        }

        [Authorize(Roles = "administrator")]
        [HttpGet]
        public ActionResult Delete(int id, string deleteNote)
        {
            //var ret = Task.Factory.StartNew<bool>(() => containerRepo.Delete(id, User.Identity.Name));
            var ret = Task.Factory.StartNew<bool>(() => containerRepo.DeleteNew(id, User.Identity.Name, deleteNote));
            bool returnvalue = ret.Result ;
            return Json(returnvalue.ToString(),JsonRequestBehavior.AllowGet );
        }
    }
}