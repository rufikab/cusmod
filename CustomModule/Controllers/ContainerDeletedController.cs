﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomModule.Models;
using Telerik.Web.Mvc;
using System.Data;
using System.IO;
using OfficeOpenXml;
using System.Threading.Tasks;
using CustomModule.Helper;

namespace CustomModule.Controllers
{
    public class AjaxFilterDeletedBindingModel
    {
        public string from { get; set; }
        public string to { get; set; }

        public string cn { get; set; }
    }
    //[Authorize]
    [Authorize(Roles = "Administrator,Penyegelan,P2,ControlRoom,DeletionOperator")]
    public class ContainerDeletedController : AsyncController
    {
        ContainerRepository containerRepo = new ContainerRepository();

        public ActionResult Index()
        {
            //var hasBroken = Task.Factory.StartNew<bool>(() => containerRepo.hasBrokenContainer(User.Identity.Name));
            //ViewData["HasBroken"] = hasBroken.Result ;
            ViewData["HasBroken"] = false;

            String dtNow = DateTime.Now.ToString("MM/dd/yyyy");
            ViewData["dtFrom"] = dtNow;
            ViewData["dtEnd"] = dtNow;
            return View();
        }

        [HttpGet]
        public ActionResult IndexAjax(AjaxFilterDeletedBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");

            var model = Task.Factory.StartNew<List<ContainerDeleted>>(() => containerRepo.GetDeletedList(User.Identity.Name, filter.from, filter.to));
            return View(model.Result );
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxBinding()
        {
            var model = Task.Factory.StartNew<List<ContainerDeleted>>(() => containerRepo.GetDeletedList(User.Identity.Name));
            return View("IndexAjax",new GridModel(model.Result ));
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxFilterBinding(AjaxFilterDeletedBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");
            var model = Task.Factory.StartNew<List<ContainerDeleted>>(() => containerRepo.GetDeletedList(User.Identity.Name, filter.from, filter.to, filter.cn));
            return View("IndexAjax", new GridModel(model.Result ));
        }
    
    }
}