﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomModule.Models;
using Telerik.Web.Mvc;
using System.Data;
using System.IO;
using OfficeOpenXml;
using System.Threading.Tasks;

namespace CustomModule.Controllers
{
    public class AjaxFilterSummaryBindingModel
    {
        public string from { get; set; }
        public string to { get; set; }

        public string cn { get; set; }
        public string docnum { get; set; }
        public string ei { get; set; }
        public string docType { get; set; }
        public string terminal { get; set; }
    }

    [Authorize]
    public class SummaryController : Controller
    {
        ContainerRepository containerRepo = new ContainerRepository();

        public ActionResult Index()
        {
            var HasBroken = Task.Factory.StartNew<bool>(() => containerRepo.hasBrokenContainer(User.Identity.Name));
            ViewData["HasBroken"] = HasBroken.Result ;

            String dtNow = DateTime.Now.ToString("MM/dd/yyyy");
            ViewData["dtFrom"] = dtNow;
            ViewData["dtEnd"] = dtNow;
            ViewData["DocumentTypes"] = containerRepo.getDocumentTypes();
            ViewData["Terminals"] = containerRepo.getTerminals();
            return View();
        }

        public ActionResult IndexAjax(AjaxFilterSummaryBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");

            //int totalPages = (containerRepo.GetAutoListCount(from, to) - 1) / pageSize;
            //String dtNow = DateTime.Now.ToString("MM/dd/yyyy");
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetSummaryList(filter.from, filter.to));
            //string total = containerRepo.GetTotalTransaction();
            ////ViewData["TotalPages"] = totalPages;
            //string[] a = new string[2];
            //a = total.Split('-');
            //ViewData["TotalImport"] = a[0];
            //ViewData["TotalExport"] = a[1];
            return View(model.Result);
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxBinding()
        {
            //int totalPages = (containerRepo.GetAutoListCount(from, to) - 1) / pageSize;
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetSummaryList());
            //ViewData["TotalPages"] = totalPages;
            //ViewData["Page"] = page;
            //string total = containerRepo.GetTotalTransaction();
            //string[] arrtotal = total.Split('-');
            //ViewData["TotalImport"] = arrtotal[0];
            //ViewData["TotalExport"] = arrtotal[1];
            return View("IndexAjax", new GridModel(model.Result ));
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxFilterBinding(AjaxFilterSummaryBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");
            //int totalPages = (containerRepo.GetAutoListCount(from, to) - 1) / pageSize;
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetSummaryList(filter.from, filter.to, filter.cn, filter.docnum, filter.ei, filter.docType, filter.terminal));
            //ViewData["TotalPages"] = totalPages;
            //ViewData["Page"] = page;
            //string total = containerRepo.GetTotalTransaction(filter.from, filter.to, filter.cn, filter.docnum, filter.ei);
            //string[] arrtotal = total.Split('-');
            //ViewData["TotalImport"] = arrtotal[0];
            //ViewData["TotalExport"] = arrtotal[1];
            return View("IndexAjax", new GridModel(model.Result ));
        }

        public ActionResult ExportCSV(AjaxFilterSummaryBindingModel filter)
        {
            var result = Task.Factory.StartNew<byte[]>(() =>
            {
                List<ContainerModel> model = Export(filter);
                return Export2CSV(model);
            });
            return File(result.Result , "text/csv", "Export" + string.Join("-", Request.QueryString.AllKeys) + Guid.NewGuid().ToString() + ".csv"); // adjust content type appropriately
        }

        public ActionResult ExportExcell(AjaxFilterSummaryBindingModel filter)
        {
            var result = Task.Factory.StartNew<byte[]>(() =>
            {
                List<ContainerModel> model = Export(filter);
                return Export2Excell.GenerateReport(model);
            });
            return File(result.Result , "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Export" + string.Join("-", Request.QueryString.AllKeys) + Guid.NewGuid().ToString() + ".xlsx"); // adjust content type appropriately
        }

        #region Non Action
        [NonAction]
        private List<ContainerModel> Export(AjaxFilterSummaryBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");
            //int totalPages = (containerRepo.GetAutoListCount(from, to) - 1) / pageSize;
            List<ContainerModel> model = containerRepo.GetSummaryList(filter.from, filter.to, filter.cn, filter.docnum, filter.ei, filter.docType, filter.terminal);
            //string total = containerRepo.GetTotalTransaction(filter.from, filter.to, filter.cn, filter.docnum, filter.ei);
            //string[] arrtotal = total.Split('-');
            //ViewData["TotalImport"] = arrtotal[0];
            //ViewData["TotalExport"] = arrtotal[1];
            return model;
        }

        [NonAction]
        private byte[] Export2CSV(List<ContainerModel> model)
        {
            CsvExport myExport = new CsvExport();
            foreach (ContainerModel container in model)
            {
                myExport.AddRow();
                myExport["TAGNUMBER"] = container.TAGNUMBER;
                myExport["TRUCKNUMBER"] = container.TRUCKNUMBER;
                myExport["GATENUMBER"] = container.GATENUMBER;
                myExport["GATEINTIME"] = container.GATEINTIME;
                myExport["TERMINALID"] = container.TerminalId;
                myExport["Container/Size"] = container.ContainerAndSize;
                myExport["DOCUMENTNUMBER"] = container.DOCUMENTNUMBER;
                myExport["DOCUMENTTYPE"] = container.DOCUMENTTYPE;
                myExport["HOLD"] = container.HOLDNOTE;
                myExport["RELEASE"] = container.RELEASENOTE;
                myExport["RELEASEBY"] = container.ReleaseBy;
                myExport["EXPORTIMPORT"] = container.EXPORTIMPORT;
            }
            return myExport.ExportToBytes();
        }

        //[NonAction]
        public string GetTotalTransaction(AjaxFilterSummaryBindingModel filter)
        {
            string total = containerRepo.GetTotalTransactionSummary(filter.from, filter.to, filter.cn, filter.docnum, filter.ei,filter.docType, filter.terminal);

            return total;
        }
        #endregion
    }
}