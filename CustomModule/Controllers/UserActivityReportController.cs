﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomModule.Models;
using Telerik.Web.Mvc;
using System.Data;
using System.IO;
using OfficeOpenXml;
using System.Threading.Tasks;

namespace CustomModule.Controllers
{
    [Authorize(Roles = "Administrator,ReportUserDetail")]
    public class UserActivityReportController : Controller
    {
        // REPORT ACTIVITY USER --- MORE DETAIL ------ //
        UserActivityRepository activityrepo = new UserActivityRepository();
        ContainerRepository containerRepo = new ContainerRepository();

        public ActionResult Index()
        {
            var HasBroken = Task.Factory.StartNew<bool>(() => containerRepo.hasBrokenContainer(User.Identity.Name));
            ViewData["HasBroken"] = HasBroken.Result ;
            return View();
        }

        public ActionResult IndexReportAjax()
        {
            var model = Task.Factory.StartNew<List<UserActivityModel>>(() => activityrepo.ReportActivity());
            return View(model.Result );
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxBindingReport()
        {
            var model = Task.Factory.StartNew<List<UserActivityModel>>(() => activityrepo.ReportActivity());
            return View("IndexReportAjax", new GridModel(model.Result ));
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxFilterBindingReport(AjaxFilterActivityBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");
            var model = Task.Factory.StartNew<List<UserActivityModel>>(() => activityrepo.ReportActivity(filter.from, filter.to));
            return View("IndexReportAjax", new GridModel(model.Result ));
        }


        public ActionResult ExportExcellReport(AjaxFilterActivityBindingModel filter)
        {
            var result = Task.Factory.StartNew<byte[]>(() =>
            {
                List<UserActivityModel> model = ExportReport(filter);
                return ExportActivityExcell.GenerateReportUser(model);
            });
            return File(result.Result , "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Export" + string.Join("-", Request.QueryString.AllKeys) + Guid.NewGuid().ToString() + ".xlsx"); // adjust content type appropriately
        }

        #region Non Action
        [NonAction]
        private List<UserActivityModel> ExportReport(AjaxFilterActivityBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");
            List<UserActivityModel> model = activityrepo.ReportActivity(filter.from, filter.to);
            return model;
        }
        #endregion
    }
}
