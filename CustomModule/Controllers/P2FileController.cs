﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.IO;
using CustomModule.Models;
using Telerik.Web.Mvc;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.Web.Security;

namespace CustomModule.Controllers
{
    public class P2FileController : Controller
    {
        Entities _entities = new Entities();

        public ActionResult Get(int id)
        {
            var p2File = (from p in _entities.P2FILE
                          where p.P2ID == id
                          orderby p.ID descending
                          select p).FirstOrDefault();
            if (p2File != null)
            {
                string filepath = p2File.DOCPATH;
                string filename = Path.GetFileName(filepath);
                if (String.IsNullOrEmpty(filename))
                {
                    filepath = p2File.RELEASEPATH;
                    filename = Path.GetFileName(filepath);
                }
                try
                {
                    byte[] filedata = System.IO.File.ReadAllBytes(filepath);

                    string contentType = MimeExtensionHelper.GetMimeType(filepath);
                    var cd = new System.Net.Mime.ContentDisposition
                    {
                        FileName = filename,
                        Inline = true,
                    };

                    Response.AppendHeader("Content-Disposition", cd.ToString());

                    return File(filedata, contentType);
                }
                catch (Exception ex)
                {
                    throw new HttpException(404, "File Not Found for P2 : " + ex.Message);
                }
            }
            else
            {
                throw new HttpException(404, "File Not Found for P2");
            }
        }
    }
}
