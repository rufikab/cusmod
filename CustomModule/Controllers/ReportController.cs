﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomModule.Models;
using System.Web.Security;
using System.Threading.Tasks;
using Telerik.Web.Mvc;
using System.Globalization;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;

namespace CustomModule.Controllers
{
    public class ReportController : Controller
    {
        ContainerRepository containerRepo = new ContainerRepository();
        private Entities _entities = new Entities();
        public ActionResult Index()
        {
           
            String dtNow = DateTime.Now.ToString("MM/dd/yyyy");
            ViewData["dtNow"] = dtNow;
            return View();
            
        }
        public ActionResult ReportList(string initialDate = "", string type="")
        {
            var model = Task.Factory.StartNew<List<DailyReport>>(() => FillItemList(initialDate));
            if (type == "chart")
            {
                return Json(model.Result, JsonRequestBehavior.AllowGet);
            }
            return View(model.Result);
        }
       
        [HttpPost]
        [GridAction]
        public ActionResult _AjaxFilterBinding(string initialDate = "")
        {
            //int totalPages = (containerRepo.GetAutoListCount(from, to) - 1) / pageSize;
            var model = Task.Factory.StartNew<List<DailyReport>>(() => FillItemList(initialDate));
            return View("ReportList", new GridModel(model.Result));
        }

        [NonAction]
        public List<DailyReport> FillItemList(string initialDate)
        {
            List<DailyReport> countDailyContainer = new List<DailyReport>();
            DateTime pickdate = DateTime.ParseExact(initialDate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            DateTime toDate = pickdate.AddDays(1);
            List<int> _hour = new List<int>();
            var data =
                _entities.CONTAINER.Where(g=> pickdate <= g.GATEINTIME && g.GATEINTIME < toDate)
                .GroupBy(xx => xx.GATEINTIME.Value.Hour)
                .Select(g => new
                {
                    HOURS = g.Key,
                    PLP = g.Where(c => c.DOCUMENTTYPE == "PLP").Count(),
                    BC = g.Where(c => c.DOCUMENTTYPE.Contains("BC")).Count(),
                    OTHERS = g.Where(c => c.DOCUMENTTYPE != "PLP" && c.DOCUMENTTYPE != "BC2").Count()
                })
                .OrderBy(c => c.HOURS).ToList();
            foreach (var c in data)
            {
                DailyReport dataC = new DailyReport();
                _hour.Add(c.HOURS);
                dataC.HOURS = c.HOURS;
                dataC.PLP = c.PLP;
                dataC.BC = c.BC;
                dataC.OTHERS = c.OTHERS;
                countDailyContainer.Add(dataC);
            }
            for(var i = 0; i < 24; i++)
            {
                if (!_hour.Contains(i))
                {
                    DailyReport dataC = new DailyReport();
                    dataC.HOURS = i;
                    dataC.PLP = 0;
                    dataC.BC = 0;
                    dataC.OTHERS = 0;
                    countDailyContainer.Add(dataC);
                }
            }
            return countDailyContainer;
        }
        public ActionResult ExportCSV(string initialDate)
        {
            var result = Task.Factory.StartNew<byte[]>(() =>
            {
                List<DailyReport> model = FillItemList(initialDate);
                return Export2CSV(model);
            });
            return File(result.Result, "text/csv", "Export" + string.Join("-", Request.QueryString.AllKeys) + Guid.NewGuid().ToString() + ".csv"); // adjust content type appropriately
        }
        public ActionResult ExportExcell(string initialDate)
        {
            var result = Task.Factory.StartNew<byte[]>(() =>
            {
                List<DailyReport> model = FillItemList(initialDate);
                DateTime pickdate = DateTime.ParseExact(initialDate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                initialDate = pickdate.ToString("dd-MM-yyyy");
                return GenerateReport(model,initialDate);
            });
            return File(result.Result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Export-Transaksi-PerJam-Tanggal" + string.Join("-", initialDate) + Guid.NewGuid().ToString() + ".xlsx"); // adjust content type appropriately
        }
        public static Byte[] GenerateReport(List<DailyReport> model, string initialDate)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                //set the workbook properties and add a default sheet in it
                p.Workbook.Properties.Author = "Halotec-Indonesia";
                p.Workbook.Properties.Title = "Export Daily Report";
                //Create a sheet
                p.Workbook.Worksheets.Add("Daily Report");
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                ws.Name = "Daily Report"; //Setting Sheet's name
                ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
                ws.Cells.Style.Font.Name = "Calibri";
                //generate datatable
                DataTable dt = new DataTable();
                dt.Columns.Add("JAM");
                dt.Columns.Add("BC 2.0");
                dt.Columns.Add("PLP");
                dt.Columns.Add("OTHERS");
                foreach (DailyReport container in model)
                {
                    DataRow dr = dt.NewRow();
                    dr["JAM"] = container.HOURS;
                    dr["BC 2.0"] = container.BC;
                    dr["PLP"] = container.PLP;
                    dr["OTHERS"] = container.OTHERS;
                    dt.Rows.Add(dr);
                }

                //Merging cells and create a center heading for out table
                ws.Cells[1, 1].Value = "DAFTAR TRANSAKSI PER JAM TANGGAL "+initialDate;
                ws.Cells[1, 1, 1, dt.Columns.Count].Merge = true;
                ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                ws.Cells[1, 1, 1, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                int rowIndex = 2;

                Export2Excell.CreateHeader(ws, ref rowIndex, dt);
                Export2Excell.CreateData(ws, ref rowIndex, dt);

                //Generate A File with Random name
                Byte[] bin = p.GetAsByteArray();
                string file = Guid.NewGuid().ToString() + ".xlsx";

                return bin;
            }
        }
        [NonAction]
        private byte[] Export2CSV(List<DailyReport> model)
        {
            CsvExport myExport = new CsvExport();
            foreach (DailyReport container in model)
            {
                myExport.AddRow();
                myExport["JAM"] = container.HOURS;
                myExport["PLP"] = container.PLP;
                myExport["BC 2.0"] = container.BC;
                myExport["OTHERS"] = container.OTHERS;
            }
            return myExport.ExportToBytes();
        }

    }
}
