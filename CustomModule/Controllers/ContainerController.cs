﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.IO;
using CustomModule.Models;
using Telerik.Web.Mvc;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.Web.Security;
using CustomModule.Helper;

namespace CustomModule.Controllers
{
    [Authorize]
    public class ContainerController : Controller
    {

        protected AutogateEntities _autogateEntities = new AutogateEntities();
        // id=0->Export
        // GET: /Container/
        ContainerRepository containerRepo = new ContainerRepository();
        UserActivityRepository activityRepo = new UserActivityRepository();
        public ActionResult Index(string id)
        {
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetList(id, User.Identity.Name));
            ViewData["Id"] = id;
            return View(model.Result );
        }

        public ActionResult IndexP2()
        {
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetP2List(User.Identity.Name));
            ViewData["Id"] = "P2";
            return View("Index",model.Result );
        }

        public ActionResult UpdatePosition()
        {
            return Content(containerRepo.updatePosition(-8));
        }


        [HttpPost]
        [GridAction]
        public ActionResult _Sorting(string id)
        {
            var model = Task.Factory.StartNew<List<ContainerModel>>(() =>
            {
                return (id == "P2")? containerRepo.GetP2List(User.Identity.Name):containerRepo.GetList(id, User.Identity.Name);
            });
            ViewData["Id"] = id;
            return View("Index",new GridModel(model.Result ));
        }

        private string updateTruckPosition(CONTAINER model)
        {
            if (model.TRANSACTION_ID.Contains("/BHD")) return "EX BEHANDLE";
            decimal truckid = Convert.ToInt64(model.TAGNUMBER);
            CON_LISTCONT containerInAutogate = (from c in _autogateEntities.CON_LISTCONT
                                                where c.TAR == model.TRANSACTION_ID.Trim() && c.NO_CONTAINER == model.CONTAINERNUMBER
                                                orderby c.GT_DATE descending
                                                select c).FirstOrDefault();

            if (containerInAutogate != null)
            {
                if (containerInAutogate.GT_DATE_OUT != null)
                {
                    if (containerInAutogate.ID_CLASS_CODE == "I")
                    {
                        return "Gate OUT CG";
                    }
                    else if (containerInAutogate.ID_CLASS_CODE == "E")
                    {
                        return "Truck Gate OUT CG";
                    }
                }

                if (containerInAutogate.TRUCKOUT_TERMINAL != null)
                {
                    if (containerInAutogate.ID_CLASS_CODE == "I")
                    {
                        return "OUT Terminal";
                    }
                    else if (containerInAutogate.ID_CLASS_CODE == "E")
                    {
                        return "Truck OUT Terminal";
                    }
                }

                if (containerInAutogate.TRUCKIN_TERMINAL != null)
                {
                    if (containerInAutogate.ID_CLASS_CODE == "I")
                    {
                        return "Truck IN Terminal";
                    }
                    else if (containerInAutogate.ID_CLASS_CODE == "E")
                    {
                        return "IN Terminal";
                    }
                }

                if (model.TRUCKPOSITION==null||model.TRUCKPOSITION.Trim() == "Approaching Yard")
                {
                    if (containerInAutogate.ID_CLASS_CODE == "I")
                    {
                        return "Truck OTW Terminal";
                    }
                    else if (containerInAutogate.ID_CLASS_CODE == "E")
                    {
                        return "OTW Terminal";
                    }
                }
            }
            return model.TRUCKPOSITION;
        }

        private string updateTruckPositionX(CONTAINER_X model)
        {
            if (model.TRANSACTION_ID.Contains("/BHD")) return "EX BEHANDLE";
            decimal truckid = Convert.ToInt64(model.TAGNUMBER);
            CON_LISTCONT containerInAutogate = (from c in _autogateEntities.CON_LISTCONT
                                                where c.TAR == model.TRANSACTION_ID.Trim() && c.NO_CONTAINER == model.CONTAINERNUMBER
                                                orderby c.GT_DATE descending
                                                select c).FirstOrDefault();

            if (containerInAutogate != null)
            {
                if (containerInAutogate.GT_DATE_OUT != null)
                {
                    if (containerInAutogate.ID_CLASS_CODE == "I")
                    {
                        return "Gate OUT CG";
                    }
                    else if (containerInAutogate.ID_CLASS_CODE == "E")
                    {
                        return "Truck Gate OUT CG";
                    }
                }

                if (containerInAutogate.TRUCKOUT_TERMINAL != null)
                {
                    if (containerInAutogate.ID_CLASS_CODE == "I")
                    {
                        return "OUT Terminal";
                    }
                    else if (containerInAutogate.ID_CLASS_CODE == "E")
                    {
                        return "Truck OUT Terminal";
                    }
                }

                if (containerInAutogate.TRUCKIN_TERMINAL != null)
                {
                    if (containerInAutogate.ID_CLASS_CODE == "I")
                    {
                        return "Truck IN Terminal";
                    }
                    else if (containerInAutogate.ID_CLASS_CODE == "E")
                    {
                        return "IN Terminal";
                    }
                }

                if (model.TRUCKPOSITION == null || model.TRUCKPOSITION.Trim() == "Approaching Yard")
                {
                    if (containerInAutogate.ID_CLASS_CODE == "I")
                    {
                        return "Truck OTW Terminal";
                    }
                    else if (containerInAutogate.ID_CLASS_CODE == "E")
                    {
                        return "OTW Terminal";
                    }
                }
            }
            return model.TRUCKPOSITION;
        }

        public ActionResult Detail(int id, string type, int next, int prev)
        {
            var container = Task.Factory.StartNew<CONTAINER>(() =>
            {
                if (next == 0 && prev == 0)
                    return containerRepo.Get(id);
                else if (next != 0)
                    return containerRepo.GetNext(id, User.Identity.Name, type);
                else
                    return containerRepo.GetPrev(id, User.Identity.Name, type);
            });
            var model = container.Result ;
            if (model == null) return Json("Empty",JsonRequestBehavior.AllowGet );
            ViewData["ImageList"] = containerRepo.GetImageList(id);
            ViewData["ImageOcrList"] = containerRepo.GetOCRImageList(id);
            ViewData["BatalMuat"] = containerRepo.GetBatalMuat(model);
            ViewData["BaseUrl"] = WebConfigurationManager.AppSettings["ImageUrl"].ToString();
            model.TRUCKPOSITION = updateTruckPosition(model);

            if (!string.IsNullOrWhiteSpace(model.DOCUMENTTYPE))
            {
                Entities _entities = new Entities();
                var translateDocumentType = _entities.AG_DOCUMENT_TYPE.Where(a => a.DOC_TYPE.Equals(model.DOCUMENTTYPE)).FirstOrDefault();
                if (translateDocumentType != null)
                    model.DOCUMENTTYPE = translateDocumentType.DOC_NAME;
            }

            string[] roles = Roles.GetRolesForUser(User.Identity.Name);
            if (roles.Contains("P2"))
            {
                ATENSI_P2 atensi = containerRepo.GetP2(model.ID);
                ViewData["atensi"] = atensi;
            }
            return View(model);
        }

        public ActionResult DetailDeleted(int id, string type, int next, int prev)
        {
            var container = Task.Factory.StartNew<CONTAINER_X>(() =>
            {
                if (next == 0 && prev == 0)
                    return containerRepo.GetX(id);
                else if (next != 0)
                    return containerRepo.GetNextX(id, User.Identity.Name, type);
                else
                    return containerRepo.GetPrevX(id, User.Identity.Name, type);
            });
            var model = container.Result;
            if (model == null) return Json("Empty", JsonRequestBehavior.AllowGet);
            ViewData["ImageList"] = containerRepo.GetImageList(id);
            ViewData["ImageOcrList"] = containerRepo.GetOCRImageListX(id);
            ViewData["BatalMuat"] = containerRepo.GetBatalMuatX(model);
            ViewData["BaseUrl"] = WebConfigurationManager.AppSettings["ImageUrl"].ToString();
            model.TRUCKPOSITION = updateTruckPositionX(model);

            if (!string.IsNullOrWhiteSpace(model.DOCUMENTTYPE))
            {
                Entities _entities = new Entities();
                var translateDocumentType = _entities.AG_DOCUMENT_TYPE.Where(a => a.DOC_TYPE.Equals(model.DOCUMENTTYPE)).FirstOrDefault();
                if (translateDocumentType != null)
                    model.DOCUMENTTYPE = translateDocumentType.DOC_NAME;
            }

            string[] roles = Roles.GetRolesForUser(User.Identity.Name);
            if (roles.Contains("P2"))
            {
                ATENSI_P2 atensi = containerRepo.GetP2(model.ID);
                ViewData["atensi"] = atensi;
            }
            return View(model);
        }

        public ActionResult Checkin(int id)
        {
            containerRepo.Check(id, User.Identity.Name);
            var container = Task.Factory.StartNew<CONTAINER>(() => containerRepo.Get(id));
            var imagelist = Task.Factory.StartNew<List<PICTURE>>(() => containerRepo.GetImageList(id));
            ViewData["ImageList"] = imagelist.Result ;
            ViewData["BaseUrl"] = WebConfigurationManager.AppSettings["ImageUrl"].ToString();
            return View("Detail",container.Result );
        }

        public ActionResult NextDetail(int id, string type)
        {
            var container = Task.Factory.StartNew<CONTAINER>(() => containerRepo.GetNext(id, User.Identity.Name, type));
            var imagelist = Task.Factory.StartNew<List<PICTURE>>(() => containerRepo.GetImageList(id));
            ViewData["ImageList"] = imagelist.Result ;
            ViewData["BaseUrl"] = WebConfigurationManager.AppSettings["ImageUrl"].ToString();
            return View("Detail",container.Result );
        }

        public ActionResult Hold(int id)
        {
            var container = containerRepo.Get(id);
            HoldModel model = new HoldModel();
            model.ID = (int)container.ID;
            model.CONTAINERNUMBER = container.CONTAINERNUMBER;
            return View(model);
        }

        [HttpPost]
        public ActionResult Hold(HoldModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                // Attempt to hold
                string status = containerRepo.Hold(model, User.Identity.Name);
                if (status == "Success")
                {
                    USERACTIVITY dataActivity = new USERACTIVITY();
                    dataActivity.USERNAME = User.Identity.Name;
                    dataActivity.HOLD = "H";
                    dataActivity.ACTIVITYDESCR = "User : " + User.Identity.Name + ", Hold Container : " + model.CONTAINERNUMBER + ", Keterangan : " + model.HOLDNOTE + "Tanggal : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm");
                    dataActivity.ADDATENSI = "-";
                    dataActivity.ADDPUTUSAN = "-";
                    dataActivity.CHECKCONTAINER = "-";
                    dataActivity.CONFIRMP2 = "-";
                    dataActivity.DELETETRX = "-";
                    dataActivity.EDITATENSI = "-";
                    dataActivity.RELEASE = "-";
                    activityRepo.saveActivity(dataActivity);
                    result = true;
                }
                else
                    ModelState.AddModelError("", "Gagal Hold : " + status);
            }
            // If we got this far, something failed, redisplay form
            if (result)
                return Json("Success", JsonRequestBehavior.AllowGet);
            else
            {
                ModelState.AddModelError("", "Gagal Hold, silahkan cek inputan");
                return View(model);
            }
        }

        public ActionResult Release(int id)
        { 
            CONTAINER container = containerRepo.Get(id);
            ReleaseModel model = new ReleaseModel();
            model.ID = (int)container.ID;
            model.CONTAINERNUMBER = container.CONTAINERNUMBER;
            if (container.DOCUMENTTYPE.Trim().ToLower() == "sppbe")
            {
                model.IS_SPPBE = true;
            }
            else
            {
                model.IS_SPPBE = false;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Release(ReleaseModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                // Attempt to release
                string status = containerRepo.Release(model, User.Identity.Name);
                if (status == "Success")
                {
                    USERACTIVITY activityuser = new USERACTIVITY();
                    activityuser.USERNAME = User.Identity.Name;
                    activityuser.HOLD = "-";
                    activityuser.ACTIVITYDESCR = "User : " + User.Identity.Name + ", Release Container :  " + model.CONTAINERNUMBER + ", Tanggal : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm");
                    activityuser.ADDATENSI = "-";
                    activityuser.ADDPUTUSAN = "-";
                    activityuser.CHECKCONTAINER = "-";
                    activityuser.CONFIRMP2 = "-";
                    activityuser.DELETETRX = "-";
                    activityuser.EDITATENSI = "-";
                    activityuser.RELEASE = "R";
                    activityRepo.saveActivity(activityuser);
                    result = true;
                }
                else
                    ModelState.AddModelError("", "Gagal Release Container : " + status);
            }
            // If we got this far, something failed, redisplay form
            if (result)
                return Json("Success", JsonRequestBehavior.AllowGet);
            else
            {
                ModelState.AddModelError("", "Gagal Release Container, cek Input");
                return View(model);
            }
        }
        
        public ActionResult ReleaseP2(int id)
        {
            Entities entities = new Entities();
            ATENSI_P2 p2 = (from s in entities.ATENSI_P2
                            where
                            s.ID==id
                            select s).FirstOrDefault();
            ReleaseModel model = new ReleaseModel();
            model.ID = id;

            model.CONTAINERNUMBER = p2.CONTAINERNUMBER;

            return View(model);
        }

        [HttpPost]
        public ActionResult ReleaseP2(ReleaseModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                // Attempt to release
                string status = containerRepo.ReleaseP2(model, User.Identity.Name, Request);
                if (status == "Success")
                {
                    USERACTIVITY activityuser = new USERACTIVITY();
                    activityuser.USERNAME = User.Identity.Name;
                    activityuser.HOLD = "-";
                    activityuser.ACTIVITYDESCR = "User : " + User.Identity.Name + ", Release Container :  " + model.CONTAINERNUMBER + ", Tanggal : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm");
                    activityuser.ADDATENSI = "-";
                    activityuser.ADDPUTUSAN = "-";
                    activityuser.CHECKCONTAINER = "-";
                    activityuser.CONFIRMP2 = "-";
                    activityuser.DELETETRX = "-";
                    activityuser.EDITATENSI = "-";
                    activityuser.RELEASE = "R";
                    activityRepo.saveActivity(activityuser);
                    result = true;
                }
                else
                {
                    ModelState.AddModelError("", "Gagal Release Container : " + status);
                }
            }
            else
            {
                ModelState.AddModelError("", "Gagal Release Container, cek Input");
            }
            // If we got this far, something failed, redisplay form
            if (result)
                return Json("Success", JsonRequestBehavior.AllowGet);
            else
            {
                return View(model);
            }
        }

        public ActionResult DetailImage(string id)
        {
            var result = Task.Factory.StartNew<byte[]>(() =>
            {
                Image image = Image.FromFile(Path.Combine(Server.MapPath("/Content/img/detail"), id + ".jpg"));
                //byte[] image = FetchImage();
                MemoryStream ms = new MemoryStream();

                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return ms.ToArray();
            });
            return File(result.Result , "image/png"); // adjust content type appropriately
        }

        [HttpPost]
        public ActionResult TrackingContainer(string INOUT,string VOYAGE, string CONTAINERNUMBER, string DOCUMENTNUMBER, string GATEINTIME)
        {
            DateTime parseDate = DateTime.ParseExact(GATEINTIME, "ddMMyyyy",null);
            string TRUCKPOSITION = string.Empty;
            TrackingContainerRepository trackingRepo = new TrackingContainerRepository();
            var tracking = trackingRepo.trackingContainer(INOUT, VOYAGE, CONTAINERNUMBER);
            if (tracking.Count > 0)
            {
                if (tracking.Count > 1)
                {
                    var data = tracking.Where(a => a.response_date_parsed <= parseDate && DOCUMENTNUMBER.Equals(a.response_no)).OrderByDescending(a => a.response_date_parsed).FirstOrDefault();
                    if (data != null)
                        TRUCKPOSITION = data.status;
                    else
                    {
                        data = tracking.Where(a => DOCUMENTNUMBER.Equals(a.response_no)).OrderBy(a => a.response_date_parsed).FirstOrDefault();
                        if (data != null)
                            TRUCKPOSITION = data.status;
                    }
                }
                else
                    TRUCKPOSITION = tracking[0].status;
            }
            return Json(TRUCKPOSITION, JsonRequestBehavior.AllowGet);
        }
    }
}
