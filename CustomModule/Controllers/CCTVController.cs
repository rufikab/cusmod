﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomModule.Models;

namespace CustomModule.Controllers
{
    [Authorize]
    public class CCTVController : Controller
    {
        protected AutogateEntities _autogateEntities = new AutogateEntities();
        //
        // GET: /CCTV/

        public ActionResult Index()
        {
            IQueryable<GateModel> ret = (from g in _autogateEntities.M_GATE_LANE
                                         orderby g.I_O,g.ID_GATE_LANE
                                   select new GateModel
                                   {
                                       NAME = g.GATE_NAME,
                                       camera = (from c in _autogateEntities.AG_CAMERA
                                                 where c.GATEID == g.ID_GATE_LANE
                                                 select c)
                                   });
            var userAgent = Request.UserAgent;
            if (userAgent.IndexOf("Firefox") > -1)
                return View("Index_old",ret.ToList());
            return View(ret.ToList());
        }

    }
}
