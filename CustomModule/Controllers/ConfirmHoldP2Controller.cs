﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using CustomModule.Models;
using System.Web.Profile;
using Telerik.Web.Mvc;
using System.Threading.Tasks;

namespace CustomModule.Controllers
{
    [Authorize(Roles = "Administrator,P2")]
    public class ConfirmHoldP2Controller : Controller
    {
        ContainerRepository containerRepo = new ContainerRepository();
        ConfirmHoldP2Repository confirmRepo = new ConfirmHoldP2Repository();
        string basepath = "/";

        public ActionResult Index()
        {
            Session["ConfirmP2FilterSession"] = null;
            //List<ATENSI_P2> ConfirmP2List = (from s in _entities.ATENSI_P2 select s).ToList();

            //int totalUsers = ConfirmP2List.Count;
            //int totalPages = ((totalUsers - 1) / 5) + 1;

            var result = Task.Factory.StartNew<ConfirmHoldP2ViewModel>(() =>
            {
                ConfirmHoldP2ViewModel model = new ConfirmHoldP2ViewModel();
                model.listItemViewModel = FillItemList();
                return model;
            });
            var HasBroken = Task.Factory.StartNew<bool>(() => containerRepo.hasBrokenContainer(User.Identity.Name));

            ViewData.Model = result.Result;
            ViewData["HasBroken"] = HasBroken.Result ;
            return View();
        }

        public ActionResult ConfirmP2List(string SessionData = "")
        {
            if (SessionData.Equals("") == false)
            {
                Session["ConfirmP2FilterSession"] = SessionData.Replace("...", "##");
            }
            var model = Task.Factory.StartNew<List<ConfirmListHoldP2ViewModel>>(() => FillItemList());
            return View(model.Result );
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxBinding()
        {
            var model = Task.Factory.StartNew<List<ConfirmListHoldP2ViewModel>>(() => FillItemList());
            return View("ConfirmP2List", new GridModel(model.Result ));
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxFilterBinding(AjaxFilterBindingModel date)
        {
            if (String.IsNullOrEmpty(date.from)) date.from = "";
            if (String.IsNullOrEmpty(date.to)) date.to = "";
            date.from = date.from.Replace("-", "/");
            date.to = date.to.Replace("-", "/");
            var model = Task.Factory.StartNew<List<ConfirmListHoldP2ViewModel>>(() => FillItemList());
            return View("ConfirmP2List", new GridModel(model.Result ));
        }

        [HttpGet]
        public ActionResult ConfirmP2Data(int id)
        {
            var result = Task.Factory.StartNew<bool>(() => confirmRepo.ConfirmP2Data(User.Identity.Name, id));
            var ret = result.Result ;
            return Json(ret.ToString(),JsonRequestBehavior.AllowGet);
        }

        #region Non Action
        [NonAction]
        List<ConfirmListHoldP2ViewModel> FillItemList(int page = 1)
        {
            if(Session!=null)
                return confirmRepo.FillItemList(Session["ConfirmP2FilterSession"], User.Identity.Name, page, "/CustomModule");
            else
                return confirmRepo.FillItemList(null, User.Identity.Name, page, "/CustomModule");
        }
        #endregion
    }
}
