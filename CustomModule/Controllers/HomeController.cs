﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.IO;
using System.Web.Configuration;
using CustomModule.Models;
using System.Web.Security;
using System.Threading.Tasks;

namespace CustomModule.Controllers
{
    [Authorize(Roles = "Administrator,Penyegelan,P2,ControlRoom,DeletionOperator")]
    [HandleError]
    public class HomeController : AsyncController
    {
        ContainerRepository containerRepo = new ContainerRepository();

        public ActionResult Index()
        {
            string[] roles = Roles.GetRolesForUser(User.Identity.Name);
            if (roles.Count() == 1 && (roles.Contains("Planner") || roles.Contains("CustomerCare")))
                return View("Index2");
            ViewData["OnlyAuto"] = (roles.Count() == 1 && roles.Contains("Penyegelan")) ? true : false;
            ViewData["ControlRoom"] = (roles.Count() >= 1 && roles.Contains("ControlRoom")) ? true : false;
            ViewData["P2"] = (roles.Contains("P2")) ? true : false;
            ViewData["Timer"] = Int32.Parse(WebConfigurationManager.AppSettings["dashboardRefreshInterval"]) * 1000;
            var HasBroken = Task.Factory.StartNew<bool>(() => containerRepo.hasBrokenContainer(User.Identity.Name));
            ViewData["HasBroken"] =HasBroken.Result ;
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult ContainerDetail()
        {
            return View();
        }

        public ActionResult DetailAtensiP2()
        {
            return View("DetailAtensiP2");
        }

        public ActionResult Release()
        {
            return View("Release");
        }

        public ActionResult ContainerImage(string id)
        {
            var result = Task.Factory.StartNew<byte[]>(() =>
            {
                Image image = Image.FromFile(Path.Combine(Server.MapPath("~/Content/img/detail"), id + ".jpg"));
                MemoryStream ms = new MemoryStream();
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return ms.ToArray();
            });
            return File(result.Result , "image/png"); 
        }
    }
}
