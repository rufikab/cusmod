﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomModule.Models;
using Telerik.Web.Mvc;
using System.Data;
using System.IO;
using System.Threading.Tasks;
using System.Web.Security;

namespace CustomModule.Controllers
{
    [Authorize]
    public class SearchController : Controller
    {
        ContainerRepository containerRepo = new ContainerRepository();

        public ActionResult Index(string searchString)
        {
            var HasBroken = Task.Factory.StartNew<bool>(() => containerRepo.hasBrokenContainer(User.Identity.Name));
            ViewData["HasBroken"] = HasBroken.Result ;
            ViewData["search"] = searchString;
            string[] roles = Roles.GetRolesForUser(User.Identity.Name);
            ViewData["ControlRoom"] = (roles.Count() == 1 && roles.Contains("ControlRoom")) ? true : false;
            return View();
        }

        public ActionResult IndexAjax(string searchString)
        {
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetSearchList(searchString, User.Identity.Name));
            return View(model.Result );
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxBinding(string searchString)
        {
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetSearchList(searchString, User.Identity.Name));
            return View("IndexAjax", new GridModel(model.Result ));
        }
    }
}