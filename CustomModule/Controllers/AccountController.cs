﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using CustomModule.Models;
using System.Web.Profile;

namespace CustomModule.Controllers
{

    [HandleError]
    public class AccountController : Controller
    {
        string email = "admin@halotec-indonesia.com";
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        // **************************************
        // URL: /Account/LogOn
        // **************************************

        public ActionResult LogOn()
        {
            return View();
        }

        [Authorize(Roles = "administrator")]
        public ActionResult CreateRole()
        {
            Roles.CreateRole("KOJAplanner");
            Roles.CreateRole("KOJAcustomerCare");
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ValidateUser(model.UserName, model.Password))
                {
                    FormsService.SignIn(model.UserName, model.RememberMe);
                    if (!String.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        string[] roles = Roles.GetRolesForUser(model.UserName);
                        string route = RedirectRole.path(roles);
                        string[] routes = route.Split('/');
                        if (routes.Length == 1)
                        {
                            return RedirectToAction("", routes[0]);
                        }
                        else if (routes.Length == 2)
                        {
                            return RedirectToAction(routes[1], routes[0]);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // **************************************
        // URL: /Account/LogOff
        // **************************************
        [Authorize]
        public ActionResult LogOff()
        {
            FormsService.SignOut();
            var test = User.Identity.IsAuthenticated;
            Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            HttpCookie cookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(cookie);
            }
            return RedirectToAction("Index", "Home");
        }

        // **************************************
        // URL: /Account/UserList
        // **************************************

        [Authorize(Roles = "Administrator")]
        public ActionResult Index(string id, int page = 1, int pageSize = 5)
        {
            MembershipUserCollection users = Membership.GetAllUsers();
            int totalUsers = 0;
            if (id != null)
            {
                users = Membership.FindUsersByName("%" + id.ToLower() + "%", page - 1, pageSize, out totalUsers);
            }
            int totalPages = ((totalUsers - 1) / pageSize) + 1;
            ViewData["Users"] = users;
            ViewData["TotalPages"] = totalPages;
            ViewData["Page"] = page;
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult UserList(string id,int page=1,int pageSize=5)
        {
            MembershipUserCollection users = Membership.GetAllUsers();
            int totalUsers = 0;
            if (id != null)
            {
                users = Membership.FindUsersByName("%" + id.ToLower() + "%", page - 1, pageSize, out totalUsers);
            }
            int totalPages = ((totalUsers - 1) / pageSize) + 1;
            ViewData["Users"] = users;
            ViewData["TotalPages"] = totalPages;
            ViewData["Page"] = page;
            return View();
        }

        // **************************************
        // URL: /Account/AddUser
        // **************************************

        [Authorize(Roles = "Administrator")]
        public ActionResult UserAdd()
        {
            var model = new RegisterModel();
            //model.Roles = System.Web.Security.Roles.GetAllRoles(); //or however you populate your roles
            ViewData["Roles"] = System.Web.Security.Roles.GetAllRoles(); //or however you populate your roles
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult UserAdd(RegisterModel model)
        {
            ViewData["Roles"] = System.Web.Security.Roles.GetAllRoles(); //or however you populate your roles
            model.Email = email;
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus = MembershipService.CreateUser(model.UserName, model.Password, model.Email);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    UserProfile profile = UserProfile.GetUserProfile(model.UserName);
                    profile.NIP = model.NIP;
                    profile.Name = model.Name;
                    profile.Save();
                    if (model.Roles!=null&&model.Roles.Length != 0)
                    {
                        Roles.AddUserToRoles(model.UserName, model.Roles);
                    }
                    MembershipUser user = Membership.GetUser(model.UserName);
                    user.IsApproved = model.Active;
                    Membership.UpdateUser(user);
                    return Json("Success");
                }
                else
                {
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View(model);
        }

        // **************************************
        // URL: /Account/UserEdit
        // **************************************

        [Authorize(Roles = "Administrator")]
        public ActionResult UserEdit(string id)
        {
            MembershipUser user = Membership.GetUser(id);
            UpdateModel model = new UpdateModel();
            model.Roles = Roles.GetRolesForUser(id);
            model.UserName = user.UserName;
            model.Email = user.Email;
            model.Active = user.IsApproved;
            try
            {
                UserProfile profile = UserProfile.GetUserProfile(model.UserName);
                model.Name = profile.Name;
                model.NIP = profile.NIP;
            }
            catch(Exception e)
            {

            }
            ViewData["Roles"] = System.Web.Security.Roles.GetAllRoles(); //or however you populate your roles
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult UserEdit(UpdateModel model)
        {
            ViewData["Roles"] = System.Web.Security.Roles.GetAllRoles(); //or however you populate your roles
            model.Email = email;
            if (ModelState.IsValid)
            {
                MembershipUser user = Membership.GetUser(model.UserName);
                user.Email = model.Email;
                user.IsApproved = model.Active;
                // Attempt to register the user
                try
                {                 
                    Membership.UpdateUser(user);
                    UserProfile profile = UserProfile.GetUserProfile(model.UserName);
                    profile.NIP = model.NIP;
                    profile.Name = model.Name;
                    profile.Save();
                    string[] oldRoles = Roles.GetRolesForUser(user.UserName);
                    Roles.RemoveUserFromRoles(user.UserName,oldRoles);
                    if (model.Roles.Length != 0)
                    {
                        Roles.AddUserToRoles(model.UserName, model.Roles);
                    }
                    if (model.NewPassword != null&&model.NewPassword != String.Empty)
                    {
                        string tempPassword = user.ResetPassword();
                        try{
                            if (user.ChangePassword(tempPassword, model.NewPassword))
                            {
                                return Json("Success",JsonRequestBehavior.AllowGet);
                            }

                        }catch(Exception e)
                        {
                            ModelState.AddModelError("", e.Message);
                            return View(model);
                        }
                    }
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                catch (System.Configuration.Provider.ProviderException e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public ActionResult UserDelete(string id)
        {
            bool ret= Membership.DeleteUser(id, true);
            return Json(ret.ToString());
        }


        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public ActionResult UserActivate(string id)
        {
            MembershipUser user = Membership.GetUser(id);
            user.IsApproved = true;
            // Attempt to register the user
            Membership.UpdateUser(user);
            return Json("True", JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public ActionResult UserDeactivate(string id)
        {
            MembershipUser user = Membership.GetUser(id);
            user.IsApproved = false;
            // Attempt to register the user
            Membership.UpdateUser(user);
            return Json("True", JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus = MembershipService.CreateUser(model.UserName, model.Password, model.Email);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    FormsService.SignIn(model.UserName, false /* createPersistentCookie */);
                    return Json("Success");
                }
                else
                {
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View(model);
        }

        // **************************************
        // URL: /Account/ChangePassword
        // **************************************

        [Authorize]
        public ActionResult ChangePassword()
        {
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View(model);
        }

        // **************************************
        // URL: /Account/ChangePasswordSuccess
        // **************************************
        [Authorize]
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

    }
}
