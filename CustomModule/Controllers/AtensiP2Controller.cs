﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using CustomModule.Models;
using System.Web.Profile;
using Telerik.Web.Mvc;
using System.Globalization;
using System.Threading.Tasks;
using CustomModule.Helper;
using System.Text;
using System.IO;
using System.Net;

namespace CustomModule.Controllers
{
    [Authorize(Roles = "Administrator,P2")]
    public class AtensiP2Controller : AsyncController
    {
        private string _xmlDir = System.Configuration.ConfigurationManager.AppSettings["messageXMLDirName"];
        private Entities _entities = new Entities();
        private UserActivityRepository activityRepo = new UserActivityRepository();
        ContainerRepository containerRepo = new ContainerRepository();
        protected AutogateEntities _autogateEntities = new AutogateEntities();

        public ActionResult Index(int page = 1, String filterData = "")
        {
            Session["AtensiP2FilterSession"] = null;

            var atensiP2List = Task.Factory.StartNew<List<ATENSI_P2>>(() => (from s in _entities.ATENSI_P2 select s).ToList());
            var hasbroken = Task.Factory.StartNew<bool>(() => containerRepo.hasBrokenContainer(User.Identity.Name));

            int totalUsers = atensiP2List.Result.Count(); ;
            int totalPages = ((totalUsers - 1) / 5) + 1;

            AtensiP2ViewModel model = new AtensiP2ViewModel();
            model.listViewModel = FillItemList(page);
            model.addItemViewModel = new AtensiP2ItemViewModel();

            model.addItemViewModel.ExportImport = "E";
            model.addItemViewModel.IsActive = 1;
            model.addItemViewModel.DateInput = DateTime.Now.ToString();
            model.addItemViewModel.DocumentDate = DateTime.Now.ToString("MM/dd/yyyy");
            model.editItemViewModel = new AtensiP2EditItemViewModel();
            
            ViewData.Model = model;
            ViewData["TotalPages"] = totalPages;
            ViewData["Page"] = page;
            ViewData["HasBroken"] = hasbroken.Result ;
            return View();
        }

        public ActionResult AtensiP2Filter(String StartDateFilter, String EndDateFilter, String ContainerNumberFilter, String DocumentNumberFilter)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    String sessionData = StartDateFilter + "##" + EndDateFilter + "##";
                    sessionData = sessionData + ((ContainerNumberFilter.Equals("")) ? "NoDataInput" : ContainerNumberFilter) + "##";
                    sessionData = sessionData + ((DocumentNumberFilter.Equals("")) ? "NoDataInput" : DocumentNumberFilter);
                    Session["AtensiP2FilterSession"] = sessionData;
                }
                catch
                { }
            }).Start();
            return RedirectToAction("AtensiP2List", "AtensiP2");
        }

        //public ActionResult AtensiP2List(String SessionData="", int page = 1)
        //{
        //    List<ATENSI_P2> atensiP2List = (from s in _entities.ATENSI_P2 select s).ToList();
        //if (SessionData.Equals("") == false)
        //{
        //    Session["AtensiP2FilterSession"] = SessionData.Replace("...", "##"); 
        //}
        //    if (Session["AtensiP2FilterSession"] != null)
        //    {
        //        try
        //        {
        //            String[] filterData = Session["AtensiP2FilterSession"].ToString().Split(new String[] { "##" }, StringSplitOptions.None);
        //            int startDateYear = (DateTime.Parse(filterData[1])).Year;
        //            int startDateMonth = (DateTime.Parse(filterData[1])).Month;
        //            int startDateDay = (DateTime.Parse(filterData[1])).Day;
        //            int endDateYear = (DateTime.Parse(filterData[2])).Year;
        //            int endDateMonth = (DateTime.Parse(filterData[2])).Month;
        //            int endDateDay = (DateTime.Parse(filterData[2])).Day;
        //            int ContainerID = (filterData[3].Equals("NoDataInput")) ? 0 : Int32.Parse(filterData[3]);
        //            String DocNumber = (filterData[4].Equals("NoDataInput")) ? "" : filterData[4];
        //            if (ContainerID == 0)
        //            {
        //                atensiP2List = (from s in _entities.ATENSI_P2
        //                                       where s.DOCUMENTNUMBER.ToLower().Contains(DocNumber) &&
        //                                       s.DATEFROM >= new DateTime(startDateYear, startDateMonth, startDateDay) &&
        //                                       s.DATETO <= new DateTime(endDateYear, endDateMonth, endDateDay)
        //                                       select s).ToList();
        //            }
        //            else
        //            {
        //                atensiP2List = (from s in _entities.ATENSI_P2
        //                                       where s.DOCUMENTNUMBER.ToLower().Contains(DocNumber) &&
        //                                       s.CONTAINER_ID == ContainerID &&
        //                                       s.DATEFROM >= new DateTime(startDateYear, startDateMonth, startDateDay) &&
        //                                       s.DATETO <= new DateTime(endDateYear, endDateMonth, endDateDay)
        //                                       select s).ToList();
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            atensiP2List = (from s in _entities.ATENSI_P2 select s).ToList();
        //        }
        //    }


        //    int totalUsers = atensiP2List.Count;
        //    int totalPages = ((totalUsers - 1) / 5) + 1;

        //    AtensiP2ViewModel model = new AtensiP2ViewModel();
        //    model.listViewModel = FillItemList(page);
        //    model.addItemViewModel = new AtensiP2ItemViewModel();
        //    model.editItemViewModel = new AtensiP2EditItemViewModel();

        //    ViewData.Model = model;
        //    ViewData["TotalPages"] = totalPages;
        //    ViewData["Page"] = page;
        //    return View();
        //}
        
        public ActionResult AtensiP2List(string SessionData = "")
        {
            if (SessionData.Equals("") == false)
                Session["AtensiP2FilterSession"] = SessionData.Replace("...", "##");
            //from = from.Replace("-", "/");
            //to = to.Replace("-", "/");
            var model = Task.Factory.StartNew<List<AtensiP2ListItemViewModel>>(() => FillItemList(1));//containerRepo.GetAutoList(from, to);
            return View(model.Result );
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxBinding()
        {
            //int totalPages = (containerRepo.GetAutoListCount(from, to) - 1) / pageSize;
            var model = Task.Factory.StartNew<List<AtensiP2ListItemViewModel>>(() => FillItemList(1));//containerRepo.GetAutoList();
            //ViewData["TotalPages"] = totalPages;
            //ViewData["Page"] = page;
            return View("AtensiP2List", new GridModel(model.Result));
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxFilterBinding(string SessionData = "")
        {
            if (SessionData.Equals("") == false)
            {
                Session["AtensiP2FilterSession"] = SessionData.Replace("...", "##");
            }
            //int totalPages = (containerRepo.GetAutoListCount(from, to) - 1) / pageSize;
            var model = Task.Factory.StartNew<List<AtensiP2ListItemViewModel>>(() => FillItemList(1));// containerRepo.GetAutoList(date.from, date.to);
            //ViewData["TotalPages"] = totalPages;
            //ViewData["Page"] = page;
            return View("AtensiP2List", new GridModel(model.Result ));
        }

        public ActionResult AtensiP2Add()
        {
            var model =new AtensiP2ItemViewModel();
            return View(model);
        }


        public ActionResult IsP2FileExist(int id)
        {
            var p2File = (from p in _entities.P2FILE
                          where p.P2ID == id
                          orderby p.ID descending
                          select p).FirstOrDefault();

            string path = "";
            if (p2File != null)
            {
                if (String.IsNullOrEmpty(p2File.DOCPATH))
                {
                    path = p2File.DOCPATH;
                }
                else
                {
                    path = p2File.RELEASEPATH;
                }
            }
            return Json(path ,
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsReleaseP2FileExist(int id)
        {
            var p2File = (from p in _entities.P2FILE
                          where p.P2ID == id
                          orderby p.ID descending
                          select p).FirstOrDefault();

            return Json((p2File != null) ? Path.GetFileName(p2File.RELEASEPATH) : "",
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetReleaseP2File(int id)
        {
            var p2File = (from p in _entities.P2FILE
                          where p.P2ID == id
                          orderby p.ID descending
                          select p).FirstOrDefault();
            if (p2File != null)
            {
                string filepath = p2File.RELEASEPATH;
                string filename = Path.GetFileName(filepath);
                byte[] filedata = System.IO.File.ReadAllBytes(filepath);

                string contentType = MimeExtensionHelper.GetMimeType(filepath);

                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = filename,
                    Inline = true,
                };

                Response.AppendHeader("Content-Disposition", cd.ToString());

                return File(filedata, contentType);
            }
            else
            {
                throw new HttpException(404, "File Not Found for P2");
            }
        }

        public ActionResult GetP2File(int id)
        {
            var p2File = (from p in _entities.P2FILE
                          where p.P2ID == id
                          orderby p.ID descending
                          select p).FirstOrDefault();
            if (p2File != null)
            {
                string filepath = p2File.DOCPATH;
                string filename = Path.GetFileName(filepath);
                if (String.IsNullOrEmpty(filename))
                {
                    filepath = p2File.RELEASEPATH;
                    filename = Path.GetFileName(filepath);
                }
                try
                {
                    byte[] filedata = System.IO.File.ReadAllBytes(filepath);

                    string contentType = MimeExtensionHelper.GetMimeType(filepath);
                    var cd = new System.Net.Mime.ContentDisposition
                    {
                        FileName = filename,
                        Inline = true,
                    };

                    Response.AppendHeader("Content-Disposition", cd.ToString());

                    return File(filedata, contentType);
                }
                catch (Exception ex)
                {
                    throw new HttpException(404, "File Not Found for P2 : " + ex.Message);
                }
            }
            else
            {
                throw new HttpException(404, "File Not Found for P2");
            }
        }

        [HttpPost]
        public ActionResult AtensiP2Add(AtensiP2ItemViewModel item)
        {
            if (item.ID == 0)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        String userName = System.Web.HttpContext.Current.User.Identity.Name;
                        MembershipUser users = Membership.GetUser();

                        var id = 0;
                        if (_entities.ATENSI_P2.Count() > 0)
                        {
                            id = (from e in _entities.ATENSI_P2
                                  select e.ID).Max();
                        }
                        id += 1;
                        ATENSI_P2 atensiP2Data = new ATENSI_P2();
                        atensiP2Data.ID = id;

                        atensiP2Data.DOCUMENTNUMBER = item.DocumentNumber;
                        atensiP2Data.DOCUMENTYPE = item.DocumentType;
                        atensiP2Data.DOCUMENTDATE = DateTime.ParseExact(item.DocumentDate.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                        //atensiP2Data.DATEFROM = DateTime.ParseExact(item.DateFrom.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                        //atensiP2Data.DATETO = DateTime.ParseExact(item.DateTo.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);

                        atensiP2Data.DATEINPUT = DateTime.Now;
                        atensiP2Data.INPUTBY = item.InputBy;
                        atensiP2Data.REASON = item.Reason;
                        atensiP2Data.USER_ID = User.Identity.Name;
                        atensiP2Data.CONTAINERNUMBER = item.ContainerID;
                        atensiP2Data.ISACTIVE = 1;
                        atensiP2Data.ISCONFIRMED = 0;
                        atensiP2Data.EXPORTIMPORT = item.ExportImport;

                        _entities.AddToATENSI_P2(atensiP2Data);
                        _entities.SaveChanges();

                        
                        try
                        {
                            foreach (string file in Request.Files)
                            {
                                var hpf = this.Request.Files[file];
                                if (hpf.ContentLength == 0)
                                {
                                    continue;
                                }

                                string p2FileName = AtensiP2File.generateFileName(item.ID, Path.GetExtension(hpf.FileName));

                                hpf.SaveAs(p2FileName);
                                id = 0;
                                if (_entities.P2FILE.Count() > 0)
                                {
                                    id = (from e in _entities.P2FILE
                                          select e.ID).Max();
                                }
                                id += 1;
                                P2FILE p2file = new P2FILE();
                                p2file.DOCPATH = p2FileName;
                                p2file.P2ID = item.ID;
                                p2file.ID = id;
                                _entities.AddToP2FILE(p2file);
                                _entities.SaveChanges();

                            }

                        }
                        catch (Exception ex)
                        {
                            ModelState.AddModelError("", "Gagal Tambah P2 : Error Upload File" + ex.Message);
                            return View(item);
                        }
                         

                        if (atensiP2Data.ISACTIVE == 1)
                            holdContainer(item.ContainerID, item.ExportImport, atensiP2Data);

                        USERACTIVITY activityuser = new USERACTIVITY();
                        activityuser.USERNAME = User.Identity.Name;
                        activityuser.HOLD = "-";
                        activityuser.ACTIVITYDESCR = "User : " + User.Identity.Name + ", Menambah data atensi dengan data Container : " + item.ContainerID + ", ExportImport : " + item.ExportImport + ", Tanggal : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm");
                        activityuser.ADDATENSI = "A";
                        activityuser.ADDPUTUSAN = "-";
                        activityuser.CHECKCONTAINER = "-";
                        activityuser.CONFIRMP2 = "-";
                        activityuser.DELETETRX = "-";
                        activityuser.EDITATENSI = "-";
                        activityuser.RELEASE = "-";
                        activityRepo.saveActivity(activityuser);
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        ModelState.AddModelError("", "Gagal Tambah P2 : Model state is not valid");
                        return View(item);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Gagal Tambah P2 : Exception" + ex.Message);
                    return View(item);
                }
            }
            else
            {
                try
                {
                    foreach (string file in Request.Files)
                    {
                        var hpf = this.Request.Files[file];
                        if (hpf.ContentLength == 0)
                        {
                            continue;
                        }

                        string p2FileName = AtensiP2File.generateFileName(item.ID, Path.GetExtension(hpf.FileName));

                        hpf.SaveAs(p2FileName);
                        P2FILE p2file = new P2FILE();
                        p2file.DOCPATH = p2FileName;
                        p2file.P2ID = item.ID;
                        _entities.AddToP2FILE(p2file);
                        _entities.SaveChanges();

                    }

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Gagal Tambah P2 : Error Upload File" + ex.Message);
                    return View(item);
                }
                return RedirectToAction("Index", "AtensiP2");
            }
        }

        [HttpPost]
        public ActionResult AtensiP2Edit(AtensiP2EditItemViewModel item)
        {
            try
            {
                
                var atensiP2Data = (from f in _entities.ATENSI_P2 where f.ID == item.EditID select f).SingleOrDefault();
                    
                atensiP2Data.DOCUMENTNUMBER = item.EditDocumentNumber;
                atensiP2Data.DOCUMENTYPE = item.EditDocumentType;
                atensiP2Data.DOCUMENTDATE = DateTime.ParseExact(item.EditDocumentDate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                  
                //atensiP2Data.DATEINPUT = DateTime.ParseExact(item.EditDateInput.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture); 
                atensiP2Data.INPUTBY = item.EditInputBy;
                atensiP2Data.REASON = item.EditReason;
                atensiP2Data.USER_ID = item.EditUserID;
                atensiP2Data.CONTAINERNUMBER = item.EditContainerID;
                atensiP2Data.ISCONFIRMED = item.EditIsConfirmed;
                atensiP2Data.ISACTIVE = item.EditIsActive;
                atensiP2Data.EXPORTIMPORT = item.EditExportImport;

                /*
                try
                {
                    int id = 0;
                    foreach (string file in Request.Files)
                    {
                        var hpf = this.Request.Files[file];
                        if (hpf.ContentLength == 0)
                        {
                            continue;
                        }

                        string p2FileName = AtensiP2File.generateFileName(item.EditID, Path.GetExtension(hpf.FileName));

                        hpf.SaveAs(p2FileName);
                        if (_entities.P2FILE.Count() > 0)
                        {
                            id = (from e in _entities.P2FILE
                                    select e.ID).Max();
                        }
                        id += 1;
                        P2FILE p2file = new P2FILE();
                        p2file.DOCPATH = p2FileName;
                        p2file.P2ID = item.EditID;
                        p2file.ID = id;
                        _entities.AddToP2FILE(p2file);
                        _entities.SaveChanges();

                    }

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Gagal Tambah P2 : Error Upload File" + ex.Message);
                    return View(item);
                }
                */

                holdContainer(item.EditContainerID, item.EditExportImport, atensiP2Data);
                

                USERACTIVITY activityuser = new USERACTIVITY();
                activityuser.USERNAME = User.Identity.Name;
                activityuser.HOLD = "-";
                activityuser.ACTIVITYDESCR = "User : " + User.Identity.Name + ", Mengubah data atensi dengan data Container : " + atensiP2Data.CONTAINERNUMBER + ", No.Dokumen " + atensiP2Data.DOCUMENTNUMBER + ", Jenis Dokumen : " + atensiP2Data.DOCUMENTYPE + ", Tanggal : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm");
                activityuser.ADDATENSI = "-";
                activityuser.ADDPUTUSAN = "-";
                activityuser.CHECKCONTAINER = "-";
                activityuser.CONFIRMP2 = "-";
                activityuser.DELETETRX = "-";
                activityuser.EDITATENSI = "E";
                activityuser.RELEASE = "-";
                activityRepo.saveActivity(activityuser);
                _entities.SaveChanges();
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Gagal Tambah P2 : Error Upload File" + ex.Message);
                return View(item);
            }
            return RedirectToAction("Index", "AtensiP2");
        }


        [HttpGet]
        public ActionResult AtensiP2Delete(int id)
        {
            var result= Task.Factory.StartNew<bool>(() =>
            {
                ATENSI_P2 deletedData = (from f in _entities.ATENSI_P2 where f.ID == id select f).SingleOrDefault();
                bool ret = true;
                try
                {
                    _entities.DeleteObject(deletedData);
                    _entities.SaveChanges();
                }
                catch
                {
                    ret = false;
                }
                return ret;
            });
            var returnvalue = result.Result ;
            return Json(returnvalue.ToString(),JsonRequestBehavior.AllowGet);
        }

        #region No Action
        [NonAction]
        public List<AtensiP2ListItemViewModel> FillItemList(int page = 1)
        {
            List<ATENSI_P2> atensiP2List = new List<ATENSI_P2>();
            IQueryable<ATENSI_P2> atensiP2IEnumerable;
            List<AtensiP2ListItemViewModel> atensiP2ItemList = new List<AtensiP2ListItemViewModel>();

            if (Session["AtensiP2FilterSession"] != null)
            {
                try
                {
                    String[] filterData = Session["AtensiP2FilterSession"].ToString().Split(new String[] { "##" }, StringSplitOptions.None);


                    String ContainerID = (filterData[3].Equals("NoDataInput")) ? "0" : filterData[3];
                    String DocNumber = (filterData[4].Equals("NoDataInput")) ? "" : filterData[4];

                    try
                    {
                        DateTime startdate = DateTime.ParseExact(filterData[1], "MM/dd/yyyy", CultureInfo.InvariantCulture);
                        DateTime enddate = DateTime.ParseExact(filterData[2], "MM/dd/yyyy", CultureInfo.InvariantCulture);

                        if (startdate == enddate)
                        {
                            enddate = startdate.AddHours(12);
                        }
                        atensiP2IEnumerable = (from s in _entities.ATENSI_P2
                                               where
                                               s.DOCUMENTDATE >= startdate &&
                                               s.DOCUMENTDATE <= enddate
                                               select s);
                    }
                    catch (Exception exDate)
                    {
                        atensiP2IEnumerable = (from s in _entities.ATENSI_P2 select s);
                    }

                    if (!ContainerID.Equals("0") && !ContainerID.Equals(""))
                    {
                        atensiP2IEnumerable = atensiP2IEnumerable.Where(a => a.CONTAINERNUMBER.Contains(ContainerID));
                    }
                    if (!DocNumber.Equals("0") && !DocNumber.Equals(""))
                    {
                        atensiP2IEnumerable = atensiP2IEnumerable.Where(a => a.DOCUMENTNUMBER.Contains(DocNumber));
                    }
                }
                catch (Exception e)
                {
                    atensiP2IEnumerable = (from s in _entities.ATENSI_P2 select s);//.OrderBy(s => s.ID).Skip((page - 1) * 10).Take(10);
                }
            }
            else
            {
                atensiP2IEnumerable = (from s in _entities.ATENSI_P2 select s);//.OrderBy(s => s.ID).Skip((page - 1) * 5).Take(5);
            }

            atensiP2IEnumerable = atensiP2IEnumerable.OrderByDescending(a => a.HOLDDATE).ThenByDescending(a => a.RELEASEDATE)
                .ThenByDescending(a => a.DATEINPUT);

            foreach (ATENSI_P2 items in atensiP2IEnumerable)
            {
                items.CONTAINERNUMBER = items.CONTAINERNUMBER.Trim();
                AtensiP2ListItemViewModel atensiP2Data = new AtensiP2ListItemViewModel();
                CONTAINER container = (from c in _entities.CONTAINER
                                       where
                                      c.ID == items.CONTAINER_ID 
                                       orderby c.ID descending
                                       select c).FirstOrDefault();
                P2FILE file = (from p in _entities.P2FILE where p.P2ID==items.ID
                               orderby p.ID descending
                               select p).FirstOrDefault();

                if (items.ISACTIVE == 1)
                {
                    atensiP2Data.Action = HoldReleaseAction.getAction(items.ID, items.EXPORTIMPORT, "H", true);
                }
                else
                {
                    atensiP2Data.Action = HoldReleaseAction.getAction(items.ID, items.EXPORTIMPORT, "R", true);
                }

                if (items.HOLDBY != null)
                {
                    atensiP2Data.Hold = items.HOLDBY;
                    if (items.HOLDDATE != null)
                    {
                        atensiP2Data.Hold += " : " + items.HOLDDATE.Value.ToString("dd-MM-yyyy HH:mm");
                    }
                } if (items.RELEASEBY != null)
                    atensiP2Data.Release = items.RELEASEBY + " : " + items.RELEASEDATE.Value.ToString("dd-MM-yyyy HH:mm");
                atensiP2Data.ExportImport = (items.EXPORTIMPORT == "E") ? "Export" : "Import";
                atensiP2Data.TerminalStatus = items.TERMINALRESPONSE;
                atensiP2Data.ID = items.ID;
                atensiP2Data.DocumentNumber = items.DOCUMENTNUMBER;
                atensiP2Data.DocumentType = items.DOCUMENTYPE;
                atensiP2Data.DocumentDate = items.DOCUMENTDATE.Value.ToString("dd-MM-yyyy");
                atensiP2Data.ContainerID = items.CONTAINERNUMBER;
                //atensiP2Data.DateValid = items.DATEFROM.Value.ToShortDateString() + "-" + items.DATETO.Value.ToShortDateString();
                atensiP2Data.DateInput = items.DATEINPUT.Value;
                atensiP2Data.InputBy = items.INPUTBY;
                atensiP2Data.Reason = items.REASON;
                atensiP2Data.IsActive = (items.ISACTIVE == 1) ? "Ya" : "Tidak";
                atensiP2Data.IsConfirmed = (items.ISCONFIRMED == 1) ? "Confirmed" : "Requested";
                atensiP2Data.UserID = items.USER_ID;

                //atensiP2Data.Action = "<a href=\"javascript:void(0);\" title=\"Delete\" onclick=\"confirmModalDeleteAtensiP2('/AtensiP2/AtensiP2Delete/" + items.ID + "','AtensiP2','/AtensiP2/AtensiP2List');\" class=\"btn btn-mini \"><i class=\"icon-trash\"></i></a>" +
                //                        "<a href=\"#myModalEdit\" title=\"Edit\" role=\"button\" class=\"btn btn-mini a-edit\" data-toggle=\"modal\" data='{\"EditID\":\"" + items.ID + "\"," +
                //                        "\"EditDocumentNumber\":\"" + items.DOCUMENTNUMBER + "\"," +
                //                        "\"EditDocumentType\":\"" + items.DOCUMENTYPE + "\"," +
                //                        "\"EditDocumentDate\":\"" + items.DOCUMENTDATE.Value.ToString("MM/dd/yyyy") + "\"," +
                //                        "\"EditContainerID\":\"" + items.CONTAINERNUMBER + "\"," +
                //                        "\"EditDateInput\":\"" + items.DATEINPUT + "\"," +
                //                        "\"EditInputBy\":\"" + items.INPUTBY + "\"," +
                //                        "\"EditReason\":\"" + items.REASON + "\"," +
                //                        "\"EditIsActive\":\"" + items.ISACTIVE + "\"," +
                //                        "\"EditIsHoldRelease\":\"" + atensiP2Data.IsHold + "\"," +
                //                        "\"EditIsConfirmed\":\"" + items.ISCONFIRMED + "\"," +
                //                        "\"EditExportImport\":\"" + items.EXPORTIMPORT + "\"," +
                //                        "\"EditUserID\":\"" + items.USER_ID + "\"}' onclick=\"edit(this)\"><i class=\"icon-edit \"></i></a>";

                if(file!=null)
                atensiP2Data.Action2 = "<a href=\"javascript:void(0);\" title=\"ViewP2File\" onclick=\"viewP2File('" + items.ID + "');\" class=\"btn\"><i class=\"icon-file\"></i></a>";
                

                atensiP2ItemList.Add(atensiP2Data);
            }

            return atensiP2ItemList;
        }

        [NonAction]
        private bool holdContainer(string cn,string exportimport,ATENSI_P2 atensip2)
        {
            DateTime thresdHoldTime = DateTime.Now.AddHours(-72);
            MessageResponse response = new MessageResponse();
            cn = cn.Trim();
            CONTAINER containerInPort = (from c in _entities.CONTAINER
                                               where c.CONTAINERNUMBER.Trim() == cn 
                                               && c.EXPORTIMPORT == exportimport
                                               orderby c.ID descending
                                               select c).FirstOrDefault();

            if (containerInPort.GATEINTIME!=null&&
                containerInPort.GATEINTIME.GetValueOrDefault() < thresdHoldTime)
            {
                containerInPort = null;
            }


            CON_LISTCONT containerInAutogate = (from c in _autogateEntities.CON_LISTCONT
                                                where c.NO_CONTAINER == cn && c.ID_CLASS_CODE==exportimport
                                                orderby c.ID descending
                                                select c).FirstOrDefault();
            if (containerInAutogate.INSERTDATE!=null&&
                containerInAutogate.INSERTDATE.GetValueOrDefault() < thresdHoldTime)
            {
                containerInPort = null;
            }

            //already blocked
            if (atensip2.CONTAINER_ID != null&&atensip2.CONTAINER_ID != 0)
            {
                containerInPort = (from c in _entities.CONTAINER
                                   where c.ID == atensip2.CONTAINER_ID
                                   select c).FirstOrDefault();
            }

            string status = "OTW";
            if (exportimport == "I" && containerInPort == null)
            {
                string call_sign = "";
                if (containerInAutogate != null)
                {
                    call_sign = (from v in _autogateEntities.VES_VOYAGE
                                    where v.ID_VES_VOYAGE == containerInAutogate.ID_VES_VOYAGE
                                    select v.ID_VESSEL).FirstOrDefault();
                }
                if (atensip2.ISACTIVE == 1)
                {
                    status = "HOLD";
                }
                else
                {
                    status = "RELEASE";
                }

                CONTAINER containerItem = new CONTAINER();
                containerItem.CONTAINERNUMBER = atensip2.CONTAINERNUMBER;
                containerItem.TRANSACTION_ID = "atensi_" + atensip2.ID;
                containerItem.VESSELNAME = "";
                containerItem.EXPORTIMPORT = "I";
                containerItem.ID = 0;
                ServicePointManager
                                    .ServerCertificateValidationCallback +=
                                    (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                     (SecurityProtocolType)(0xc0 | 0x300 | 0xc00)
                       | SecurityProtocolType.Ssl3;
                response = CustomsBlockPost.Request(containerItem, atensip2,
                    call_sign,
                    status
                    );

                if (response.Result == CustomsBlockPost.SUCCESS_MSG
                    || response.Result == CustomsBlockPost.ALREADY_BLOCKED_MSG)
                {
                    atensip2.ISCONFIRMED = 1;
                }

                atensip2.TERMINALRESPONSE = response.Result + "-" + status
                        + " (" + DateTime.Now.ToString("dd-MM-yyyy HH:mm") + ")";

                if (atensip2.ISACTIVE == 1)
                {
                    atensip2.HOLDBY = atensip2.INPUTBY.Trim();
                    if(atensip2.ISCONFIRMED==1)
                        atensip2.HOLDDATE = DateTime.Now;
                }
                else
                {
                    atensip2.RELEASEBY = User.Identity.Name;
                    atensip2.RELEASEDATE = DateTime.Now;
                }
                

                _entities.SaveChanges();
                return true;
            }

            
            containerInAutogate = (from c in _autogateEntities.CON_LISTCONT
                                                where c.TAR == containerInPort.TRANSACTION_ID.Trim()
                                                orderby c.ID descending
                                                select c).FirstOrDefault();
            if (containerInAutogate != null)
            {
                status = "OTW";
                if (containerInPort.EXPORTIMPORT == "I" || containerInAutogate.TRUCKIN_TERMINAL != null)
                {
                    status = "HOLD";
                }

                if (containerInPort.EXPORTIMPORT == "I" && containerInPort.GATEOUTTIME!=null)
                {
                    return true;
                }

                if (atensip2.ISACTIVE == 1)
                {
                    string call_sign = "";
                    call_sign = (from v in _autogateEntities.VES_VOYAGE
                                    where v.ID_VES_VOYAGE == containerInAutogate.ID_VES_VOYAGE
                                    select v.ID_VESSEL).FirstOrDefault();
                    ServicePointManager
                                   .ServerCertificateValidationCallback +=
                                   (sender, cert, chain, sslPolicyErrors) => true;
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                         (SecurityProtocolType)(0xc0 | 0x300 | 0xc00)
                           | SecurityProtocolType.Ssl3;
                    response = CustomsBlockPost.Request(containerInPort, atensip2,
                        call_sign,
                        status);
                    if (response.Result == CustomsBlockPost.SUCCESS_MSG
                        || response.Result == CustomsBlockPost.ALREADY_BLOCKED_MSG)
                    {
                        atensip2.ISCONFIRMED = 1;
                    }


                    atensip2.TERMINALRESPONSE = response.Result +"-"+status 
                        +" (" + DateTime.Now.ToString("dd-MM-yyyy HH:mm") + ")";
                    

                    if (containerInPort.EXPORTIMPORT == "E" || containerInPort.GATEOUTTIME == null)
                    {
                        if (containerInPort.HOLDTYPE.Trim() == "A")
                        {
                            containerInPort.HOLDTYPE = "AP";
                        }
                        else
                        {
                            containerInPort.HOLDTYPE = "P";
                        }
                        containerInPort.HOLDSTATUS = "H";
                        containerInPort.HOLDBY = atensip2.INPUTBY.Trim();
                        containerInPort.HOLDTIME = DateTime.Now;
                        atensip2.CONTAINER_ID = containerInPort.ID;
                    }
                        
                }
                else
                {
					if (containerInPort.HOLDTYPE == "AP")
                    {
                        containerInPort.HOLDSTATUS = "H";
                        containerInPort.HOLDTYPE = "A";
                    }
                    else
                    {
                        containerInPort.HOLDSTATUS = "R";
                    }
                }
            }
            

            _entities.SaveChanges();
            return containerInPort!=null;
        }
        #endregion
    }
}
