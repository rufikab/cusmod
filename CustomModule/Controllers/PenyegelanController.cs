﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomModule.Models;
using Telerik.Web.Mvc;
using System.Web.Configuration;
using System.Threading.Tasks;

namespace CustomModule.Controllers
{
    public class AjaxFilterBindingModel
    {
        public string from { get; set; }
        public string to { get; set; }
    }
    [Authorize(Roles = "Administrator,Penyegelan,ControlRoom")]
    public class PenyegelanController : Controller
    {
        ContainerRepository containerRepo = new ContainerRepository();
        UserActivityRepository activityRepo = new UserActivityRepository();

        public ActionResult Index()
        {
            var HasBroken = Task.Factory.StartNew<bool>(() => containerRepo.hasBrokenContainer(User.Identity.Name));
            ViewData["HasBroken"] = HasBroken.Result ;
            return View();
        }

        public ActionResult IndexAjax(string from = "", string to = "")
        {
            from = from.Replace("-", "/");
            to = to.Replace("-", "/");
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetAutoList(User.Identity.Name, from, to));
            return View(model.Result );
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxBinding()
        {
            //int totalPages = (containerRepo.GetAutoListCount(from, to) - 1) / pageSize;
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetAutoList(User.Identity.Name));
            //ViewData["TotalPages"] = totalPages;
            //ViewData["Page"] = page;
            return View("IndexAjax",new GridModel(model.Result ));
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxFilterBinding(AjaxFilterBindingModel date)
        {
            if (String.IsNullOrEmpty(date.from)) date.from = "";
            if (String.IsNullOrEmpty(date.to)) date.to = "";
            date.from = date.from.Replace("-", "/");
            date.to = date.to.Replace("-", "/");
            //int totalPages = (containerRepo.GetAutoListCount(from, to) - 1) / pageSize;
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetAutoList(User.Identity.Name, date.from, date.to));
            //ViewData["TotalPages"] = totalPages;
            //ViewData["Page"] = page;
            return View("IndexAjax", new GridModel(model.Result));
        }

        public ActionResult Putusan(int id)
        {
            var result = Task.Factory.StartNew<PutusanModel>(() => {
                CONTAINER container = containerRepo.Get(id);
                PutusanModel model = new PutusanModel();
                model.CONTAINER_ID = container.ID;
                model.CONTAINERNUMBER = container.CONTAINERNUMBER;
                if (container.SEAL == "R")
                {
                    model.Utuh = false;
                }
                else
                {
                    model.Utuh = true;
                }
                return model;
            });
            return View(result.Result );
        }

        public ActionResult Hold(int id)
        {
            var container = Task.Factory.StartNew<CONTAINER>(() => containerRepo.Get(id));
            var imageList = Task.Factory.StartNew<List<PICTURE>>(() => containerRepo.GetImageList(id));

            CONTAINER containerModel = container.Result ;

            ViewData["ImageList"] = imageList.Result ;
            ViewData["BaseUrl"] = WebConfigurationManager.AppSettings["ImageUrl"].ToString();
            var result = Task.Factory.StartNew<PutusanModel>(() =>
            {
                PutusanModel model = new PutusanModel();
                model.CONTAINER_ID = containerModel.ID;
                model.CONTAINERNUMBER = containerModel.CONTAINERNUMBER;
                if (containerModel.SEAL == "R")
                {
                    model.Utuh = false;
                }
                else
                {
                    model.Utuh = true;
                }
                return model;
            });
            ViewData["Container"] = containerModel;
            return View(result.Result );
        }

        [HttpPost]
        public ActionResult Putusan(PutusanModel model)
        {
            var result = Task.Factory.StartNew<bool>(() =>
            {
                if (ModelState.IsValid)
                {
                    // Attempt to hold
                    string status = containerRepo.Putusan(model, User.Identity.Name);
                    if (status == "Success")
                    {
                        USERACTIVITY activityuser = new USERACTIVITY();
                        activityuser.USERNAME = User.Identity.Name;
                        activityuser.HOLD = "-";
                        activityuser.ACTIVITYDESCR = "User : " + User.Identity.Name + ", Menambah Data Putusan dengan data container : " + model.CONTAINERNUMBER + ", Note : " + model.NOTE + ", Tanggal : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm");
                        activityuser.ADDATENSI = "-";
                        activityuser.ADDPUTUSAN = "P";
                        activityuser.CHECKCONTAINER = "-";
                        activityuser.CONFIRMP2 = "-";
                        activityuser.DELETETRX = "-";
                        activityuser.EDITATENSI = "-";
                        activityuser.RELEASE = "-";
                        activityRepo.saveActivity(activityuser);
                        return true;
                    }
                    else
                    {
                        ModelState.AddModelError("", status);
                    }
                }
                return false;
            });
            var returnvalue = result.Result ;
            // If we got this far, something failed, redisplay form
            if(returnvalue)
                return Json("Success",JsonRequestBehavior.AllowGet );
            else
                return View(model);
        }

        [HttpPost]
        public ActionResult Hold(PutusanModel model)
        {
            var container = Task.Factory.StartNew<CONTAINER>(() => containerRepo.Get(model.CONTAINER_ID));
            var imageList = Task.Factory.StartNew<List<PICTURE>>(() => containerRepo.GetImageList(model.CONTAINER_ID));
            CONTAINER containerModel = container.Result ;

            ViewData["Container"] = containerModel;
            ViewData["ImageList"] = imageList.Result ;
            ViewData["BaseUrl"] = WebConfigurationManager.AppSettings["ImageUrl"].ToString();
            model.CONTAINERNUMBER = containerModel.CONTAINERNUMBER;
            var result = Task.Factory.StartNew<bool>(() =>
            {
                if (ModelState.IsValid)
                {
                    string status = containerRepo.Putusan(model, User.Identity.Name);
                    if (status == "Success")
                        return true;
                    else
                        ModelState.AddModelError("", status);
                }
                return false;
            });
            var returnvalue = result.Result ;
            // If we got this far, something failed, redisplay form
            if(returnvalue)
                return Json("Success",JsonRequestBehavior.AllowGet );
            else
                return View(model);
        }
    }
}
