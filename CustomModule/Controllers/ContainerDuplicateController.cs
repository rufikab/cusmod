﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomModule.Models;
using Telerik.Web.Mvc;
using System.Data;
using System.IO;
using OfficeOpenXml;
using System.Threading.Tasks;
using CustomModule.Helper;

namespace CustomModule.Controllers
{
    public class AjaxFilterDuplicateBindingModel
    {
        public string from { get; set; }
        public string to { get; set; }
    }
    //[Authorize]
    [Authorize(Roles = "Administrator,Penyegelan,P2,ControlRoom,DeletionOperator")]
    public class ContainerDuplicateController : AsyncController
    {
        ContainerRepository containerRepo = new ContainerRepository();

        public ActionResult Index()
        {
            //var hasBroken = Task.Factory.StartNew<bool>(() => containerRepo.hasBrokenContainer(User.Identity.Name));
            //ViewData["HasBroken"] = hasBroken.Result;
            ViewData["HasBroken"] = false;

            String dtNow = DateTime.Now.ToString("MM/dd/yyyy");
            ViewData["dtFrom"] = dtNow;
            ViewData["dtEnd"] = dtNow;
            return View();
        }

        [HttpGet]
        public ActionResult IndexAjax(AjaxFilterDuplicateBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");

            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetDuplicateList(User.Identity.Name, filter.from, filter.to));

            return View(model.Result );
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxBinding()
        {
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetDuplicateList(User.Identity.Name));
            return View("IndexAjax",new GridModel(model.Result ));
        }

        [HttpPost]
        [GridAction]
        public ActionResult _AjaxFilterBinding(AjaxFilterDuplicateBindingModel filter)
        {
            if (String.IsNullOrEmpty(filter.from)) filter.from = "";
            if (String.IsNullOrEmpty(filter.to)) filter.to = "";
            filter.from = filter.from.Replace("-", "/");
            filter.to = filter.to.Replace("-", "/");
            var model = Task.Factory.StartNew<List<ContainerModel>>(() => containerRepo.GetDuplicateList(User.Identity.Name, filter.from, filter.to));
            return View("IndexAjax", new GridModel(model.Result));
        }

        [HttpGet]
        public ActionResult Delete(string conID, string deleteNote)
        {
            if(String.IsNullOrEmpty(conID) || String.IsNullOrEmpty(deleteNote))
                return Json("Parameter not correct!", JsonRequestBehavior.AllowGet);
            else
            {
                //var delete = containerRepo.GetDuplicateDelete(conID);
                ////var delete = "true";
                //if (delete == "true")
                //{
                //    return Json(containerRepo.GetUpdateDelete(User.Identity.Name, conID, deleteNote), JsonRequestBehavior.AllowGet);
                //}
                //else
                //    return Json(false, JsonRequestBehavior.AllowGet);

                var ret = Task.Factory.StartNew<string>(() => containerRepo.GetDuplicateDeleteNew(User.Identity.Name, conID, deleteNote));
                return Json(ret.Result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}