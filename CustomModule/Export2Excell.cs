﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using CustomModule.Models;

namespace CustomModule
{
    public class Export2Excell
    {
        /// <summary>
        /// Generates the report.
        /// </summary>
        public static Byte[] GenerateReport(List<ContainerModel> model)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                //set the workbook properties and add a default sheet in it
                SetWorkbookProperties(p);
                //Create a sheet
                ExcelWorksheet ws = CreateSheet(p,"Export arsip");
                DataTable dt = CreateDataTable(model); //My Function which generates DataTable

                //Merging cells and create a center heading for out table
                ws.Cells[1, 1].Value = "Export arsip";
                ws.Cells[1, 1, 1, dt.Columns.Count].Merge = true;
                ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                ws.Cells[1, 1, 1, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                int rowIndex = 2;

                CreateHeader(ws, ref rowIndex, dt);
                CreateData(ws, ref rowIndex, dt);

                //Generate A File with Random name
                Byte[] bin = p.GetAsByteArray();
                string file = Guid.NewGuid().ToString() + ".xlsx";
                
                return bin;
            }
        }

        private static ExcelWorksheet CreateSheet(ExcelPackage p, string sheetName)
        {
            p.Workbook.Worksheets.Add(sheetName);
            ExcelWorksheet ws = p.Workbook.Worksheets[1];
            ws.Name = sheetName; //Setting Sheet's name
            ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
            ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet

            return ws;
        }

        /// <summary>
        /// Sets the workbook properties and adds a default sheet.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns></returns>
        private static void SetWorkbookProperties(ExcelPackage p)
        {
            //Here setting some document properties
            p.Workbook.Properties.Author = "Halotec-Indonesia";
            p.Workbook.Properties.Title = "Export arsip";
        }

        public static void CreateHeader(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {
            int colIndex = 1;
            foreach (DataColumn dc in dt.Columns) //Creating Headings
            {
                var cell = ws.Cells[rowIndex, colIndex];

                //Setting the background color of header cells to Orange
                var fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(Color.Orange);

                //Setting Top/left,right/bottom borders.
                var border = cell.Style.Border;
                border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                //Setting Value in cell
                cell.Value = dc.ColumnName;

                colIndex++;
            }
        }

        public static void CreateData(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {
            int colIndex=0;
            foreach (DataRow dr in dt.Rows) // Adding Data into rows
            {
                colIndex = 1;
                rowIndex++;

                foreach (DataColumn dc in dt.Columns)
                {
                    var cell = ws.Cells[rowIndex, colIndex];

                    //Setting Value in cell
                    cell.Value = dr[dc.ColumnName];

                    //Setting borders of cell
                    var border = cell.Style.Border;
                    border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                    colIndex++;
                }
            }
        }

        /// <summary>
        /// Creates the data table 
        /// </summary>
        /// <returns>DataTable</returns>
        private static DataTable CreateDataTable(List<ContainerModel> model)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("No. Tag");
            dt.Columns.Add("No. Polisi");
            dt.Columns.Add("No. Gate");
            dt.Columns.Add("Brutto weights");
            dt.Columns.Add("Waktu Masuk");
            dt.Columns.Add("Waktu Keluar");
            dt.Columns.Add("Terminal ID");
            dt.Columns.Add("No. Container / Size");
            dt.Columns.Add("Full/Empty");
            dt.Columns.Add("Dok. Pabean");
            dt.Columns.Add("Dok. Type");
            dt.Columns.Add("Jenis Kegiatan");
            dt.Columns.Add("Keterangan Hold");
            dt.Columns.Add("Held By");
            dt.Columns.Add("Keterangan Release");
            dt.Columns.Add("Release By");

            foreach (ContainerModel container in model)
            {
                DataRow dr = dt.NewRow();
                dr["No. Tag"] = container.TAGNUMBER;
                dr["No. Polisi"] = container.TRUCKNUMBER;
                dr["No. Gate"] = container.GATENUMBER;
                dr["Brutto weights"] = container.BRUTTOWEIGHTS;
                dr["Waktu Masuk"] = container.GATEINTIME;
                dr["Waktu Keluar"] = container.GATEOUTTIME;
                dr["Terminal ID"] = container.TerminalId;
                dr["No. Container / Size"] = container.ContainerAndSize;
                dr["Full/Empty"] = container.FULLEMPTY;
                dr["Dok. Pabean"] = container.DOCUMENTNUMBER;
                dr["Dok. Type"] = container.DOCUMENTTYPE;
                dr["Jenis Kegiatan"] = container.EXPORTIMPORT;
                dr["Keterangan Hold"] = container.HOLDNOTE;
                dr["Held By"] = container.HoldBy;
                dr["Keterangan Release"] = container.RELEASENOTE;
                dr["Release By"] = container.ReleaseBy;

                dt.Rows.Add(dr);
            }

            return dt;
        }
    }
}