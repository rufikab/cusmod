﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.Security;

namespace CustomModule
{
    public class MenuHelper
    {
        public static string setActive(string menu)
        {
            if (HttpContext.Current.Request.Path == "/" && menu == "") return "active";
            if (HttpContext.Current.Request.Path != "" && menu == "") return ""; 
            if (HttpContext.Current.Request.Path.ToLower().Contains(menu.ToLower()))
            {
                if (HttpContext.Current.Request.Path.ToLower().Contains("container") && menu.ToLower() == "delete")
                {
                    return "";
                }
                return  "active";
            }
            else
            {
                return "";
            }
        }
        public static bool isAuthorized(string menu)
        {
            string[] roles = Roles.GetRolesForUser( System.Web.HttpContext.Current.User.Identity.Name);
            bool ret = false;
            if (menu == "")
            {
                if(roles.Count() == 1 && (roles.Contains("KOJAplanner") || roles.Contains("KOJAcustomerCare")))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            if (roles.Contains("ControlRoom")) ret|= "dashboard"==menu.ToLower();
            if (roles.Contains("P2")) ret |= "atensip2" == menu.ToLower();
            if (roles.Contains("Administrator")) ret |= "account" == menu.ToLower();
            if (roles.Contains("Administrator")) ret |= "arsip" == menu.ToLower();
            if (roles.Contains("Administrator")) ret |= "delete" == menu.ToLower();
            if (roles.Contains("Administrator")) ret |= "atensip2" == menu.ToLower();
            if (roles.Contains("Administrator")) ret |= "penyegelan" == menu.ToLower();
            if (roles.Contains("Administrator")) ret |= "confirmholdp2" == menu.ToLower();
            if (roles.Contains("Administrator")) ret |= "useractivitytask" == menu.ToLower();
            if (roles.Contains("Administrator")) ret |= "useractivityreport" == menu.ToLower();

            if (roles.Contains("ControlRoom")) ret |= "arsip" == menu.ToLower();
            if (roles.Contains("ControlRoom")) ret |= "penyegelan" == menu.ToLower();
            if (roles.Contains("Penyegelan")) ret |= "arsip" == menu.ToLower();
            if (roles.Contains("P2")) ret |= "arsip" == menu.ToLower();
            if (roles.Contains("P2")) ret |= "confirmholdp2" == menu.ToLower();
            if (roles.Contains("P2")) ret |= "confirmholdp2" == menu.ToLower();
            if (roles.Contains("Planner")) ret |= "confirmholdp2" == menu.ToLower();
            if (roles.Contains("DeletionOperator")) ret |= "arsip" == menu.ToLower();
            if (roles.Contains("DeletionOperator")) ret |= "delete" == menu.ToLower();

            if (roles.Contains("ReportUserTask")) ret |= "useractivitytask" == menu.ToLower();
            if (roles.Contains("ReportUserDetail")) ret |= "useractivityreport" == menu.ToLower();

            return ret;
        }
        public static string SideMenu()
        {
            var resultString = "";
            string virtualDirectory = HttpContext.Current.Request.ApplicationPath;
            if(virtualDirectory.Length>1)
                virtualDirectory+="\\";
            if (isAuthorized(""))
            {
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory, "Beranda", "dashboard", setActive(""));
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\" target=\"_blank\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory + "CCTV", "CCTV", "facetime-video", setActive("cctv"));
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory + "Report ", "Report", "book", setActive("report"));
            }
            if (isAuthorized("atensip2"))
            {
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory+"AtensiP2", "Atensi P2", "exclamation-sign", setActive("atensip2"));
            }
            if (isAuthorized("confirmholdp2"))
            {
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory + "ConfirmHoldP2 ", "Confirm Hold P2 ", "thumbs-up", setActive("confirmholdp2"));
            }
            if (isAuthorized("penyegelan"))
            {
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory + "Penyegelan", "Pemeriksaan segel", "check", setActive("penyegelan"));
            }
            if (isAuthorized("account"))
            {
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory + "Account", "Administrasi User", "user", setActive("account"));
            }
            /*
            if (isAuthorized(""))
            {
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", "/", "Laporan", "file", setActive("laporan"));
            }
             */
            if (isAuthorized("arsip"))
            {
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory + "ContainerDuplicate", "Container Duplicate", "copy", setActive("containerduplicate"));
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory + "ContainerDeleted", "Container Deleted", "remove-circle", setActive("containerdelete"));

                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory + "Arsip", "Arsip", "folder-open", setActive("arsip"));
            }
            if (isAuthorized("delete"))
            {
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory + "Delete", "Delete Transaction", "trash", setActive("delete"));
            }

            if (isAuthorized("useractivitytask"))
            {
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory + "UserActivityTask", "Rangkuman Aktivitas User", "list-alt", setActive("useractivitytask"));
            }

            if (isAuthorized("useractivityreport"))
            {
                resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory + "UserActivityReport", "Detail Aktivitas User", "tasks", setActive("useractivityreport"));
            }
            resultString += string.Format("<li class=\"{3}\"><a href=\"{0}\"><span>{1}</span><i class=\"icon-{2}\"></i></a></li>", virtualDirectory + "Summary", "Rangkuman", "book", setActive("summary"));
            return resultString;
        }
    }
}