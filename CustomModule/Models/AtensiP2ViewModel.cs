﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CustomModule.Models
{
    public class AtensiP2ViewModel
    {
        public AtensiP2ItemViewModel addItemViewModel { get; set; }
        public AtensiP2EditItemViewModel editItemViewModel { get; set; }
        public List<AtensiP2ListItemViewModel> listViewModel { get; set; }
    }

    public class AtensiP2ItemViewModel
    {
        [Required]
        public int ID { get; set; }
        
        public String DocumentNumber { get; set; }
        public String DocumentType { get; set; }
        [Required]
        public string DocumentDate { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        [Required]
        public String ContainerID { get; set; }
        public String ExportImport { get; set; }
        [Required]
        public string DateInput { get; set; }
        [Required]
        public String InputBy { get; set; }
        public String Reason { get; set; }
        [Required]
        public int IsActive { get; set; }
        [Required]
        public int IsConfirmed { get; set; }

        public HttpPostedFileBase docFile { get; set; }
    }

    public class AtensiP2EditItemViewModel
    {
        [Required]
        public int EditID { get; set; }
        public String EditDocumentNumber { get; set; }
        public String EditDocumentType { get; set; }
        [Required]
        public string EditDocumentDate { get; set; }

        public string EditDateFrom { get; set; }
        public string EditDateTo { get; set; }
        
        [Required]
        public String EditContainerID { get; set; }
        public String EditExportImport { get; set; }
        [Required]
        public string EditDateInput { get; set; }
        [Required]
        public String EditInputBy { get; set; }

        public String EditReason { get; set; }
        [Required]
        public int EditIsActive { get; set; }
        [Required]
        public int EditIsConfirmed { get; set; }
        [Required]
        public String EditUserID { get; set; }
        public HttpPostedFileBase EditdocFile { get; set; }
    }

    public class AtensiP2ListItemViewModel
    {
        public int ID { get; set; }
        public String DocumentNumber { get; set; }
        public String DocumentType { get; set; }
        public String DocumentDate { get; set; }
        public String ContainerID { get; set; }
        public String DateValid { get; set; }
        public DateTime DateInput { get; set; }
        public String InputBy { get; set; }
        public String Reason { get; set; }
        public String IsHold { get; set; }
        public String IsActive { get; set; }
        public String IsConfirmed { get; set; }
        public String TerminalStatus { get; set; }
        public String UserID { get; set; }
        public String Action { get; set; }
        public String Action2 { get; set; }
        public String ExportImport { get; set; }
        public String Release { get; set; }
        public String Hold { get; set; }
    }
}