﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
namespace CustomModule.Models
{
    public class UserActivityRepository:BaseRepository 
    {
        public void saveActivity(USERACTIVITY activities) {
            long id = 0;
            if (_entities.USERACTIVITY.Count() > 0)
                id = _entities.USERACTIVITY.Max(a => a.ID);
            activities.ID = id + 1;
            activities.ACTIVITYDATE = DateTime.Now;

            _entities.USERACTIVITY.AddObject(activities);
            _entities.SaveChanges();
        }

        public List<UserActivityModel> CountUser(string dateFrom = "", string dateTo = "") {
            List<UserActivityModel> ListUser = new List<UserActivityModel>();

            List<ReportModel> reportListHold = new List<ReportModel>();

            var listactivity = (from a in _entities.USERACTIVITY
                                select a
                            ).ToList();

            MembershipUserCollection users = Membership.GetAllUsers();
            int counterhold = 0;
            int countercheck = 0;
            int counterrelease = 0;
            int counteraddatensi = 0;
            int countereditatensi = 0;
            int counterconfirm = 0;
            int counteraddputusan = 0;
            int counterdelete = 0;

            if (!String.IsNullOrEmpty(dateFrom) && String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.Parse(dateFrom);
                listactivity = listactivity.Where(c => c.ACTIVITYDATE >= dFrom).ToList();
            }
            else if (String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dTo = DateTime.Parse(dateTo).AddDays(1);
                listactivity = listactivity.Where(c => c.ACTIVITYDATE < dTo).ToList();
            }
            else if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.Parse(dateFrom);
                DateTime dTo = DateTime.Parse(dateTo).AddDays(1);
                listactivity = listactivity.Where(c => c.ACTIVITYDATE >= dFrom && c.ACTIVITYDATE < dTo).ToList();
            }

            foreach (MembershipUser user in users)
            {
                var _user = listactivity.Where(a => a.USERNAME.ToLower() == user.UserName.ToLower()).FirstOrDefault();
                if (_user != null)
                {
                    counterhold = listactivity.Where(a => a.USERNAME.ToLower() == _user.USERNAME.ToLower() && a.HOLD == "H").Count();
                    countercheck = listactivity.Where(a => a.USERNAME.ToLower() == _user.USERNAME.ToLower() && a.CHECKCONTAINER == "C").Count();
                    counterrelease = listactivity.Where(a => a.USERNAME.ToLower() == _user.USERNAME.ToLower() && a.RELEASE == "R").Count();
                    counteraddatensi = listactivity.Where(a => a.USERNAME.ToLower() == _user.USERNAME.ToLower() && a.ADDATENSI == "A").Count();
                    countereditatensi = listactivity.Where(a => a.USERNAME.ToLower() == _user.USERNAME.ToLower() && a.EDITATENSI == "E").Count();
                    counterconfirm = listactivity.Where(a => a.USERNAME.ToLower() == _user.USERNAME.ToLower() && a.CONFIRMP2 == "C").Count();
                    counteraddputusan = listactivity.Where(a => a.USERNAME.ToLower() == _user.USERNAME.ToLower() && a.ADDPUTUSAN == "P").Count();
                    counterdelete = listactivity.Where(a => a.USERNAME.ToLower() == _user.USERNAME.ToLower() && a.DELETETRX == "D").Count();

                    UserActivityModel newuser = new UserActivityModel();
                    newuser.USERNAME = _user.USERNAME.ToLower();
                    newuser.HOLD = counterhold;
                    newuser.CHECKCONTAINER = countercheck;
                    newuser.RELEASE = counterrelease;
                    newuser.ADDATENSI = counteraddatensi;
                    newuser.EDITATENSI = countereditatensi;
                    newuser.CONFIRMP2 = counterconfirm;
                    newuser.ADDPUTUSAN = counteraddputusan;
                    newuser.DELETETRX = counterdelete;

                    ListUser.Add(newuser);
                }
            }
            return ListUser;
        }

        public List<UserActivityModel> ReportActivity(string dateFrom = "", string dateTo = "")
        {
            List<UserActivityModel> ListUser = new List<UserActivityModel>();

            List<ReportModel> reportListHold = new List<ReportModel>();

            var listactivity = (from a in _entities.USERACTIVITY
                                select a
                            ).ToList();

           
            if (!String.IsNullOrEmpty(dateFrom) && String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.Parse(dateFrom);
                listactivity = listactivity.Where(c => c.ACTIVITYDATE >= dFrom).ToList();
            }
            else if (String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dTo = DateTime.Parse(dateTo).AddDays(1);
                listactivity = listactivity.Where(c => c.ACTIVITYDATE < dTo).ToList();
            }
            else if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.Parse(dateFrom);
                DateTime dTo = DateTime.Parse(dateTo).AddDays(1);
                listactivity = listactivity.Where(c => c.ACTIVITYDATE >= dFrom && c.ACTIVITYDATE < dTo).ToList();
            }
            else
            {
                DateTime dFrom = DateTime.Now.AddHours(-8);
                listactivity = listactivity.Where(c => c.ACTIVITYDATE >= dFrom).ToList();
            }

            listactivity = listactivity.OrderBy(c => c.USERNAME).ToList();
            //listactivity = listactivity.OrderBy(Func<USERACTIVITY, "USERNAME">);
            string name = "";
            foreach (USERACTIVITY _user in listactivity)
            {
                UserActivityModel newuser = new UserActivityModel();
                if (name != _user.USERNAME.ToLower())
                {
                    newuser.USERNAME = _user.USERNAME.ToLower();
                }
                else {
                    newuser.USERNAME = "";
                }
                newuser.HOLD = 0;
                newuser.CHECKCONTAINER = 0;
                newuser.RELEASE = 0;
                newuser.ADDATENSI = 0;
                newuser.EDITATENSI = 0;
                newuser.CONFIRMP2 = 0;
                newuser.ADDPUTUSAN = 0;
                newuser.DELETETRX = 0;
                newuser.ACTIVITYDESCR = _user.ACTIVITYDESCR;
                newuser.ACTIVITYDATE = _user.ACTIVITYDATE.Value;
                ListUser.Add(newuser);

                name = _user.USERNAME.ToLower();
            }
            return ListUser;
        }
    }

     
}