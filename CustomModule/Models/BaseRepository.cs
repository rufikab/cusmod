﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomModule.Models
{
    public class BaseRepository
    {
        protected Entities _entities = new Entities();
        protected string errorMessage = string.Empty;

        #region "Constructor"
        public BaseRepository()
        { }
        #endregion

        /// <summary>
        /// Get the error message from the last operation
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
        }
    }
}