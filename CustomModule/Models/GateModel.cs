﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomModule.Models
{
    public class GateModel
    {
        public string NAME { get; set; }
        public IQueryable<AG_CAMERA> camera { get; set; }
    }
}
