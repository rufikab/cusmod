﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomModule.Models
{
    public class ConfirmHoldP2ViewModel
    {
        public List<ConfirmListHoldP2ViewModel> listItemViewModel { get; set; }
    }

    public class ConfirmListHoldP2ViewModel
    {
        public int ID { get; set; }
        public String IsConfirmed { get; set; }
        public String P2File { get; set; }
        public String DocumentNumber { get; set; }
        public String DocumentDate { get; set; }
        public String DocumentType { get; set; }
        public String ContainerID { get; set; }
        public String Action { get; set; }
        public String TerminalResponse { get; set; }
    }
}