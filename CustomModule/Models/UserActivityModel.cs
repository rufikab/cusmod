﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomModule.Models
{
    public class UserActivityModel
    {
        public int ID { get; set; }
        public string USERNAME { get; set; }
        public DateTime ACTIVITYDATE { get; set; }
        public string ACTIVITYDESCR { get; set; }
        public int HOLD { get; set; }
        public int RELEASE { get; set; }
        public int CHECKCONTAINER { get; set; }
        public int ADDATENSI { get; set; }
        public int EDITATENSI { get; set; }
        public int CONFIRMP2 { get; set; }
        public int ADDPUTUSAN { get; set; }
        public int DELETETRX { get; set; }

        
    }

    public class ReportModel {
        public string Username { get; set; }
        public int Count { get; set; }
    }
}