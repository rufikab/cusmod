﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.Web.Security;
using CustomModule.Helper;
using System.IO;
using System.Globalization;
using System.Configuration;
using System.Net;

namespace CustomModule.Models
{
    public class ContainerRepository:BaseRepository
    {
        private int hoursDiff = -8;
        public class DocumentType
        {
            public string EI { get; set; }
            public string Type { get; set; }
        }

        public UserActivityRepository activityrepo = new UserActivityRepository();
        protected AutogateEntities _autogateEntities = new AutogateEntities();
        private FileLogger _logger = new FileLogger();

        private string updateTruckPosition(CONTAINER model)
        {

            if (model.TRUCKPOSITION != null)
            {
                if (model.TRUCKPOSITION.Trim() != "Approaching Yard" && model.TRUCKPOSITION.Trim() != "")
                {
                    return model.TRUCKPOSITION;
                }
                else if (model.TRUCKPOSITION.Trim() == "Approaching Yard")
                {
                    if (model.EXPORTIMPORT.Trim() == "I")
                    {
                        return "Truck OTW Terminal";
                    }
                    else
                    {
                        return "OTW Terminal";
                    }
                }
                else return "";
            }
            else
            {
                if (model.EXPORTIMPORT.Trim() == "I")
                {
                    return "Truck OTW Terminal";
                }
                else
                {
                    return "OTW Terminal";
                }
            };
        }


        public string saveTruckPosition(CONTAINER model)
        {
            if (model.TRANSACTION_ID.Contains("/BHD")) return "EX BEHANDLE";
            if (model.TRUCKPOSITION != null)
            {
                if (model.TRUCKPOSITION.Trim() != "Approaching Yard" && model.TRUCKPOSITION.Trim() != "")
                {
                    return model.TRUCKPOSITION;
                }
            }

            CON_LISTCONT containerInAutogate = (from c in _autogateEntities.CON_LISTCONT
                                                where c.TAR == model.TRANSACTION_ID.Trim() && c.NO_CONTAINER == model.CONTAINERNUMBER.Trim()
                                                orderby c.GT_DATE descending
                                                select c).FirstOrDefault();

            string position = "";
            if (containerInAutogate != null)
            {
                if (containerInAutogate.GT_DATE_OUT != null)
                {
                    if (containerInAutogate.ID_CLASS_CODE == "I")
                    {
                        position = "Gate OUT CG";
                    }
                    else if (containerInAutogate.ID_CLASS_CODE == "E")
                    {
                        position = "Truck Gate OUT CG";
                    }
                }
                else if (containerInAutogate.TRUCKOUT_TERMINAL != null)
                {
                    if (containerInAutogate.ID_CLASS_CODE == "I")
                    {
                        position = "OUT Terminal";
                    }
                    else if (containerInAutogate.ID_CLASS_CODE == "E")
                    {
                        position = "Truck OUT Terminal";
                    }
                }
                else if (containerInAutogate.TRUCKIN_TERMINAL != null)
                {
                    if (containerInAutogate.ID_CLASS_CODE == "I")
                    {
                        position = "Truck IN Terminal";
                    }
                    else if (containerInAutogate.ID_CLASS_CODE == "E")
                    {
                        position = "IN Terminal";
                    }
                }
                else if (model.TRUCKPOSITION.Trim() == "Approaching Yard")
                {
                    if (containerInAutogate.ID_CLASS_CODE == "I")
                    {
                        position = "Truck OTW Terminal";
                    }
                    else if (containerInAutogate.ID_CLASS_CODE == "E")
                    {
                        position = "OTW Terminal";
                    }
                }
            }
            if (!String.IsNullOrEmpty(position))
            {
                model.TRUCKPOSITION = position;
                _entities.SaveChanges();
            }
            return model.TRUCKPOSITION;
        }

        public string updatePosition(int hoursDiff=-8)
        {
            DateTime treshold = DateTime.Now.AddHours(hoursDiff);
            var list = (from c in _entities.CONTAINER
                        where c.GATEINTIME >= treshold && c.TRUCKPOSITION.Trim() != "Gate OUT CG" && c.TRUCKPOSITION.Trim() != "Truck Gate OUT CG" 
                        select c).ToList().OrderBy(x => Guid.NewGuid()).Take(10);
            string ret = "";
            foreach (CONTAINER c in list)
            {
                ret+=c.CONTAINERNUMBER+":"+saveTruckPosition(c)+"#";
            }
            return ret;
        }

        public string checkNull(string data)
        {
            return (data == null) ? "" : data;
        }

        public List<ContainerModel> GetList(string id, string username)
        {

            if (id.ToLower() == "e")
            {
                var a = GetListExport(username);
                return a;
            }
            else if (id.ToLower() == "i")
            {
                return GetListImport(username);
            }
            else
            {
                return GetListOOG(username);
            }
        }

        public List<ContainerModel> GetListOOG(string username)
        {
            string r = "R";
            string a = "A";
            string id = "I";
            string p2 = "P";
            string p2a = "AP";
            string[] roles = Roles.GetRolesForUser(username);

            bool canHoldManual = roles.Contains("ControlRoom");
            bool canHoldAutoE = canHoldManual;
            bool canHoldP2 = roles.Contains("P2");
            bool canHoldAutoI = roles.Contains("Penyegelan");
            if (roles.Contains("Administrator"))
            {
                canHoldManual = true;
                canHoldAutoE = true;
                canHoldAutoI = true;
            }
            //canHoldP2 = true
            //canHoldManual = true;
            DateTime treshold = DateTime.Now.AddHours(hoursDiff);

            var list = (from c in _entities.CONTAINER
                        where (c.EXPORTIMPORT.Trim() == id && c.HOLDTYPE != "D" && c.GATEINTIME >= treshold && c.GATENUMBER.Trim()=="GATE08I")
                        select c);

            if (roles.Count() == 1 && roles.Contains("Penyegelan"))
            {
                list = list.Where(c => c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT.Trim() == "I" && c.HOLDSTATUS.Trim() != "R");
            }

            List<ContainerModel> ListModel = new List<ContainerModel>();


            return list.ToList().Select(c => new ContainerModel
            {
                ID = c.ID,
                CONTAINERNUMBER = (c.CONTAINERNUMBER == null) ? "" : c.CONTAINERNUMBER,
                CONTAINERSIZE = (c.CONTAINERSIZE == null) ? "" : c.CONTAINERSIZE,
                TAGNUMBER = c.TAGNUMBER,
                TRUCKNUMBER = c.TRUCKNUMBER,
                TRUCKPOSITION = updateTruckPosition(c),
                GATENUMBER = (c.GATENUMBER == null) ? "" : c.GATENUMBER,
                GATEINTIME = (c.GATEINTIME == null) ? DateTime.Now : (DateTime)c.GATEINTIME,
                SGATEOUTTIME = ((c.GATEOUTTIME != null) ? c.GATEOUTTIME.ToString() : ""),
                GATEOUTTIMES = (c.GATEOUTTIME == null) ? (DateTime)c.GATEINTIME : (DateTime)c.GATEOUTTIME,
                DOCUMENTNUMBER = (c.DOCUMENTNUMBER == null) ? "" : c.DOCUMENTNUMBER,
                HOLDTYPE = (c.HOLDTYPE == null) ? "" : HoldType.type(c.HOLDTYPE),
                HOLDSTATUS = (c.HOLDSTATUS.Trim() == "H") ? HoldType.type(c.HOLDTYPE) : "",
                Note = (c.HOLDSTATUS.Trim() == "H") ? c.HOLDNOTE : c.RELEASENOTE,
                SEAL = (c.SEAL == null) ? "" : Seal.kondisi(c.SEAL),
                SEALNOTE = (c.SEALNOTE == null) ? "" : c.SEALNOTE,
                KODEPENYEGELAN = (c.KODEPENYEGELAN == null) ? "" : c.KODEPENYEGELAN,
                CHECKEDBY = c.CHECKEDBY,
                CHECKEDTIME = c.CHECKEDTIME.GetValueOrDefault(),
                StrCheckedTime = (c.CHECKEDTIME.HasValue) ? c.CHECKEDTIME.ToString() : "",
                ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? c.CONTAINERNUMBER + "/" + "00" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
                HoldBy = (c.HOLDBY == null) ? "" : c.HOLDBY,
                ReleaseBy = (c.RELEASEBY == null) ? "" : c.RELEASEBY,
                GATEOUTTIME = (c.GATEOUTTIME == null) ? null : c.GATEOUTTIME,
                Action = (canHoldP2 && (c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2a) || (canHoldAutoE && c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT == "E" || canHoldAutoI && c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT == "I") || canHoldManual && c.HOLDTYPE.Trim() == "M") ? HoldReleaseAction.getAction(c.ID, c.EXPORTIMPORT, c.HOLDSTATUS, c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2a) : HoldReleaseAction.getDetailOnly(c.ID, c.EXPORTIMPORT),
                TerminalId = c.TERMINALID
            }).ToList();
        }

        public List<ContainerModel> GetListImport(string username)
        {
            string r = "R";
            string a = "A";
            string id = "I";
            string p2 = "P";
            string p2a = "AP";
            string[] roles = Roles.GetRolesForUser(username);

            bool canHoldManual = roles.Contains("ControlRoom");
            bool canHoldAutoE = canHoldManual;
            bool canHoldP2 = roles.Contains("P2");
            bool canHoldAutoI = roles.Contains("Penyegelan");
            if (roles.Contains("Administrator"))
            {
                canHoldManual = true;
                canHoldAutoE = true;
                canHoldAutoI = true;
            }
            //canHoldP2 = true
            //canHoldManual = true;
            DateTime treshold = DateTime.Now.AddHours(hoursDiff);



            //List<string> containerInAutogate = (from c in _autogateEntities.CON_LISTCONT
            //                                     orderby c.GT_DATE descending
            //                                    select c.TAR).ToList();

            var list = (from c in _entities.CONTAINER
                        where (c.EXPORTIMPORT.Trim() == id && c.HOLDTYPE != "D" && c.GATEINTIME >= treshold)
                        select c);

            //var list = (from c in _entities.CONTAINER
            //            where (c.EXPORTIMPORT.Trim() == id && !c.GATEOUTTIME.HasValue && c.GATEINTIME >= treshold) ||
            //            (c.EXPORTIMPORT.Trim() == id && c.HOLDSTATUS == "H")
            //            select c); //real one

            //var list = (from c in _entities.CONTAINER
            //            where (c.EXPORTIMPORT.Trim() == id && !c.GATEOUTTIME.HasValue && c.GATEINTIME >= treshold) ||
            //            (c.EXPORTIMPORT.Trim() == id && c.HOLDTYPE != "D")
            //            select c); //test
            if (roles.Count() == 1 && roles.Contains("Penyegelan"))
            {
                list = list.Where(c => c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT.Trim() == "I" && c.HOLDSTATUS.Trim() != "R");
            }

            /*
            if (roles.Contains("ControlRoom") && !roles.Contains("Penyegelan"))
            {
                list = (from c in _entities.CONTAINER
                        where (c.EXPORTIMPORT.Trim() == id && c.GATEOUTTIME == null && (c.GATEINTIME >= now || c.HOLDSTATUS.Trim() == "H") && c.HOLDTYPE.Trim() != "A") 
                        || (c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT.Trim() == "E" && c.HOLDSTATUS.Trim() != "R")
                            select c);
            }
            */
            List<ContainerModel> ListModel = new List<ContainerModel>();

              
            return list.ToList().Select(c => new ContainerModel
            {
                ID = c.ID,
                CONTAINERNUMBER = (c.CONTAINERNUMBER == null  ) ?"" : c.CONTAINERNUMBER,
                CONTAINERSIZE = (c.CONTAINERSIZE == null) ? "" : c.CONTAINERSIZE,
                TAGNUMBER = c.TAGNUMBER,
                TRUCKNUMBER = c.TRUCKNUMBER,
                TRUCKPOSITION = updateTruckPosition(c),
                GATENUMBER = (c.GATENUMBER == null) ? "" : c.GATENUMBER,
                GATEINTIME = (c.GATEINTIME == null) ? DateTime.Now : (DateTime)c.GATEINTIME,
                SGATEOUTTIME = ((c.GATEOUTTIME != null) ? c.GATEOUTTIME.ToString() : ""),
                GATEOUTTIMES = (c.GATEOUTTIME == null) ? (DateTime)c.GATEINTIME : (DateTime)c.GATEOUTTIME,
                DOCUMENTNUMBER = (c.DOCUMENTNUMBER == null) ? "" : c.DOCUMENTNUMBER,
                HOLDTYPE = (c.HOLDTYPE == null) ? "" : HoldType.type(c.HOLDTYPE),
                HOLDSTATUS = (c.HOLDSTATUS.Trim() == "H") ? HoldType.type(c.HOLDTYPE) : "",
                Note = (c.HOLDSTATUS.Trim() == "H") ? c.HOLDNOTE : c.RELEASENOTE,
                SEAL = (c.SEAL == null) ? "" : Seal.kondisi(c.SEAL),
                SEALNOTE = (c.SEALNOTE == null) ? "" : c.SEALNOTE,
                KODEPENYEGELAN = (c.KODEPENYEGELAN == null) ? "" : c.KODEPENYEGELAN,
                CHECKEDBY = c.CHECKEDBY,
                CHECKEDTIME = c.CHECKEDTIME.GetValueOrDefault(),
                StrCheckedTime = (c.CHECKEDTIME.HasValue) ? c.CHECKEDTIME.ToString() : "",
                ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? c.CONTAINERNUMBER + "/" + "00" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
                HoldBy = (c.HOLDBY == null) ? "" : c.HOLDBY,
                ReleaseBy = (c.RELEASEBY == null) ? "" : c.RELEASEBY,
                GATEOUTTIME = (c.GATEOUTTIME == null) ? null : c.GATEOUTTIME,
                Action = (canHoldP2 && (c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2a) || (canHoldAutoE && c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT == "E" || canHoldAutoI && c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT == "I") || canHoldManual && c.HOLDTYPE.Trim() == "M") ? HoldReleaseAction.getAction(c.ID, c.EXPORTIMPORT, c.HOLDSTATUS, c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2a) : HoldReleaseAction.getDetailOnly(c.ID, c.EXPORTIMPORT),
                TerminalId = c.TERMINALID
            }).ToList();
        }

        public List<ContainerModel> GetListExport(string username)
        {
            string r = "R";
            string a = "A";

            string id = "E";
            string p2 = "P";
            string p2a = "AP";
            string[] roles = Roles.GetRolesForUser(username);
            bool canHoldManual = roles.Contains("ControlRoom");
            bool canHoldAutoE = canHoldManual;
            bool canHoldP2 = roles.Contains("P2");
            bool canHoldAutoI = roles.Contains("Penyegelan");
            if (roles.Contains("Administrator"))
            {
                canHoldManual = true;
                canHoldAutoE = true;
                canHoldAutoI = true;
            }
            //canHoldP2 = true;
            //canHoldManual = true;
            DateTime treshold = DateTime.Now.AddHours(hoursDiff);

            var list = (from c in _entities.CONTAINER
                        where (c.EXPORTIMPORT.Trim() == id && c.HOLDTYPE != "D" && c.GATEINTIME >= treshold)
                        select c);
            //var list = (from c in _entities.CONTAINER
            //            where (c.EXPORTIMPORT.Trim() == id && !c.GATEOUTTIME.HasValue && c.GATEINTIME >= treshold) ||
            //            (c.EXPORTIMPORT.Trim() == id && c.HOLDSTATUS == "H" && c.HOLDTYPE != "A")
            //            select c); //real one

            //var list = (from c in _entities.CONTAINER
            //            where (c.EXPORTIMPORT.Trim() == id && !c.GATEOUTTIME.HasValue && c.GATEINTIME >= treshold) ||
            //            (c.EXPORTIMPORT.Trim() == id && c.HOLDTYPE != "D")// && c.HOLDTYPE != "A")
            //            select c); //test
            if (roles.Count() == 1 && roles.Contains("Penyegelan"))
            {
                list = list.Where(c => c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT.Trim() == "E" && c.HOLDSTATUS.Trim() != "R");
            }

            /*
            if (roles.Contains("ControlRoom") && !roles.Contains("Penyegelan"))
            {
                list = (from c in _entities.CONTAINER
                        where (c.EXPORTIMPORT.Trim() == id && c.GATEOUTTIME == null && (c.GATEINTIME >= now || c.HOLDSTATUS.Trim() == "H") && c.HOLDTYPE.Trim() != "A") 
                        || (c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT.Trim() == "E" && c.HOLDSTATUS.Trim() != "R")
                            select c);
            }
            */

            List<ContainerModel> ret = new List<ContainerModel>();

            foreach (CONTAINER c in list)
            {
                ret.Add(new ContainerModel
                {
                    ID = c.ID,
                    CONTAINERNUMBER = (c.CONTAINERNUMBER == null) ? "" : c.CONTAINERNUMBER,
                    CONTAINERSIZE = (c.CONTAINERSIZE == null) ? "" : c.CONTAINERSIZE,
                    TAGNUMBER = c.TAGNUMBER,
                    TRUCKNUMBER = c.TRUCKNUMBER,
                    TRUCKPOSITION = updateTruckPosition(c),
                    GATENUMBER = (c.GATENUMBER == null) ? "" : c.GATENUMBER,
                    GATEINTIME = (c.GATEINTIME == null) ? null : (DateTime?)Convert.ToDateTime(c.GATEINTIME),
                    GATEOUTTIMES = (c.GATEOUTTIME == null) ? (DateTime)c.GATEINTIME : (DateTime)c.GATEOUTTIME,
                    GATEOUTTIME = (c.GATEOUTTIME == null) ? null : (DateTime?)Convert.ToDateTime(c.GATEOUTTIME),

                    SGATEOUTTIME = ((c.GATEOUTTIME != null) ? c.GATEOUTTIME.ToString() : ""),
                    DOCUMENTNUMBER = (c.DOCUMENTNUMBER == null) ? "" : c.DOCUMENTNUMBER,
                    HOLDTYPE = (c.HOLDTYPE == null) ? "" : HoldType.type(c.HOLDTYPE),
                    HOLDSTATUS = (c.HOLDSTATUS.Trim() == "H") ? HoldType.type(c.HOLDTYPE) : "",
                    Note = (c.HOLDSTATUS.Trim() == "H") ? c.HOLDNOTE : c.RELEASENOTE,
                    SEAL = (c.SEAL == null) ? "" : Seal.kondisi(c.SEAL),
                    SEALNOTE = (c.SEALNOTE == null) ? "" : c.SEALNOTE,
                    KODEPENYEGELAN = (c.KODEPENYEGELAN == null) ? "" : c.KODEPENYEGELAN,
                    CHECKEDBY = c.CHECKEDBY,
                    CHECKEDTIME = c.CHECKEDTIME.GetValueOrDefault(),
                    StrCheckedTime = (c.CHECKEDTIME.HasValue) ? c.CHECKEDTIME.ToString() : "",
                    ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? c.CONTAINERNUMBER + "/" + "00" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
                    HoldBy = (c.HOLDBY == null) ? "" : c.HOLDBY,
                    ReleaseBy = (c.RELEASEBY == null) ? "" : c.RELEASEBY,
                    Action = (canHoldP2 && (c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2a) || (canHoldAutoE && c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT == "E" || canHoldAutoI && c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT == "I") || canHoldManual && c.HOLDTYPE.Trim() == "M") ? HoldReleaseAction.getAction(c.ID, c.EXPORTIMPORT, c.HOLDSTATUS, c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2a) : HoldReleaseAction.getDetailOnly(c.ID, c.EXPORTIMPORT),
                    TerminalId = c.TERMINALID
                });
            }

            return ret;

            //return list.ToList().Select(c => new ContainerModel
            //{
            //    ID = c.ID,
            //    CONTAINERNUMBER = (c.CONTAINERNUMBER == null) ? "" : c.CONTAINERNUMBER,
            //    CONTAINERSIZE = (c.CONTAINERSIZE == null) ? "" : c.CONTAINERSIZE,
            //    TAGNUMBER = c.TAGNUMBER,
            //    TRUCKNUMBER = c.TRUCKNUMBER,
            //    TRUCKPOSITION = c.TRUCKPOSITION,
            //    GATENUMBER = (c.GATENUMBER == null) ? "" : c.GATENUMBER,
            //    GATEINTIME = (c.GATEINTIME == null) ? DateTime.Now : (DateTime)c.GATEINTIME,
            //    GATEOUTTIMES = (c.GATEOUTTIME == null) ? (DateTime)c.GATEINTIME : (DateTime)c.GATEOUTTIME,
            //    DOCUMENTNUMBER = (c.DOCUMENTNUMBER == null) ? "" : c.DOCUMENTNUMBER,
            //    HOLDTYPE = (c.HOLDTYPE == null) ? "" : HoldType.type(c.HOLDTYPE),
            //    HOLDSTATUS = (c.HOLDSTATUS.Trim() == "H") ? HoldType.type(c.HOLDTYPE) : "",
            //    Note = (c.HOLDSTATUS.Trim() == "H") ? c.HOLDNOTE : c.RELEASENOTE,
            //    SEAL = (c.SEAL == null) ? "" : Seal.kondisi(c.SEAL),
            //    SEALNOTE = (c.SEALNOTE == null) ? "" : c.SEALNOTE,
            //    KODEPENYEGELAN = (c.KODEPENYEGELAN == null) ? "" : c.KODEPENYEGELAN,
            //    CHECKEDBY = c.CHECKEDBY,
            //    CHECKEDTIME = c.CHECKEDTIME.GetValueOrDefault(),
            //    StrCheckedTime = (c.CHECKEDTIME.HasValue == true) ? c.CHECKEDTIME.Value.ToString("dd-MM-yyy") : "",
            //    ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? c.CONTAINERNUMBER + "/" + "00" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
            //    HoldBy = (c.HOLDBY == null) ? "" : c.HOLDBY,
            //    ReleaseBy = (c.RELEASEBY == null ) ? "" : c.RELEASEBY,
            //    GATEOUTTIME = (c.GATEOUTTIME == null) ? null : (DateTime?)Convert.ToDateTime(c.GATEOUTTIME),
            //    Action = (canHoldP2 && (c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2r) || (canHoldAutoE && c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT == "E" || canHoldAutoI && c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT == "I") || canHoldManual && c.HOLDTYPE.Trim() == "M") ? HoldReleaseAction.getAction(c.ID, c.EXPORTIMPORT, c.HOLDSTATUS) : HoldReleaseAction.getDetailOnly(c.ID, c.EXPORTIMPORT),
            //    TerminalId = c.TERMINALID
            //}).ToList();
        }

        public List<PICTURE> GetImageList(int id)
        {

            //CON_LISTCONT containerInAutogate = (from c in _autogateEntities.AG_CAMERA
            //                                    select c).FirstOrDefault();
            var list = (from e in _entities.PICTURE
                        where e.CONTAINER_ID==id
                        orderby e.ID descending
                        select e).ToList();
            //var list = (from p in _entities.PICTURE
            //            where p.CONTAINER_ID == id
            //            select p).ToList();
            return list;
        }

        private Int64 dateToOcrInt(DateTime time)
        {
            Int64 ret = 1;
            string retS = time.Year.ToString().Substring(2);
            retS += time.Month.ToString().PadLeft(2, '0');
            retS += time.Day.ToString().PadLeft(2, '0');
            retS += time.Hour.ToString().PadLeft(2, '0');
            retS += time.Minute.ToString().PadLeft(2, '0');
            retS += time.Second.ToString().PadLeft(2, '0');
            ret = Int64.Parse(retS);
            ret *= 100000;
            return ret;
        }

        public List<string> GetOCRImageList(int id)
        {
            string ocrImgBasePath = ConfigurationManager.AppSettings["imageOcrUrl"];
            CONTAINER container = this.Get(id);
            AG_GATEPASS agp = (from c in _autogateEntities.AG_GATEPASS
                               where c.TAR==container.TRANSACTION_ID
                               orderby c.ID descending
                               select c).FirstOrDefault();
            List<string> imageList = new List<string>();
            AG_OCR_CNTR ocrInAutogate = null;
            bool ocrImageFound = false;

            if (agp!= null)
            {
                AG_OCR_LOGS ocrLogInAutogate = (from c in _autogateEntities.AG_OCR_LOGS
                                                where c.TRANSACTION_ID == agp.TRANSACTIONID
                                                orderby c.ID descending
                                                select c).FirstOrDefault();
                if (ocrLogInAutogate != null)
                {

                    ocrInAutogate = (from c in _autogateEntities.AG_OCR_CNTR
                                     where c.ID == ocrLogInAutogate.OCR_ID
                                     orderby c.ID descending
                                     select c).FirstOrDefault();
                    ocrImageFound = true;
                }
            }

            if(!ocrImageFound)
            {
                DateTime? startTime = container.GATEINTIME;
                if (container.EXPORTIMPORT.Trim() == "I")
                {
                    if(container.GATEOUTTIME!=null)
                        startTime = container.GATEOUTTIME;
                    else
                        startTime = DateTime.Now;
                }
                Int64 startTimeLong = dateToOcrInt(startTime.GetValueOrDefault().AddMinutes(-60));
                Int64 endTimeLong = dateToOcrInt(startTime.GetValueOrDefault());
                ocrInAutogate = (from c in _autogateEntities.AG_OCR_CNTR
                                             where 
                                             (
                                                 c.CNTRNR.Contains(container.CONTAINERNUMBER.Trim())||
                                                 c.CNTRNR_CORRECTION.Contains(container.CONTAINERNUMBER.Trim())
                                             )
                                             && startTimeLong <= c.TRUCKID && c.TRUCKID <= endTimeLong
                                             orderby c.ID descending
                                             select c).FirstOrDefault();

                if (ocrInAutogate == null)
                {
                    ocrInAutogate = (from c in _autogateEntities.AG_OCR_CNTR
                                     where
                                     (
                                         container.CONTAINERNUMBER.Trim().Contains(c.CNTRNR) ||
                                         container.CONTAINERNUMBER.Trim().Contains(c.CNTRNR_CORRECTION)
                                     )
                                     && startTimeLong <= c.TRUCKID && c.TRUCKID <= endTimeLong
                                     orderby c.ID descending
                                     select c).FirstOrDefault();
                    if (ocrInAutogate == null)
                    {
                        startTimeLong = dateToOcrInt(startTime.GetValueOrDefault().AddMinutes(-20));
                        endTimeLong = dateToOcrInt(startTime.GetValueOrDefault());
                        //find the simmiliar one
                        List<AG_OCR_CNTR> ocrInAutogateList = (from c in _autogateEntities.AG_OCR_CNTR
                                         where startTimeLong <= c.TRUCKID && c.TRUCKID <= endTimeLong
                                         orderby c.ID descending
                                         select c).ToList();
                        foreach(AG_OCR_CNTR agCntr in ocrInAutogateList){

                            if (!String.IsNullOrEmpty(agCntr.CNTRNR))
                                if (MainHelper.StringDistance(container.CONTAINERNUMBER, agCntr.CNTRNR) > 4)
                                {
                                    ocrInAutogate = null;
                                }
                                else
                                {
                                    ocrInAutogate = agCntr;
                                    break;
                                }
                        }
                    }
                }
            }

            if (ocrInAutogate != null)
            {
                //front
                imageList.Add(ocrImgBasePath + "?id_ocr=" + ocrInAutogate.ID + "&id=01");
                //back
                imageList.Add(ocrImgBasePath + "?id_ocr=" + ocrInAutogate.ID + "&id=10");
            }
            return imageList;
        }

        public List<string> GetOCRImageListX(int id)
        {
            string ocrImgBasePath = ConfigurationManager.AppSettings["imageOcrUrl"];
            CONTAINER_X container = this.GetX(id);
            AG_GATEPASS agp = (from c in _autogateEntities.AG_GATEPASS
                               where c.TAR == container.TRANSACTION_ID
                               orderby c.ID descending
                               select c).FirstOrDefault();
            List<string> imageList = new List<string>();
            AG_OCR_CNTR ocrInAutogate = null;
            bool ocrImageFound = false;

            if (agp != null)
            {
                AG_OCR_LOGS ocrLogInAutogate = (from c in _autogateEntities.AG_OCR_LOGS
                                                where c.TRANSACTION_ID == agp.TRANSACTIONID
                                                orderby c.ID descending
                                                select c).FirstOrDefault();
                if (ocrLogInAutogate != null)
                {

                    ocrInAutogate = (from c in _autogateEntities.AG_OCR_CNTR
                                     where c.ID == ocrLogInAutogate.OCR_ID
                                     orderby c.ID descending
                                     select c).FirstOrDefault();
                    ocrImageFound = true;
                }
            }

            if (!ocrImageFound)
            {
                DateTime? startTime = container.GATEINTIME;
                if (container.EXPORTIMPORT.Trim() == "I")
                {
                    if (container.GATEOUTTIME != null)
                        startTime = container.GATEOUTTIME;
                    else
                        startTime = DateTime.Now;
                }
                Int64 startTimeLong = dateToOcrInt(startTime.GetValueOrDefault().AddMinutes(-60));
                Int64 endTimeLong = dateToOcrInt(startTime.GetValueOrDefault());
                ocrInAutogate = (from c in _autogateEntities.AG_OCR_CNTR
                                 where
                                 (
                                     c.CNTRNR.Contains(container.CONTAINERNUMBER.Trim()) ||
                                     c.CNTRNR_CORRECTION.Contains(container.CONTAINERNUMBER.Trim())
                                 )
                                 && startTimeLong <= c.TRUCKID && c.TRUCKID <= endTimeLong
                                 orderby c.ID descending
                                 select c).FirstOrDefault();

                if (ocrInAutogate == null)
                {
                    ocrInAutogate = (from c in _autogateEntities.AG_OCR_CNTR
                                     where
                                     (
                                         container.CONTAINERNUMBER.Trim().Contains(c.CNTRNR) ||
                                         container.CONTAINERNUMBER.Trim().Contains(c.CNTRNR_CORRECTION)
                                     )
                                     && startTimeLong <= c.TRUCKID && c.TRUCKID <= endTimeLong
                                     orderby c.ID descending
                                     select c).FirstOrDefault();
                    if (ocrInAutogate == null)
                    {
                        startTimeLong = dateToOcrInt(startTime.GetValueOrDefault().AddMinutes(-20));
                        endTimeLong = dateToOcrInt(startTime.GetValueOrDefault());
                        //find the simmiliar one
                        List<AG_OCR_CNTR> ocrInAutogateList = (from c in _autogateEntities.AG_OCR_CNTR
                                                               where startTimeLong <= c.TRUCKID && c.TRUCKID <= endTimeLong
                                                               orderby c.ID descending
                                                               select c).ToList();
                        foreach (AG_OCR_CNTR agCntr in ocrInAutogateList)
                        {

                            if (!String.IsNullOrEmpty(agCntr.CNTRNR))
                                if (MainHelper.StringDistance(container.CONTAINERNUMBER, agCntr.CNTRNR) > 4)
                                {
                                    ocrInAutogate = null;
                                }
                                else
                                {
                                    ocrInAutogate = agCntr;
                                    break;
                                }
                        }
                    }
                }
            }

            if (ocrInAutogate != null)
            {
                //front
                imageList.Add(ocrImgBasePath + "?id_ocr=" + ocrInAutogate.ID + "&id=01");
                //back
                imageList.Add(ocrImgBasePath + "?id_ocr=" + ocrInAutogate.ID + "&id=10");
            }
            return imageList;
        }

        public List<ContainerModel> GetP2List(string username)
        {
            string r = "R";
            string p2 = "P";
            string p2a = "AP";
            string[] roles = Roles.GetRolesForUser(username);
            bool canHold = roles.Contains("P2");
            //canHold = roles.Contains("Administrator");
            DateTime now = DateTime.Now.AddHours(hoursDiff);
            var list = (from c in _entities.CONTAINER
                        where (c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2a) 
                        && (c.GATEINTIME >= now || c.HOLDSTATUS.Trim() == "H")
                        select c).ToList();
            return list.Select(c => new ContainerModel
            {
                ID = c.ID,
                CONTAINERNUMBER = (c.CONTAINERNUMBER == null) ? "" : c.CONTAINERNUMBER,
                CONTAINERSIZE = (c.CONTAINERSIZE == null) ? "" : c.CONTAINERSIZE,
                TAGNUMBER = c.TAGNUMBER,
                TRUCKNUMBER = c.TRUCKNUMBER,
                TRUCKPOSITION = updateTruckPosition(c),
                GATENUMBER = (c.GATENUMBER == null) ? "" : c.GATENUMBER,
                GATEINTIME = (c.GATEINTIME == null) ? DateTime.Now : (DateTime)c.GATEINTIME,
                SGATEOUTTIME = ((c.GATEOUTTIME != null) ? c.GATEOUTTIME.ToString() : ""),
                DOCUMENTNUMBER = (c.TRUCKNUMBER == null) ? "" : c.DOCUMENTNUMBER,
                HOLDTYPE = (c.HOLDTYPE == null) ? "" : HoldType.type(c.HOLDTYPE),
                HOLDSTATUS = (c.HOLDSTATUS.Trim() == "H") ? HoldType.type(c.HOLDTYPE) : "",
                SEAL = (c.SEAL == null) ? "" : Seal.kondisi(c.SEAL),
                SEALNOTE = (c.SEALNOTE == null) ? "" : c.SEALNOTE,
                Note = (c.HOLDSTATUS.Trim() == "H") ? c.HOLDNOTE : c.RELEASENOTE,
                KODEPENYEGELAN = (c.KODEPENYEGELAN == null) ? "" : c.KODEPENYEGELAN,
                CHECKEDBY = c.CHECKEDBY,
                HoldBy = (c.HOLDBY == null) ? "" : c.HOLDBY,
                ReleaseBy = (c.RELEASEBY == null) ? "" : c.RELEASEBY,
                CHECKEDTIME = c.CHECKEDTIME.GetValueOrDefault(),
                StrCheckedTime = (c.CHECKEDTIME.HasValue) ? c.CHECKEDTIME.ToString() : "",
                ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? "" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
                Action = (canHold && (c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2a)) ? HoldReleaseAction.getAction(c.ID, c.EXPORTIMPORT, c.HOLDSTATUS, true) : HoldReleaseAction.getDetailOnly(c.ID, "P2")
            }).ToList();
        }

        public List<ContainerModel> GetAutoList(string username,string dateFrom = "", string dateTo = "")
        {
            string r = "R";
            string a = "A";
            string u = "U";

            string[] roles = Roles.GetRolesForUser(username);
            var list = (from c in _entities.CONTAINER
                        where c.HOLDTYPE.Trim() == a ||c.SEAL =="R"
                        select c);
            if (roles.Contains("Penyegelan") && !roles.Contains("ControlRoom"))
            {
                list = list.Where(c => c.EXPORTIMPORT.Trim() == "I");
            }
            if (roles.Contains("ControlRoom") && !roles.Contains("Penyegelan"))
            {
                list = list.Where(c => c.EXPORTIMPORT.Trim() == "E");
            }
            if (!String.IsNullOrEmpty(dateFrom) && String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.Parse(dateFrom);
                list = list.Where(c => c.GATEINTIME >= dFrom);
            }
            else if (String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dTo = DateTime.Parse(dateTo).AddDays(1);
                list = list.Where(c => c.GATEINTIME < dTo);
            }
            else if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.Parse(dateFrom);
                DateTime dTo = DateTime.Parse(dateTo).AddDays(1);
                list = list.Where(c => c.GATEINTIME >= dFrom && c.GATEINTIME < dTo);
            }

            List<ContainerModel> ret = new List<ContainerModel>();

            foreach (CONTAINER c in list)
            {
                ret.Add(new ContainerModel
                {
                    ID = c.ID,
                    CONTAINERNUMBER = (c.CONTAINERNUMBER == null) ? "" : c.CONTAINERNUMBER,
                    CONTAINERSIZE = (c.CONTAINERSIZE == null) ? "" : c.CONTAINERSIZE,
                    TAGNUMBER = c.TAGNUMBER,
                    TRUCKPOSITION = updateTruckPosition(c),
                    TRUCKNUMBER = c.TRUCKNUMBER,
                    GATENUMBER = (c.GATENUMBER == null) ? "" : c.GATENUMBER,
                    GATEINTIME = (c.GATEINTIME == null) ? DateTime.Now : (DateTime)c.GATEINTIME,
                    SGATEOUTTIME = ((c.GATEOUTTIME != null) ? c.GATEOUTTIME.ToString() : ""),
                    DOCUMENTNUMBER = (c.TRUCKNUMBER == null) ? "" : c.DOCUMENTNUMBER,
                    HOLDTYPE = (c.HOLDTYPE == null) ? "" : HoldType.type(c.HOLDTYPE),
                    SEAL = (c.SEAL == null) ? "" : Seal.kondisi(c.SEAL),
                    SEALNOTE = (c.SEALNOTE == null) ? "" : c.SEALNOTE,
                    KODEPENYEGELAN = (c.KODEPENYEGELAN == null) ? "" : c.KODEPENYEGELAN,
                    CHECKEDBY = c.CHECKEDBY,
                    CHECKEDTIME = c.CHECKEDTIME.GetValueOrDefault(),
                    StrCheckedTime = (c.CHECKEDTIME.HasValue) ? c.CHECKEDTIME.ToString() : "",
                    ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? "" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
                    EXPORTIMPORT = (c.EXPORTIMPORT == null) ? "" : ExportImport.detail(c.EXPORTIMPORT),
                    DOCUMENTTYPE = (c.DOCUMENTTYPE == null) ? "" : c.DOCUMENTTYPE,
                    Action = PutusanAction.getAction(c.ID, c.HOLDSTATUS)
                });
            }
            return ret;
        }

        public List<ContainerModel> GetArsipList(string dateFrom = "", string dateTo = "", 
            string cn = "", string docnum = "", string ei = "", string docType = "", string terminal = "")
        {
            string r = "R";
            DateTime now = DateTime.Now.AddHours(hoursDiff);
            var list = (from c in _entities.CONTAINER
                        where (c.GATEOUTTIME.HasValue) ||
                        (!c.GATEOUTTIME.HasValue && c.GATEINTIME < now && c.HOLDSTATUS.Trim() != "H") ||
                        (!c.GATEOUTTIME.HasValue && c.GATEINTIME < now && c.HOLDSTATUS.Trim() == "H" && c.HOLDTYPE == "A" && c.EXPORTIMPORT == "E" )||
                        (c.HOLDTYPE.Trim() == "D")
                        select c);

            if (!String.IsNullOrEmpty(dateFrom) && String.IsNullOrEmpty(dateTo))
            {
                //DateTime dFrom = DateTime.ParseExact(dateFrom, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                DateTime dFrom = DateTime.ParseExact(dateFrom, "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                list = list.Where(c => c.GATEINTIME >= dFrom);
            }
            else if (String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                //DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                list = list.Where(c => c.GATEINTIME < dTo);
            }
            else if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                //DateTime dFrom = DateTime.ParseExact(dateFrom, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                //DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                DateTime dFrom = DateTime.ParseExact(dateFrom, "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                list = list.Where(c => c.GATEINTIME >= dFrom && c.GATEINTIME < dTo && c.GATEOUTTIME != null);
            }

            if (!String.IsNullOrEmpty(cn))
            {
                list = list.Where(c => c.CONTAINERNUMBER.ToLower().Contains(cn.ToLower() ));
            }

            if (!String.IsNullOrEmpty(docnum))
            {
                list = list.Where(c => c.DOCUMENTNUMBER.ToLower().Contains(docnum.ToLower() ));
            }

            if (!String.IsNullOrEmpty(ei))
            {
                list = list.Where(c => c.EXPORTIMPORT == ei);
            }

            if (!String.IsNullOrEmpty(docType))
            {
                list = list.Where(c => c.DOCUMENTTYPE.Trim() == docType);
            }

            if (!String.IsNullOrEmpty(terminal))
            {
                list = list.Where(c => c.TERMINALID.Trim() == terminal);
            }

            List<ContainerModel> ret = new List<ContainerModel>();

            foreach (CONTAINER c in list)
            {
                string fullempty = (c.INTELLIGENCENOTE == null) ? "" : (String.IsNullOrEmpty(c.INTELLIGENCENOTE.Trim()) ? "EMPTY" : "FULL");
                //if (String.IsNullOrEmpty(fullempty))
                //{
                //    try
                //    {

                //        var autogatecontainer = (from ac in _autogateEntities.CON_LISTCONT
                //                                 where
                //                                 ac.NO_CONTAINER == c.CONTAINERNUMBER.Trim() &&
                //                                 ac.ID_CLASS_CODE == c.EXPORTIMPORT.Trim() &&
                //                                 ac.TAR == c.TRANSACTION_ID.Trim()
                //                                 select ac).ToList();
                //        if (autogatecontainer != null)
                //        {
                //            _logger.Info("Found Con List Data");
                //            if (autogatecontainer.Count() == 1)
                //            {
                //                var contdata = autogatecontainer.FirstOrDefault();
                //                fullempty = (contdata.CONT_STATUS.Trim() == "FULL" ? "FULL" : "EMPTY");

                //                if (fullempty == "EMPTY")
                //                {
                //                    c.INTELLIGENCENOTE = " ";
                //                }
                //                else
                //                {
                //                    c.INTELLIGENCENOTE = ".";
                //                }
                //            }
                //            else
                //            {
                //                _logger.Info("To Many Con List Data with tar :  "+c.TRANSACTION_ID);
                //                //to many data
                //            }

                //            _entities.SaveChanges();
                //        }
                //        else
                //        {
                //            //conlist not found
                //        }
                //    }catch(Exception ex)
                //    {

                //        _logger.Error(ex.Message);
                //    }
                //}
                ret.Add(new ContainerModel
                {
                    ID = c.ID,
                    EXPORTIMPORT = (c.EXPORTIMPORT == null) ? "" : ExportImport.detail(c.EXPORTIMPORT),
                    CONTAINERNUMBER = (c.CONTAINERNUMBER == null) ? "" : c.CONTAINERNUMBER,
                    CONTAINERSIZE = (c.CONTAINERSIZE == null) ? "" : c.CONTAINERSIZE,
                    TAGNUMBER = c.TAGNUMBER,
                    TRUCKNUMBER = c.TRUCKNUMBER,
                    TRUCKPOSITION = updateTruckPosition(c),
                    GATENUMBER = (c.GATENUMBER == null) ? "" : c.GATENUMBER,
                    GATEINTIME = (!c.GATEINTIME.HasValue ) ? DateTime.MinValue  : (DateTime)c.GATEINTIME,
                    GATEOUTTIME = (!c.GATEOUTTIME.HasValue) ? null : c.GATEOUTTIME,  
                    DOCUMENTNUMBER = (c.DOCUMENTNUMBER  == null) ? "" : c.DOCUMENTNUMBER,
                    HOLDSTATUS = (c.HOLDSTATUS.Trim() == "H") ? HoldType.type(c.HOLDTYPE) : "",
                    SEAL = (c.SEAL == null) ? "" : Seal.kondisi(c.SEAL),
                    SEALNOTE = (c.SEALNOTE == null) ? "" : c.SEALNOTE,
                    FULLEMPTY = fullempty,
                    HOLDNOTE = (c.HOLDNOTE == null) ? "" : c.HOLDNOTE,
                    RELEASENOTE = (c.RELEASENOTE == null) ? "" : c.RELEASENOTE,
                    Note = (c.HOLDTYPE.Trim() == "D") ? "Batal masuk" : "",
                    KODEPENYEGELAN = (c.KODEPENYEGELAN == null) ? "" : c.KODEPENYEGELAN,
                    BRUTTOWEIGHTS = (Double)c.BRUTTOWEIGHTS,
                    ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? "" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
                    Action = ArsipAction.getAction(c.ID, c.EXPORTIMPORT),
                    TerminalId = c.TERMINALID,
                    DOCUMENTTYPE = c.DOCUMENTTYPE
                });

            }
            return ret;
        }

        public List<ContainerDeleted> GetDeletedList(string username, string dateFrom = "", string dateTo = "", string cn = "")
        {
            //string r = "R";
            DateTime now = DateTime.Now.AddHours(hoursDiff);
            var list = (from c in _entities.CONTAINER_X
                        select c);

            if (!String.IsNullOrEmpty(dateFrom) && String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.ParseExact(dateFrom, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                list = list.Where(c => c.DELETE_TIME >= dFrom);
            }
            else if (String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                list = list.Where(c => c.DELETE_TIME < dTo);
            }
            else if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.ParseExact(dateFrom, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                list = list.Where(c => c.DELETE_TIME >= dFrom && c.DELETE_TIME < dTo);
            }

            if (!String.IsNullOrEmpty(cn))
            {
                list = list.Where(c => c.CONTAINERNUMBER.ToLower().Contains(cn.ToLower()));
            }

            List<ContainerDeleted> ret = new List<ContainerDeleted>();

            foreach (CONTAINER_X c in list)
            {
                ret.Add(new ContainerDeleted
                {
                    ID = c.ID,
                    CONTAINER_ID = c.CONTAINER_ID.HasValue ? c.CONTAINER_ID.Value : 0,
                    DELETETIME = c.DELETE_TIME,
                    DELETEBY = c.DELETE_BY,
                    DELETENOTE = c.DELETE_NOTE,

                    EXPORTIMPORT = (c.EXPORTIMPORT == null) ? "" : ExportImport.detail(c.EXPORTIMPORT),
                    CONTAINERNUMBER = (c.CONTAINERNUMBER == null) ? "" : c.CONTAINERNUMBER,
                    CONTAINERSIZE = (c.CONTAINERSIZE == null) ? "" : c.CONTAINERSIZE,
                    TAGNUMBER = c.TAGNUMBER,
                    TRUCKNUMBER = c.TRUCKNUMBER,
                    //TRUCKPOSITION = updateTruckPosition(c),
                    GATENUMBER = (c.GATENUMBER == null) ? "" : c.GATENUMBER,
                    GATEINTIME = (!c.GATEINTIME.HasValue) ? DateTime.MinValue : (DateTime)c.GATEINTIME,
                    GATEOUTTIME = (!c.GATEOUTTIME.HasValue) ? null : c.GATEOUTTIME,
                    DOCUMENTNUMBER = (c.DOCUMENTNUMBER == null) ? "" : c.DOCUMENTNUMBER,
                    HOLDSTATUS = (c.HOLDSTATUS.Trim() == "H") ? HoldType.type(c.HOLDTYPE) : "",
                    SEAL = (c.SEAL == null) ? "" : Seal.kondisi(c.SEAL),
                    SEALNOTE = (c.SEALNOTE == null) ? "" : c.SEALNOTE,
                    HOLDNOTE = (c.HOLDNOTE == null) ? "" : c.HOLDNOTE,
                    RELEASENOTE = (c.RELEASENOTE == null) ? "" : c.RELEASENOTE,
                    Note = (c.HOLDTYPE.Trim() == "D") ? "Batal masuk" : "",
                    KODEPENYEGELAN = (c.KODEPENYEGELAN == null) ? "" : c.KODEPENYEGELAN,
                    BRUTTOWEIGHTS = (Double)c.BRUTTOWEIGHTS,
                    ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? "" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
                    Action = DuplicateDeleteAction.getAction(c.CONTAINER_ID.Value, c.EXPORTIMPORT),

                    TerminalId = c.TERMINALID,
                    DOCUMENTTYPE = c.DOCUMENTTYPE
                });
            }
            return ret;
        }

        public string GetUpdateDelete(string username, string conID, string deleteNote)
        {
            List<int> listID = conID.Split(new char[] { ',' }).Select(Int32.Parse).ToList();

            var updateDelete = from cd in _entities.CONTAINER_X
                               where listID.Contains(cd.CONTAINER_ID.Value)
                               select cd;

            foreach (var item in updateDelete)
            {
                //item.DELETE_TIME = DateTime.Now;
                item.DELETE_BY = username;
                item.DELETE_NOTE = deleteNote;
            }

            try
            {
                _entities.SaveChanges();

                return true.ToString();
            }
            catch (Exception e)
            {
                return e.InnerException.Message;
            }
        }

        public string GetDuplicateDelete(string conID)
        {
            List<int> listID = conID.Split(new char[] { ',' }).Select(Int32.Parse).ToList();

            var deleteContainer = from c in _entities.CONTAINER
                                  where listID.Contains(c.ID)
                                  select c;

            foreach (var item in deleteContainer)
            {
                _entities.CONTAINER.DeleteObject(item);
            }

            try
            {
                _entities.SaveChanges();

                return true.ToString();
            }
            catch (Exception e)
            {
                return e.InnerException.Message;
            }
        }

        public string GetDuplicateDeleteNew(string username, string conID, string deleteNote)
        {
            List<int> listID = conID.Split(new char[] { ',' }).Select(Int32.Parse).ToList();

            var deleteContainer = from c in _entities.CONTAINER
                                  where listID.Contains(c.ID)
                                  select c;

            foreach (var item in deleteContainer)
            {
                CONTAINER_X rowDeleted = new CONTAINER_X();
                rowDeleted.CONTAINER_ID = item.ID;
                rowDeleted.DELETE_TIME = DateTime.Now;
                rowDeleted.DELETE_BY = username;
                rowDeleted.DELETE_NOTE = deleteNote;

                rowDeleted.CONTAINERNUMBER = item.CONTAINERNUMBER;
                rowDeleted.GATEINTIME = item.GATEINTIME;
                rowDeleted.TRUCKNUMBER = item.TRUCKNUMBER;
                rowDeleted.TRUCKCOMPANYNAME = item.TRUCKCOMPANYNAME;
                rowDeleted.TRUCKPOSITION = item.TRUCKPOSITION;
                rowDeleted.VESSELNAME = item.VESSELNAME;
                rowDeleted.VOYAGE = item.VOYAGE;
                rowDeleted.ATA = item.ATA;
                rowDeleted.ATD = item.ATD;
                rowDeleted.PORTDISCHARGE = item.PORTDISCHARGE;
                rowDeleted.HOLDREMARK = item.HOLDREMARK;
                rowDeleted.TAGNUMBER = item.TAGNUMBER;
                rowDeleted.GATENUMBER = item.GATENUMBER;
                rowDeleted.CONTAINERSIZE3 = item.CONTAINERSIZE3;
                rowDeleted.EXPORTIMPORT = item.EXPORTIMPORT;
                rowDeleted.HOLDSTATUS = item.HOLDSTATUS;
                rowDeleted.HOLDTYPE = item.HOLDTYPE;
                rowDeleted.DOCUMENTTYPE = item.DOCUMENTTYPE;
                rowDeleted.DOCUMENTNUMBER = item.DOCUMENTNUMBER;
                rowDeleted.DOCUMENTDATE = item.DOCUMENTDATE;
                rowDeleted.CONSIGNEE = item.CONSIGNEE;
                rowDeleted.INTELLIGENCENOTE = item.INTELLIGENCENOTE;
                rowDeleted.BLNUMBER = item.BLNUMBER;
                rowDeleted.BCNUMBER = item.BCNUMBER;
                rowDeleted.KPBCCODE = item.KPBCCODE;
                rowDeleted.TRANSACTION_ID = item.TRANSACTION_ID;
                rowDeleted.HOLDTIME = item.HOLDTIME;
                rowDeleted.HOLDNOTE = item.HOLDNOTE;
                rowDeleted.HOLDBY = item.HOLDBY;
                rowDeleted.RELEASETIME = item.RELEASETIME;
                rowDeleted.RELEASENOTE = item.RELEASENOTE;
                rowDeleted.RELEASEBY = item.RELEASEBY;
                rowDeleted.SEAL = item.SEAL;
                rowDeleted.SEALNOTE = item.SEALNOTE;
                rowDeleted.GATEOUTTIME = item.GATEOUTTIME;
                rowDeleted.CHECKEDBY = item.CHECKEDBY;
                rowDeleted.CHECKEDTIME = item.CHECKEDTIME;
                rowDeleted.KODEPENYEGELAN = item.KODEPENYEGELAN;
                rowDeleted.BRUTTOWEIGHTS = item.BRUTTOWEIGHTS;
                rowDeleted.CONTAINERSIZE = item.CONTAINERSIZE;
                rowDeleted.TERMINALID = item.TERMINALID;

                _entities.CONTAINER_X.AddObject(rowDeleted);
                _entities.CONTAINER.DeleteObject(item);
            }

            try
            {
                _entities.SaveChanges();
                return true.ToString();
            }
            catch (Exception e)
            {
                return e.InnerException.Message;
            }
        }

        public List<ContainerModel> GetDuplicateList(string username, string dateFrom = "", string dateTo = "")
        {
            //string r = "R";
            DateTime now = DateTime.Now.AddHours(hoursDiff);
            var list = (from c in _entities.CONTAINER
                        where c.GATEINTIME.HasValue
                        select c);

            //if (!String.IsNullOrEmpty(dateTo))
            //{
            //    DateTime dFrom = DateTime.ParseExact(dateTo, "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture).AddDays(-2);
            //    DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture).AddDays(1).AddSeconds(-1);
            //    list = list.Where(c => c.GATEINTIME >= dFrom && c.GATEINTIME < dTo);
            //}

            if (!String.IsNullOrEmpty(dateFrom) && String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.ParseExact(dateFrom, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                list = list.Where(c => c.GATEINTIME >= dFrom);
            }
            else if (String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                list = list.Where(c => c.GATEINTIME < dTo);
            }
            else if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.ParseExact(dateFrom, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                list = list.Where(c => c.GATEINTIME >= dFrom && c.GATEINTIME < dTo);
            }

            list = list.GroupBy(x => new { x.CONTAINERNUMBER, x.EXPORTIMPORT }).Where(g => g.Count() > 1).SelectMany(z => z);

            List<ContainerModel> ret = new List<ContainerModel>();

            foreach (CONTAINER c in list)
            {
                ret.Add(new ContainerModel
                {
                    ID = c.ID,
                    EXPORTIMPORT = (c.EXPORTIMPORT == null) ? "" : ExportImport.detail(c.EXPORTIMPORT),
                    CONTAINERNUMBER = (c.CONTAINERNUMBER == null) ? "" : c.CONTAINERNUMBER,
                    CONTAINERSIZE = (c.CONTAINERSIZE == null) ? "" : c.CONTAINERSIZE,
                    TAGNUMBER = c.TAGNUMBER,
                    TRUCKNUMBER = c.TRUCKNUMBER,
                    TRUCKPOSITION = updateTruckPosition(c),
                    GATENUMBER = (c.GATENUMBER == null) ? "" : c.GATENUMBER,
                    GATEINTIME = (!c.GATEINTIME.HasValue) ? DateTime.MinValue : (DateTime)c.GATEINTIME,
                    GATEOUTTIME = (!c.GATEOUTTIME.HasValue) ? null : c.GATEOUTTIME,
                    DOCUMENTNUMBER = (c.DOCUMENTNUMBER == null) ? "" : c.DOCUMENTNUMBER,
                    HOLDSTATUS = (c.HOLDSTATUS.Trim() == "H") ? HoldType.type(c.HOLDTYPE) : "",
                    SEAL = (c.SEAL == null) ? "" : Seal.kondisi(c.SEAL),
                    SEALNOTE = (c.SEALNOTE == null) ? "" : c.SEALNOTE,
                    HOLDNOTE = (c.HOLDNOTE == null) ? "" : c.HOLDNOTE,
                    RELEASENOTE = (c.RELEASENOTE == null) ? "" : c.RELEASENOTE,
                    Note = (c.HOLDTYPE.Trim() == "D") ? "Batal masuk" : "",
                    KODEPENYEGELAN = (c.KODEPENYEGELAN == null) ? "" : c.KODEPENYEGELAN,
                    BRUTTOWEIGHTS = (Double)c.BRUTTOWEIGHTS,
                    ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? "" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
                    Action = DuplicateDeleteAction.getAction(c.ID, c.EXPORTIMPORT),
                    ActionCheckbox = DuplicateDeleteAction.getCheckbox(c.ID),

                    TerminalId = c.TERMINALID,
                    DOCUMENTTYPE = c.DOCUMENTTYPE
                });
            }
            return ret;
        }

        //Summary
        public List<ContainerModel> GetSummaryList(string dateFrom = "", string dateTo = "",
            string cn = "", string docnum = "", string ei = "", string docType = "", string terminal = "")
        {
            DateTime now = DateTime.Today.AddDays(1);
            //DateTime xnow = DateTime.Today.AddDays(-1);
            var list = (from c in _entities.CONTAINER
                        where (!c.GATEOUTTIME.HasValue)
                        select c);

            var a = list.Count();

            //var list = (from a in _entities.CONTAINER select a);

            if (!String.IsNullOrEmpty(dateFrom) && String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom =  DateTime.ParseExact(dateFrom, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                list = list.Where(c => c.GATEINTIME >= dFrom);
            }
            else if (String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                list = list.Where(c => c.GATEINTIME < dTo);
            }
            else if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.ParseExact(dateFrom, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                list = list.Where(c => c.GATEINTIME >= dFrom && c.GATEINTIME < dTo);
            }

            if (!String.IsNullOrEmpty(cn))
            {
                list = list.Where(c => c.CONTAINERNUMBER.ToLower().Contains(cn.ToLower()));
            }

            if (!String.IsNullOrEmpty(docnum))
            {
                list = list.Where(c => c.DOCUMENTNUMBER.ToLower().Contains(docnum.ToLower()));
            }

            if (!String.IsNullOrEmpty(ei))
            {
                list = list.Where(c => c.EXPORTIMPORT == ei);
            }

            if (!String.IsNullOrEmpty(docType))
            {
                list = list.Where(c => c.DOCUMENTTYPE.Trim() == docType);
            }

            if (!String.IsNullOrEmpty(terminal))
            {
                list = list.Where(c => c.TERMINALID.Trim() == terminal);
            }

            return list.ToList().Select(c => new ContainerModel
            {
                ID = c.ID,
                EXPORTIMPORT = (c.EXPORTIMPORT == null) ? "" : ExportImport.detail(c.EXPORTIMPORT),
                CONTAINERNUMBER = (c.CONTAINERNUMBER == null) ? "" : c.CONTAINERNUMBER,
                CONTAINERSIZE = (c.CONTAINERSIZE == null) ? "" : c.CONTAINERSIZE,
                TAGNUMBER = c.TAGNUMBER,
                TRUCKNUMBER = c.TRUCKNUMBER,
                GATENUMBER = (c.GATENUMBER == null) ? "" : c.GATENUMBER,
                GATEINTIME = (!c.GATEINTIME.HasValue) ? DateTime.MinValue : (DateTime)c.GATEINTIME,
                GATEOUTTIME = (!c.GATEOUTTIME.HasValue) ? null : c.GATEOUTTIME,
                DOCUMENTNUMBER = (c.DOCUMENTNUMBER == null) ? "" : c.DOCUMENTNUMBER,
                HOLDSTATUS = (c.HOLDSTATUS.Trim() == "H") ? HoldType.type(c.HOLDTYPE) : "",
                SEAL = (c.SEAL == null) ? "" : Seal.kondisi(c.SEAL),
                SEALNOTE = (c.SEALNOTE == null) ? "" : c.SEALNOTE,
                HOLDNOTE = (c.HOLDNOTE == null) ? "" : c.HOLDNOTE,
                RELEASENOTE = (c.RELEASENOTE == null) ? "" : c.RELEASENOTE,
                Note = (c.HOLDTYPE.Trim() == "D") ? "Batal masuk" : "",
                KODEPENYEGELAN = (c.KODEPENYEGELAN == null) ? "" : c.KODEPENYEGELAN,
                BRUTTOWEIGHTS = (Double)c.BRUTTOWEIGHTS,
                ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? "" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
                Action = ArsipAction.getAction(c.ID, c.EXPORTIMPORT),
                TerminalId = c.TERMINALID,
                DOCUMENTTYPE = c.DOCUMENTTYPE
            }).ToList();

            //List<ContainerModel> ret = new List<ContainerModel>();

            //foreach (CONTAINER c in list)
            //{
            //    ret.Add(new ContainerModel
            //    {
            //        ID = c.ID,
            //        EXPORTIMPORT = (c.EXPORTIMPORT == null) ? "" : ExportImport.detail(c.EXPORTIMPORT),
            //        CONTAINERNUMBER = (c.CONTAINERNUMBER == null) ? "" : c.CONTAINERNUMBER,
            //        CONTAINERSIZE = (c.CONTAINERSIZE == null) ? "" : c.CONTAINERSIZE,
            //        TAGNUMBER = c.TAGNUMBER,
            //        TRUCKNUMBER = c.TRUCKNUMBER,
            //        GATENUMBER = (c.GATENUMBER == null) ? "" : c.GATENUMBER,
            //        GATEINTIME = (!c.GATEINTIME.HasValue) ? DateTime.MinValue : (DateTime)c.GATEINTIME,
            //        GATEOUTTIME = (!c.GATEOUTTIME.HasValue) ? null : c.GATEOUTTIME,
            //        DOCUMENTNUMBER = (c.DOCUMENTNUMBER == null) ? "" : c.DOCUMENTNUMBER,
            //        HOLDSTATUS = (c.HOLDSTATUS.Trim() == "H") ? HoldType.type(c.HOLDTYPE) : "",
            //        SEAL = (c.SEAL == null) ? "" : Seal.kondisi(c.SEAL),
            //        SEALNOTE = (c.SEALNOTE == null) ? "" : c.SEALNOTE,
            //        HOLDNOTE = (c.HOLDNOTE == null) ? "" : c.HOLDNOTE,
            //        RELEASENOTE = (c.RELEASENOTE == null) ? "" : c.RELEASENOTE,
            //        Note = (c.HOLDTYPE.Trim() == "D") ? "Batal masuk" : "",
            //        KODEPENYEGELAN = (c.KODEPENYEGELAN == null) ? "" : c.KODEPENYEGELAN,
            //        BRUTTOWEIGHTS = (Double)c.BRUTTOWEIGHTS,
            //        ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? "" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
            //        Action = ArsipAction.getAction(c.ID, c.EXPORTIMPORT)
            //    });
            //}
            //return ret;
        }

        public string GetTotalTransactionSummary(string dateFrom = "", string dateTo = "",
            string cn = "", string docnum = "", string ei = "", string docType = "", string terminal = "")
        {
            var list = (from c in _entities.CONTAINER
                        select c);

            if (!String.IsNullOrEmpty(dateFrom) && String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.Parse(dateFrom);
                list = list.Where(c => c.GATEINTIME >= dFrom);
            }
            else if (String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dTo = DateTime.Parse(dateTo).AddDays(1);
                list = list.Where(c => c.GATEINTIME < dTo);
            }
            else if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                DateTime dFrom = DateTime.Parse(dateFrom);
                DateTime dTo = DateTime.Parse(dateTo).AddDays(1);
                list = list.Where(c => c.GATEINTIME >= dFrom && c.GATEINTIME < dTo);
            }

            if (!String.IsNullOrEmpty(cn))
            {
                list = list.Where(c => c.CONTAINERNUMBER.ToLower().Contains(cn.ToLower()));
            }

            if (!String.IsNullOrEmpty(docnum))
            {
                list = list.Where(c => c.DOCUMENTNUMBER.ToLower().Contains(docnum.ToLower()));
            }


            if (!String.IsNullOrEmpty(ei))
            {
                list = list.Where(c => c.EXPORTIMPORT == ei);
            }

            if (!String.IsNullOrEmpty(docType))
            {
                list = list.Where(c => c.DOCUMENTTYPE.Trim() == docType);
            }

            if (!String.IsNullOrEmpty(terminal))
            {
                list = list.Where(c => c.TERMINALID.Trim() == terminal);
            }

            int import = list.Where(c => c.EXPORTIMPORT == "I").Count();
            int export = list.Where(c => c.EXPORTIMPORT == "E").Count();

            string total = import.ToString() + "-" + export.ToString();
            return total;
        }
//--END SUMMARY--//
        public List<StatisticModel> GetTotalTransaction(string dateFrom = "", string dateTo = "",
            string cn = "", string docnum = "", string ei = "", string docType = "", string terminal = "")
        {
            string r = "R";
            DateTime now = DateTime.Now.AddHours(hoursDiff);
            var list = (from c in _entities.CONTAINER
                        where (c.GATEOUTTIME.HasValue) ||
                        (!c.GATEOUTTIME.HasValue && c.GATEINTIME < now && c.HOLDSTATUS.Trim() != "H") ||
                        (!c.GATEOUTTIME.HasValue && c.GATEINTIME < now && c.HOLDSTATUS.Trim() == "H" && c.HOLDTYPE == "A" && c.EXPORTIMPORT == "E") ||
                        (c.HOLDTYPE.Trim() == "D")
                        select c);

            if (!String.IsNullOrEmpty(dateFrom) && String.IsNullOrEmpty(dateTo))
            {
                //DateTime dFrom = DateTime.Parse(dateFrom);
                DateTime dFrom = DateTime.ParseExact(dateFrom, "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                list = list.Where(c => c.GATEINTIME >= dFrom);
            }
            else if (String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                //DateTime dTo = DateTime.Parse(dateTo).AddDays(1);
                DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                list = list.Where(c => c.GATEINTIME < dTo);
            }
            else if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                //DateTime dFrom = DateTime.Parse(dateFrom);
                //DateTime dTo = DateTime.Parse(dateTo).AddDays(1);
                DateTime dFrom = DateTime.ParseExact(dateFrom, "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                DateTime dTo = DateTime.ParseExact(dateTo, "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                list = list.Where(c => c.GATEINTIME >= dFrom && c.GATEINTIME < dTo && c.GATEOUTTIME != null);
            }

            if (!String.IsNullOrEmpty(cn))
            {
                list = list.Where(c => c.CONTAINERNUMBER.ToLower().Contains(cn.ToLower()));
            }

            if (!String.IsNullOrEmpty(docnum))
            {
                list = list.Where(c => c.DOCUMENTNUMBER.ToLower().Contains(docnum.ToLower()));
            }

            if (!String.IsNullOrEmpty(ei))
            {
                list = list.Where(c => c.EXPORTIMPORT == ei);
            }
            List<StatisticModel> result = new List<StatisticModel>();

            int import = list.Where(c => c.EXPORTIMPORT == "I").Count();
            int export = list.Where(c => c.EXPORTIMPORT == "E").Count();

            result.Add(new StatisticModel { name = "IMPORT", total = import.ToString(), type="1" });
            result.Add(new StatisticModel { name = "EXPORT", total = export.ToString(), type = "1" });

            var data = list.Select(a=> new StatisticModel { name=a.TRANSACTION_ID, total=a.DOCUMENTTYPE.ToUpper()}).Distinct().ToList();
            var groups = data.GroupBy(a => a.total).Select(a => new StatisticModel { name = a.Key.ToString(), total = a.Count().ToString(), type="2" }).ToList();

            result.AddRange(groups);

            //string total = import.ToString() + "-" + export.ToString();
            return result;
        }

        public List<ContainerModel> GetDeleteList(string cn = "", string voyage = "", string mappath="")
        {
            List<ContainerModel> ret = new List<ContainerModel>();
            //DateTime treshold = DateTime.Now.AddHours(hoursDiff);
            if (String.IsNullOrEmpty(cn) && String.IsNullOrEmpty(voyage))
            {
                return ret;
            }
            var list = (from c in _entities.CONTAINER
                        where (c.HOLDTYPE != "D") //&& c.GATEINTIME > treshold
                        select c);

            //var list = (from a in _entities.CONTAINER where a.HOLDTYPE != "D" select a);
            

            if (!String.IsNullOrEmpty(cn))
            {
                list = list.Where(c => c.CONTAINERNUMBER.Contains(cn));
            }

            if (!String.IsNullOrEmpty(voyage))
            {
                list = list.Where(c => c.VOYAGE.Contains(voyage));
            }

            list = list.OrderByDescending(c => c.GATEINTIME);


            foreach (CONTAINER c in list)
            {
                ret.Add(new ContainerModel
                {
                    ID = c.ID,
                    ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? "" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
                    GATEINTIME = (c.GATEINTIME == null) ? DateTime.MinValue : Convert.ToDateTime(c.GATEINTIME),
                    VOYAGE = (c.VOYAGE == null) ? "" : c.VOYAGE,
                    //Action = DeleteAction.getAction(c.ID, c.CONTAINERNUMBER, c.EXPORTIMPORT,mappath)
                    Action = DeleteAction.getActionNew(c.ID, c.CONTAINERNUMBER, c.EXPORTIMPORT)
                });
            }
            return ret;

            //return list.ToList().Select(c => new ContainerModel
            //{
            //    ID = c.ID,
            //    ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? "" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
            //    GATEINTIME = (c.GATEINTIME == null) ? DateTime.MinValue : Convert.ToDateTime(c.GATEINTIME),
            //    VOYAGE = (c.VOYAGE == null) ? "" : c.VOYAGE,
            //    Action = DeleteAction.getAction(c.ID, c.CONTAINERNUMBER, c.EXPORTIMPORT)
            //}
            //).ToList();
        }

        public List<ContainerModel> GetSearchList(string search,string username)
        {
            string p2 = "P";
            string p2a = "AP";
            search = search.Trim().ToLower();
            string[] roles = Roles.GetRolesForUser(username);
            bool canHoldManual = roles.Contains("ControlRoom");
            bool canHoldAutoE = canHoldManual;
            bool canHoldP2 = roles.Contains("P2");
            bool canHoldAutoI = roles.Contains("Penyegelan");
            DateTime now = DateTime.Now.AddHours(hoursDiff);
            var list = (from c in _entities.CONTAINER
                        where
                        c.CONTAINERNUMBER.ToLower().Contains(search) ||
                        c.CONSIGNEE.ToLower().Contains(search) ||
                        c.DOCUMENTNUMBER.ToLower().Contains(search) 
                        select c);
            if (roles.Count() == 1 && roles.Contains("Penyegelan"))
            {
                list = list.Where(c => c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT.Trim() == "I" && c.HOLDSTATUS.Trim() != "R");
            }
            return list.ToList().Select(c => new ContainerModel
            {
                ID = c.ID,
                CONTAINERNUMBER = (c.CONTAINERNUMBER == null) ? "" : c.CONTAINERNUMBER,
                CONTAINERSIZE = (c.CONTAINERSIZE == null) ? "" : c.CONTAINERSIZE,
                TAGNUMBER = c.TAGNUMBER,
                TRUCKNUMBER = c.TRUCKNUMBER,
                TRUCKPOSITION = updateTruckPosition(c),
                GATENUMBER = (c.GATENUMBER == null) ? "" : c.GATENUMBER,
                GATEINTIME = (c.GATEINTIME == null) ? DateTime.Now : (DateTime)c.GATEINTIME,
                DOCUMENTNUMBER = (c.DOCUMENTNUMBER == null) ? "" : c.DOCUMENTNUMBER,
                CONSIGNEE = c.CONSIGNEE,
                HOLDTYPE = (c.HOLDTYPE == null) ? "" : HoldType.type(c.HOLDTYPE),
                HOLDSTATUS = (c.HOLDSTATUS.Trim() == "H") ? HoldType.type(c.HOLDTYPE) : "",
                Note = (c.HOLDSTATUS.Trim() == "H") ? c.HOLDNOTE : c.RELEASENOTE,
                SEAL = (c.SEAL == null) ? "" : Seal.kondisi(c.SEAL),
                SEALNOTE = (c.SEALNOTE == null) ? "" : c.SEALNOTE,
                KODEPENYEGELAN = (c.KODEPENYEGELAN == null) ? "" : c.KODEPENYEGELAN,
                CHECKEDBY = c.CHECKEDBY,
                CHECKEDTIME = c.CHECKEDTIME.GetValueOrDefault(),
                StrCheckedTime = (c.CHECKEDTIME.HasValue == true) ? c.CHECKEDTIME.Value.ToString("dd-MM-yyy") : "",
                EXPORTIMPORT = (c.EXPORTIMPORT == null) ? "" : ExportImport.detail(c.EXPORTIMPORT),
                ContainerAndSize = (c.CONTAINERNUMBER == null || c.CONTAINERSIZE == null) ? "" : c.CONTAINERNUMBER + "/" + c.CONTAINERSIZE,
                Action = (canHoldP2 && (c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2a) || (canHoldAutoE && c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT == "E" || canHoldAutoI && c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT == "I") || c.HOLDTYPE.Trim() == "M") ? HoldReleaseAction.getAction(c.ID, c.EXPORTIMPORT, c.HOLDSTATUS, c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2a) : HoldReleaseAction.getDetailOnly(c.ID, c.EXPORTIMPORT)
            }).ToList();
        }

        public CONTAINER Get(int id)
        {
            var model = (from c in _entities.CONTAINER
                         where c.ID == id
                         select c).FirstOrDefault();
            return model;
        }

        public CONTAINER_X GetX(int id)
        {
            var model = (from c in _entities.CONTAINER_X
                         where c.ID == id
                         select c).FirstOrDefault();
            return model;
        }


        public ATENSI_P2 GetP2(int cid)
        {
            var model = (from a in _entities.ATENSI_P2
                         where a.CONTAINER_ID == cid
                         select a).FirstOrDefault();
            return model;
        }

        public CONTAINER GetBatalMuat(CONTAINER container)
        {
            if (container.DOCUMENTTYPE.Trim().ToLower() != "sppbe") return null;
            var model = (from c in _entities.CONTAINER
                         where c.CONTAINERNUMBER.Trim() == container.CONTAINERNUMBER.Trim()
                         && c.EXPORTIMPORT.Trim()=="E"
                         orderby c.ID descending
                         select c).FirstOrDefault();
            return model;
        }

        public CONTAINER_X GetBatalMuatX(CONTAINER_X container)
        {
            if (container.DOCUMENTTYPE.Trim().ToLower() != "sppbe") return null;
            var model = (from c in _entities.CONTAINER_X
                         where c.CONTAINERNUMBER.Trim() == container.CONTAINERNUMBER.Trim()
                         && c.EXPORTIMPORT.Trim() == "E"
                         orderby c.ID descending
                         select c).FirstOrDefault();
            return model;
        }

        public String Check(int id,string username)
        {
            var model = (from c in _entities.CONTAINER
                         where c.ID == id
                         select c).FirstOrDefault();
            if (model == null) return "Failed";
            try
            {
                model.CHECKEDBY = username;
                model.CHECKEDTIME = DateTime.Now;
                _entities.SaveChanges();
                USERACTIVITY dataActivity = new USERACTIVITY();

                dataActivity.USERNAME = username;
                dataActivity.ACTIVITYDESCR = "User : " + username + ", Check Container : " + model.CONTAINERNUMBER + ", No.Polisi : " + model.TRUCKNUMBER.Trim() + ", Tag: " + model.TAGNUMBER.Trim() + ", Tanggal : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm");
                dataActivity.HOLD = "-";
                dataActivity.RELEASE = "-";
                dataActivity.CHECKCONTAINER = "C";
                dataActivity.ADDATENSI = "-";
                dataActivity.EDITATENSI = "-";
                dataActivity.CONFIRMP2 = "-";
                dataActivity.ADDPUTUSAN = "-";
                dataActivity.DELETETRX = "-";
                
                activityrepo.saveActivity(dataActivity);
            }
            catch (Exception e)
            {
                return e.Message;
            }
            return "Success";
        }

        public CONTAINER GetNext(int id, string username, string type)
        {
            return GetNextPrev(id, username, type);
        }

        public CONTAINER_X GetNextX(int id, string username, string type)
        {
            return GetNextPrevX(id, username, type);
        }

        public CONTAINER GetPrev(int id, string username, string type)
        {
            return GetNextPrev(id, username, type,0);
        }

        public CONTAINER_X GetPrevX(int id, string username, string type)
        {
            return GetNextPrevX(id, username, type, 0);
        }

        public CONTAINER GetNextPrev(int id,string username,string type,int next=1)
        {
            string p2 = "P";
            string p2a = "AP";

            string[] roles = Roles.GetRolesForUser(username);
            //canHoldP2 = true;
            //canHoldManual = true;
            DateTime treshold = DateTime.Now.AddHours(hoursDiff);

            IQueryable<CONTAINER> list = (from c in _entities.CONTAINER
                        where (c.EXPORTIMPORT.Trim() == type && !c.GATEOUTTIME.HasValue && c.GATEINTIME >= treshold) ||
                        (c.EXPORTIMPORT.Trim() == type)
                        //&& c.HOLDSTATUS.Trim() == "H")
                        orderby c.ID
                        select c);
            if (type == "E")
            {
                list = list.Where(c => c.HOLDTYPE != "A");
            }

            if (roles.Count() == 1 && roles.Contains("Penyegelan"))
            {
                list = list.Where(c => c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT.Trim() == "I" && c.HOLDSTATUS.Trim() != "R");
            }


            if (type == "P2")
            {
                list = (from c in _entities.CONTAINER
                        where (c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2a) && c.GATEOUTTIME == null && (c.GATEINTIME >= treshold || c.HOLDSTATUS.Trim() == "H")
                            orderby c.ID
                            select c);
            }
            CONTAINER model;
            if (next == 1)
            {
                model = list.Where(c => c.ID > id).FirstOrDefault();
            }
            else
            {
                model = list.OrderByDescending(c=>c.ID).Where(c => c.ID < id).FirstOrDefault();
            }
            return model;
        }

        public CONTAINER_X GetNextPrevX(int id, string username, string type, int next = 1)
        {
            string p2 = "P";
            string p2a = "AP";

            string[] roles = Roles.GetRolesForUser(username);
            //canHoldP2 = true;
            //canHoldManual = true;
            DateTime treshold = DateTime.Now.AddHours(hoursDiff);

            IQueryable<CONTAINER_X> list = (from c in _entities.CONTAINER_X
                                          where (c.EXPORTIMPORT.Trim() == type && !c.GATEOUTTIME.HasValue && c.GATEINTIME >= treshold) ||
                                          (c.EXPORTIMPORT.Trim() == type)
                                          //&& c.HOLDSTATUS.Trim() == "H")
                                          orderby c.ID
                                          select c);
            if (type == "E")
            {
                list = list.Where(c => c.HOLDTYPE != "A");
            }

            if (roles.Count() == 1 && roles.Contains("Penyegelan"))
            {
                list = list.Where(c => c.HOLDTYPE.Trim() == "A" && c.EXPORTIMPORT.Trim() == "I" && c.HOLDSTATUS.Trim() != "R");
            }


            if (type == "P2")
            {
                list = (from c in _entities.CONTAINER_X
                        where (c.HOLDTYPE.Trim() == p2 || c.HOLDTYPE.Trim() == p2a) && c.GATEOUTTIME == null && (c.GATEINTIME >= treshold || c.HOLDSTATUS.Trim() == "H")
                        orderby c.ID
                        select c);
            }
            CONTAINER_X model;
            if (next == 1)
            {
                model = list.Where(c => c.ID > id).FirstOrDefault();
            }
            else
            {
                model = list.OrderByDescending(c => c.ID).Where(c => c.ID < id).FirstOrDefault();
            }
            return model;
        }

        public bool hasBrokenContainer(string username)
        {
            string[] roles = Roles.GetRolesForUser(username);
            if (!roles.Contains("Penyegelan")) return false;
            var model = (from c in _entities.CONTAINER
                         where c.SEAL.Trim() == "R" && c.HOLDSTATUS.Trim() != "R"
                         select c).ToList();
            return model.Count>0;
        }

        public List<DocumentType> getDocumentTypes()
        {


            List<DocumentType> ret = (from c in _entities.CONTAINER
                                                      group c by new { c.DOCUMENTTYPE, c.EXPORTIMPORT }
                                                          into grp
                                                          select new DocumentType
                                                          {
                                                              EI = grp.Key.EXPORTIMPORT,
                                                              Type = grp.Key.DOCUMENTTYPE
                                                          }).ToList();
            return ret;
        }

        public List<string> getTerminals()
        {
            return _entities.CONTAINER.GroupBy(c => c.TERMINALID).Select(c => c.Key).ToList();
        }

        public bool Delete(int id, string username)
        {
            var model = (from c in _entities.CONTAINER
                         where c.ID == id
                         select c).FirstOrDefault();
            if (model == null) return false;
            try
            {
                model.HOLDTYPE = "D";
                model.CONTAINERNUMBER = model.CONTAINERNUMBER;
                if (String.IsNullOrEmpty(model.GATEOUTTIME.ToString()))
                    model.GATEOUTTIME = DateTime.Now;

                _entities.SaveChanges();
                _entities.DELETE_CONTAINER();
                USERACTIVITY activityuser = new USERACTIVITY();
                activityuser.USERNAME = username;
                activityuser.HOLD = "-";
                activityuser.ACTIVITYDESCR = "User : " + username + ", menghapus data container : " + model.CONTAINERNUMBER + ", No.Polisi : " + model.TRUCKNUMBER + ", No. Tag" + model.TAGNUMBER + ", Tanggal : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm");
                activityuser.ADDATENSI = "-";
                activityuser.ADDPUTUSAN = "-";
                activityuser.CHECKCONTAINER = "-";
                activityuser.CONFIRMP2 = "-";
                activityuser.DELETETRX = "D";
                activityuser.EDITATENSI = "-";
                activityuser.RELEASE = "-";
                activityrepo.saveActivity(activityuser);

            }
            catch (Exception e)
            {
                return false;
            }
            return true;

            //_entities.DeleteObject(model);
            //return _entities.SaveChanges() != 0;
        }

        public bool DeleteNew(int id, string username, string deleteNote)
        {
            var model = (from c in _entities.CONTAINER
                         where c.ID == id
                         select c).FirstOrDefault();
            if (model == null) return false;
            try
            {
                //model.HOLDTYPE = "D";
                //model.CONTAINERNUMBER = model.CONTAINERNUMBER;
                //if (String.IsNullOrEmpty(model.GATEOUTTIME.ToString()))
                //    model.GATEOUTTIME = DateTime.Now;

                CONTAINER_X rowDeleted = new CONTAINER_X();
                rowDeleted.CONTAINER_ID = model.ID;
                rowDeleted.DELETE_TIME = DateTime.Now;
                rowDeleted.DELETE_BY = username;
                rowDeleted.DELETE_NOTE = deleteNote;

                rowDeleted.CONTAINERNUMBER = model.CONTAINERNUMBER;
                rowDeleted.GATEINTIME = model.GATEINTIME;
                rowDeleted.TRUCKNUMBER = model.TRUCKNUMBER;
                rowDeleted.TRUCKCOMPANYNAME = model.TRUCKCOMPANYNAME;
                rowDeleted.TRUCKPOSITION = model.TRUCKPOSITION;
                rowDeleted.VESSELNAME = model.VESSELNAME;
                rowDeleted.VOYAGE = model.VOYAGE;
                rowDeleted.ATA = model.ATA;
                rowDeleted.ATD = model.ATD;
                rowDeleted.PORTDISCHARGE = model.PORTDISCHARGE;
                rowDeleted.HOLDREMARK = model.HOLDREMARK;
                rowDeleted.TAGNUMBER = model.TAGNUMBER;
                rowDeleted.GATENUMBER = model.GATENUMBER;
                rowDeleted.CONTAINERSIZE3 = model.CONTAINERSIZE3;
                rowDeleted.EXPORTIMPORT = model.EXPORTIMPORT;
                rowDeleted.HOLDSTATUS = model.HOLDSTATUS;
                rowDeleted.HOLDTYPE = model.HOLDTYPE;
                rowDeleted.DOCUMENTTYPE = model.DOCUMENTTYPE;
                rowDeleted.DOCUMENTNUMBER = model.DOCUMENTNUMBER;
                rowDeleted.DOCUMENTDATE = model.DOCUMENTDATE;
                rowDeleted.CONSIGNEE = model.CONSIGNEE;
                rowDeleted.INTELLIGENCENOTE = model.INTELLIGENCENOTE;
                rowDeleted.BLNUMBER = model.BLNUMBER;
                rowDeleted.BCNUMBER = model.BCNUMBER;
                rowDeleted.KPBCCODE = model.KPBCCODE;
                rowDeleted.TRANSACTION_ID = model.TRANSACTION_ID;
                rowDeleted.HOLDTIME = model.HOLDTIME;
                rowDeleted.HOLDNOTE = model.HOLDNOTE;
                rowDeleted.HOLDBY = model.HOLDBY;
                rowDeleted.RELEASETIME = model.RELEASETIME;
                rowDeleted.RELEASENOTE = model.RELEASENOTE;
                rowDeleted.RELEASEBY = model.RELEASEBY;
                rowDeleted.SEAL = model.SEAL;
                rowDeleted.SEALNOTE = model.SEALNOTE;
                rowDeleted.GATEOUTTIME = model.GATEOUTTIME;
                rowDeleted.CHECKEDBY = model.CHECKEDBY;
                rowDeleted.CHECKEDTIME = model.CHECKEDTIME;
                rowDeleted.KODEPENYEGELAN = model.KODEPENYEGELAN;
                rowDeleted.BRUTTOWEIGHTS = model.BRUTTOWEIGHTS;
                rowDeleted.CONTAINERSIZE = model.CONTAINERSIZE;
                rowDeleted.TERMINALID = model.TERMINALID;

                _entities.CONTAINER_X.AddObject(rowDeleted);
                _entities.CONTAINER.DeleteObject(model);

                _entities.SaveChanges();
                //_entities.DELETE_CONTAINER();
                USERACTIVITY activityuser = new USERACTIVITY();
                activityuser.USERNAME = username;
                activityuser.HOLD = "-";
                activityuser.ACTIVITYDESCR = "User : " + username + ", menghapus data container : " + model.CONTAINERNUMBER + ", No.Polisi : " + model.TRUCKNUMBER + ", No. Tag" + model.TAGNUMBER + ", Tanggal : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm");
                activityuser.ADDATENSI = "-";
                activityuser.ADDPUTUSAN = "-";
                activityuser.CHECKCONTAINER = "-";
                activityuser.CONFIRMP2 = "-";
                activityuser.DELETETRX = "D";
                activityuser.EDITATENSI = "-";
                activityuser.RELEASE = "-";
                activityrepo.saveActivity(activityuser);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;

            //_entities.DeleteObject(model);
            //return _entities.SaveChanges() != 0;
        }

        public string Hold(HoldModel model, string username)
        {

            string p2 = "P";
            string p2a= "AP";
            var container = (from c in _entities.CONTAINER
                             where c.ID == model.ID
                             select c).FirstOrDefault();
            if (container.HOLDSTATUS == "H")
            {
                return "Already been hold, please reload page";
            }
            else
            {
                bool canHold = false;
                string[] roles = Roles.GetRolesForUser(username);
                if (container.HOLDTYPE.Trim() == p2 || container.HOLDTYPE.Trim() == p2a)
                {
                    canHold = roles.Contains("P2");
                }
                else
                {
                    canHold = roles.Contains("ControlRoom");
                }
                canHold = true;
                if (!canHold) return "Insufficient privilege";
                try
                {
                    container.HOLDSTATUS = "H";
                    container.HOLDNOTE = model.HOLDNOTE;
                    container.HOLDTIME = DateTime.Now;
                    container.HOLDBY = username;
                    if (container.HOLDTYPE.Trim() == p2|| container.HOLDTYPE.Trim() == p2a)
                    {
                        CON_LISTCONT containerInAutogate = (from c in _autogateEntities.CON_LISTCONT
                                                            where c.TAR == container.TRANSACTION_ID.Trim()
                                                            select c).FirstOrDefault();
                        string call_sign = "";
                        call_sign = (from v in _autogateEntities.VES_VOYAGE
                                     where v.ID_VES_VOYAGE == containerInAutogate.ID_VES_VOYAGE
                                     select v.ID_VESSEL).FirstOrDefault();

                        MessageResponse response = CustomsBlockPost.Post(container.EXPORTIMPORT,
                            container.CONTAINERNUMBER,
                            call_sign,
                            container.VOYAGE.Trim(),
                            "HOLD");
                    }
                    _entities.SaveChanges();
                }
                catch (Exception e)
                {
                    return e.Message;
                }
                return "Success";
            }
        }

        public string ReleaseP2(ReleaseModel model, string username,HttpRequestBase req)
        {
            FileLogger logger = new FileLogger();
            string ret = "";
            if (req != null)
            {

                ATENSI_P2 p2 = (from s in _entities.ATENSI_P2
                                where
                                s.ID == model.ID
                                select s).FirstOrDefault();
                try
                {
                    bool hasFile = false;
                    if (req.Files == null)
                        return "Error File harus diisi";
                    else
                    foreach (string file in req.Files)
                    {
                        var hpf = req.Files[file];
                        if (hpf.ContentLength == 0)
                        {
                            ret = "File harus diisi";
                            continue;
                        }
                        hasFile = true;
                        string p2FileName = AtensiP2File.generateFileName(model.ID, Path.GetExtension(hpf.FileName), "Release");

                        hpf.SaveAs(p2FileName);

                        P2FILE p2file;

                        p2file = (from p in _entities.P2FILE
                                  where p.CONTAINER_ID == model.ID
                                  orderby p.ID descending
                                  select p).FirstOrDefault();
                        bool addReleaseFile = false;
                        if (p2file == null)
                        {
                            addReleaseFile = true;
                            p2file = new P2FILE();
                            int id = 0;
                            if (_entities.P2FILE.Count() > 0)
                            {
                                id = (from e in _entities.P2FILE
                                      select e.ID).Max();
                            }
                            id += 1;
                            p2file.ID = id;
                        }

                        p2file.RELEASEPATH = p2FileName;
                        p2file.CONTAINER_ID = p2.CONTAINER_ID;
                        p2file.P2ID = model.ID;

                        try
                        {
                            if (addReleaseFile)
                                _entities.AddToP2FILE(p2file);
                            _entities.SaveChanges();
                            ret = "Success";
                        }
                        catch (Exception ex)
                        {
                            ret = ex.Message;
                        }
                    }

                    if (hasFile)
                    {
                        model.ID = p2.CONTAINER_ID.GetValueOrDefault();

                        ServicePointManager
                               .ServerCertificateValidationCallback +=
                               (sender, cert, chain, sslPolicyErrors) => true;
                        ServicePointManager.Expect100Continue = true;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                             (SecurityProtocolType)(0xc0 | 0x300 | 0xc00)
                               | SecurityProtocolType.Ssl3;
                        CONTAINER containerItem = new CONTAINER();
                        containerItem.CONTAINERNUMBER = p2.CONTAINERNUMBER;
                        containerItem.TRANSACTION_ID = "atensi_" + p2.ID;
                        containerItem.VESSELNAME = "";
                        containerItem.EXPORTIMPORT = p2.EXPORTIMPORT;
                        containerItem.ID = 0;
                        try
                        {
                            MessageResponse resp = CustomsBlockPost.Request(containerItem, p2, "", "RELEASE");
                            string msgRet = resp.Result;
                            logger.Debug(containerItem.CONTAINERNUMBER + "- RELEASE: " + ret);
                            
                            p2.TERMINALRESPONSE = ret + " - RELEASE (" + DateTime.Now.ToString("dd-MM-yyyy HH:mm") + ")";
                            if (msgRet.ToLower().Contains("success"))
                            {

                                p2.ISACTIVE = 0;
                                p2.RELEASEBY = username;
                                p2.RELEASENOTE = model.RELEASENOTE;
                                p2.RELEASEDATE = DateTime.Now;
                            }
                            if (ret.ToLower().Contains("success"))
                            {
                                ret = msgRet;
                            }
                            else
                            {
                                ret += " - "+msgRet;
                            }
                            _entities.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            p2.TERMINALRESPONSE = ex.Message + " - RELEASE (" + DateTime.Now.ToString("dd-MM-yyyy HH:mm") + ")";

                            logger.Debug(containerItem.CONTAINERNUMBER + "- RELEASE: " + ex.Message);
                            _entities.SaveChanges();
                        }
                    }
                    else
                        ret = "File harus diisi"; 

                }
                catch (Exception ex)
                {
                    return "Error Upload File" + ex.Message;
                }
            }
            else
            {
                ret = "File harus diisi";
            }
            return ret;
        }

        public string Release(ReleaseModel model, string username)
        {
            var container = (from c in _entities.CONTAINER
                             where c.ID == model.ID
                             select c).FirstOrDefault();
            if (container != null)
            {
                if (container.HOLDSTATUS == "R")
                {
                    return "Already Released, please reload page";
                }
                else
                {
                    bool canHold = false;
                    string[] roles = Roles.GetRolesForUser(username);
                    if (container.HOLDTYPE.Trim() == "P" || container.HOLDTYPE.Trim() == "AP")
                    {
                        canHold = roles.Contains("P2");
                    }
                    else
                    {
                        canHold = roles.Contains("ControlRoom");
                    }
                    canHold = true;
                    if (!canHold) return "Insufficient privilege";
                    try
                    {
                        if (container.HOLDTYPE.Trim() == "P" || container.HOLDTYPE.Trim() == "AP")
                        {
                            CON_LISTCONT containerInAutogate = (from c in _autogateEntities.CON_LISTCONT
                                                                where c.TAR == container.TRANSACTION_ID.Trim()
                                                                select c).FirstOrDefault();
                            string call_sign = "";
                            call_sign = (from v in _autogateEntities.VES_VOYAGE
                                         where v.ID_VES_VOYAGE == containerInAutogate.ID_VES_VOYAGE
                                         select v.ID_VESSEL).FirstOrDefault();
                            MessageResponse response = CustomsBlockPost.Post(container.EXPORTIMPORT,
                                container.CONTAINERNUMBER,
                                call_sign,
                                container.VOYAGE.Trim(),
                                "RELEASE");

                            if (response.Result == "Success" || true)
                            {
                                container.HOLDSTATUS = "R";
                                container.RELEASENOTE = model.RELEASENOTE;
                                container.RELEASETIME = DateTime.Now;
                                container.RELEASEBY = username;
                                if (container.TRUCKPOSITION != null && container.TRUCKPOSITION.Contains("On Hold"))
                                {
                                    container.TRUCKPOSITION = "On Gate";
                                }
                                //container.GATEOUTTIME = null;

                                var atensi = (from a in _entities.ATENSI_P2
                                              where a.CONTAINER_ID == container.ID
                                              select a).FirstOrDefault();
                                atensi.ISACTIVE = 0;
                                atensi.RELEASEBY = username;
                                atensi.RELEASENOTE = model.RELEASENOTE;
                                atensi.RELEASEDATE = DateTime.Now;

                                _entities.SaveChanges();
                            }
                            else
                            {
                                if (container.HOLDTYPE.Trim() == "PR")
                                {
                                    container.HOLDSTATUS = "R";
                                    container.RELEASENOTE = model.RELEASENOTE;
                                    container.RELEASETIME = DateTime.Now;
                                    container.RELEASEBY = username;
                                    //container.GATEOUTTIME = null;
                                    _entities.SaveChanges();
                                }
                                else
                                {

                                    return response.Result;
                                }
                            }
                        }

                        if (container.HOLDTYPE == "AP")
                        {
                            container.HOLDSTATUS = "H";
                            container.HOLDTYPE = "A";
                        }
                        else
                        {
                            container.HOLDSTATUS = "R";
                        }
                        container.RELEASENOTE = model.RELEASENOTE;
                        container.RELEASETIME = DateTime.Now;
                        container.RELEASEBY = username;
                        //container.GATEOUTTIME = null;
                        _entities.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        return e.Message;
                    }
                    return "Success";
                }
            }
            else
            {
                return "No Container";
            }
        }

        public string Putusan(PutusanModel model, string username)
        {
            var container = (from c in _entities.CONTAINER
                             where c.ID == model.ID
                             select c).FirstOrDefault();
            if (container.HOLDSTATUS == "R" && false)
            {
                return "Already Released, please reload page";
            }
            else
            {
                try
                {
                    PUTUSANCEKSEGEL putusan = new PUTUSANCEKSEGEL();

                    int id = 0;
                    if (_entities.PUTUSANCEKSEGEL.Count() > 0)
                        id = _entities.PUTUSANCEKSEGEL.Max(a => a.ID);
                    putusan.ID = id + 1;
                    putusan.CONTAINER_ID = container.ID;
                    putusan.NOTE = model.NOTE;
                    putusan.USER_ID = username;
                    putusan.DATETIME = DateTime.Now;

                    if (model.Utuh)
                    {
                        container.HOLDSTATUS = "R";
                        container.SEAL = "U";
                        putusan.SEAL = "U";
                    }
                    else
                    {
                        if (container.HOLDSTATUS.Trim() == "R")
                        {
                            container.HOLDTYPE = "M";
                            container.HOLDNOTE = model.NOTE;
                        }
                        container.HOLDSTATUS = "H";
                        container.SEAL = "R";
                        putusan.SEAL = "R";
                    }
                    container.SEALNOTE = model.NOTE;
                    container.KODEPENYEGELAN = model.KODEPENYEGELAN;

                    _entities.AddToPUTUSANCEKSEGEL(putusan);
                    _entities.SaveChanges();
                }
                catch (Exception e)
                {
                    return e.Message;
                }
                return "Success";
            }
        }
    }
}