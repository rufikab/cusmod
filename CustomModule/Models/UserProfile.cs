﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;

namespace CustomModule.Models
{
    public class UserProfile : ProfileBase
    {
        public static UserProfile GetUserProfile(string username)
        {
            return Create(username) as UserProfile;
        }
        public static UserProfile GetUserProfile()
        {
            return Create(Membership.GetUser().UserName) as UserProfile;
        }

        [SettingsAllowAnonymous(false)]
        [ProfileProvider("OracleProfileProvider")]
        public string NIP
        {
            get { return base["NIP"].ToString(); }
            set { base["NIP"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        [ProfileProvider("OracleProfileProvider")]
        public string Name
        {
            get { return base["Name"].ToString(); }
            set { base["Name"] = value; }
        }
    }
}