﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomModule.Models
{
    public class MessageResponse
    {
        public string Result { get; set; }
        public MessageException messageException { get; set; }

        public MessageResponse()
        {
            messageException = new MessageException();
        }
    }

    public class MessageException
    {
        public string errorCode { get; set; }

        public string errorMessage { get; set; }

        public string errorParam1 { get; set; }

        public string errorParam2 { get; set; }

        public string errorParam3 { get; set; }

        public string tag { get; set; }
    }
}