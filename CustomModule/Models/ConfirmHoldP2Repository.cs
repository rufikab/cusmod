﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.IO;
using CustomModule.Helper;
using System.Text;
using System.Xml;

namespace CustomModule.Models
{
    public class ConfirmHoldP2Repository:BaseRepository
    {
        private string _xmlDir = System.Configuration.ConfigurationManager.AppSettings["messageXMLDirName"];
        private string _messageHoldUrl = System.Configuration.ConfigurationManager.AppSettings["messageHoldUrl"];
        private string _virtualPath = System.Configuration.ConfigurationManager.AppSettings["VirtualPath"];
        protected AutogateEntities _autogateEntities = new AutogateEntities();
        private MessageResponse ParseMessageHoldResponse(string xml)
        {
            MessageResponse result = new MessageResponse();
            XmlDocument xDoc = new XmlDocument();

            xDoc.LoadXml(xml);

            if (xDoc.InnerText!=null)
            {
                MessageResponse resultItem = new MessageResponse();
                result.Result = xDoc.InnerText;
            }
            else
            {
                MessageException obj = new MessageException();
                obj.errorCode = "404";
                obj.errorMessage = "NotFound";
                result.messageException = obj;
            }

            return result;
        }

        public bool ConfirmP2Data(string username, int id)
        {
            MessageResponse response = new MessageResponse();

            UserActivityRepository activityRepo = new UserActivityRepository();
            ATENSI_P2 atensi = (from f in _entities.ATENSI_P2 where f.ID == id select f).SingleOrDefault();
            bool ret = true;
            atensi.CONTAINERNUMBER = atensi.CONTAINERNUMBER.Trim();
            try
            {
                USERACTIVITY activityuser = new USERACTIVITY();
                activityuser.USERNAME = username;
                activityuser.HOLD = "-";
                activityuser.ACTIVITYDESCR = "User : " + username + ", Melakukan konfirmasi Data Atensi dengan data container : " +
                    atensi.CONTAINERNUMBER + ", Nomor Dokumen : " + atensi.DOCUMENTNUMBER + ", Tanggal : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm");
                activityuser.ADDATENSI = "-";
                activityuser.ADDPUTUSAN = "-";
                activityuser.CHECKCONTAINER = "-";
                activityuser.CONFIRMP2 = "C";
                activityuser.DELETETRX = "-";
                activityuser.EDITATENSI = "-";
                activityuser.RELEASE = "-";
                activityRepo.saveActivity(activityuser);

                CONTAINER containerInPort = (from c in _entities.CONTAINER
                                                   where c.CONTAINERNUMBER.Trim() == atensi.CONTAINERNUMBER 
                                                   orderby c.ID descending
                                                   select c).FirstOrDefault();
                CON_LISTCONT containerInAutogate = (from c in _autogateEntities.CON_LISTCONT
                                                    where c.TAR == containerInPort.TRANSACTION_ID.Trim()
                                                    select c).FirstOrDefault();
                if (containerInAutogate != null)
                {
                    string status = "OTW";
                    if (containerInPort.EXPORTIMPORT == "I" || containerInAutogate.TRUCKIN_TERMINAL != null)
                    {
                        status = "HOLD";
                    }

                    P2FILE p2File = (from p in _entities.P2FILE
                                     where p.P2ID == atensi.ID
                                     orderby p.ID descending
                                     select p).FirstOrDefault();
                    string file = "";
                    if (p2File != null)
                    {
                        file = "/AtensiP2/GetP2File/" + containerInPort.ID;
                    }


                    string call_sign = "";
                    call_sign = (from v in _autogateEntities.VES_VOYAGE
                                 where v.ID_VES_VOYAGE == containerInAutogate.ID_VES_VOYAGE
                                 select v.ID_VESSEL).FirstOrDefault();


                    response = CustomsBlockPost.Request(containerInPort,atensi,
                        call_sign,
                        status);

                    if (response.Result == CustomsBlockPost.SUCCESS_MSG
                        || response.Result == CustomsBlockPost.ALREADY_BLOCKED_MSG)
                    {
                        atensi.ISCONFIRMED = 1;
                        atensi.CONTAINER_ID = containerInPort.ID;
                        containerInPort.HOLDTYPE = "P";
                        containerInPort.HOLDBY = atensi.INPUTBY.Trim();
                        containerInPort.HOLDTIME = DateTime.Now;
                    }
                }

                _entities.SaveChanges();
            }
            catch 
            {
                ret = false;
            }
            return ret;
        }
        public List<ConfirmListHoldP2ViewModel> FillItemList(object filter,string username, int page = 1,string mappath=null)
        {
            List<ATENSI_P2> ConfirmP2List = new List<ATENSI_P2>();
            IQueryable<ATENSI_P2> ConfirmP2IEnumerable;
            List<ConfirmListHoldP2ViewModel> ConfirmP2ItemList = new List<ConfirmListHoldP2ViewModel>();

            if (filter != null)
            {
                try
                {
                    String[] filterData = filter.ToString().Split(new String[] { "##" }, StringSplitOptions.None);

                    string ContainerID = (filterData[1].Equals("NoDataInput")) ? "" : filterData[1];
                    String DocNumber = (filterData[2].Equals("NoDataInput")) ? "" : filterData[2];
                    if (ContainerID == "" && DocNumber == "")
                    {
                        ConfirmP2IEnumerable = (from s in _entities.ATENSI_P2
                                                join c in _entities.CONTAINER on s.CONTAINERNUMBER.Trim() equals c.CONTAINERNUMBER.Trim()
                                                where s.ISACTIVE == 1 && c.HOLDSTATUS.Trim() != "R" && (c.HOLDTYPE.Trim() == "PR" || c.HOLDTYPE.Trim() == "P")
                                                select s);
                    }
                    else if (ContainerID == "" && DocNumber != "")
                    {
                        ConfirmP2IEnumerable = (from s in _entities.ATENSI_P2
                                                join c in _entities.CONTAINER on s.CONTAINERNUMBER.Trim() equals c.CONTAINERNUMBER
                                                where s.DOCUMENTNUMBER.ToLower().Contains(DocNumber.Trim().ToLower()) &&
                                                s.ISACTIVE == 1 && c.HOLDSTATUS.Trim() != "R" && (c.HOLDTYPE.Trim() == "PR" || c.HOLDTYPE.Trim() == "P")
                                                select s);
                    }
                    else if (ContainerID != "" && DocNumber == "")
                    {
                        ConfirmP2IEnumerable = (from s in _entities.ATENSI_P2
                                                join c in _entities.CONTAINER on s.CONTAINERNUMBER.Trim() equals c.CONTAINERNUMBER
                                                where s.CONTAINERNUMBER.ToLower().Trim().Contains(ContainerID.Trim().ToLower()) &&
                                                s.ISACTIVE == 1 && c.HOLDSTATUS.Trim() != "R" && (c.HOLDTYPE.Trim() == "PR" || c.HOLDTYPE.Trim() == "P")
                                                select s);
                    }
                    else
                    {
                        ConfirmP2IEnumerable = (from s in _entities.ATENSI_P2
                                                join c in _entities.CONTAINER on s.CONTAINERNUMBER.Trim() equals c.CONTAINERNUMBER.Trim()
                                                where s.DOCUMENTNUMBER.ToLower().Contains(DocNumber.Trim().ToLower()) &&
                                                s.CONTAINERNUMBER.ToLower().Contains(ContainerID.ToLower().Trim()) &&
                                                s.ISACTIVE == 1 && c.HOLDSTATUS.Trim() != "R" && (c.HOLDTYPE.Trim() == "PR" || c.HOLDTYPE.Trim() == "P")
                                                select s);
                    }
                }
                catch (Exception e)
                {
                    ConfirmP2IEnumerable = (from s in _entities.ATENSI_P2
                                            join c in _entities.CONTAINER on s.CONTAINERNUMBER.Trim() equals c.CONTAINERNUMBER
                                            where s.ISACTIVE == 1
                                             && c.HOLDSTATUS.Trim() != "R" && (c.HOLDTYPE.Trim() == "PR" || c.HOLDTYPE.Trim() == "P")
                                            select s);
                }
            }
            else
            {
                ConfirmP2IEnumerable = (from s in _entities.ATENSI_P2
                                        join c in _entities.CONTAINER on s.CONTAINERNUMBER.Trim() equals c.CONTAINERNUMBER
                                        where s.ISACTIVE == 1
                                         && c.HOLDSTATUS.Trim() != "R" && (c.HOLDTYPE.Trim() == "PR" || c.HOLDTYPE.Trim() == "P")
                                        select s);
            }
            string[] roles = Roles.GetRolesForUser(username);
            if (roles.Contains("CustomerCare") && !roles.Contains("Planner"))
            {

                var ccList = ConfirmP2IEnumerable.Join(_entities.CONTAINER, // the source table of the inner join
                                              a => a.CONTAINERNUMBER.Trim(),        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                                              c => c.CONTAINERNUMBER.Trim(),   // Select the foreign key (the second part of the "on" clause)
                                              (a, c) => new { A = a, C = c })// selection
                                              .Where(ac => ac.C.EXPORTIMPORT == "I");
                ConfirmP2IEnumerable = ccList.Select(x => x.A);
            }
            else if (roles.Contains("Planner") && !roles.Contains("CustomerCare"))
            {
                var plannerList = ConfirmP2IEnumerable.Join(_entities.CONTAINER, // the source table of the inner join
                                              a => a.CONTAINERNUMBER.Trim(),        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                                              c => c.CONTAINERNUMBER.Trim(),   // Select the foreign key (the second part of the "on" clause)
                                              (a, c) => new { A = a, C = c })// selection
                                              .Where(ac => ac.C.EXPORTIMPORT == "E");
                ConfirmP2IEnumerable = plannerList.Select(x => x.A);
            }
            foreach (ATENSI_P2 items in ConfirmP2IEnumerable)
            {
                P2FILE p2File = (from p in _entities.P2FILE
                                 where p.P2ID == items.ID
                                 orderby p.ID descending
                                 select p).FirstOrDefault();
                if (ConfirmP2ItemList.Where(c => c.ID == items.ID).Count() > 0) continue;
                ConfirmListHoldP2ViewModel ConfirmP2Data = new ConfirmListHoldP2ViewModel();
                ConfirmP2Data.ID = items.ID;
                ConfirmP2Data.TerminalResponse = items.TERMINALRESPONSE;
                ConfirmP2Data.DocumentNumber = items.DOCUMENTNUMBER;
                ConfirmP2Data.DocumentType = items.DOCUMENTYPE;
                ConfirmP2Data.DocumentDate = items.DOCUMENTDATE.Value.ToShortDateString();
                ConfirmP2Data.ContainerID = items.CONTAINERNUMBER;
                ConfirmP2Data.IsConfirmed = (items.ISCONFIRMED == 1) ? "Confirmed" : "Requested";
                if (items.ISCONFIRMED != 1)
                {
                    ConfirmP2Data.Action = "<a href=\"javascript:void(0);\" title=\"Confirm\" onclick=\"confirmModalDataAtensiP2('/ConfirmHoldP2/ConfirmP2Data/" + items.ID + "','ConfirmP2','/ConfirmHoldP2/ConfirmP2List');\" class=\"btn btn-mini\">Confirm</a>";
                }
                else
                {
                    ConfirmP2Data.Action = "";
                }
                if (p2File != null)
                {
                    ConfirmP2Data.P2File = "<a href=\"" + _virtualPath + "/AtensiP2/GetP2File/" + items.ID + "\" title=\"View File\" class=\"btn btn-mini\">View File</a>";
                }
                ConfirmP2ItemList.Add(ConfirmP2Data);
            }

            return ConfirmP2ItemList;
        }
    }
}