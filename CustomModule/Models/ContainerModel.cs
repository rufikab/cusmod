﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CustomModule.Models
{
    public class ReleaseModel
    {
        public int ID{ get; set; }
        public string CONTAINERNUMBER { get; set; }
        [Required]
        public string RELEASENOTE { get; set; }
        public int RELEASEBY { get; set; }
        public bool IS_SPPBE{ get; set; }
        public HttpPostedFileBase ReleaseFile { get; set; }
    }
    public class HoldModel
    {
        public int ID { get; set; }
        [Required]
        public string CONTAINERNUMBER { get; set; }
        [Required]
        public string HOLDNOTE { get; set; }
        public int HOLDBY { get; set; }
    }

    public class PutusanModel
    {
        public int ID { get; set; }
        public int CONTAINER_ID { get; set; }
        [Required]
        public string CONTAINERNUMBER { get; set; }
        [Required]
        public string NOTE { get; set; }
        public string KODEPENYEGELAN { get; set; }
        public bool Utuh { get; set; }
    }

    public class ContainerDeleted
    {
        public int ID { get; set; }
        public int CONTAINER_ID { get; set; }
        public Nullable<DateTime> DELETETIME { get; set; }
        public string DELETEBY { get; set; }
        public string DELETENOTE { get; set; }

        public string CONTAINERNUMBER { get; set; }
        public string VOYAGE { get; set; }
        public string CONTAINERSIZE { get; set; }
        public string EXPORTIMPORT { get; set; }
        public string TAGNUMBER { get; set; }
        public string TRUCKNUMBER { get; set; }
        public string TRUCKPOSITION { get; set; }
        public string GATENUMBER { get; set; }
        public Nullable<DateTime> GATEINTIME { get; set; }
        public string DOCUMENTNUMBER { get; set; }
        public string CONSIGNEE { get; set; }
        public string HOLDTYPE { get; set; }
        public string HOLDNOTE { get; set; }
        public string HOLDSTATUS { get; set; }
        public string RELEASENOTE { get; set; }
        public string SEAL { get; set; }
        public string SEALNOTE { get; set; }
        public string KODEPENYEGELAN { get; set; }
        public string CHECKEDBY { get; set; }
        public DateTime CHECKEDTIME { get; set; }
        public double BRUTTOWEIGHTS { get; set; }
        public string DOCUMENTTYPE { get; set; }
        //additional

        public DateTime GATEOUTTIMES { get; set; }
        public string SGATEOUTTIME { get; set; }
        public string ContainerAndSize { get; set; }
        public string Action { get; set; }
        public string Note { get; set; }
        public Nullable<DateTime> GATEOUTTIME { get; set; }
        public string HoldBy { get; set; }
        public string ReleaseBy { get; set; }

        public string StrCheckedTime { get; set; }
        //public DateTime StrGateOutTime { get; set; }

        public string TerminalId { get; set; }
    }

    public class ContainerModel
    {
        public int ID { get; set; }
        public string CONTAINERNUMBER { get; set; }
        public string VOYAGE { get; set; }
        public string CONTAINERSIZE { get; set; }
        public string EXPORTIMPORT { get; set; }
        public string TAGNUMBER { get; set; }
        public string TRUCKNUMBER { get; set; }
        public string TRUCKPOSITION { get; set; }
        public string GATENUMBER { get; set; }
        public Nullable<DateTime> GATEINTIME { get; set; }
        public string DOCUMENTNUMBER { get; set; }
        public string CONSIGNEE { get; set; }
        public string HOLDTYPE { get; set; }
        public string HOLDNOTE { get; set; }
        public string HOLDSTATUS { get; set; }
        public string RELEASENOTE { get; set; }
        public string SEAL { get; set; }
        public string SEALNOTE { get; set; }
        public string FULLEMPTY { get; set; }
        public string KODEPENYEGELAN { get; set; }
        public string CHECKEDBY { get; set; }
        public DateTime CHECKEDTIME { get; set; }
        public double BRUTTOWEIGHTS { get; set; }
        public string DOCUMENTTYPE { get; set; }
        //additional

        public DateTime GATEOUTTIMES { get; set; }
        public string SGATEOUTTIME { get; set; }
        public string ContainerAndSize { get; set; }
        public string Action { get; set; }
        public string ActionCheckbox { get; set; }
        public string Note { get; set; }
        public Nullable<DateTime> GATEOUTTIME { get; set; }
        public string HoldBy { get; set; }
        public string ReleaseBy { get; set; }

        public string StrCheckedTime { get; set; }
        //public DateTime StrGateOutTime { get; set; }

        public string TerminalId { get; set; }
    }

    public static class HoldType
    {
        public static string type(string id)
        {
            if(String.IsNullOrEmpty(id)) return "";
            switch (id.Trim())
            {
                case "M": return "Hold-Manual";
                case "A": return "Hold-Auto";
                case "P": return "Hold-P2";
                case "PR": return "Hold-P2(Requested)";
                case "AP": return "Hold-P2"; 
            }
            return "X";
        }
    }

    public static class HoldStatus
    {
        public static string status(string id)
        {
            if (String.IsNullOrEmpty(id)) return "";
            switch (id.Trim())
            {
                case "R": return "Released";
                case "H": return "Hold";
            }
            return "";
        }
    }

    public static class ExportImport
    {
        public static string detail(string ei)
        {
            if (String.IsNullOrEmpty(ei)) return "";
            switch (ei.Trim())
            {
                case "E": return "Export";
                case "I": return "Import";
            }
            return "X";
        }
    }

    public class DailyReport
    {
        public int HOURS { get; set; }
        [Required]
        public int PLP { get; set; }
        public int BC { get; set; }
        public int OTHERS { get; set; }

    }
    public static class Seal
    {
        public static string kondisi(string kondisi)
        {
            if (String.IsNullOrEmpty(kondisi)) return "";
            switch (kondisi.Trim())
            {
                case "R": return "Rusak";
                case "U": return "Utuh";
            }
            return "X";
        }
    }

    public static class HoldReleaseAction
    {
        public static string getDetailOnly(int id, string ei, int hold = 0)
        {
            if (id == 0) return "";
            string s = "<a href=\"javascript:void(0)\" onclick=\"containerModal('{0}','{1}','{2}')\" title=\"Detail\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-th-list\"></i></a>";
            return String.Format(s, id,ei,hold);
        }

        public static string getAction(int id,string ei,string holdstatus,bool p2=false)
        {
            string action = "";
            string detail;
            if (holdstatus.Trim() == "H")
            {

                detail = getDetailOnly(id, ei);
                if(p2)
                    action = String.Format("<a href=\"javascript:void(0)\" onclick=\"releaseP2Modal('{0}','{1}')\" title=\"Release\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-lock\"></i></a>", id, ei);
                else
                    action = String.Format("<a href=\"javascript:void(0)\" onclick=\"releaseModal('{0}','{1}')\" title=\"Release\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-lock\"></i></a>", id, ei);
                
            }
            else
            {

                detail = getDetailOnly(id, ei, 1);
                if (!p2)
                    action = String.Format("<a href=\"javascript:void(0)\" onclick=\"holdModal('{0}','{1}')\" title=\"Hold\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-unlock\"></i></a>", id, ei);
                
            }
            return detail+action;
        }
    }

    public static class PutusanAction
    {
        public static string getDetailOnly(int id,int hold=0)
        {
            string s = "<a href=\"javascript:void(0)\" onclick=\"containerModal('{0}','{1}')\" title=\"Detail\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-th-list\"></i></a>";
            return String.Format(s, id,hold);
        }

        public static string getAction(int id, string holdstatus)
        {
            string action = getDetailOnly(id,1);
            if (holdstatus.Trim() != "R"||true) //re hold
            {
                action = action + String.Format("<a href=\"javascript:void(0)\" onclick=\"putusanModal('{0}')\" title=\"Putusan\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-pencil\"></i></a>", id);
            }
            return action;
        }
    }

    public static class ArsipAction
    {
        public static string getAction(int id, string type)
        {
            string s = "<a href=\"javascript:void(0)\" onclick=\"containerModal('{0}', '{1}')\" title=\"Detail\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-th-list\"></i></a>";
            string action = String.Format(s, id, type);
            return action;

            //old delete type//
        }
    }

    public static class DuplicateDeleteAction
    {
        public static string getAction(int id, string type)
        {
            string s = "<a href=\"javascript:void(0)\" onclick=\"containerModal('{0}', '{1}')\" title=\"Detail\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-th-list\"></i></a>";
            return String.Format(s, id, type);
        }

        public static string getCheckbox(int id)
        {
            string html = "<input type='checkbox' class='duplicate-check' data-conID='{0}'>";
            return String.Format(html, id);
        }
    }

    public static class SummaryAction
    {
        public static string getAction(int id, string type)
        {
            string s = "<a href=\"javascript:void(0)\" onclick=\"containerModal('{0}', '{1}')\" title=\"Detail\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-th-list\"></i></a>";
            string action = String.Format(s, id, type);
            return action;

            //old delete type//
        }
    }
    public static class ReportAction
    {
        public static string getAction(int id, string type)
        {
            string s = "<a href=\"javascript:void(0)\" onclick=\"containerModal('{0}', '{1}')\" title=\"Detail\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-th-list\"></i></a>";
            string action = String.Format(s, id, type);
            return action;

            //old delete type//
        }
    }

    public static class DeleteAction
    {
        public static string getDetailOnly(int id, string type)
        {
            string s = "<a href=\"javascript:void(0)\" onclick=\"containerModal('{0}','{1}')\" title=\"Detail\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-th-list\"></i></a>";
            return String.Format(s, id, type);
        }
        public static string getAction(int id,string cn, string type,string mappath)
        {
            string s = "<a href=\"javascript:void(0)\" onclick=\"confirmModalContainerDelete('" + mappath + "Delete/Delete/{0}','delete','" + mappath + "Delete/IndexAjax','{1}')\" title=\"Delete\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-trash\"></i></a>";
            string action = getDetailOnly(id, type) + String.Format(s, id, cn);
            return action;
        }

        public static string getActionNew(int id, string cn, string type)
        {
            string s = "<a href=\"javascript:void(0)\" onclick=\"deleteModal('{0}', '{1}')\" title=\"Delete\" role=\"button\" class=\"btn btn-mini\" ><i class=\"icon-trash\"></i></a>";
            string action = getDetailOnly(id, type) + String.Format(s, id, cn);
            return action;
        }
    }
}