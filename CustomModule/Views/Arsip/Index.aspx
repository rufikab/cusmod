﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Arsip
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<style type="text/css">
.card {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 120px;
    border: 1px solid #cccccc;
    float:left;
    margin-right:10px;
    margin-top:10px;
}

/* On mouse-over, add a deeper shadow */
.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

/* Add some padding inside the card container */
.container {
    padding: 2px 16px;
}
.card-title1
{
    background-color:#05842c;
    color:white;
    padding:5px;
    margin-top: 0px;
}
.card-title2
{
    background-color:#9e3d3d;
    color:white;
    padding:5px;
    margin-top: 0px;
}

label.datetimepicker2{
    margin-right: 10px;
}

div.datetimepicker2{
    margin-left: 10px;
    margin-right: 10px;
}

div.bootstrap-datetimepicker-widget .picker-switch > a{
    font-size: 25px;
    line-height: 25px;
    padding-bottom: 0;
}
</style>
<div class="alert <%=((bool)ViewData["HasBroken"])?"":"hide"%> alert-error pull-right ">
	    <button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4>Warning!</h4>
		Ada kontainer yang Rusak.
	</div>
    <fieldset>
		<legend><i class="icon-folder-open"></i> Arsip</legend>
		<form class="form-horizontal">
			<div class="control-group">
				<label class="datetimepicker2 control-label" for="inputEmail">Tanggal Berlaku :</label>
				<%--<div class="controls">
					<input class="datepicker" type="text" id="fromDate" value="<%=(ViewData["dtFrom"])%>" /> - 
                    <input class="datepicker" type="text" id="toDate" value="<%=(ViewData["dtEnd"])%>" />
				</div>--%>
                <div class="datetimepicker2 input-append">
                    <input data-format="MM/dd/yyyy HH:mm:ss PP" type="text" id="fromDate" value="<%=(ViewData["dtFrom"])%>"/>
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div> - 
                <div class="datetimepicker2 input-append">
                    <input data-format="MM/dd/yyyy HH:mm:ss PP" type="text" id="toDate" value="<%=(ViewData["dtEnd"])%>"/>
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputPassword">No. Container :</label>
				<div class="controls">
					<input name="inputNoContainer" type="text" onKeyDown="limitText(this.form.limitedtextfield,this.form.countdown,15);" 
onKeyUp="limitText(this.form.limitedtextfield,this.form.countdown,15);" maxlength="11" id="inputNoContainer" placeholder="No. Container">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputPassword">No.Dok. Pabean :</label>
				<div class="controls">
					<input type="text" id="docNumber">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputPassword">Jenis Kegiatan :</label>
				<div class="controls">
					<select name="exportImport" id="exportImport">
						<option value="">All</option>
						<option value="E">Export</option>
						<option value="I">Import</option>
					</select>
				</div>
			</div>
            
			<div class="control-group">   
				<label class="control-label" for="inputPassword">Type Document :</label>
				<div class="controls">
					<select name="documentType" id="documentType">
						<option value="">All</option>
                        <%foreach (CustomModule.Models.ContainerRepository.DocumentType docType in (List<CustomModule.Models.ContainerRepository.DocumentType>)ViewData["DocumentTypes"])
                          {
                              %>
						    <option value="<%=docType.Type%>" class="option<%=docType.EI%>"><%=docType.Type%></option>
                        <%} %>
					</select>
				</div>
			</div>
			<div class="control-group">   
				<label class="control-label" for="inputPassword">Terminal :</label>
				<div class="controls">
					<select name="documentType" id="Select1">
						<option value="">All</option>
                        <%foreach(string terminal in (List<string>)ViewData["Terminals"]) {%>
						    <option value="<%=terminal%>"><%=terminal%></option>
                        <%} %>
					</select>
				</div>
			</div>

			<div class="control-group">
				<div class="controls">
					<a title="Cari" class="btn" onclick="filterGridData();"><i class="icon-search"></i></a>
                    <div class="btn-group pull-right">
					<a class="btn pull-right dropdown-toggle" data-toggle="dropdown">Export <span class="caret"></span></a>
                    <ul class="dropdown-menu pull-right">
                    <li><a id="exportCSV">Export to CSV</a></li>
                    <li><a id="exportExcell">Export to Excell</a></li>
                    </ul>
				</div>
				</div>
			</div>
            <div class="card-content control-group" style="padding-left: 25px;padding-right: 25px;">
            </div>
		</form>
	</fieldset>
    <div id="topProgress" class="progress progress-striped" style="display:none">
            <div  class="bar" style="width: 100%;"></div>
        </div>
    <div id="arsipList" class="DataGridXScroll span12">
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<style>
    .width1400
    {
        width: 1400px;
    }
    .DataGridXScroll
    {
        overflow-x: scroll;
        text-align: left;
    }
</style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
<!--/ Modal Detail-->

<div id="containerModal" class="modal hide fade fullwidth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div id="containerModalProgress" class="modal-body progress progress-striped">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <div id="containerModalCreateContent" ></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-primary" id="printContainer">Cetak</button>
	</div>
</div>

<script src="<%=Url.Content("~/Content/js/bootstrap-datetimepicker.min.js")%>" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $('.datetimepicker2').datetimepicker({
            language: 'en',
            pick12HourFormat: true
        });
    });

    containerModal = function (id, type) {
        $("#containerModal").unbind();
        // wire up the buttons to dismiss the modal when shown
        $("#containerModal").bind("show", function () {
            $(".modal-footer").hide();
            $("#containerModal").addClass("container");
            $("#containerModalProgress").show();
            $("#containerModalCreateContent").hide();
            $("#containerModalProgressTitle").show();
            $("#containerModal .btn:not(.btn-primary)").click(function (e) {
                // hide the dialog box
                $("#containerModal").modal('hide');
            });
            $("#printContainer").click(function (e) {
                $("#containerModalCreateContent").print();
                //$("#containerModal").modal('hide');
            });
//            $.get(baseurl+"/Container/Detail/" + id, function (data) { //old
            $.get(baseurl+"/Container/Detail/?id=" + id+"&type="+type+"&next="+0+"&prev="+0, function (data) {
            
                $("#containerModal").css("margin-top", "-282px");
                $(".modal-footer").show();
                $("#containerModalProgressTitle").hide();
                $("#containerModalProgress").hide();
                $("#containerModalCreateContent").html(data);
                $("#containerModalCreateContent").show();
            });
        });
        // remove the event listeners when the dialog is hidden
        $("#containerModal").bind("hide", function () {
            // remove event listeners on the buttons
            $("#containerModal .btn").unbind();
            $("#containerModal").removeClass("container");
        });
        // finally, wire up the actual modal functionality and show the dialog
        $("#containerModal").modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true // this parameter ensures the modal is shown immediately
        });
    }

    function getDate(date){
        if(!date){
            date ="";
        }else{
            date = date.split('/');
            if ($.isArray(date)) {
                date = date[0] + '-' + date[1] + '-' + date[2];
            }
        }  
        return  date;
    }

    function OnDataBinding(e){
        $("#topProgress").show();
        var grid = $("#ContainerList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
    }

    function OnDataBound(e){
        $("#topProgress").hide();
    }

    function getParamUrl()
    {
    fromDate = $("#fromDate").val();
        toDate = $("#toDate").val();
        fromDate = getDate(fromDate);
        toDate = getDate(toDate);
        cn = $("#inputNoContainer").val();
        docnum = $("#docNumber").val();
        ei = $("#exportImport").val();
        docType = $("#documentType").val();
        return "/?from=" + fromDate + "&to=" + toDate+ "&cn=" + cn+ "&docnum=" + docnum +"&ei=" + ei+"&docType=" + docType;
    }

    function getAjaxUrl() {
        var selectUrl = "<%=Url.Action("_AjaxFilterBinding","Arsip")%>" +getParamUrl();
        return selectUrl;
    }

    function filterGridData() {
        var grid = $("#ContainerList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
//        console.log(getAjaxUrl());
        grid.ajaxRequest();

        fromDate = $("#fromDate").val();
        toDate = $("#toDate").val();
        fromDate = getDate(fromDate);
        toDate = getDate(toDate);
        cn = $("#inputNoContainer").val();
        docnum = $("#docNumber").val();
        ei = $("#exportImport").val();
        
        $.get(baseurl+"/Arsip/GetTotalTransaction/?from=" + fromDate + "&to=" + toDate + "&cn=" + cn + "&docnum=" + docnum + "&ei=" + ei, function(data){
            $("div.card-content").html("");
            var html="";
            for(var i=0;i<data.length;i++){
                html+='<div class="card">'+
                  '<h4 class="card-title'+data[i].type+'"><b>'+data[i].name+'</b></h4>'+
                  '<div class="container">'+
                    '<p>'+data[i].total+'</p>'+
                  '</div>'+
                '</div>';
            }
            $("div.card-content").html(html);
        });
        return false;
    }

    $("#exportCSV").click(function(){
        $(this).attr('target','_blank');
        window.open("<%=Url.Action("ExportCSV","Arsip")%>"+getParamUrl());
    });

    $("#exportExcell").click(function(){
        $(this).attr('target','_blank');
        window.open("<%=Url.Action("ExportExcell","Arsip")%>"+getParamUrl());
    });

    function loadList() {
        $("#topProgress").show();
        $.get(baseurl+"/Arsip/IndexAjax" + getParamUrl(), function (data) {
            $("#topProgress").hide();
            $("#arsipList").html(data);
        });
        $.get(baseurl+"/Arsip/GetTotalTransaction" + getParamUrl(), function (data) {
            $("div.card-content").html("");
            var html="";
            for(var i=0;i<data.length;i++){
                html+='<div class="card">'+
                  '<h4 class="card-title'+data[i].type+'"><b>'+data[i].name+'</b></h4>'+
                  '<div class="container">'+
                    '<p>'+data[i].total+'</p>'+
                  '</div>'+
                '</div>';
            }
            $("div.card-content").html(html);
        });
        return false;
    }
    
    $(document).ready(function(){
        $("#exportImport").change(function(){          
            var value = $(this).val();
            if(value=='E'){
                $("select .optionI").hide()
                $("select .optionE").show();
            }else if(value=='I'){
                $("select .optionE").hide();
                $("select .optionI").show();
            }else{
                $("select option").show();
            }
            $("#documentType").val('') .trigger('change');
        });
        loadList();
    });


</script>
</asp:Content>
