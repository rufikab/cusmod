﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<List<CustomModule.Models.UserActivityModel>>" %>
                        <%
                        Html.Telerik()
                              .Grid(Model)
                              .Name("ContainerList")
                              .ClientEvents(events=>events.OnDataBinding("OnDataBinding").OnDataBound("OnDataBound"))
                              .Columns(columns =>
                              {
                                  columns.Bound(o => o.USERNAME).Title("Username");
                                  columns.Bound(o => o.HOLD).Title("Jlh. Hold");
                                  columns.Bound(o => o.RELEASE).Title("Jlh. Release");
                                  columns.Bound(o => o.CHECKCONTAINER).Title("Jlh. Cek Container");
                                  columns.Bound(o => o.ADDATENSI).Title("Jlh. Tambah Atensi");
                                  columns.Bound(o => o.EDITATENSI).Title("Jlh. Edit Atensi");
                                  columns.Bound(o => o.CONFIRMP2).Title("Jlh. Confirm P2");
                                  columns.Bound(o => o.ADDPUTUSAN).Title("Jlh. Tambah Putusan");
                                  columns.Bound(o => o.DELETETRX).Title("Jlh. Hapus Transaksi");
                              })
                              .DataBinding(d => d.Ajax().Select("_AjaxBinding", "UserActivity"))
                              .Pageable(paging=>paging.PageSize(20)
                                  .Style(GridPagerStyles.NextPreviousAndNumeric)
                                  .Position(GridPagerPosition.Bottom))
                              .Footer(true)
                              .HtmlAttributes(new{@class="width1400"})
                              .Filterable().Render();
                      %>