﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
                <fieldset>
					<legend><i class="icon-dashboard"></i> Beranda</legend>
                    <div class="alert <%=((bool)ViewData["HasBroken"])?"":"hide"%> alert-error pull-right ">
	                    <button type="button" class="close" data-dismiss="alert">&times;</button>
		                <h4>Warning!</h4>
		                Ada Segel yang Rusak.
	                </div>
                     <button type="button" class="btn btn-success active" data-toggle="button" id="autorefresh">Auto Refresh : ON</button>
                    <div class="clearfix"></div>
					<div class="accordion" id="accordion2">
					    <div class="accordion-group" data-selected="1" data-type="import">
						    <div class="accordion-heading">
							    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
								    IMPORT
							    </a>
						    </div>
						    <div id="collapseOne" class="accordion-body collapse in">
							    <div class="accordion-inner" id="importList">                            
                                <%Html.Telerik().Grid<CustomModule.Models.ContainerModel>()
                                      .Name("ContainerIList")
                                      .Columns(columns =>
                                      {
                                          columns.Bound(o => o.TAGNUMBER).Title("No. Tag");
                                          columns.Bound(o => o.TRUCKNUMBER).Title("No. Polisi");
                                          columns.Bound(o => o.TerminalId).Title("Terminal ID");
                                          columns.Bound(o => o.GATENUMBER).Title("No. Gate in");
                                          columns.Bound(o => o.GATEINTIME).Title("Waktu Masuk");
                                          columns.Bound(o => o.SGATEOUTTIME).Title("Waktu Keluar");
                                          columns.Bound(o => o.ContainerAndSize).Title("No. Container / Size");
                                          columns.Bound(o => o.DOCUMENTNUMBER).Title("Dok. Pabean");
                                          columns.Bound(o => o.HOLDSTATUS).Title("Hold Status");
                                          columns.Bound(o => o.TRUCKPOSITION).Title("Movement");
                                          columns.Bound(o => o.Note).Title("Keterangan");
                                          columns.Bound(o => o.HoldBy).Title("Hold By");
                                          columns.Bound(o => o.ReleaseBy).Title("Released By");
                                          columns.Bound(o => o.CHECKEDBY).Title("Checked By");
                                          columns.Bound(o => o.StrCheckedTime).Title("Checked Time");
                                          columns.Bound(o => o.Action).Title("Action").Encoded(false);
                                          columns.Bound(o => o.GATEOUTTIMES).Hidden();
                                      })
                                      .Sortable(sorting => sorting
                                          .SortMode(GridSortMode.SingleColumn)
                                          .OrderBy(order =>
                                              {
                                                  order.Add(o => o.GATEOUTTIMES).Descending();
                                              }
                                          ))
                                      .DataBinding(d => d.Ajax().Select("_Sorting", "Container", new { id = "I" }))
                                      .Pageable(paging => paging.PageSize(10)
                                          .Style(GridPagerStyles.NextPreviousAndNumeric)
                                          .Position(GridPagerPosition.Bottom))
                                      .Footer(true)
                                      .Filterable().Render();
                              %>
                                </div>
						    </div>
					    </div>
					    <div class="accordion-group <%=((bool)ViewData["OnlyAuto"])?"hide":""%> " data-selected="2" data-type="export">
						    <div class="accordion-heading">
							    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
								    EXPORT
							    </a>
						    </div>
						    <div id="collapseTwo" class="accordion-body collapse">
                                <div class="accordion-inner" id="exportList">
                                <%Html.Telerik().Grid<CustomModule.Models.ContainerModel>()
                                      .Name("ContainerEList")
                                      .Columns(columns =>
                                      {
                                          columns.Bound(o => o.TAGNUMBER).Title("No. Tag");
                                          columns.Bound(o => o.TRUCKNUMBER).Title("No. Polisi");
                                          columns.Bound(o => o.TerminalId).Title("Terminal ID");
                                          columns.Bound(o => o.GATENUMBER).Title("No. Gate in");
                                          columns.Bound(o => o.GATEINTIME).Title("Waktu Masuk");
                                          columns.Bound(o => o.SGATEOUTTIME).Title("Waktu Keluar");
                                          columns.Bound(o => o.ContainerAndSize).Title("No. Container / Size");
                                          columns.Bound(o => o.DOCUMENTNUMBER).Title("Dok. Pabean");
                                          columns.Bound(o => o.HOLDSTATUS).Title("Hold Status");
                                          columns.Bound(o => o.TRUCKPOSITION).Title("Movement");
                                          columns.Bound(o => o.Note).Title("Keterangan");
                                          columns.Bound(o => o.HoldBy).Title("Held By");
                                          columns.Bound(o => o.ReleaseBy).Title("Released By");
                                          columns.Bound(o => o.CHECKEDBY).Title("Checked By");
                                          columns.Bound(o => o.StrCheckedTime).Title("Checked Time");
                                          columns.Bound(o => o.Action).Title("Action").Encoded(false);
                                          columns.Bound(o => o.GATEOUTTIMES).Hidden();

                                      })
                                      .Sortable(sorting => sorting
                                          .SortMode(GridSortMode.SingleColumn)
                                          .OrderBy(order =>
                                              {
                                                  order.Add(o => o.GATEOUTTIMES).Descending();
                                              }
                                          ))
                                      .DataBinding(d => d.Ajax().Select("_Sorting", "Container", new { id = "E" }))
                                      .Pageable(paging => paging.PageSize(10)
                                          .Style(GridPagerStyles.NextPreviousAndNumeric)
                                          .Position(GridPagerPosition.Bottom))
                                      .Footer(true)
                                      .Filterable().Render();
                              %>
                                </div>
						    </div>
					    </div>
					    <div class="accordion-group" data-selected="3" data-type="oog">
						    <div class="accordion-heading">
							    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
							    OOG
							    </a>
						    </div>
						    <div id="collapseFour" class="accordion-body collapse">
							    <div class="accordion-inner" id="oogList">
                                <%Html.Telerik().Grid<CustomModule.Models.ContainerModel>()
                                      .Name("OOGList")
                                      .Columns(columns =>
                                      {
                                          columns.Bound(o => o.TAGNUMBER).Title("No. Tag");
                                          columns.Bound(o => o.TRUCKNUMBER).Title("No. Polisi");
                                          columns.Bound(o => o.GATENUMBER).Title("No. Gate in");
                                          columns.Bound(o => o.GATEINTIME).Title("Waktu Masuk");
                                          columns.Bound(o => o.SGATEOUTTIME).Title("Waktu Keluar");
                                          columns.Bound(o => o.ContainerAndSize).Title("No. Container / Size");
                                          columns.Bound(o => o.DOCUMENTNUMBER).Title("Dok. Pabean");
                                          columns.Bound(o => o.HOLDSTATUS).Title("Hold Status");
                                          columns.Bound(o => o.TRUCKPOSITION).Title("Movement");
                                          columns.Bound(o => o.Note).Title("Keterangan");
                                          columns.Bound(o => o.HoldBy).Title("Held By");
                                          columns.Bound(o => o.ReleaseBy).Title("Released By");
                                          columns.Bound(o => o.CHECKEDBY).Title("Checked By");
                                          columns.Bound(o => o.StrCheckedTime).Title("Checked Time");
                                          columns.Bound(o => o.Action).Title("Action").Encoded(false);
                                          columns.Bound(o => o.GATEOUTTIMES).Hidden();

                                      })
                                      .Sortable(sorting => sorting
                                          .SortMode(GridSortMode.SingleColumn)
                                          .OrderBy(order =>
                                              {
                                                  order.Add(o => o.GATEOUTTIMES).Descending();
                                              }
                                          ))
                                      .DataBinding(d => d.Ajax().Select("_Sorting", "Container", new { id = "OOG" }))
                                      .Pageable(paging => paging.PageSize(10)
                                          .Style(GridPagerStyles.NextPreviousAndNumeric)
                                          .Position(GridPagerPosition.Bottom))
                                      .Footer(true)
                                      .Filterable().Render();
                              %>
                                </div>
						    </div>
					    </div>
                        
					    <div class="accordion-group <%=((bool)ViewData["P2"])?"":"hide"%> " data-selected="3" data-type="atensi">
						    <div class="accordion-heading">
							    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
							    ATENSI P2
							    </a>
						    </div>
						    <div id="collapseThree" class="accordion-body collapse">
							    <div class="accordion-inner" id="atensiList">
                                <%Html.Telerik().Grid<CustomModule.Models.ContainerModel>()
                                      .Name("ContainerP2List")
                                      .Columns(columns =>
                                      {
                                          columns.Bound(o => o.TAGNUMBER).Title("No. Tag");
                                          columns.Bound(o => o.TRUCKNUMBER).Title("No. Polisi");
                                          columns.Bound(o => o.GATENUMBER).Title("No. Gate in");
                                          columns.Bound(o => o.GATEINTIME).Title("Waktu Masuk");
                                          columns.Bound(o => o.SGATEOUTTIME).Title("Waktu Keluar");
                                          columns.Bound(o => o.ContainerAndSize).Title("No. Container / Size");
                                          columns.Bound(o => o.DOCUMENTNUMBER).Title("Dok. Pabean");
                                          columns.Bound(o => o.HOLDSTATUS).Title("Hold Status");
                                          columns.Bound(o => o.TRUCKPOSITION).Title("Movement");
                                          columns.Bound(o => o.Note).Title("Keterangan");
                                          columns.Bound(o => o.HoldBy).Title("Held By");
                                          columns.Bound(o => o.ReleaseBy).Title("Released By");
                                          columns.Bound(o => o.CHECKEDBY).Title("Checked By");
                                          columns.Bound(o => o.StrCheckedTime).Title("Checked Time");
                                          columns.Bound(o => o.Action).Title("Action").Encoded(false);
                                          columns.Bound(o => o.GATEOUTTIMES).Hidden();

                                      })
                                      .Sortable(sorting => sorting
                                          .SortMode(GridSortMode.SingleColumn)
                                          .OrderBy(order =>
                                              {
                                                  order.Add(o => o.GATEOUTTIMES).Descending();
                                              }
                                          ))
                                      .DataBinding(d => d.Ajax().Select("_Sorting", "Container", new { id = "P2" }))
                                      .Pageable(paging => paging.PageSize(10)
                                          .Style(GridPagerStyles.NextPreviousAndNumeric)
                                          .Position(GridPagerPosition.Bottom))
                                      .Footer(true)
                                      .Filterable().Render();
                              %>
                                </div>
						    </div>
					    </div>
                    
                    </div>
                    </div>
				</fieldset>
</asp:Content>
<asp:Content ID="script" ContentPlaceHolderID="ScriptContent" runat="server"> 
<!-- Modal DETAIL-->
<div id="modalDetail" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		<h3>Input Dokumen Atensi P2</h3>
	</div>
    <div id="modalDetailProgress" class="modal-body progress progress-striped">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <div id="modalDetailContent"></div>
    <div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
		<button class="btn btn-primary" id="release-p2">Release</button>
		<button class="btn btn-primary" id="save-p2">Simpan</button>
	</div>
</div>
<!--/ Modal Detail-->

<div id="containerModal" class="modal hide fade fullwidth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div id="containerModalProgress" class="modal-body progress progress-striped">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <div id="containerModalCreateContent" ></div>
	<div class="modal-footer">
		<button class="btn btn-success" id="prev">Previous</button>
        <% if ((bool)ViewData["ControlRoom"])
           {%>
		<button class="btn" aria-hidden="true" id="check">Check</button>
        <%} %>
		<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="closeBtn">Close</button>
        <% if ((bool)ViewData["ControlRoom"])
           {%>
		<button class="btn" aria-hidden="true" id="holdContainer">Hold</button>
        <%} %>
		<button class="btn btn-primary" id="printContainer">Cetak</button>
		<button class="btn btn-success" id="next">Next</button>
	</div>
</div>
<script type="text/javascript">
    var importSelected=1;
    var exportSelected=2;
    var atensiSelected=3;
    var oogSelected=3;
    var currentSelected;
    var currentTimeout=null;
    var firstimport = false;
    var firstexport = false;
    var firstatensi = false;    
    var reloadingimport = false;
    var reloadingexport = false;
    var reloadingatensi = false;
    var modalShowed = false;
    var prevType = null;

    $(document).ready(function(){ 
        currentSelected=importSelected;
        
        $(".accordion-group").click(function(){
            currentSelected=parseInt($(this).attr("data-selected"));
            if($("#autorefresh").hasClass('active')){
                window.clearTimeout(currentTimeout);
                initList();
            }
        });
        $("#autorefresh").click(function () {
            if($(this).hasClass('active')){
                window.clearTimeout(currentTimeout);
                $(this).html("Auto Refresh : OFF").removeClass("btn-success").addClass("btn-danger");    
            }else{
                $(this).html("Auto Refresh : ON").removeClass("btn-danger").addClass("btn-success");  
                initList();
            }
        });
        initList();
    });    
    containerModal = function (id, type, hold,next,prev) {
        $("#containerModal").unbind();
        modalShowed = true;
        $("#containerModal").bind("show", function () {
            hold = hold || 0;
            if (hold == 1) {
                $("#holdContainer").show();
            } else {
                $("#holdContainer").hide();
            }
            $("#containerModal .modal-footer").hide();
            $("#containerModal").addClass("container");
            $("#containerModalProgress").show();
            $("#containerModalCreateContent").hide();
            $("#containerModalProgressTitle").show();
            $("#closeBtn").click(function (e) {
                $("#containerModal").modal('hide');
            });
            $("#printContainer").click(function (e) {
                $("#containerModalCreateContent").print();
            });
            $("#holdContainer").click(function (e) {
                $("#containerModal").modal('hide');
                holdModal(id, type);
            });
            $("#check").attr("disabled", "disabled");
            $("#check").click(function (e) {
                id = $("#containerId").val();
                $("#containerModal .modal-footer .btn").attr("disabled", "disabled");
                $("#containerModalProgress").show();
                $("#containerModalCreateContent").hide();
                $.get(baseurl+"/Container/Checkin/"+id, function (data) {
                        $("#containerModal").css("margin-top", "-282px");
                        $("#containerModal .btn").removeAttr("disabled");
                        $("#containerModalCreateContent").show();
                        $("#containerModalCreateContent").html(data);
                        $("#containerModalProgress").hide();
                        $("#containerModalProgressTitle").hide();
                        if($("#checkedBy").html()=="") $("#check").removeAttr("disabled");
                        else $("#check").attr("disabled", "disabled");
                });
            });
            if(!next) next = 0;
            if(!prev) prev = 0;
            $.get(baseurl+"/Container/Detail/?id=" + id+"&type="+type+"&next="+next+"&prev="+prev, function (data) {
               
                if(data=="Empty"){
                     $("#containerModal").modal('hide');    
                }else{
                    $("#containerModal").css("margin-top", "-282px");
                    $("#containerModal .modal-footer").show();
                    $("#containerModalProgressTitle").hide();
                    $("#containerModalProgress").hide();
                    $("#containerModalCreateContent").html(data);
                    $("#containerModalCreateContent").show();
                    if($("#checkedBy").html()=="") $("#check").removeAttr("disabled");
                    holdStatus = $("#holdStatus").val();
                    $("#holdContainer").show();
                    if (holdStatus == "H") {
                        $("#holdContainer").hide();
                    }
                }
            }); 
            $("#next").click(function (e) {
                id = $("#containerId").val();
                $("#containerModal .modal-footer").hide();
                $("#containerModal").addClass("container");
                $("#containerModalProgress").show();
                $("#containerModalCreateContent").hide();
                $("#containerModalProgressTitle").show();
                $.get(baseurl+"/Container/Detail/?id=" + id+"&type="+type+"&next=1&prev=0", function (data) {
                    if(data=="Empty"){
                         $("#containerModal").modal('hide');  
                         alert("No Next Data");  
                    }else{
                        $("#containerModal").css("margin-top", "282px");
                        $("#containerModal .modal-footer").show();
                        $("#containerModalProgressTitle").hide();
                        $("#containerModalProgress").hide();
                        $("#containerModalCreateContent").html(data);
                        $("#containerModalCreateContent").show();
                        if($("#checkedBy").html()=="") $("#check").removeAttr("disabled");
                        holdStatus = $("#holdStatus").val();
                        $("#holdContainer").show();
                        if (holdStatus == "H") {
                            $("#holdContainer").hide();
                        }
                    }
                }); 
            });
            $("#prev").click(function (e) {
                id = $("#containerId").val();
                $("#containerModal .modal-footer").hide();
                $("#containerModal").addClass("container");
                $("#containerModalProgress").show();
                $("#containerModalCreateContent").hide();
                $("#containerModalProgressTitle").show();
                $.get(baseurl+"/Container/Detail/?id=" + id+"&type="+type+"&prev=1&next=0", function (data) {
                    if(data=="Empty"){
                         $("#containerModal").modal('hide');    
                         alert("No Prev Data");
                    }else{
                        $("#containerModal").css("margin-top", "-282px");
                        $("#containerModal .modal-footer").show();
                        $("#containerModalProgressTitle").hide();
                        $("#containerModalProgress").hide();
                        $("#containerModalCreateContent").html(data);
                        $("#containerModalCreateContent").show();
                        if($("#checkedBy").html()=="") $("#check").removeAttr("disabled");
                        holdStatus = $("#holdStatus").val();
                        $("#holdContainer").show();
                        if (holdStatus == "H") {
                            $("#holdContainer").hide();
                        }
                    }
                }); 
            });
        });
        // remove the event listeners when the dialog is hidden
        $("#containerModal").bind("hide", function () {
            // remove event listeners on the buttons
            $("#containerModal .btn").unbind();
            $("#containerModal").removeClass("container");
            modalShowed = false;
        });
        // finally, wire up the actual modal functionality and show the dialog
        $("#containerModal").modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true // this parameter ensures the modal is shown immediately
        });
    };
    holdModal = function (id, type) {
        title = (type == "E") ? "export" : "import";
        url = '/Container/Hold/' + id;
        createModal(url, title, "/Container/Index/" + type, "Hold");
    };
   initList=function(){
        console.log(currentSelected);
        <% if((bool)ViewData["OnlyAuto"]){%>
            refreshGrid("ContainerEList");
        <% }else{ %>
            if(currentSelected==importSelected)
                refreshGrid("ContainerIList");
            if(currentSelected==exportSelected)
                refreshGrid("ContainerEList");
            if(currentSelected==atensiSelected)
                refreshGrid("ContainerP2List");
            if(currentSelected==oogSelected)
                refreshGrid("OOGList");
        <% } %>
    };
    refresh=function (){
        if($("#autorefresh").hasClass('active')){
            var grid = $('#ContainerEList').data('tGrid');
            grid.pageTo(1);
            var grid = $('#ContainerIList').data('tGrid');
            grid.pageTo(1);
            var grid = $('#ContainerP2List').data('tGrid');
            grid.pageTo(1);
            var grid = $('#OOGList').data('tGrid');
            grid.pageTo(1);
        }
    };
   refreshGrid=function(id){
        console.log("refresh grid "+ id);
        
        var grid = $("#"+id).data("tGrid");
        grid.ajaxRequest();
        <% if((bool)ViewData["OnlyAuto"]){%>
        if(!modalShowed) currentTimeout=setTimeout("refreshGrid('"+id+"')", <%=(int)ViewData["Timer"]%>+30+Math.floor(Math.random()*5));
        <% }else{ %> 
            if(!modalShowed) currentTimeout=setTimeout("refreshGrid('"+id+"')", <%=(int)ViewData["Timer"]%>+30+Math.floor(Math.random()*5));
        <% } %>
   };
    releaseModal = function (id, type) {
        title = (type == "E") ? "export" : "import";
        url = '/Container/Release/' + id;
        createModal(url, title, "/Container/Index/" + type, "Release");
    };
    
    releaseP2Modal = function (id, type) {
        title = (type == "E") ? "export" : "import";
        url = '/Container/ReleaseP2/' + id;
        createModal(url, title, "/Container/Index/" + type, "Release P2");
    };
</script>
</asp:Content>