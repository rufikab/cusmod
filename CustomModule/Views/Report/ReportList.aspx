﻿<style>
    .t-widget .t-header, .t-widget table td{
   text-align: center;
}
</style>
<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<List<CustomModule.Models.DailyReport>>" %>
    <div id="topProgress" class="modal-body progress progress-striped active pull-left"  style="display:none;width: 85%;padding: 0;margin: 3px 0px 5px;">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <div class="clearfix"></div>
<%
        Html.Telerik()
                .Grid(Model)
                .Name("ReportList")
                .ClientEvents(events => events.OnDataBinding("OnDataBinding").OnDataBound("OnDataBound"))
                .Columns(columns =>
                {
                     columns.Bound(o => o.HOURS).Title("JAM");
                     columns.Bound(o => o.PLP).Title("PLP");
                     columns.Bound(o => o.BC).Title("BC 2.0");
                     columns.Bound(o => o.OTHERS).Title("OTHERS");
                })
                .Sortable(sorting => sorting
                    .SortMode(GridSortMode.MultipleColumn)
                    )
                .DataBinding(d => d.Ajax().Select("_AjaxBinding", "Report"))
                .Footer(true)
                .Render();
        %>

