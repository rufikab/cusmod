﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Report Page
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
		<legend><i class="icon-exclamation-sign"></i> Report</legend>
        <form class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="inpuTanggal">Tanggal :</label>
				<div class="controls">
					<input class="datepicker" type="text" id="initialDateFilter" name="InitialDateFilter" value="<%=(ViewData["dtNow"])%>" style="width:100px" /> 
                    <a title="Cari" class="btn" onclick="searchData();"><i class="icon-search"></i></a>
                     <div class="btn-group pull-right">
					    <a class="btn pull-right dropdown-toggle" data-toggle="dropdown">Export <span class="caret"></span></a>
                        <ul class="dropdown-menu pull-right">
                            <li><a id="exportCSV">Export to CSV</a></li>
                            <li><a id="exportExcell">Export to Excell</a></li>
                        </ul>  
				    </div>
				</div>
			</div>
        </form>
    </fieldset>
     <div id="ReportList">
        <%-- <%Html.RenderPartial("ReportList", Model); %> --%>
    </div>  
    <div id ="ReportChart">
        <h3>Report Line Chart</h3>
        <div id="chartLoader" class="progress progress-striped" style="display:none">
            <div class="bar" style="width: 100%;"></div>
        </div>
        <div id="chartContainer" style="display:none">
            <canvas id="lineChart" width="70%"></canvas>     
        </div>
    </div>

</asp:Content>
<asp:Content ID="script" ContentPlaceHolderID="ScriptContent" runat="server">
<script type="text/javascript">
    var lineChart;
    const _label = [
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
        '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
        '21', '22', '23'
    ];
    function searchData() {
        $("#chartContainer").hide();
        $("#topProgress").show();
        $("#chartLoader").show();
        $.get(baseurl + "/Report/ReportList/" + "?initialDate=" + $("#initialDateFilter").val(), function (data) {
            $("#topProgress").hide();
            $("#ReportList").html(data);
            $.get(baseurl + "/Report/ReportList/" + "?initialDate=" + $("#initialDateFilter").val() + "&type=chart", function (data) {
                drawChart(data);
                $("#chartLoader").hide();
                $("#chartContainer").show();
            });
        });
        
        return false;
    }
    function onFirstLoadChart() {
        const data = {
          labels: _label,
          datasets: [{
            label: 'Chart',
            data: [],
            fill: false,
            tension: 0.1
         }]
        };
        lineChart = new Chart($('#lineChart'), {
            type: 'line',
            data: data
        });
       
    }
    function loadList() {
        onFirstLoadChart();
        $("#topProgress").show();
        $("#chartLoader").show();
        $.get(baseurl + "/Report/ReportList/" + "?initialDate=" + $("#initialDateFilter").val() +"&type=list", function (data) {
            $("#topProgress").hide();
            $("#ReportList").html(data);
            $.get(baseurl + "/Report/ReportList/" + "?initialDate=" + $("#initialDateFilter").val() + "&type=chart", function (data) {
                drawChart(data);
                $("#chartLoader").hide();
                 $("#chartContainer").show();
            });
            
        });
        
        return false;
    }
    function drawChart(data) {
        var bc = [];
        var plp = [];
        var others = [];
        data.forEach(function (value, index, array) {
            bc.push(value.BC);
            plp.push(value.PLP);
            others.push(value.OTHERS);
        });
        lineChart.data = {
            datasets: [
                {
                    label: 'BC',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor :'rgb(255, 99, 132)',
                    data: bc
                },
                {
                    label: 'PLP',
                    backgroundColor: 'rgb(54, 162, 235)',
                    borderColor : 'rgb(54, 162, 235)',
                    data: plp
                },
                {
                    label: 'OTHERS',
                    backgroundColor: 'rgb(75, 192, 192)',
                     borderColor :'rgb(75, 192, 192)',
                    data: others
                }
            ],
            labels: _label
        };
        lineChart.update();
    }
    function getAjaxUrl() {        
        var selectUrl = "<%=Url.Action("_AjaxFilterBinding","Report")%>" + "/?initialDate=";
        return selectUrl;
    }
    function OnDataBinding(e){
        $("#topProgress").show();
        var grid = $("#ReportList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
            //console.log(grid);
    }

    function OnDataBound(e){
        $("#topProgress").hide();
    }
    $("#exportCSV").click(function(){
        $(this).attr('target','_blank');
        window.open("<%=Url.Action("ExportCSV","Report")%>"  + "/?initialDate="+ $("#initialDateFilter").val());
    });
     $("#exportExcell").click(function(){
        $(this).attr('target','_blank');
        window.open("<%=Url.Action("ExportExcell","Report")%>"+ "/?initialDate="+ $("#initialDateFilter").val());
    });
    $(document).ready(function () {
        loadList();
    });
</script>
</asp:Content>
