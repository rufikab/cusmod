﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.ConfirmHoldP2ViewModel>" %>

<asp:Content ID="ConfirmHoldP2Title" ContentPlaceHolderID="TitleContent" runat="server">
	Confirm Hold P2 - Custom Modules
</asp:Content>

<asp:Content ID="ConfirmHoldP2Content" ContentPlaceHolderID="MainContent" runat="server">
<div class="alert <%=((bool)ViewData["HasBroken"])?"":"hide"%> alert-error pull-right ">
	    <button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4>Warning!</h4>
		Ada kontainer yang Rusak.
	</div>
    <fieldset>
		<legend><i class="icon-thumbs-up"></i> Confirm Hold P2</legend>
		<form class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="inputPassword">No. Container :</label>
				<div class="controls">
					<input name="ContainerNumberFilter" type="text" maxlength="11" id="ContainerNumberFilter" placeholder="No. Container" style="width:100px" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputPassword">No.Dok. Pabean :</label>
				<div class="controls">
					<input type="text" name="DocumentNumberFilter" id="DocumentNumberFilter" placeholder="No. Dok. Pabean" />
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<button title="Cari" type="submit" class="btn" onclick="return search()"><i class="icon-search"></i></button>
				</div>
			</div>
		</form>
	</fieldset>
        <div id="topProgress" class="modal-body progress progress-striped active pull-left"  style="display:none;width: 100%;padding: 0;margin: 3px 0px 5px;">
            <div  class="bar" style="width: 100%;"></div>
        </div>
        <div class="clearfix"></div>
    <div id="ConfirmP2List">
    
        <%Html.RenderPartial("ConfirmP2List", Model.listItemViewModel); %>
    </div>  
</asp:Content>

<asp:Content ID="script" ContentPlaceHolderID="ScriptContent" runat="server">
<script type="text/javascript">
    function search() {
        $("#topProgress").show();
        var containerID = ($("#ContainerNumberFilter").val() == null) ? "NoDataInput" : $("#ContainerNumberFilter").val();
        var documentNumber = ($("#DocumentNumberFilter").val() == null) ? "NoDataInput" : $("#DocumentNumberFilter").val();
        var filterString = "filter..." + containerID + "..." + documentNumber;

        $.get(baseurl+"/ConfirmHoldP2/ConfirmP2List/?SessionData=" + filterString, function (data) {
            $("#topProgress").hide();
            $("#ConfirmP2List").html(data);
        });
        return false;
    }
    function getAjaxUrl() {
        fromDate = $("#fromDate").val();
        toDate = $("#toDate").val();
        fromDate = getDate(fromDate);
        toDate = getDate(toDate);
        
        var selectUrl = "<%=Url.Action("_AjaxFilterBinding","ConfirmHoldP2")%>" + "/?from=" + fromDate + "&to=" + toDate;
        return selectUrl;
    }

    function OnDataBinding(e){
        $("#topProgress").show();
        var grid = $("#ContainerList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
    }

    function OnDataBound(e){
        $("#topProgress").hide();
    }

    function loadList() {
        $("#topProgress").show();
        $.get(baseurl+"/ConfirmHoldP2/ConfirmP2List", function (data) {
            $("#topProgress").hide();
            $("#ConfirmP2List").html(data);
        });
        return false;
    }

    $(document).ready(function(){ 
    loadList();
    });
</script>
</asp:Content>
