﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<List<CustomModule.Models.ConfirmListHoldP2ViewModel>>" %>
<%
        Html.Telerik()
                .Grid(Model)
                .Name("ContainerList")
                .ClientEvents(events => events.OnDataBinding("OnDataBinding").OnDataBound("OnDataBound"))
                .Columns(columns =>
                {
                    columns.Bound(o => o.IsConfirmed).Title("Status");
                    columns.Bound(o => o.TerminalResponse).Title("Status Terminal");
                    columns.Bound(o => o.P2File).Title("Dokumen").Encoded(false);
                    columns.Bound(o => o.DocumentNumber).Title("No. Dok. Pabean");
                    columns.Bound(o => o.DocumentDate).Title("Tgl. Dok. Pabean");
                    columns.Bound(o => o.DocumentType).Title("Jenis Dok. Pabean");
                    columns.Bound(o => o.ContainerID).Title("No. Container");
                    columns.Bound(o => o.Action).Title("Action").Encoded(false);
                })
                .Sortable(sorting => sorting
                    .SortMode(GridSortMode.MultipleColumn)
                    .OrderBy(order =>
                    {
                        order.Add(o => o.IsConfirmed);
                        order.Add(o => o.DocumentNumber);
                        order.Add(o => o.DocumentDate);
                        order.Add(o => o.ContainerID);
                    }
                    ))
                .DataBinding(d => d.Ajax().Select("_AjaxBinding", "ConfirmHoldP2"))
                .Pageable(paging=>paging.PageSize(20)
                    .Style(GridPagerStyles.NextPreviousAndNumeric)
                    .Position(GridPagerPosition.Bottom))
                .Footer(true)
                .Filterable().Render();
        %>