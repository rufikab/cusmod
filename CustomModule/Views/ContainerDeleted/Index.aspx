﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Container Deleted
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<style type="text/css">
.card {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 120px;
    border: 1px solid #cccccc;
    float:left;
    margin-right:10px;
    margin-top:10px;
}

/* On mouse-over, add a deeper shadow */
.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

/* Add some padding inside the card container */
.container {
    padding: 2px 16px;
}
.card-title1
{
    background-color:#05842c;
    color:white;
    padding:5px;
    margin-top: 0px;
}
.card-title2
{
    background-color:#9e3d3d;
    color:white;
    padding:5px;
    margin-top: 0px;
}

label.datetimepicker2{
    margin-right: 10px;
}

div.datetimepicker2{
    margin-left: 10px;
    margin-right: 10px;
}
</style>
<div class="alert <%=((bool)ViewData["HasBroken"])?"":"hide"%> alert-error pull-right ">
	    <button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4>Warning!</h4>
		Ada kontainer yang Rusak.
	</div>
    <fieldset>
		<legend><i class="icon-remove-circle"></i> Container Deleted</legend>
		<form class="form-horizontal">
			<div class="control-group">
				<label class="datetimepicker2 control-label" for="inputEmail">Tanggal Delete :</label>
                <div class="datetimepicker2 input-append">
                    <input data-format="MM/dd/yyyy" type="text" id="fromDate" value="<%=(ViewData["dtFrom"])%>"/>
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div> - 
                <div class="datetimepicker2 input-append">
                    <input data-format="MM/dd/yyyy" type="text" id="toDate" value="<%=(ViewData["dtEnd"])%>"/>
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputPassword">No. Container :</label>
				<div class="controls">
					<input name="inputNoContainer" type="text" onKeyDown="limitText(this.form.limitedtextfield,this.form.countdown,15);" 
onKeyUp="limitText(this.form.limitedtextfield,this.form.countdown,15);" maxlength="11" id="inputNoContainer" placeholder="No. Container">
				</div>
			</div>

			<div class="control-group">
				<div class="controls">
					<a title="Cari" class="btn" onclick="filterGridData();"><i class="icon-search"></i></a>
                    <%--<div class="btn-group pull-right">
					    <a class="btn pull-right dropdown-toggle" data-toggle="dropdown">Export <span class="caret"></span></a>
                        <ul class="dropdown-menu pull-right">
                        <li><a id="exportCSV">Export to CSV</a></li>
                        <li><a id="exportExcell">Export to Excell</a></li>
                        </ul>
				    </div>--%>
				</div>
			</div>
            <div class="card-content control-group" style="padding-left: 25px;padding-right: 25px;">
            </div>
		</form>
	</fieldset>
    <div id="topProgress" class="progress progress-striped" style="display:none">
            <div  class="bar" style="width: 100%;"></div>
        </div>
    <div id="arsipList" class="DataGridXScroll span12">
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<style>
    .width1400
    {
        width: 1400px;
    }
    .DataGridXScroll
    {
        overflow-x: scroll;
        text-align: left;
    }
</style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
<!--/ Modal Detail-->

<div id="containerModal" class="modal hide fade fullwidth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div id="containerModalProgress" class="modal-body progress progress-striped">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <div id="containerModalCreateContent" ></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-primary" id="printContainer">Cetak</button>
	</div>
</div>

<script src="<%=Url.Content("~/Content/js/bootstrap-datetimepicker.min.js")%>" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $('.datetimepicker2').datetimepicker({
            language: 'en',
            //pick12HourFormat: true
            pickTime: false
        });
    });

    containerModal = function (id, type) {
        $("#containerModal").unbind();
        // wire up the buttons to dismiss the modal when shown
        $("#containerModal").bind("show", function () {
            $(".modal-footer").hide();
            $("#containerModal").addClass("container");
            $("#containerModalProgress").show();
            $("#containerModalCreateContent").hide();
            $("#containerModalProgressTitle").show();
            $("#containerModal .btn:not(.btn-primary)").click(function (e) {
                // hide the dialog box
                $("#containerModal").modal('hide');
            });
            $("#printContainer").click(function (e) {
                $("#containerModalCreateContent").print();
                //$("#containerModal").modal('hide');
            });
//            $.get(baseurl+"/Container/Detail/" + id, function (data) { //old
            $.get(baseurl +"/Container/DetailDeleted/?id=" + id+"&type="+type+"&next="+0+"&prev="+0, function (data) {
            
                $("#containerModal").css("margin-top", "-282px");
                $(".modal-footer").show();
                $("#containerModalProgressTitle").hide();
                $("#containerModalProgress").hide();
                $("#containerModalCreateContent").html(data);
                $("#containerModalCreateContent").show();
            });
        });
        // remove the event listeners when the dialog is hidden
        $("#containerModal").bind("hide", function () {
            // remove event listeners on the buttons
            $("#containerModal .btn").unbind();
            $("#containerModal").removeClass("container");
        });
        // finally, wire up the actual modal functionality and show the dialog
        $("#containerModal").modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true // this parameter ensures the modal is shown immediately
        });
    }

    function getDate(date){
        if(!date){
            date ="";
        }else{
            date = date.split('/');
            if ($.isArray(date)) {
                date = date[0] + '-' + date[1] + '-' + date[2];
            }
        }  
        return  date;
    }

    function OnDataBinding(e){
        $("#topProgress").show();
        var grid = $("#ContainerDeletedList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
    }

    function OnDataBound(e){
        $("#topProgress").hide();
    }

    function getParamUrl(){
        fromDate = $("#fromDate").val();
        toDate = $("#toDate").val();
        fromDate = getDate(fromDate);
        toDate = getDate(toDate);
        cn = $("#inputNoContainer").val();

        return "/?from=" + fromDate + "&to=" + toDate+ "&cn=" + cn;
    }

    function getAjaxUrl() {
        var selectUrl = "<%=Url.Action("_AjaxFilterBinding","ContainerDeleted")%>" +getParamUrl();
        return selectUrl;
    }

    function filterGridData() {
        var grid = $("#ContainerDeletedList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
        grid.ajaxRequest();
        return false;
    }

    <%--$("#exportCSV").click(function(){
        $(this).attr('target','_blank');
        window.open("<%=Url.Action("ExportCSV","ContainerDeleted")%>"+getParamUrl());
    });

    $("#exportExcell").click(function(){
        $(this).attr('target','_blank');
        window.open("<%=Url.Action("ExportExcell","ContainerDeleted")%>"+getParamUrl());
    });--%>

    function loadList() {
        $("#topProgress").show();
        $.get(baseurl +"/ContainerDeleted/IndexAjax" + getParamUrl(), function (data) {
            $("#topProgress").hide();
            $("#arsipList").html(data);
        });
        return false;
    }
    
    $(document).ready(function(){
        loadList();
    });


</script>
</asp:Content>
