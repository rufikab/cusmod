﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.PutusanModel>" %>
    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		<h3>Input Putusan</h3>
	</div>
	<div class="modal-body">
		<% using (Html.BeginForm("Putusan", "Penyegelan", FormMethod.Post, new { @class = "form-horizontal" }))
     { %>
            <%= Html.ValidationSummary(true, "Error.", new { @class = "alert alert-error" })%>
		  
			<div class="control-group">
				<label class="control-label" for="inputNoContainer">No. Container :</label>
				<div class="controls">
                    <%=Html.HiddenFor(model => model.CONTAINER_ID)%>
                    <%=Html.HiddenFor(model => model.CONTAINERNUMBER)%>
			    	<%=Html.TextBoxFor(model => model.CONTAINERNUMBER, new { @disabled = "disabled" })%><br />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputEmail">Kondisi :</label>
				<div class="controls">
                    <div class="btn-group" data-toggle="buttons-radio" id="utuh">
						<button type="button" onClick="$('#Utuh').val('True');" class="btn btn-radio <%=(Model.Utuh)?"active":""%>" >Utuh</button>
						<button type="button" onClick="$('#Utuh').val('False');" class="btn btn-radio <%=(!Model.Utuh)?"active":""%>">Rusak</button>
                        <input id="Utuh" type="hidden" name="Utuh" value="<%=(Model.Utuh)?"True":"False"%>">
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="alasan">Keterangan :</label>
				<div class="controls">
                    <%=Html.TextAreaFor(model => model.NOTE, new { @class = "input-xlarge" })%><br />
                    <%=Html.ValidationMessageFor(model => model.NOTE, null, new { @class = "alert alert-error" })%>
				</div>
			</div>
        <% }%>
	</div>