﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.PutusanModel>" %>
<%CustomModule.CONTAINER container = (CustomModule.CONTAINER)ViewData["Container"]; %>
   <div class="modal-body">
   <% using (Html.BeginForm("Hold", "Penyegelan", FormMethod.Post, new { @class = "form-horizontal" }))
     { %>
            <%= Html.ValidationSummary(true, "Error.", new { @class = "alert alert-error" })%>
		<div class="row-fluid">
			<div class="span12">
				<div class="span6">
					<div id="myCarousel" class="carousel slide">
						<!-- Carousel items -->
						<div class="carousel-inner">
					        <%
                                var first = false;
                                var active = "active";
                                foreach (CustomModule.PICTURE src in (List<CustomModule.PICTURE>)ViewData["ImageList"])
                                {
                              %>
					        <div class="<%=active %> item">
                                <a href="http://<%=Request.ServerVariables["SERVER_NAME"]%><%=ViewData["BaseUrl"]%><%=src.PICTURE_ID%>" target="_blank"><img src="http://<%=Request.ServerVariables["SERVER_NAME"]%><%=ViewData["BaseUrl"]%><%=src.PICTURE_ID%>" alt="Picture <%=src.PICTURE_ID%>"/></a>
                                    <%--<img src="<%=ViewData["BaseUrl"]%><%=src.PICTURE_ID%>" alt="Picture <%=src.PICTURE_ID%>"/>--%>
					        </div>
                            <% 
                                    if (!first) { first = true; active = ""; }
                              } %>
				        </div>
						<!-- Carousel nav -->
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
						<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
						
					</div>
				</div>
				
				<div class="span6">
                    <%=Html.HiddenFor(model => model.CONTAINER_ID)%>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Nomor :</label>
						<div class="controls">
			    	        <%=Html.TextBoxFor(model => model.CONTAINERNUMBER, new { @disabled = "disabled" })%><br />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Kondisi :</label>
						<div class="controls">
                            <div class="btn-group" data-toggle="buttons-radio" id="utuh">
						        <button type="button" onClick="$('#Utuh').val('True');$('#holdContainer').html('Release')" class="btn btn-radio <%=(Model.Utuh)?"active":""%>" >Utuh</button>
						        <button type="button" onClick="$('#Utuh').val('False');$('#holdContainer').html('Hold')" class="btn btn-radio <%=(!Model.Utuh)?"active":""%>">Rusak</button>
                                <input id="Utuh" type="hidden" name="Utuh" value="<%=(Model.Utuh)?"True":"False"%>">
					        </div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Keterangan :</label>
						<div class="controls">
                            <%=Html.TextAreaFor(model => model.NOTE, new { @class = "input-xlarge" })%><br />
                            <%=Html.ValidationMessageFor(model =>model.NOTE, null, new { @class = "alert alert-error" })%>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6">
			<table class="table table-condensed table-detail table-striped">
				<thead>
					<th colspan="2">Data Container</th>
				</thead>
				<tbody>
					<tr>
						<td class="td-label">Container No. :</td>
						<td><%=container.CONTAINERNUMBER %></td>
					</tr>
					<tr>
						<td class="td-label">Gate in Time :</td>
						<td><%=container.GATEINTIME %></td>
					</tr>
                    
					<tr>
						<td class="td-label">Brutto Weights :</td>
                        <%if (container.BRUTTOWEIGHTS == null || container.BRUTTOWEIGHTS == 0)
                          { %>
						<td></td>
                        <%}else{ %>
						<td><%=container.BRUTTOWEIGHTS.Value.ToString("#,###.##")%></td>
                         <%} %>
					</tr>
					<tr>
						<td class="td-label">Truck No. :</td>
						<td><%=container.TRUCKNUMBER %></td>
					</tr>
					<tr>
						<td class="td-label">Truck Company Name :</td>
						<td><%=container.TRUCKCOMPANYNAME %></td>
					</tr>
					<tr>
						<td class="td-label">Truck Position :</td>
						<td><%=container.TRUCKPOSITION %></td>
					</tr>
					<tr>
						<td class="td-label">Vessel Name</td>
						<td><%=container.VESSELNAME %></td>
					</tr>
					<tr>
						<td class="td-label">Voyage :</td>
						<td><%=container.VOYAGE %></td>
					</tr>
					<tr>
						<td class="td-label">ATB (Actual Time Berthing) :</td>
						<td><%=container.ATA.Substring(6, 2) + '/' + container.ATA.Substring(4, 2) + '/' + container.ATA.Substring(0, 4)%></td>
					</tr>
					<tr>
						<td class="td-label">ATD (Actual Time Departure) :</td>
						<td><%=container.ATD.Substring(6, 2) + '/' + container.ATD.Substring(4, 2) + '/' + container.ATD.Substring(0, 4)%></td>
					</tr>
					<tr>
						<td class="td-label">Port of Discharge :</td>
						<td><%=container.PORTDISCHARGE %></td>
					</tr>
					<tr>
						<td class="td-label">Hold Remark :</td>
						<td><%=container.HOLDNOTE %></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="span6">
			<table class="table table-condensed table-detail table-striped">
				<thead>
					<th colspan="2">Data Customs</th>
				</thead>
				<tbody>
					<tr>
						<td class="td-label">Document Type :</td>
						<td><%=container.DOCUMENTTYPE %></td>
					</tr>
					<tr>
						<td class="td-label">Document No. :</td>
						<td><%=container.DOCUMENTNUMBER %></td>
					</tr>
					<tr>
						<td class="td-label">Document Date :</td>
						<td><%=container.DOCUMENTDATE %></td>
					</tr>
					<tr>
						<td class="td-label">Shippe / Consignee :</td>
						<td><%=container.CONSIGNEE %></td>
					</tr>
					<tr>
						<td class="td-label">Intelligence Note :</td>
						<td><%=container.INTELLIGENCENOTE %></td>
					</tr>
					<tr>
						<td class="td-label">BL Number</td>
						<td><%=container.BLNUMBER %></td>
					</tr>
					<tr>
						<td class="td-label">BC 1.1 Number :</td>
						<td><%=container.BCNUMBER %></td>
					</tr>
					<tr>
						<td class="td-label">KPBC Code :</td>
						<td><%=container.KPBCCODE %></td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>
        
        <% }%>
	</div>