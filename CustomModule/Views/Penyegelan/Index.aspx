﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Pemeriksaan segel
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="alert <%=((bool)ViewData["HasBroken"])?"":"hide"%> alert-error pull-right ">
	    <button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4>Warning!</h4>
		Ada kontainer yang Rusak.
	</div>
        <fieldset>
			<legend><i class="icon-check"></i> Pemeriksaan Segel</legend>
			<form class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="inputEmail">Tanggal :</label>
					<div class="controls">
						<input class="datepicker" type="text" id="fromDate" /> - <input class="datepicker" type="text" id="toDate" />
					</div>
				</div>
				<div class="control-group">
					<div class="controls">

                        <a href="javascript:void(0)" title="Cari" class="btn" onclick="return filterGridData()"><i class="icon-search"></i></a>
					</div>
				</div>
			</form>
		</fieldset>
        <div id="topProgress" class="progress progress-striped" style="display:none">
            <div  class="bar" style="width: 100%;"></div>
        </div>
        <div id="penyegelanList">
        </div>
</asp:Content>

<asp:Content ID="script" ContentPlaceHolderID="ScriptContent" runat="server"> 
<!--/ Modal Detail-->

<div id="containerModal" class="modal hide fade fullwidth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div id="containerModalProgress" class="modal-body progress progress-striped">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <div id="containerModalCreateContent" ></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        
		<button class="btn btn-primary" aria-hidden="true" id="holdContainer">Hold</button>
		<button class="btn btn-primary" id="printContainer">Cetak</button>
	</div>
</div>
<script type="text/javascript">
    containerModal = function (id, type) {
        $("#containerModal").unbind();
        // wire up the buttons to dismiss the modal when shown
        $("#containerModal").bind("show", function () {
            $(".modal-footer").hide();
            $("#containerModal").addClass("container");
            $("#containerModalProgress").show();
            $("#containerModalCreateContent").hide();
            $("#containerModalProgressTitle").show();
            $("#containerModal .btn:not(.btn-primary)").click(function (e) {
                // hide the dialog box
                $("#containerModal").modal('hide');
            });
            $("#printContainer").click(function (e) {
                $("#containerModalCreateContent").print();
            });
            $("#holdContainer").click(function (e) {
                var form = $("#containerModalCreateContent").find("form");
                $("#containerModalProgress").show();
                $("#containerModalCreateContent").hide();
                $.post(form.attr("action"), form.serialize(), function (data) {
                    if (data == "Success") {
                        $.get(baseurl+"/Penyegelan/IndexAjax", function (data) {
                            $("#penyegelanList").html(data);
                            $("#containerModal").modal('hide');
                        });
                    } else {
                        $("#containerModalCreateContent").show();
                        $("#containerModalCreateContent").html(data);
                        $("#containerModalProgress").hide();
                    }
                    if ($("#containerModal span.alert").html() == "") {
                        $("#containerModal span.alert").hide();
                    } else {
                        $("#containerModal span.alert").show();
                    }
                });
            });
            $.get(baseurl+"/Penyegelan/Hold/" + id, function (data) {
                $("#containerModal").css("margin-top", "-282px");
                $(".modal-footer").show();
                $("#containerModalProgressTitle").hide();
                $("#containerModalProgress").hide();
                $("#containerModalCreateContent").html(data);
                $("#containerModalCreateContent").show();
                if ($("#containerModal span.alert").html() == "") {
                    $("#containerModal span.alert").hide();
                } else {
                    $("#containerModal span.alert").show();
                }
            });
        });
        // remove the event listeners when the dialog is hidden
        $("#containerModal").bind("hide", function () {
            // remove event listeners on the buttons
            $("#containerModal .btn").unbind();
            $("#containerModal").removeClass("container");
        });
        // finally, wire up the actual modal functionality and show the dialog
        $("#containerModal").modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true // this parameter ensures the modal is shown immediately
        });
    }

    putusanModal = function (id) {
        url = '/Penyegelan/Putusan/' + id;
        createModal(url, "penyegelan", "/Penyegelan/IndexAjax/");
    }

    function getDate(date){
        if(!date){
            date ="";
        }else{
            date = date.split('/');
            if ($.isArray(date)) {
                date = date[0] + '-' + date[1] + '-' + date[2];
            }
        }  
        return  date;
    }

    function getAjaxUrl() {
        fromDate = $("#fromDate").val();
        toDate = $("#toDate").val();
        fromDate = getDate(fromDate);
        toDate = getDate(toDate);
        
        var selectUrl = "<%=Url.Action("_AjaxFilterBinding","Penyegelan")%>" + "/?from=" + fromDate + "&to=" + toDate;
        return selectUrl;
    }


    function OnDataBinding(e){
        $("#topProgress").show();
        var grid = $("#ContainerList").data('tGrid');
        //console.log(grid);
        grid.ajax.selectUrl = getAjaxUrl();
    }

    function OnDataBound(e){
        $("#topProgress").hide();
    }

    function filterGridData() {
        var grid = $("#ContainerList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
        grid.ajaxRequest();
    }

    function loadList() {
        $("#topProgress").show();
        $.get(baseurl+"/Penyegelan/IndexAjax/", function (data) {
            $("#topProgress").hide();
            $("#penyegelanList").html(data);
        });
        return false;
    }

    function SetKodeForPosko(){
        var day = pad($("#POSKODD").val(), 2);  
        var month = pad($("#POSKOMM").val(), 2);  
        var code = pad($("#POSKO").val(), 2);
        var year = $("#POSKOYYYY").val();
        $("#KodePenyegelan").val(day + month + code + "/POSKO/" + year);
    }
    function SetKodeForHanggar(){
        var code = pad($("#HANGGAR").val(), 7);
        var year = $("#HANGGARYYYY").val();
        $("#KodePenyegelan").val(code + "/KOJA/" + year);
    }

    function pad (str, max) {
      return str.length < max ? pad("0" + str, max) : str;
    }  

    function ShowRowCode(type){
        var dt = new Date();

        if(type==1){
            $("#HANGGARField").hide();
            $("#POSKOField").show();
            $("#POSKODD").val(dt.getDate());
            $("#POSKOMM").val(dt.getMonth());
            $("#POSKOYYYY").val(dt.getFullYear());
        }
        else{
            $("#HANGGARField").show();
            $("#POSKOField").hide();
            $("#HANGGARYYYY").val(dt.getFullYear());
        }
    }

    $(document).ready(function(){ 
        loadList();
    });
</script>
</asp:Content>