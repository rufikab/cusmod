﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
    <div class="modal-body">
		<form class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="noDokumen">No. Dokumen :</label>
				<div class="controls">
					<input type="text" id="noDokumen"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="noDokumen">Jenis Dokumen :</label>
				<div class="controls">
					<input type="text" id="noDokumen"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="noDokumen">Tanggal Dokumen :</label>
				<div class="controls">
					<input class="datepicker" type="text" id="noDokumen"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputNoContainer">No. Container</label>
				<div class="controls">
					<input name="limitedtextfield" type="text" onKeyDown="limitText(this.form.limitedtextfield,this.form.countdown,15);" 
onKeyUp="limitText(this.form.limitedtextfield,this.form.countdown,15);" maxlength="11" id="inputNoContainer" placeholder="No. Container">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputEmail">Masa Berlaku :</label>
				<div class="controls">
					<input class="datepicker" type="text" id="incomingDate" /> - <input class="datepicker" type="text" id="incomingDate" /> 
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputEmail">Aktif :</label>
				<div class="controls">
					<div class="btn-group" data-toggle="buttons-radio">
						<button type="button" class="btn">Ya</button>
						<button type="button" class="btn">Tidak</button>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="noDokumen">Keterangan :</label>
				<div class="controls">
					<textarea></textarea>
				</div>
			</div>
		</form>
	</div>