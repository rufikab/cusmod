﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<List<CustomModule.Models.UserActivityModel>>" %>
                        <%
                        Html.Telerik()
                              .Grid(Model)
                              .Name("ContainerList")
                              .ClientEvents(events=>events.OnDataBinding("OnDataBinding").OnDataBound("OnDataBound"))
                              .Columns(columns =>
                              {
                                  columns.Bound(o => o.USERNAME).Title("Username");
                                  columns.Bound(o => o.ACTIVITYDESCR).Title("Aktivitas");
                                  columns.Bound(o => o.ACTIVITYDATE).Title("Tanggal");
                              })
                              .DataBinding(d => d.Ajax().Select("_AjaxBindingReport", "UserActivityReport"))
                              .Pageable(paging=>paging.PageSize(20)
                                  .Style(GridPagerStyles.NextPreviousAndNumeric)
                                  .Position(GridPagerPosition.Bottom))
                              .Footer(true)
                              .HtmlAttributes(new{@class="width1400"})
                              .Filterable().Render();
                      %>