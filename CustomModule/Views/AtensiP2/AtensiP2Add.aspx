﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.AtensiP2ItemViewModel>" %>

    <%=Html.ValidationSummary(true, "Tidak dapat menyimpan. mohon lengkapi masukan Anda terlebih dahulu.", new { @class = "alert alert-error" })%>
    <%=Html.HiddenFor(model=> model.ID) %>
    <div class="control-group">
		<label class="control-label" for="DocumentNumber">No. Dokumen :</label>
		<div class="controls">
            <%=Html.TextBoxFor(model => model.DocumentNumber) %>
            <%=Html.ValidationMessageFor(model => model.DocumentNumber, null, new { @class = "alert alert-error", @style="display:table;" })%>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="DocumentType">Jenis Dokumen :</label>
		<div class="controls">
            <%=Html.TextBoxFor(model => model.DocumentType) %>
		    <%=Html.ValidationMessageFor(model => model.DocumentType, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="DocumentDate">Tanggal Dokumen :</label>
		<div class="controls">
			<%=Html.TextBoxFor(model => model.DocumentDate, new { @class = "datepicker" })%>
		    <%=Html.ValidationMessageFor(model => model.DocumentDate, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>
    
    <div class="control-group">
		<label class="control-label" for="IsActive">Export/Import :</label>
		<div class="controls">
            <%=Html.HiddenFor(model => model.ExportImport)%>
			<div class="btn-group" data-toggle="buttons-radio">
				<button type="button" class="btn active" onclick="$('#ExportImport').val('E')">Export</button>
				<button type="button" class="btn" onclick="$('#ExportImport').val('I')">Import</button>
			</div>
		    <%=Html.ValidationMessageFor(model => model.ExportImport, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="ContainerID"><b></font>No. Kontainer :</b></label>
		<div class="controls">
			<%=Html.TextBoxFor(model => model.ContainerID, new { @maxlength="11", @placeholder="No. Container" })%>
		    <%=Html.ValidationMessageFor(model => model.ContainerID, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>
    
	<div class="control-group">
		<label class="control-label" for="docFile">File Dokumen :</label>
		<div class="controls">
            <%=Html.TextBoxFor(model => model.docFile, new { @type = "file" })%>
		    <%=Html.ValidationMessageFor(model => model.docFile, null, new { @class = "alert alert-error", @style = "display:table;" })%>

		</div>
	</div>
   
    <div class="control-group" style="display:none">
		<label class="control-label" for="IsActive">Aktif :</label>
		<div class="controls">
            <%=Html.HiddenFor(model => model.IsActive) %>
			<div class="btn-group" data-toggle="buttons-radio">
				<button type="button" class="btn active" onclick="return ActivateP2(1)">Ya</button>
				<button type="button" class="btn" onclick="return ActivateP2(0)">Tidak</button>
			</div>
		    <%=Html.ValidationMessageFor(model => model.IsActive, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="DateInput">Tanggal Input/Jam :</label>
		<div class="controls">
			<%=Html.TextBoxFor(model => model.DateInput, new {@readOnly = "readOnly" })%>
            <%=Html.ValidationMessageFor(model => model.DateInput, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="InputBy">Di-input oleh :</label>
		<div class="controls">
			<%=Html.TextBoxFor(model => model.InputBy, new { @Value=User.Identity.Name, @readOnly="readOnly" }) %>
		    <%=Html.ValidationMessageFor(model => model.InputBy, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="Reason">Alasan :</label>
		<div class="controls">
			<%=Html.TextAreaFor(model => model.Reason,3, 3, new { @placeholder="Alasan" }) %>
            <%=Html.ValidationMessageFor(model => model.Reason, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>