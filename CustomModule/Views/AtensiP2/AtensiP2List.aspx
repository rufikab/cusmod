﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<List<CustomModule.Models.AtensiP2ListItemViewModel>>" %>
<div style="text-align:right" class="pull-right">
        <a href="#myModal" title="Tambah" role="button" class="btn small btn-primary" data-toggle="modal" onclick="ResetAtensiP2Form()">Tambah Baru</a>
    </div>
    <div id="topProgress" class="modal-body progress progress-striped active pull-left"  style="display:none;width: 85%;padding: 0;margin: 3px 0px 5px;">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <div class="clearfix"></div>
<%
        Html.Telerik()
                .Grid(Model)
                .Name("ContainerList")
                .ClientEvents(events => events.OnDataBinding("OnDataBinding").OnDataBound("OnDataBound"))
                .Columns(columns =>
                {
                    columns.Bound(o => o.ExportImport).Title("E/I");
                    columns.Bound(o => o.Hold).Title("Hold").Width(150);
                    columns.Bound(o => o.Release).Title("Release").Width(150);
                    columns.Bound(o => o.IsConfirmed).Title("Status");
                    columns.Bound(o => o.TerminalStatus).Title("Status Terminal").Width(100);
                    columns.Bound(o => o.DocumentNumber).Title("No. Dok. Pabean");
                    columns.Bound(o => o.DocumentDate).Title("Tgl. Dok. Pabean");
                    columns.Bound(o => o.DocumentType).Title("Jenis Dok. Pabean");
                    columns.Bound(o => o.ContainerID).Title("No. Container");
                    columns.Bound(o => o.Reason).Title("Note");
                    columns.Bound(o => o.Action).Title("Action").Encoded(false);
                    columns.Bound(o => o.Action2).Title("").Encoded(false);
                })
                .Sortable(sorting => sorting
                    .SortMode(GridSortMode.MultipleColumn)
                    )
                .DataBinding(d => d.Ajax().Select("_AjaxBinding", "AtensiP2"))
                .Pageable(paging=>paging.PageSize(20)
                    .Style(GridPagerStyles.NextPreviousAndNumeric)
                    .Position(GridPagerPosition.Bottom))
                .Footer(true)
                .Filterable().Render();
        %>

<%--<table class="table table-bordered table-striped">
	<thead>
        <th>
		    <a href="#myModal" title="Tambah" role="button" class="btn mini" data-toggle="modal"><i class="icon-plus-sign"></i></a>
        </th>
		<th>Status Hold</th>
		<th>No. Dok. Pabean</th>
		<th>Tgl. Dok. Pabean</th>
		<th>Jenis Dok. Pabean</th>
		<th>No. Container / Size</th>
		<th>Masa Berlaku</th>
		<th>Aktif</th>
		<th>Action</th>
	</thead>
	<tbody>
            <% int i = 1; 
        if(Model.listViewModel != null){
            foreach (var item in Model.listViewModel)
            {  %>
		<tr>
			<td class="center"><%=i++ %></td>
			<td><%=(item.IsConfirmed == 1) ? "Confirmed" : "Requested" %></td>
			<td><%=item.DocumentNumber %></td>
			<td><%=item.DocumentDate %></td>
			<td><%=item.DocumentType %></td>
			<td><%=item.ContainerID %></td>
			<td><%=(item.DateFrom.ToShortDateString() + " - " +item.DateTo.ToShortDateString()) %></td>
			<td><%=(item.IsActive == 1) ? "Ya" : "Tidak" %></td>
			<td class="center">
                <a href="javascript:void(0);" title="Delete"  onclick="confirmModalDeleteAtensiP2('/AtensiP2/AtensiP2Delete/<%=item.ID%>','AtensiP2','/AtensiP2/AtensiP2List');"  class="btn btn-mini"><i class="icon-trash"></i></a>
				<a href="#editAtensiP2" title="Edit" role="button" class="btn btn-mini a-edit" data-toggle="modal" data='{"EditID":"<%: item.ID %>",
                                                            "EditDocumentNumber":"<%: item.DocumentNumber %>",
                                                            "EditDocumentType":"<%: item.DocumentType %>",
                                                            "EditDocumentDate":"<%: item.DocumentDate.ToShortDateString() %>",
                                                            "EditContainerID":"<%: item.ContainerID %>",
                                                            "EditDateFrom":"<%: item.DateFrom.ToShortDateString() %>",
                                                            "EditDateTo":"<%: item.DateTo.ToShortDateString() %>",
                                                            "EditDateInput":"<%: item.DateInput.ToShortDateString() %>",
                                                            "EditInputBy":"<%: item.InputBy %>",
                                                            "EditReason":"<%: item.Reason %>",
                                                            "EditIsActive":"<%: item.IsActive %>",
                                                            "EditIsConfirmed":"<%: item.IsConfirmed%>",
                                                            "EditUserID":"<%: item.UserID %>"}'><i class="icon-edit"></i></a>
			</td>
		</tr>
		    <%} 
        }%>
	</tbody>
</table>
<%if ((int)ViewData["TotalPages"] > 1)
    { %>
<div class="pagination pagination-centered">
	<ul>
		<li><a href="javascript:void(0)" onclick="gotoPage(<%=(int)ViewData["Page"]-1%>)">Prev</a></li>
        <%for (i = 1; i <= (int)ViewData["TotalPages"]; i++)
            { %>
		<li><a href="javascript:void(0)" onclick="gotoPage(<%=i%>)"><%=i %></a></li>
        <% }%>
		<li><a href="javascript:void(0)" onclick="gotoPage(<%=(int)ViewData["Page"]+1%>)">Next</a></li>
	</ul>
</div>
<% }%>--%>
