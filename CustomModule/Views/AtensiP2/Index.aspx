﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.AtensiP2ViewModel>" %>

<asp:Content ID="AtensiP2Title" ContentPlaceHolderID="TitleContent" runat="server">
	Atensi P2 - Custom Modules
</asp:Content>

<asp:Content ID="AtensiP2Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="alert <%=((bool)ViewData["HasBroken"])?"":"hide"%> alert-error pull-right ">
	    <button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4>Warning!</h4>
		Ada kontainer yang Rusak.
	</div>
	<fieldset>
		<legend><i class="icon-exclamation-sign"></i> Atensi P2</legend>
   <%-- <% using (Html.BeginForm("AtensiP2Filter", "AtensiP2", FormMethod.Post, new { @class = "form-horizontal" }))
        { %>--%>
		<form class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="inputEmail">Tgl Dok. Pabean :</label>
				<div class="controls">
					<input class="datepicker" type="text" id="StartDateFilter" name="StartDateFilter" style="width:100px" /> - <input class="datepicker" type="text" id="EndDateFilter" name="EndDateFilter" style="width:100px" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputPassword">No. Container :</label>
				<div class="controls">
					<input name="ContainerNumberFilter" type="text" maxlength="11" id="ContainerNumberFilter" placeholder="No. Container" style="width:100px" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputPassword">No.Dok. Pabean :</label>
				<div class="controls">
					<input type="text" name="DocumentNumberFilter" id="DocumentNumberFilter" placeholder="No. Dok. Pabean" />
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<button title="Cari" type="submit" class="btn" onclick="return search()"><i class="icon-search"></i></button>
				</div>
			</div>
				</form>
    <%--<% } %>--%>
	</fieldset>

    <div id="AtensiP2List">
        <%--<%Html.RenderPartial("AtensiP2List", Model.listViewModel); %>--%>
    </div>  

    <!-- Modal Add Atensi P2-->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-backdrop="static" data-keyboard="false">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		<h3 id="myModalLabel">Input Dokumen Atensi P2</h3>
	</div>
    <div id="modalProgressAdd" class="modal-body progress progress-striped active"  style="display:none">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <% using (Html.BeginForm("AtensiP2Add", "AtensiP2", FormMethod.Post, new { @class = "form-horizontal", @id = "AtensiP2Add", @enctype = "multipart/form-data" }))
        { %>
	<div id="modalContentAdd" class="modal-body" style="display:block">
        <%Html.RenderPartial("AtensiP2Add", Model.addItemViewModel); %>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
		<button class="btn btn-primary">Simpan</button>
	</div>
    <% } %>
</div>
<!--/ Modal Add Atensi P2-->   

<!-- Modal Edit Atensi P2-->
<div id="myModalEdit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-backdrop="static" data-keyboard="false">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		<h3 id="H1">Edit Dokumen Atensi P2</h3>
	</div>
    <div id="modalProgressEdit" class="modal-body progress progress-striped active"  style="display:none">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <% using (Html.BeginForm("AtensiP2Edit", "AtensiP2", FormMethod.Post, new { @class = "form-horizontal", @id = "AtensiP2Edit" }))
        { %>
	<div id="modalContentEdit" class="modal-body" style="display:block">
        <%Html.RenderPartial("AtensiP2Edit", Model.editItemViewModel); %>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
		<button class="btn btn-primary">Simpan</button>
	</div>
    <% } %>
</div>
<!--/ Modal Edit Atensi P2-->

<!-- Modal List Atensi P2-->
<div id="containerModal" class="modal hide fade fullwidth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div id="containerModalProgress" class="modal-body progress progress-striped">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <div id="containerModalCreateContent" ></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-primary" id="printContainer">Cetak</button>
	</div>
</div>
<!-- End Modal List Atensi P2-->
</asp:Content>

<asp:Content ID="script" ContentPlaceHolderID="ScriptContent" runat="server">
<script type="text/javascript">

    function search() {
        $("#topProgress").show();

        var startDate = $("#StartDateFilter").val();
        var endDate = $("#EndDateFilter").val();
        var containerID = ($("#ContainerNumberFilter").val() == null) ? "NoDataInput" : $("#ContainerNumberFilter").val();
        var documentNumber = ($("#DocumentNumberFilter").val() == null) ? "NoDataInput" : $("#DocumentNumberFilter").val();
        var filterString = "filter..." + startDate + "..." + endDate + "..." + containerID + "..." + documentNumber;

        $.get(baseurl+"/AtensiP2/AtensiP2List/?SessionData=" + filterString, function (data) {
            $("#topProgress").hide();
            $("#AtensiP2List").html(data);
        });
        return false;
    }

    function gotoPage(page) {
        $("#topProgress").show();
        $.get(baseurl+"/AtensiP2/AtensiP2List/" + "?page=" + page, function (data) {
            $("#topProgress").hide();
            $("#AtensiP2List").html(data);
        });
        return false;
    }

    function edit(e) {
        var a = $(e);
        data = jQuery.parseJSON(a.attr("data"));
        console.log(data);
        $("#EditID").val(data.EditID);
        $("#EditDocumentNumber").val(data.EditDocumentNumber);
        $("#EditDocumentType").val(data.EditDocumentType);
        $("#EditDocumentDate").val(data.EditDocumentDate);
        $("#EditContainerID").val(data.EditContainerID);
        $("#EditDateInput").val(data.EditDateInput);
        $("#EditInputBy").val(data.EditInputBy);
        $("#EditReason").val(data.EditReason);
        $("#EditIsActive").val(data.EditIsActive);
        $("#EditExportImport").val(data.EditExportImport);
        
        var cntltxtInput = $("#EditActiveYesButton");
        var cntltxtInput2 = $("#EditActiveNoButton");
        $(cntltxtInput).attr("class", "btn");
        $(cntltxtInput2).attr("class", "btn");

        if(data.EditIsActive ==1||data.EditIsActive == "1"){
            $(cntltxtInput).attr("class", "btn active");
        }
        else if(data.EditIsActive ==0||data.EditIsActive == "0"){
            $(cntltxtInput2).attr("class", "btn active");
        }

        
        if(data.EditExportImport == 'E'){
            $("#EditImportButton").attr("class", "btn");
            $("#EditExportButton").attr("class", "btn active");
        }
        else if(data.EditExportImport == 'I'){
            $("#EditExportButton").attr("class", "btn");
            $("#EditImportButton").attr("class", "btn active");
        }

        $("#EditIsConfirmed").val(data.EditIsConfirmed);
        $("#EditUserID").val(data.EditUserID);
        
        $('#viewEditFile').hide();
        $.get(baseurl+"/AtensiP2/IsP2FileExist/" + data.EditID, function (ret) {
            console.log(ret);
            if(ret&&ret!=''){
                file = $('#viewEditFile');
                $(file).attr('href',baseurl+"/AtensiP2/GetP2File/"+data.EditID);
                $(file).show();
            }
        });

        $('#viewReleaseFile').hide();
        $.get(baseurl+"/AtensiP2/IsReleaseP2FileExist/" + data.EditID, function (ret) {
            console.log(ret);
            if(ret&&ret!=''){
                file = $('#viewReleaseFile');
                $(file).attr('href',baseurl+"/AtensiP2/GetReleaseP2File/"+data.EditID);
                $(file).show();
            }
        });

        if(data.EditIsHoldRelease=="Released"){
            $('#EditdocFile').hide();
            $('#viewReleaseFile').show();
        }else{
            $('#EditdocFile').show();
            $('#viewReleaseFile').hide();
        }
    };

    function viewP2File(id){
        $.get( baseurl+'/AtensiP2/IsP2FileExist/'+id,function (data) {
            console.log(data);
            if(data!=''){
                window.location =  baseurl+'/AtensiP2/GetP2File/' + id;
            }
        });
    }

    function getAjaxUrl() {        
        fromDate = $("#fromDate").val();
        toDate = $("#toDate").val();
        fromDate = getDate(fromDate);
        toDate = getDate(toDate);
        
        var selectUrl = "<%=Url.Action("_AjaxFilterBinding","AtensiP2")%>" + "/?from=" + fromDate + "&to=" + toDate;
        return selectUrl;
    }

    function OnDataBinding(e){
        $("#topProgress").show();
        var grid = $("#ContainerList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
            //console.log(grid);
    }

    function OnDataBound(e){
        $("#topProgress").hide();
    }
    
    
    function filterGridData() {
        var grid = $("#ContainerList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
        grid.ajaxRequest();
    }

    function ActivateP2(isActive){
        $("#IsActive").val(isActive);
        $("#EditIsActive").val(isActive);
    }

    function loadList() {
        $("#topProgress").show();
        $.get(baseurl+"/AtensiP2/AtensiP2List/", function (data) {
            $("#topProgress").hide();
            $("#AtensiP2List").html(data);
        });
        return false;
    }
    $(function () {
        $('span.field-validation-valid').hide();
        $('#AtensiP2Add').submit(function (e) {
            //var formData = new FormData($('#AtensiP2Add'));
            
            var formData = new FormData($(this)[0]);
            formData.append('file', $("#docFile")[0].files[0]);
            e.preventDefault();
            e.stopPropagation();

            $("#modalProgressAdd").show();
            $("#modalContentAdd").hide();

            $.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: $(this).attr('action'),
                data: formData,
                success: function (data) {
                    location.reload(); // reload page
                },
                error: function (x, y, z) {
                    $("#modalProgressAdd").hide();
                    $("#modalContentAdd").show();
                    if (x.responseText == "Success")
                        location.reload();
                    $('#AtensiP2Add .modal-body').html(x.responseText);
                    $('#DocumentDate').datepicker();
                    $('span.field-validation-valid').hide();
                },
                dataType: "json"
            });
        });

        $('#AtensiP2Edit').submit(function (e) {
            var formData = new FormData($(this)[0]);
            formData.append('file', $("#EditdocFile")[0].files[0]);
            e.preventDefault();
            e.stopPropagation();

            $("#modalProgressEdit").show();
            $("#modalContentEdit").hide();

            $.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: $(this).attr('action'),
                data: formData,
                success: function (data) {
                    location.reload(); // reload page
                },
                error: function (x, y, z) {
                    $("#modalProgressEdit").hide();
                    $("#modalContentEdit").show();
                    if (x.responseText == "Success")
                        location.reload();
                    $('#AtensiP2Edit .modal-body').html(x.responseText);
                    $('#DocumentDate').datepicker();
                    $('span.field-validation-valid').hide();
                },
                dataType: "json"
            });
        });
    });

    ResetAtensiP2Form = function () {
        $("#modalContent").find(".input-validation-error").removeClass("input-validation-error");
        $("#modalContent").find(".field-validation-error").remove();
        $("#modalContent").find(".validation-summary-errors").remove();

        $('.modal-footer').find(".btn").removeAttr('disabled');
    }

    ResetAtensiP2FormEdit = function () {
        $("#modalContentEdit").find(".input-validation-error").removeClass("input-validation-error");
        $("#modalContentEdit").find(".field-validation-error").remove();
        $("#modalContentEdit").find(".validation-summary-errors").remove();

        $('.modal-footer').find(".btn").removeAttr('disabled');
    }

    containerModal = function (id, type, hold,next,prev) {
        //console.log("Tes Modal");
        var a = $("#containerModal").unbind();
        
        modalShowed = true;
        console.log("Before");

        $("#containerModal").bind("show", function () {

            console.log("Ada");
            hold = hold || 0;
            if (hold == 1) {
                $("#holdContainer").show();
            } else {
                $("#holdContainer").hide();
            }
            $("#containerModal .modal-footer").hide();
            $("#containerModal").addClass("container");
            $("#containerModalProgress").show();
            $("#containerModalCreateContent").hide();
            $("#containerModalProgressTitle").show();
            $("#closeBtn").click(function (e) {
                $("#containerModal").modal('hide');
            });
            $("#printContainer").click(function (e) {
                $("#containerModalCreateContent").print();
            });
            $("#holdContainer").click(function (e) {
                $("#containerModal").modal('hide');
                holdModal(id, type);
            });
            $("#check").attr("disabled", "disabled");
            if(!next) next = 0;
            if(!prev) prev = 0;
            $.get(baseurl+"/Container/Detail/?id=" + id+"&type="+type+"&next="+next+"&prev="+prev, function (data) {
               
                if(data=="Empty"){
                     $("#containerModal").modal('hide');    
                }else{
                    $("#containerModal").css("margin-top", "-282px");
                    $("#containerModal .modal-footer").show();
                    $("#containerModalProgressTitle").hide();
                    $("#containerModalProgress").hide();
                    $("#containerModalCreateContent").html(data);
                    $("#containerModalCreateContent").show();
                    if($("#checkedBy").html()=="") $("#check").removeAttr("disabled");
                }
            }); 
            
        });
        // remove the event listeners when the dialog is hidden
        $("#containerModal").bind("hide", function () {
            // remove event listeners on the buttons
            $("#containerModal .btn").unbind();
            $("#containerModal").removeClass("container");
            modalShowed = false;
        });
        // finally, wire up the actual modal functionality and show the dialog
        $("#containerModal").modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true // this parameter ensures the modal is shown immediately
        });
    };
    
    releaseP2Modal = function (id, type) {
        title = (type == "E") ? "export" : "import";
        url = '/Container/ReleaseP2/' + id;
        createModal(url, title, "/Container/Index/" + type, "Release P2");
    };

    $(document).ready(function(){ 
        loadList();
    });
</script>
</asp:Content>
