﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.AtensiP2EditItemViewModel>" %>
	
    <%=Html.ValidationSummary(true, "Tidak dapat menyimpan. mohon lengkapi masukan Anda terlebih dahulu.", new { @class = "alert alert-error" })%>
    <%=Html.HiddenFor(model=> model.EditID) %>
    <%=Html.HiddenFor(model => model.EditUserID)%>
    <%=Html.HiddenFor(model => model.EditIsConfirmed)%>
    <div class="control-group">
		<label class="control-label" for="DocumentNumber">No. Dokumen :</label>
		<div class="controls">
            <%=Html.TextBoxFor(model => model.EditDocumentNumber) %>
            <%=Html.ValidationMessageFor(model => model.EditDocumentNumber, null, new { @class = "alert alert-error", @style="display:table;" })%>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="DocumentType">Jenis Dokumen :</label>
		<div class="controls">
            <%=Html.TextBoxFor(model => model.EditDocumentType) %>
		    <%=Html.ValidationMessageFor(model => model.EditDocumentType, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="DocumentDate">Tanggal Dokumen :</label>
		<div class="controls">
			<%=Html.TextBoxFor(model => model.EditDocumentDate, new { @class = "datepicker" })%>
		    <%=Html.ValidationMessageFor(model => model.EditDocumentDate, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>
    
    <div class="control-group">
		<label class="control-label" for="IsActive">Export/Import :</label>
		<div class="controls">
            <%=Html.HiddenFor(model => model.EditExportImport)%>
			<div class="btn-group" data-toggle="buttons-radio">
				<button type="button" id="EditExportButton" class="btn" onclick="$('#EditExportImport').val('E')">Export</button>
				<button type="button" id="EditImportButton" class="btn" onclick="$('#EditExportImport').val('I')">Import</button>
			</div>
		    <%=Html.ValidationMessageFor(model => model.EditExportImport, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="ContainerID">No. Kontainer :</label>
		<div class="controls">
			<%=Html.TextBoxFor(model => model.EditContainerID, new { @maxlength="11", @placeholder="No. Container" })%>
		    <%=Html.ValidationMessageFor(model => model.EditContainerID, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>
    
	<div class="control-group">
		<label class="control-label" for="docFile">File Dokumen :</label>
		<div class="controls">
            <a href="" target="_blank" style="display:none" id="viewEditFile">View File</a>
            <a href="" target="_blank" style="display:none" id="viewReleaseFile">View Release File</a>
            <%=Html.TextBoxFor(model => model.EditdocFile, new { @type = "file" })%>
		    <%=Html.ValidationMessageFor(model => model.EditdocFile, null, new { @class = "alert alert-error", @style = "display:table;" })%>

		</div>
	</div>
    <div class="control-group">
		<label class="control-label" for="IsActive">Aktif :</label>
		<div class="controls">
            <%=Html.HiddenFor(model => model.EditIsActive) %>
			<div class="btn-group" data-toggle="buttons-radio">
				<button type="button" id="EditActiveYesButton" class="btn" onclick="return ActivateP2(1)">Ya</button>
				<button type="button" id="EditActiveNoButton" class="btn" onclick="return ActivateP2(0)">Tidak</button>
			</div>
		    <%=Html.ValidationMessageFor(model => model.EditIsActive, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="DateInput">Tanggal Input/Jam :</label>
		<div class="controls">
			<%=Html.TextBoxFor(model => model.EditDateInput, new { @Value = DateTime.Now.ToString(), @readOnly = "readOnly" })%>
            <%=Html.ValidationMessageFor(model => model.EditDateInput, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="InputBy">Di-input oleh :</label>
		<div class="controls">
			<%=Html.TextBoxFor(model => model.EditInputBy, new { @Value=User.Identity.Name, @readOnly="readOnly" }) %>
		    <%=Html.ValidationMessageFor(model => model.EditInputBy, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="Reason">Alasan :</label>
		<div class="controls">
			<%=Html.TextAreaFor(model => model.EditReason,3, 3, new { @placeholder="Alasan" }) %>
            <%=Html.ValidationMessageFor(model => model.EditReason, null, new { @class = "alert alert-error", @style = "display:table;" })%>
		</div>
	</div>