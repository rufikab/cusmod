﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<List<CustomModule.Models.ContainerModel>>" %>
                    <%Html.Telerik().Grid(Model)
                              .Name("Container" + ViewData["Id"] + "List")
                              .Columns(columns =>
                              {
                                  columns.Bound(o => o.TAGNUMBER).Title("No. Tag");
                                  columns.Bound(o => o.TRUCKNUMBER).Title("No. Polisi");
                                  columns.Bound(o => o.GATENUMBER).Title("No. Gate in");
                                  columns.Bound(o => o.GATEINTIME).Title("Waktu Masuk");
                                  columns.Bound(o => o.SGATEOUTTIME).Title("Waktu Keluar");
                                  columns.Bound(o => o.ContainerAndSize).Title("No. Container / Size");
                                  columns.Bound(o => o.DOCUMENTNUMBER).Title("Dok. Pabean");
                                  columns.Bound(o => o.HOLDSTATUS).Title("Hold Status");
                                  columns.Bound(o => o.TRUCKPOSITION).Title("Movement");
                                  columns.Bound(o => o.Note).Title("Keterangan");
                                  columns.Bound(o => o.HoldBy).Title("Held By");
                                  columns.Bound(o => o.ReleaseBy).Title("Released By");
                                  columns.Bound(o => o.CHECKEDBY).Title("Checked By");
                                  columns.Bound(o => o.StrCheckedTime).Title("Checked Time");
                                  columns.Bound(o => o.Action).Title("Action").Encoded(false);
                                  columns.Bound(o => o.GATEOUTTIMES).Hidden();

                              })
                              .Sortable(sorting => sorting
                                  .SortMode(GridSortMode.SingleColumn)
                                  .OrderBy(order =>
                                      {
                                          order.Add(o => o.GATEOUTTIMES).Descending();
                                      }
                                  ))
                              .DataBinding(d => d.Ajax().Select("_Sorting", "Container", new { id = ViewData["Id"] }))
                              .Pageable(paging => paging.PageSize(10)
                                  .Style(GridPagerStyles.NextPreviousAndNumeric)
                                  .Position(GridPagerPosition.Bottom))
                              .Footer(true)
                              .Filterable().Render();
                      %>