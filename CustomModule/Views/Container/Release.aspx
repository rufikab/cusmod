﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.ReleaseModel>" %>
    <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        <%if (Model.IS_SPPBE)
          { %>
	    <h3>Input Keterangan Batal Export</h3>
        <%}else{%>
	    <h3>Input Alasan Release</h3>
        <%}%>
    </div>
    <div class="modal-body">
	  <% using (Html.BeginForm("Release", "Container", FormMethod.Post, new { @class = "form-horizontal" }))
      { %>
            <%= Html.ValidationSummary(true, "Error. ", new { @class = "alert alert-error" })%>
		    <div class="control-group">
			    <label class="control-label" for="inputNoContainer">No. Container :</label>
			    <div class="controls">
				    <%=Html.HiddenFor(model => model.ID)%>
                    <%=Html.HiddenFor(model => model.CONTAINERNUMBER)%>
			    	<%=Html.TextBoxFor(model => model.CONTAINERNUMBER, new { @disabled = "disabled" })%><br />
			    </div>
		    </div>
		    <div class="control-group">
			    <label class="control-label" for="alasan">Keterangan :</label>
			    <div class="controls">
				    <%=Html.TextAreaFor(model => model.RELEASENOTE)%><br />
                    <%=Html.ValidationMessageFor(model => model.RELEASENOTE, null, new { @class = "alert alert-error" })%>
			    </div>
		    </div>
        <% }%>
    </div>