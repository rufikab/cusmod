﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.HoldModel>" %>
    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		<h3>Input Alasan Hold</h3>
	</div>
	<div class="modal-body">
		<% using (Html.BeginForm("Hold", "Container", FormMethod.Post, new { @class = "form-horizontal" }))
     { %>
            <%= Html.ValidationSummary(true, "Error. ", new { @class = "alert alert-error" })%>
		  
			<div class="control-group">
				<label class="control-label" for="inputNoContainer">No. Container :</label>
				<div class="controls">
                    <%=Html.HiddenFor(model => model.ID)%>
                    <%=Html.HiddenFor(model => model.CONTAINERNUMBER)%>
			    	<%=Html.TextBoxFor(model => model.CONTAINERNUMBER, new { @disabled = "disabled" })%><br />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="alasan">Keterangan :</label>
				<div class="controls">
			    	<%=Html.TextAreaFor(model => model.HOLDNOTE)%><br />
                    <%=Html.ValidationMessageFor(model => model.HOLDNOTE, null, new { @class = "alert alert-error" })%>
				</div>
			</div>
        <% }%>
	</div>