﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<CustomModule.CONTAINER>" %>
    <div class="modal-body">
		<div class="truck-view">
			<% var imageList = (List<CustomModule.PICTURE>)ViewData["ImageList"];%>
			<% var imageOcrList = (List<string>)ViewData["ImageOcrList"];%>
			<% 
            
                var baseUrl = Request.ServerVariables["SERVER_NAME"];
                if (baseUrl == "localhost")
                {
                    baseUrl = "10.10.33.44";
                }
            if (imageList.Count() > 0 || imageOcrList.Count() > 0)
            { %>
			<div id="myCarousel" class="carousel slide">
				<!-- Carousel items -->
				<div class="carousel-inner">
                    <%
                        var first = false;
                        var active = "active";

                        foreach (CustomModule.PICTURE src in imageList)
                        {
                      %>

					<div class="<%=active %> item">
                    <a href="http://<%=baseUrl%><%=ViewData["BaseUrl"]%><%=src.PICTURE_ID%>" target="_blank"><img src="http://<%=baseUrl%><%=ViewData["BaseUrl"]%><%=src.PICTURE_ID%>" alt="Picture <%=src.PICTURE_ID%>"/></a>
                        <%--<img src="<%=ViewData["BaseUrl"]%><%=src.PICTURE_ID%>" alt="Picture <%=src.PICTURE_ID%>"/>--%>
					</div>
                    <% 
                            if (!first) { first = true; active = ""; }
                      } %>

                      <%
                            foreach (string src in imageOcrList)
                        {
                      %>
                        <div class="<%=active %> item">
                        <a href="http://<%=baseUrl%><%=src%>" target="_blank">
                        <img src="http://<%=baseUrl%><%=src%>" alt="Picture <%=src%>"/></a>
					    </div>
                      <% 
                      } %>
				</div>
				<!-- Carousel nav -->
				<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
				<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
			</div>
			
            <%}%>
            
		</div>
		<div class="span11">
			<table class="table table-condensed table-detail table-striped">
            <thead>
				<th colspan="2">Detail Informasi</th>
			</thead></table>
        </div>
		<div class="span6">
            <input type="hidden" name="ID" value="<%=Model.ID%>" id="containerId" />
            <input type="hidden" name="HOLDSTATUS" value="<%=Model.HOLDSTATUS.Trim()%>" id="holdStatus" />
			<table class="table table-condensed table-detail table-striped">
				<tbody>
					<tr>
						<td class="td-label">Container No. :</td>
						<td><%=Model.CONTAINERNUMBER %></td>
					</tr>
					<tr>
						<td class="td-label">Common Gate IN Time :</td>
						<td><%=Model.GATEINTIME %></td>
					</tr>
					<tr>
						<td class="td-label">Common Gate OUT Time :</td>
						<td><%=Model.GATEOUTTIME %></td>
					</tr>
					<tr>
						<td class="td-label">Brutto Weights :</td>
                        <%if (Model.BRUTTOWEIGHTS==null||Model.BRUTTOWEIGHTS == 0)
                          { %>
						<td></td>
                        <%}else{ %>
						<td><%=Model.BRUTTOWEIGHTS.Value.ToString("#,###.##")%></td>
                         <%} %>
					</tr>
					<tr>
						<td class="td-label">Truck No. :</td>
						<td><%=Model.TRUCKNUMBER %></td>
					</tr>
					<tr>
						<td class="td-label">Truck Company Name :</td>
						<td><%=Model.TRUCKCOMPANYNAME %></td>
					</tr>
					<tr>
						<td class="td-label">Truck Position :</td>
						<td ><%=Model.TRUCKPOSITION %></td>
					</tr>
					<tr>
						<td class="td-label">Container Position :</td>
						<td id="ContainerPosition"> ...</td>
					</tr>
					<tr>
						<td class="td-label">Vessel Name</td>
						<td><%=Model.VESSELNAME %></td>
					</tr>
					<tr>
						<td class="td-label">Voyage :</td>
						<td><%=Model.VOYAGE %></td>
					</tr>
					<tr>
						<td class="td-label">ATB (Actual Time Berthing) :</td>
						<td>
                          <%if (Model.ATA!=null)
                          { %>
                        <%=Model.ATA.Substring(6, 2) + '/' + Model.ATA.Substring(4, 2) + '/' + Model.ATA.Substring(0, 4)%>
                        <%} %>
                        </td>
					</tr>
					<tr>
						<td class="td-label">ATD (Actual Time Departure) :</td>
						<td>
						<%if (Model.ATD!=null)
                          { %>
						<%=Model.ATD.Substring(6, 2) + '/' + Model.ATD.Substring(4, 2) + '/' + Model.ATD.Substring(0, 4)%>
						 <%} %>
						</td>
					</tr>
					<tr>
						<td class="td-label">Port of Discharge :</td>
						<td><%=Model.PORTDISCHARGE %></td>
					</tr>
                    <%if(ViewData["atensi"]!=null){
                          CustomModule.ATENSI_P2 atensi = ( CustomModule.ATENSI_P2)ViewData["atensi"];
                    %>
                         <%if (atensi.HOLDDATE.HasValue)
                          { %>
                            <tr>
						        <td class="td-label">Hold by :</td>
						        <td><span id="Span3"><%=atensi.HOLDBY%></span>
                                <%if (atensi.HOLDDATE.HasValue)
                                  { %>
                               (<%=atensi.HOLDDATE.GetValueOrDefault().ToString("dd-MM-yyy hh:mm")%>)</td>
                                <%} %>
					        </tr>
                            <%} %>
                            <%if (atensi.RELEASEDATE.HasValue)
                                  { %>
					        <tr>
						        <td class="td-label">Release Note:</td>
						        <td><%=atensi.RELEASENOTE%></td>
					        </tr>
                            <tr>
						        <td class="td-label">Release by :</td>
						        <td><span id="Span4"><%=atensi.RELEASEBY%></span>
                                <%if (atensi.RELEASEDATE.HasValue)
                                  { %>
                               (<%=atensi.RELEASEDATE.GetValueOrDefault().ToString("dd-MM-yyy hh:mm")%>)</td>
                                <%} %>
					        </tr>
                            <%} %>
                    
                    <%}else{ %>
                        
                        <%if (Model.HOLDTIME.HasValue)
                          { %>
					        <tr>
						        <td class="td-label">Hold Remark :</td>
						        <td><%=Model.HOLDNOTE %></td>
					        </tr>
                            <tr>
						        <td class="td-label">Hold by :</td>
						        <td><span id="Span1"><%=Model.HOLDBY%></span>
                                <%if (Model.HOLDTIME.HasValue)
                                  { %>
                               (<%=Model.HOLDTIME.GetValueOrDefault().ToString("dd-MM-yyy hh:mm")%>)</td>
                                <%} %>
					        </tr>
                            <%} %>
                            <%if (Model.RELEASETIME.HasValue)
                                  { %>
                                  
                                    <%if(ViewData["BatalMuat"]!=null){%>
                                    
                                   <%}else{%>
                                   
					                    <tr>
						                    <td class="td-label">Release Note:</td>
						                    <td><%=Model.RELEASENOTE %></td>
					                    </tr>
                                        <tr>
						                    <td class="td-label">Release by :</td>
						                    <td><span id="Span2"><%=Model.RELEASEBY%></span>
                                            <%if (Model.RELEASETIME.HasValue)
                                              { %>
                                           (<%=Model.RELEASETIME.GetValueOrDefault().ToString("dd-MM-yyy hh:mm")%>)</td>
                                            <%} %>
					                    </tr>
                                   <%}%>
                            <%} %>
                            <tr>
						        <td class="td-label">Checked by :</td>
						        <td><span id="checkedBy"><%=Model.CHECKEDBY%></span>
                                <%if (Model.CHECKEDTIME.HasValue)
                                  { %>
                               (<%=Model.CHECKEDTIME.GetValueOrDefault().ToString("dd-MM-yyy hh:mm")%>)</td>
                                <%} %>
					        </tr>
                    
                    <%} %>
				</tbody>
			</table>
		</div>
        <div class="span5">
			<table class="table table-condensed table-detail table-striped">
				<tbody>
                    <%if(ViewData["BatalMuat"]==null){%>
                        <tr>
						    <td class="td-label">Export/Import:</td>
						    <td><%=(Model.EXPORTIMPORT.Trim() == "E" ? "Export" : "Import")%></td>
					    </tr>
                    <%}%>
					<tr>
						<td class="td-label">Document Type :</td>
						<td><%=Model.DOCUMENTTYPE %></td>
					</tr>
					<tr>
						<td class="td-label">Document No. :</td>
						<td><%=Model.DOCUMENTNUMBER %></td>
					</tr>
					<tr>
						<td class="td-label">Document Date :</td>
						<td>
                        <% if (Model.DOCUMENTDATE.HasValue) { %>
                            <%=Model.DOCUMENTDATE.GetValueOrDefault().ToString("dd-MM-yyy")%></td>
                        <%   } %>
                        
					</tr>
					<tr>
						<td class="td-label">Shippe / Consignee :</td>
						<td><%=Model.CONSIGNEE %></td>
					</tr>
					<tr>
						<td class="td-label">Intelligence Note :</td>
						<td><%=Model.INTELLIGENCENOTE %></td>
					</tr>
					<tr>
						<td class="td-label">BL Number</td>
						<td><%=Model.BLNUMBER %></td>
					</tr>
					<tr>
						<td class="td-label">BC 1.1 Number :</td>
						<td><%=Model.BCNUMBER %></td>
					</tr>
					<tr>
						<td class="td-label">KPBC Code :</td>
						<td><%=Model.KPBCCODE %></td>
					</tr>
                    <%if(ViewData["BatalMuat"]!=null){%>
                                    
					    <tr>
						    <td class="td-label">Keterangan :</td>
						    <td><%=Model.RELEASENOTE %></td>
					    </tr>
                        <tr>
						    <td class="td-label">Input by :</td>
						    <td><span id="Span10"><%=Model.RELEASEBY%></span>
                            <%if (Model.RELEASETIME.HasValue)
                                { %>
                            (<%=Model.RELEASETIME.GetValueOrDefault().ToString("dd-MM-yyy hh:mm")%>)</td>
                            <%} %>
					    </tr>

                    <%}%>
				</tbody>
			</table>
		</div>

        <%if(ViewData["BatalMuat"]!=null){
              CustomModule.CONTAINER batalMuat = (CustomModule.CONTAINER)ViewData["BatalMuat"];
               %>
               
        
		<div class="span11">
			<table class="table table-condensed table-detail table-striped">
            <thead>
				<th colspan="2">Detail Informasi Batal Export</th>
			</thead></table>
        </div>
		<div class="span6">
			<table class="table table-condensed table-detail table-striped">
				<tbody>
					<tr>
						<td class="td-label">Container No. :</td>
						<td><%=batalMuat.CONTAINERNUMBER%></td>
					</tr>
					<tr>
						<td class="td-label">Common Gate IN Time :</td>
						<td><%=batalMuat.GATEINTIME%></td>
					</tr>
					<tr>
						<td class="td-label">Common Gate OUT Time :</td>
						<td><%=batalMuat.GATEOUTTIME%></td>
					</tr>
					<tr>
						<td class="td-label">Brutto Weights :</td>
                        <%if (batalMuat.BRUTTOWEIGHTS == null || batalMuat.BRUTTOWEIGHTS == 0)
                          { %>
						<td></td>
                        <%}else{ %>
						<td><%=batalMuat.BRUTTOWEIGHTS.Value.ToString("#,###.##")%></td>
                         <%} %>
					</tr>
					<tr>
						<td class="td-label">Truck No. :</td>
						<td><%=batalMuat.TRUCKNUMBER%></td>
					</tr>
					<tr>
						<td class="td-label">Truck Company Name :</td>
						<td><%=batalMuat.TRUCKCOMPANYNAME%></td>
					</tr>
					<tr>
						<td class="td-label">Truck Position :</td>
						<td><%=batalMuat.TRUCKPOSITION%></td>
					</tr>
					<tr>
						<td class="td-label">Vessel Name</td>
						<td><%=batalMuat.VESSELNAME%></td>
					</tr>
					<tr>
						<td class="td-label">Voyage :</td>
						<td><%=batalMuat.VOYAGE%></td>
					</tr>
					<tr>
						<td class="td-label">ATB (Actual Time Berthing) :</td>
						<td>
                          <%if (batalMuat.ATA != null)
                          { %>
                        <%=batalMuat.ATA.Substring(6, 2) + '/' + batalMuat.ATA.Substring(4, 2) + '/' + batalMuat.ATA.Substring(0, 4)%>
                        <%} %>
                        </td>
					</tr>
					<tr>
						<td class="td-label">ATD (Actual Time Departure) :</td>
						<td><%=batalMuat.ATD.Substring(6, 2) + '/' + batalMuat.ATD.Substring(4, 2) + '/' + batalMuat.ATD.Substring(0, 4)%></td>
					</tr>
					<tr>
						<td class="td-label">Port of Discharge :</td>
						<td><%=batalMuat.PORTDISCHARGE%></td>
					</tr>
                    <%if(ViewData["atensi"]!=null){
                          CustomModule.ATENSI_P2 atensi = ( CustomModule.ATENSI_P2)ViewData["atensi"];
                    %>
                         <%if (atensi.HOLDDATE.HasValue)
                          { %>
                            <tr>
						        <td class="td-label">Hold by :</td>
						        <td><span id="Span5"><%=atensi.HOLDBY%></span>
                                <%if (atensi.HOLDDATE.HasValue)
                                  { %>
                               (<%=atensi.HOLDDATE.GetValueOrDefault().ToString("dd-MM-yyy hh:mm")%>)</td>
                                <%} %>
					        </tr>
                            <%} %>
                            <%if (atensi.RELEASEDATE.HasValue)
                                  { %>
					        <tr>
						        <td class="td-label">Release Note:</td>
						        <td><%=atensi.RELEASENOTE%></td>
					        </tr>
                            <tr>
						        <td class="td-label">Release by :</td>
						        <td><span id="Span6"><%=atensi.RELEASEBY%></span>
                                <%if (atensi.RELEASEDATE.HasValue)
                                  { %>
                               (<%=atensi.RELEASEDATE.GetValueOrDefault().ToString("dd-MM-yyy hh:mm")%>)</td>
                                <%} %>
					        </tr>
                            <%} %>
                    
                    <%}else{ %>
                        
                        <%if (batalMuat.HOLDTIME.HasValue)
                          { %>
					        <tr>
						        <td class="td-label">Hold Remark :</td>
						        <td><%=Model.HOLDNOTE %></td>
					        </tr>
                            <tr>
						        <td class="td-label">Hold by :</td>
						        <td><span id="Span7"><%=batalMuat.HOLDBY%></span>
                                <%if (batalMuat.HOLDTIME.HasValue)
                                  { %>
                               (<%=batalMuat.HOLDTIME.GetValueOrDefault().ToString("dd-MM-yyy hh:mm")%>)</td>
                                <%} %>
					        </tr>
                            <%} %>
                            <%if (batalMuat.RELEASETIME.HasValue)
                                  { %>
					        <tr>
						        <td class="td-label">Release Note:</td>
						        <td><%=batalMuat.RELEASENOTE%></td>
					        </tr>
                            <tr>
						        <td class="td-label">Release by :</td>
						        <td><span id="Span8"><%=Model.RELEASEBY%></span>
                                <%if (batalMuat.RELEASETIME.HasValue)
                                  { %>
                               (<%=batalMuat.RELEASETIME.GetValueOrDefault().ToString("dd-MM-yyy hh:mm")%>)</td>
                                <%} %>
					        </tr>
                            <%} %>
                            <tr>
						        <td class="td-label">Checked by :</td>
						        <td><span id="Span9"><%=batalMuat.CHECKEDBY%></span>
                                <%if (batalMuat.CHECKEDTIME.HasValue)
                                  { %>
                               (<%=batalMuat.CHECKEDTIME.GetValueOrDefault().ToString("dd-MM-yyy hh:mm")%>)</td>
                                <%} %>
					        </tr>
                    
                    <%} %>
				</tbody>
			</table>
		</div>
        <div class="span5" style="font-color:red">
			<table class="table table-condensed table-detail table-striped">
				<tbody>
					<tr>
						<td class="td-label">Export/Import:</td>
						<td><%=(batalMuat.EXPORTIMPORT.Trim()=="E"?"Export":"Import") %></td>
					</tr>
                    <tr>
						<td class="td-label">Brutto Weights :</td>
                        <%if (batalMuat.BRUTTOWEIGHTS == null || batalMuat.BRUTTOWEIGHTS == 0)
                          { %>
						<td></td>
                        <%}else{ %>
						<td><%=batalMuat.BRUTTOWEIGHTS.Value.ToString("#,###.##")%></td>
                         <%} %>
					</tr>
					<tr>
						<td class="td-label">Document Type :</td>
						<td><%=batalMuat.DOCUMENTTYPE%></td>
					</tr>
					<tr>
						<td class="td-label">Document No. :</td>
						<td><%=batalMuat.DOCUMENTNUMBER%></td>
					</tr>
					<tr>
						<td class="td-label">Document Date :</td>
						<td>
                        <% if (batalMuat.DOCUMENTDATE.HasValue)
                           { %>
                            <%=batalMuat.DOCUMENTDATE.GetValueOrDefault().ToString("dd-MM-yyy")%></td>
                        <%   } %>
                        
					</tr>
					<tr>
						<td class="td-label">Shippe / Consignee :</td>
						<td><%=batalMuat.CONSIGNEE%></td>
					</tr>
					<tr>
						<td class="td-label">Intelligence Note :</td>
						<td><%=batalMuat.INTELLIGENCENOTE%></td>
					</tr>
					<tr>
						<td class="td-label">BL Number</td>
						<td><%=batalMuat.BLNUMBER%></td>
					</tr>
					<tr>
						<td class="td-label">BC 1.1 Number :</td>
						<td><%=batalMuat.BCNUMBER%></td>
					</tr>
					<tr>
						<td class="td-label">KPBC Code :</td>
						<td><%=batalMuat.KPBCCODE%></td>
					</tr>
				</tbody>
			</table>
		</div>
        <%} %>

	</div>
    <script type="text/javascript">
        var INOUT = "<%=(Model.EXPORTIMPORT.Trim() == "E" ? "IN" : "OUT")%>";
        var VOYAGE = "<%=Model.VOYAGE %>";
        var DOCUMENTNUMBER = "<%=Model.DOCUMENTNUMBER %>";
        var CONTAINERNUMBER = "<%=Model.CONTAINERNUMBER %>";
        var GATEINTIME = "<%=Model.GATEINTIME.Value.ToString("ddMMyyyy")%>";
        function loadTracking(){
         $.ajax({
                type: "POST",
                datatype: "json",
                url: baseurl + "/Container/TrackingContainer",
                data: {
                    INOUT:INOUT,
                    VOYAGE:VOYAGE,
                    DOCUMENTNUMBER:DOCUMENTNUMBER,
                    CONTAINERNUMBER:CONTAINERNUMBER,
                    GATEINTIME:GATEINTIME
                },
                success: function (data) {
                    if(data!="")
                        $("#ContainerPosition").html(data +" <a href='javascript:void(0)' onclick='loadTracking()'>(UPDATE)</a>");
                },
                error: function () {
                }
            });
        }
        
        /*
        $('#cctvImage').click(function (e){
            $('#myCarousel').show();
            $('#ocrCarousel').hide();
        });
        $('#ocrImage').click(function (e){
            $('#myCarousel').hide();
            $('#ocrCarousel').show();
        });
        */

        $(function () {
            loadTracking();
        });
    </script>













