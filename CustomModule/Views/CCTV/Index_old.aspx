﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/SiteCCTV.Master" 

Inherits="System.Web.Mvc.ViewPage<List<CustomModule.Models.GateModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	CCTV
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

                   <table class="table">
                        <% int i = 1; %>
                        <%foreach (CustomModule.Models.GateModel gate in Model)
                          { %>
                        <tr>
                            <% int j = i; %>
                            <%foreach (CustomModule.AG_CAMERA camera in gate.camera.ToList())
                              { %>
                            <td id="titleframe<%=i%>">
                            <div style="margin: 15px;"><%=gate.NAME%> - (<%=camera.NAME %> - <%=camera.CAMERAIP %>)</div>
                            </td>
                            <%
                                i++;
                              } %>     
                        </tr>

                        <tr>
                        
                            <% i = j; %>
                            <%foreach (CustomModule.AG_CAMERA camera in gate.camera.ToList())
                              { %>
                              
                            <td id = "frame<%=i%>">
                                <div style="border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px; max-height:280px;">
                                    <img src="http://root:root@<%=camera.CAMERAIP %>/axis-cgi/mjpg/video.cgi?resolution=320x240" style="height:240; width:320" alt="" />
                                </div>              
                             </td>
                            <%i++;
                              } %> 
                        </tr>
                        <%}%>

                   </table>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        var selectedframe = "";
        var selectedip = "";
        var selectedtitle = "";
        var selectedGate = "";
        $(function () {
            selectedframe = "";
            selectedip = "";
            selectedtitle = "";
            selectedGate = "";
        });

        setFramedropdown = function (objF, objFr) {
            selectedframe = objF;
            $("#FrameLabel").html(objFr);
        }

        setIPdropdown = function (objIp, strObjIp) {
            selectedip = objIp;
            selectedtitle = strObjIp;
            $("#IpLabel").html(strObjIp);
        }


        setGatedropdown = function (gate) {
            $("#lblGate").html(gate);
            if (gate == "ENTRY GATE" || gate == "EXIT GATE") {
                document.getElementById("btnFrame").disabled = true;
                document.getElementById("btnIP").disabled = true;
            } else {
                document.getElementById("btnFrame").disabled = false;
                document.getElementById("btnIP").disabled = false;
            }
            selectedGate = gate;
        }
        change = function () {
            if (selectedGate == "ENTRY GATE") {    
                var baseURLframe1 = "http://root:root@10.116.224.10/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe2 = "http://root:root@10.116.224.11/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe3 = "http://root:root@10.116.224.12/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe4 = "http://root:root@10.116.224.28/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe5 = "http://root:root@10.116.224.29/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe6 = "http://root:root@10.116.224.30/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe7 = "http://root:root@10.116.224.46/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe8 = "http://root:root@10.116.224.47/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe9 = "http://root:root@10.116.224.48/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe10 = "http://root:root@10.116.224.64/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe11 = "http://root:root@10.116.224.65/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe12 = "http://root:root@10.116.224.66/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe13 = "http://root:root@10.116.224.82/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe14 = "http://root:root@10.116.224.83/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe15 = "http://root:root@10.116.224.84/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe16 = "http://root:root@10.116.224.100/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe17 = "http://root:root@10.116.224.101/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe18 = "http://root:root@10.116.224.103/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe19 = "http://root:root@10.116.224.118/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe20 = "http://root:root@10.116.224.119/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe21 = "http://root:root@10.116.224.120/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var strhtmlframe1 = "";
                strhtmlframe1 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe1 += "<img src='" + baseURLframe1 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe2 = "";
                strhtmlframe2 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe2 += "<img src='" + baseURLframe2 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe3 = "";
                strhtmlframe3 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe3 += "<img src='" + baseURLframe3 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe4 = "";
                strhtmlframe4 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe4 += "<img src='" + baseURLframe4 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe5 = "";
                strhtmlframe5 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe5 += "<img src='" + baseURLframe5 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe6 = "";
                strhtmlframe6 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe6 += "<img src='" + baseURLframe6 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe7 = "";
                strhtmlframe7 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe7 += "<img src='" + baseURLframe7 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe8 = "";
                strhtmlframe8 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe8 += "<img src='" + baseURLframe8 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe9 = "";
                strhtmlframe9 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe9 += "<img src='" + baseURLframe9 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe10 = "";
                strhtmlframe10 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe10 += "<img src='" + baseURLframe10 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe11 = "";
                strhtmlframe11 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe11 += "<img src='" + baseURLframe11 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe12 = "";
                strhtmlframe12 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe12 += "<img src='" + baseURLframe12 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe13 = "";
                strhtmlframe13 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe13 += "<img src='" + baseURLframe13 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe14 = "";
                strhtmlframe14 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe14 += "<img src='" + baseURLframe14 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe15 = "";
                strhtmlframe15 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe15 += "<img src='" + baseURLframe15 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe16 = "";
                strhtmlframe16 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe16 += "<img src='" + baseURLframe16 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe17 = "";
                strhtmlframe17 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe17 += "<img src='" + baseURLframe17 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe18 = "";
                strhtmlframe18 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe18 += "<img src='" + baseURLframe18 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe19 = "";
                strhtmlframe19 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe19 += "<img src='" + baseURLframe19 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe20 = "";
                strhtmlframe20 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe20 += "<img src='" + baseURLframe20 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe21 = "";
                strhtmlframe21 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe21 += "<img src='" + baseURLframe21 + "' style='height:240;width:320' alt=''>";

                $("#titleframe1").html("<div style='margin: 15px;'>FRAME I - (ENTRY 1 - 10.116.224.10)</div>");
                $("#titleframe2").html("<div style='margin: 15px;'>FRAME II - (ENTRY 2 - 10.116.224.11)</div>");
                $("#titleframe3").html("<div style='margin: 15px;'>FRAME III - (ENTRY 3 - 10.116.224.12)</div>");
                $("#titleframe4").html("<div style='margin: 15px;'>FRAME IV - (ENTRY 4 - 10.116.224.28)</div>");
                $("#titleframe5").html("<div style='margin: 15px;'>FRAME V - (ENTRY 5 - 10.116.224.29)</div>");
                $("#titleframe6").html("<div style='margin: 15px;'>FRAME VI - (ENTRY 6 - 10.116.224.30)</div>");
                $("#titleframe7").html("<div style='margin: 15px;'>FRAME VII - (ENTRY 7 - 10.116.224.46)</div>");
                $("#titleframe8").html("<div style='margin: 15px;'>FRAME VII - (ENTRY 8 - 10.116.224.47)</div>");
                $("#titleframe9").html("<div style='margin: 15px;'>FRAME IX - (ENTRY 9 - 10.116.224.48)</div>");
                $("#titleframe10").html("<div style='margin: 15px;'>FRAME X - (ENTRY 10 - 10.116.224.64)</div>");
                $("#titleframe11").html("<div style='margin: 15px;'>FRAME XI - (ENTRY 11 - 10.116.224.65)</div>");
                $("#titleframe12").html("<div style='margin: 15px;'>FRAME XII - (ENTRY 12 - 10.116.224.66)</div>");
                $("#titleframe13").html("<div style='margin: 15px;'>FRAME XIII - (ENTRY 13 - 10.116.224.82)</div>");
                $("#titleframe14").html("<div style='margin: 15px;'>FRAME XIV - (ENTRY 14 - 10.116.224.83)</div>");
                $("#titleframe15").html("<div style='margin: 15px;'>FRAME XV - (ENTRY 15 - 10.116.224.84)</div>");
                $("#titleframe16").html("<div style='margin: 15px;'>FRAME XVI - (ENTRY 16 - 10.116.224.100)</div>");
                $("#titleframe17").html("<div style='margin: 15px;'>FRAME XVII - (ENTRY 17 - 10.116.224.101)</div>");
                $("#titleframe18").html("<div style='margin: 15px;'>FRAME XVIII - (ENTRY 18 - 10.116.224.102)</div>");
                $("#titleframe19").html("<div style='margin: 15px;'>FRAME XIX - (ENTRY 19 - 10.116.224.118)</div>");
                $("#titleframe20").html("<div style='margin: 15px;'>FRAME XX - (ENTRY 20 - 10.116.224.119)</div>");
                $("#titleframe21").html("<div style='margin: 15px;'>FRAME XXI - (ENTRY 21 - 10.116.224.120)</div>");

                $("#frame1").html(strhtmlframe1);
                $("#frame2").html(strhtmlframe2);
                $("#frame3").html(strhtmlframe3);
                $("#frame4").html(strhtmlframe4);
                $("#frame5").html(strhtmlframe5);
                $("#frame6").html(strhtmlframe6);
                $("#frame7").html(strhtmlframe7);
                $("#frame8").html(strhtmlframe8);
                $("#frame9").html(strhtmlframe9);
                $("#frame10").html(strhtmlframe10);
                $("#frame11").html(strhtmlframe11);
                $("#frame12").html(strhtmlframe12);
                $("#frame13").html(strhtmlframe13);
                $("#frame14").html(strhtmlframe14);
                $("#frame15").html(strhtmlframe15);
                $("#frame16").html(strhtmlframe16);
                $("#frame17").html(strhtmlframe17);
                $("#frame18").html(strhtmlframe18);
                $("#frame19").html(strhtmlframe19);
                $("#frame20").html(strhtmlframe20);
                $("#frame21").html(strhtmlframe21);

            }
            else if (selectedGate == "EXIT GATE") {
                var baseURLframe1 = "http://root:root@10.116.225.9/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe2 = "http://root:root@10.116.225.11/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe3 = "http://root:root@10.116.225.27/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe4 = "http://root:root@10.116.225.29/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe5 = "http://root:root@10.116.225.30/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe6 = "http://root:root@10.116.225.47/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe7 = "http://root:root@10.116.225.48/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe8 = "http://root:root@10.116.225.63/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe9 = "http://root:root@10.116.225.66/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe10 = "http://root:root@10.116.225.81/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe11 = "http://root:root@10.116.225.84/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var baseURLframe12 = "http://root:root@10.116.225.99/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var strhtmlframe1 = "";
                strhtmlframe1 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe1 += "<img src='" + baseURLframe1 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe2 = "";
                strhtmlframe2 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe2 += "<img src='" + baseURLframe2 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe3 = "";
                strhtmlframe3 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe3 += "<img src='" + baseURLframe3 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe4 = "";
                strhtmlframe4 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe4 += "<img src='" + baseURLframe4 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe5 = "";
                strhtmlframe5 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe5 += "<img src='" + baseURLframe5 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe6 = "";
                strhtmlframe6 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe6 += "<img src='" + baseURLframe6 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe7 = "";
                strhtmlframe7 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe7 += "<img src='" + baseURLframe7 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe8 = "";
                strhtmlframe8 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe8 += "<img src='" + baseURLframe8 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe9 = "";
                strhtmlframe9 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe9 += "<img src='" + baseURLframe9 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe10 = "";
                strhtmlframe10 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe10 += "<img src='" + baseURLframe10 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe11 = "";
                strhtmlframe11 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe11 += "<img src='" + baseURLframe11 + "' style='height:240;width:320' alt=''>";

                var strhtmlframe12 = "";
                strhtmlframe12 += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtmlframe12 += "<img src='" + baseURLframe12 + "' style='height:240;width:320' alt=''>";

                $("#titleframe1").html("<div style='margin: 15px;'>FRAME I - (EXIT 1 - 10.116.225.9)</div>");
                $("#titleframe2").html("<div style='margin: 15px;'>FRAME II - (EXIT 2 - 10.116.225.11)</div>");
                $("#titleframe3").html("<div style='margin: 15px;'>FRAME III - (EXIT 3 - 10.116.225.27)</div>");
                $("#titleframe4").html("<div style='margin: 15px;'>FRAME IV - (EXIT 4 - 10.116.225.29)</div>");
                $("#titleframe5").html("<div style='margin: 15px;'>FRAME V - (EXIT 5 - 10.116.225.30)</div>");
                $("#titleframe6").html("<div style='margin: 15px;'>FRAME VI - (EXIT 6 - 10.116.225.47)</div>");
                $("#titleframe7").html("<div style='margin: 15px;'>FRAME VII - (EXIT 7 - 10.116.225.48)</div>");
                $("#titleframe8").html("<div style='margin: 15px;'>FRAME VII - (EXIT 8 - 10.116.225.63)</div>");
                $("#titleframe9").html("<div style='margin: 15px;'>FRAME IX - (EXIT 9 - 10.116.225.66)</div>");
                $("#titleframe10").html("<div style='margin: 15px;'>FRAME X - (EXIT 10 - 10.116.225.81)</div>");
                $("#titleframe11").html("<div style='margin: 15px;'>FRAME XI - (EXIT 11 - 10.116.225.84)</div>");
                $("#titleframe12").html("<div style='margin: 15px;'>FRAME XII - (EXIT 12 - 10.116.225.99)</div>");

                $("#frame1").html(strhtmlframe1);
                $("#frame2").html(strhtmlframe2);
                $("#frame3").html(strhtmlframe3);
                $("#frame4").html(strhtmlframe4);
                $("#frame5").html(strhtmlframe5);
                $("#frame6").html(strhtmlframe6);
                $("#frame7").html(strhtmlframe7);
                $("#frame8").html(strhtmlframe8);
                $("#frame9").html(strhtmlframe9);
                $("#frame10").html(strhtmlframe10);
                $("#frame11").html(strhtmlframe11);
                $("#frame12").html(strhtmlframe12);
            }else if (selectedframe == "" || selectedip == "") {
                alert("Silahkan pilih Frame dan IP tujuan");
            } else {
                var baseURL = "http://root:0000@" + selectedip + "/axis-cgi/mjpg/video.cgi?resolution=320x240";
                var strhtml = "";
                strhtml += "<div style='border: 3px solid rgb(0, 0, 0); overflow: hidden; margin: 10px auto; max-width: 325px;max-height:280px;'>";
                strhtml += "<img src='" + baseURL + "' style='height:240;width:320' alt=''>";
                //                <img src="http://root:0000@192.168.20.60/axis-cgi/mjpg/video.cgi?resolution=320x240" style="height:240; width:320" alt="" />
                if (selectedframe == "frame1") {
                    $("#titleframe1").html("<div style='margin: 15px;'>FRAME I - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame2") {
                    $("#titleframe2").html("<div style='margin: 15px;'>FRAME II - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame3") {
                    $("#titleframe3").html("<div style='margin: 15px;'>FRAME III - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame4") {
                    $("#titleframe4").html("<div style='margin: 15px;'>FRAME IV - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame5") {
                    $("#titleframe5").html("<div style='margin: 15px;'>FRAME V - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame6") {
                    $("#titleframe6").html("<div style='margin: 15px;'>FRAME VI - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame7") {
                    $("#titleframe7").html("<div style='margin: 15px;'>FRAME VII - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame8") {
                    $("#titleframe8").html("<div style='margin: 15px;'>FRAME VIII - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame9") {
                    $("#titleframe9").html("<div style='margin: 15px;'>FRAME IX - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame10") {
                    $("#titleframe10").html("<div style='margin: 15px;'>FRAME X - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame11") {
                    $("#titleframe11").html("<div style='margin: 15px;'>FRAME XI - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame12") {
                    $("#titleframe12").html("<div style='margin: 15px;'>FRAME XII - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame13") {
                    $("#titleframe13").html("<div style='margin: 15px;'>FRAME XIII - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame14") {
                    $("#titleframe14").html("<div style='margin: 15px;'>FRAME XIV - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame15") {
                    $("#titleframe15").html("<div style='margin: 15px;'>FRAME XV - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame16") {
                    $("#titleframe16").html("<div style='margin: 15px;'>FRAME XVI - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame17") {
                    $("#titleframe17").html("<div style='margin: 15px;'>FRAME XVII - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame18") {
                    $("#titleframe18").html("<div style='margin: 15px;'>FRAME XVIII - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame19") {
                    $("#titleframe19").html("<div style='margin: 15px;'>FRAME XIX - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame20") {
                    $("#titleframe20").html("<div style='margin: 15px;'>FRAME XX - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame21") {
                    $("#titleframe21").html("<div style='margin: 15px;'>FRAME XXI - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame22") {
                    $("#titleframe22").html("<div style='margin: 15px;'>FRAME XXII - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame23") {
                    $("#titleframe23").html("<div style='margin: 15px;'>FRAME XXIII - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame24") {
                    $("#titleframe24").html("<div style='margin: 15px;'>FRAME XXIV - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame25") {
                    $("#titleframe25").html("<div style='margin: 15px;'>FRAME XXV - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame26") {
                    $("#titleframe26").html("<div style='margin: 15px;'>FRAME XXVI - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame27") {
                    $("#titleframe27").html("<div style='margin: 15px;'>FRAME XXVII - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame28") {
                    $("#titleframe28").html("<div style='margin: 15px;'>FRAME XXVIII - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame29") {
                    $("#titleframe29").html("<div style='margin: 15px;'>FRAME XXIX - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame30") {
                    $("#titleframe30").html("<div style='margin: 15px;'>FRAME XXX - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame31") {
                    $("#titleframe31").html("<div style='margin: 15px;'>FRAME XXXI - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame32") {
                    $("#titleframe32").html("<div style='margin: 15px;'>FRAME XXXII - (" + selectedtitle + ")</div>");
                } else if (selectedframe == "frame33") {
                    $("#titleframe33").html("<div style='margin: 15px;'>FRAME XXXIII - (" + selectedtitle + ")</div>");
                }

                $("#" + selectedframe).html(strhtml);
            }
        }
    </script>
</asp:Content>
