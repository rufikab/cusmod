﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<List<CustomModule.Models.ContainerModel>>" %>
                        <%
                            Html.Telerik()
                                  .Grid(Model)
                                  .Name("ContainerList")
                                  .ClientEvents(events => events.OnDataBinding("OnDataBinding").OnDataBound("OnDataBound"))
                                  .Columns(columns =>
                                  {
                                      columns.Bound(o => o.ContainerAndSize).Title("No. Container / Size");
                                      columns.Bound(o => o.GATEINTIME).Title("Entry Date");
                                      columns.Bound(o => o.VOYAGE).Title("Voyage");
                                      columns.Bound(o => o.Action).Title("Action").Encoded(false);
                                  })
                                  .Sortable(sorting => sorting
                                      .SortMode(GridSortMode.SingleColumn)
                                      //.OrderBy(order =>
                                      //{
                                      //    order.Add(o => o.GATEINTIME);                                          
                                      //}
                                      //)
                                      )
                                  .DataBinding(d => d.Ajax().Select("_AjaxBinding", "Delete"))
                                  .Pageable(paging => paging.PageSize(30)
                                      .Style(GridPagerStyles.NextPreviousAndNumeric)
                                      .Position(GridPagerPosition.Bottom))
                                  .Footer(true)
                                  .Filterable().Render();
                      %>