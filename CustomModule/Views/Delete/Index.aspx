﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Delete
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="alert <%=((bool)ViewData["HasBroken"])?"":"hide"%> alert-error pull-right ">
	    <button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4>Warning!</h4>
		Ada kontainer yang Rusak.
	</div>
    <fieldset>
		<legend><i class="icon-trash"></i> Delete</legend>
		<form class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="inputPassword">No. Container :</label>
				<div class="controls">
					<input name="inputNoContainer" type="text" onKeyDown="limitText(this.form.limitedtextfield,this.form.countdown,15);" 
onKeyUp="limitText(this.form.limitedtextfield,this.form.countdown,15);" maxlength="11" id="inputNoContainer" placeholder="No. Container">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputPassword">Voyage :</label>
				<div class="controls">
					<input type="text" id="voyage">
				</div>
			</div>
            <div class="control-group">
				<div class="controls">
					<a title="Cari" class="btn btn-search" onclick="filterGridData();"><i class="icon-search"></i></a>
				</div>
			</div>
		</form>
	</fieldset>
    <div id="topProgress" class="progress progress-striped" style="display:none">
            <div  class="bar" style="width: 100%;"></div>
        </div>
    <div id="deleteList">
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
<!--/ Modal Detail-->

<div id="containerModal" class="modal hide fade fullwidth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div id="containerModalProgress" class="modal-body progress progress-striped">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <div id="containerModalCreateContent" ></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-danger" id="deleteContainer">Delete</button>
		<button class="btn btn-primary" id="printContainer">Cetak</button>
	</div>
</div>
<script type="text/javascript">
    containerModal = function (id, type) {
        $("#containerModal").unbind();
        // wire up the buttons to dismiss the modal when shown
        $("#containerModal").bind("show", function () {
            $("#containerModal").addClass("container");
            $("#containerModalProgress").show();
            $("#containerModalCreateContent").hide();
            $("#containerModalProgressTitle").show();
            $("#containerModal .btn:not(.btn-primary)").click(function (e) {
                // hide the dialog box
                $("#containerModal").modal('hide');
            });
            $("#deleteContainer").click(function (e) {
                $("#containerModal").modal('hide');
                confirmModalContainerDelete('/Delete/Delete/'+id,'delete','/Delete/IndexAjax',id)
            });
            $("#printContainer").click(function (e) {
                $("#containerModalCreateContent").print();
                //$("#containerModal").modal('hide');
            });
            $.get(baseurl+"/Container/Detail/?id=" + id + "&type=" + type + "&next=0&prev=0" , function (data) {
                $("#containerModal").css("margin-top", "-282px");
                $(".modal-footer").show();
                $("#containerModalProgressTitle").hide();
                $("#containerModalProgress").hide();
                $("#containerModalCreateContent").html(data);
                $("#containerModalCreateContent").show();
            });
        });
        // remove the event listeners when the dialog is hidden
        $("#containerModal").bind("hide", function () {
            // remove event listeners on the buttons
            $("#containerModal .btn").unbind();
            $("#containerModal").removeClass("container");
        });
        // finally, wire up the actual modal functionality and show the dialog
        $("#containerModal").modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true // this parameter ensures the modal is shown immediately
        });
    }

    deleteModal = function (conID, cn) {
        $.confirm({
            title: 'Delete this Container '+ cn +'?',
            boxWidth: '20%',
            useBootstrap: false,
            content:
                '<div class="form-group">' +
                '<label>Please input reason for delete!</label><br>' +
                '<input type="text" placeholder="Reason" class="input-reason form-control" required />' +
                '</div>',
            buttons: {
                formSubmit: {
                    text: 'Delete',
                    btnClass: 'btn-red',
                    action: function () {
                        let reason = this.$content.find('.input-reason').val();
                        if (!reason) {
                            $.alert('please input delete reason!!');
                            return false;
                        }

                        $.confirm({
                            boxWidth: '20%',
                            useBootstrap: false,
                            content: function () {
                                var self = this;
                                self.setTitle('Delete Response...');
                                return $.ajax({
                                    url: baseurl + '/Delete/Delete',
                                    type: "GET",
                                    datatype: "json",
                                    data: {
                                        id: conID,
                                        deleteNote: reason
                                    }
                                }).done(function (e) {
                                    if (e.message.toLowerCase() == "true")
                                        self.setContent("Delete Data Success...!");
                                    else
                                        self.setContent(e.message);
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                    self.setContentAppend('Please try again later...');
                                });
                            },
                            buttons: {
                                close: function () {
                                    $("form > .control-group a.btn-search").trigger('click');
                                    //window.location.reload();
                                }
                            }
                        });
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    }

    function getDate(date){
        if(!date){
            date ="";
        }else{
            date = date.split('/');
            if ($.isArray(date)) {
                date = date[1] + '-' + date[0] + '-' + date[2];
            }
        }  
        return  date;
    }

    function OnDataBinding(e){
        $("#topProgress").show();
        var grid = $("#ContainerList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
    }

    function OnDataBound(e){
        $("#topProgress").hide();
    }

    function getParamUrl()
    {
        cn = $("#inputNoContainer").val();
        voyage = $("#voyage").val();
        return "/?cn=" + cn+ "&voyage=" + voyage;
    }

    function getAjaxUrl() {
        var selectUrl = "<%=Url.Action("_AjaxFilterBinding","Delete")%>" +getParamUrl();
        return selectUrl;
    }

    function filterGridData() {
        var grid = $("#ContainerList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
        grid.ajaxRequest();
        return false;
    }

    function loadList() {
        $("#topProgress").show();
        $.get(baseurl+"/Delete/IndexAjax/", function (data) {
            $("#topProgress").hide();
            $("#deleteList").html(data);
        });
        return false;
    }

    $(document).ready(function(){ 
    loadList();
    });
    confirmModalContainerDelete = function (url, title, listurl,cn) {
        confirmModal(url, title, listurl, "Delete", "Anda yakin akan menghapus container : " + cn + " ?")
    }
</script>
</asp:Content>
