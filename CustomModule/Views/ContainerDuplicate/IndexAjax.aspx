﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<List<CustomModule.Models.ContainerModel>>" %>
                        <%
                        Html.Telerik()
                              .Grid(Model)
                              .Name("ContainerDuplicateList")
                              .ClientEvents(events=>events.OnDataBinding("OnDataBinding").OnDataBound("OnDataBound"))
                              .Columns(columns =>
                              {
                                  columns.Bound(o => o.ActionCheckbox).Title("").Encoded(false);
                                  columns.Bound(o => o.TAGNUMBER).Title("No. Tag");
                                  columns.Bound(o => o.TRUCKNUMBER).Title("No. Polisi");
                                  columns.Bound(o => o.GATENUMBER).Title("No. Gate in");
                                  columns.Bound(o => o.GATEINTIME).Title("Waktu Masuk");
                                  columns.Bound(o => o.GATEOUTTIME).Title("Waktu Keluar");
                                  columns.Bound(o => o.TerminalId).Title("Terminal ID");
                                  columns.Bound(o => o.ContainerAndSize).Title("No. Container / Size");
                                  columns.Bound(o => o.DOCUMENTNUMBER).Title("Dok. Pabean");
                                  columns.Bound(o => o.DOCUMENTTYPE).Title("Dok. Tipe");
                                  columns.Bound(o => o.SEALNOTE).Title("Putusan");
                                  columns.Bound(o => o.HOLDTYPE).Title("Hold Type");
                                  columns.Bound(o => o.HOLDNOTE).Title("Hold Note");
                                  columns.Bound(o => o.RELEASENOTE).Title("Release Note");
                                  columns.Bound(o => o.EXPORTIMPORT).Title("Jenis Kegiatan");
                                  columns.Bound(o => o.Note).Title("Note");
                                  columns.Bound(o => o.Action).Title("Action").Encoded(false);
                              })
                              .Sortable(sorting => sorting
                                  .SortMode(GridSortMode.SingleColumn)
                                  .OrderBy(order =>
                                  {
                                      order.Add(o => o.GATEINTIME).Descending() ;
                                  }
                                  ))
                              .DataBinding(d => d.Ajax().Select("_AjaxBinding", "ContainerDuplicate"))
                              .Pageable(paging=>paging.PageSize(20)
                                  .Style(GridPagerStyles.NextPreviousAndNumeric)
                                  .Position(GridPagerPosition.Bottom))
                              .Footer(true)
                              .HtmlAttributes(new{@class="width1400"})
                              .Filterable().Render();
                      %>