﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Container Duplicate
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link href="<%=Url.Content("~/Content/styles/jquery-confirm.min.css")%>" type="text/css" rel="stylesheet"/>
<style type="text/css">
.card {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 120px;
    border: 1px solid #cccccc;
    float:left;
    margin-right:10px;
    margin-top:10px;
}

/* On mouse-over, add a deeper shadow */
.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

/* Add some padding inside the card container */
.container {
    padding: 2px 16px;
}
.card-title1
{
    background-color:#05842c;
    color:white;
    padding:5px;
    margin-top: 0px;
}
.card-title2
{
    background-color:#9e3d3d;
    color:white;
    padding:5px;
    margin-top: 0px;
}

label.datetimepicker2{
    margin-right: 10px;
}

div.datetimepicker2{
    margin-left: 10px;
    margin-right: 10px;
}
</style>
<div class="alert <%=((bool)ViewData["HasBroken"])?"":"hide"%> alert-error pull-right ">
	    <button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4>Warning!</h4>
		Ada kontainer yang Rusak.
	</div>
    <fieldset>
		<legend><i class="icon-copy"></i> Container Duplicate</legend>
		<form class="form-horizontal">
			<div class="control-group">
				<label class="datetimepicker2 control-label" for="inputEmail">Tanggal :</label>
                <div class="datetimepicker2 input-append">
                    <input data-format="MM/dd/yyyy" type="text" id="fromDate" value="<%=(ViewData["dtFrom"])%>"/>
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div> - 
                <div class="datetimepicker2 input-append">
                    <input data-format="MM/dd/yyyy" type="text" id="toDate" value="<%=(ViewData["dtEnd"])%>"/>
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
			</div>

			<div class="control-group">
				<div class="controls">
					<a title="Cari" class="btn btn-search" onclick="filterGridData();"><i class="icon-search"></i></a>
                    <%--<div class="btn-group pull-right">
					    <a class="btn pull-right dropdown-toggle" data-toggle="dropdown">Export <span class="caret"></span></a>
                        <ul class="dropdown-menu pull-right">
                        <li><a id="exportCSV">Export to CSV</a></li>
                        <li><a id="exportExcell">Export to Excell</a></li>
                        </ul>
				    </div>--%>
				</div>
			</div>
            <div class="card-content control-group" style="padding-left: 25px;padding-right: 25px;">
            </div>
		</form>
	</fieldset>
    <div id="topProgress" class="progress progress-striped" style="display:none">
            <div  class="bar" style="width: 100%;"></div>
        </div>
    <div id="arsipList" class="DataGridXScroll span12">
    </div>

    <div class="control-group">
		<div class="controls">
			<a title="Delete" class="btn" onclick="checkboxItem();" style="margin: 15px 35px;"><i class="icon-trash"></i> Delete Checked Item</a>
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<style>
    .width1400
    {
        width: 1400px;
    }
    .DataGridXScroll
    {
        overflow-x: scroll;
        text-align: left;
    }
</style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
<!--/ Modal Detail-->

<div id="containerModal" class="modal hide fade fullwidth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div id="containerModalProgress" class="modal-body progress progress-striped">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <div id="containerModalCreateContent" ></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-primary" id="printContainer">Cetak</button>
	</div>
</div>

<script src="<%=Url.Content("~/Content/js/bootstrap-datetimepicker.min.js")%>" type="text/javascript"></script>
<script src="<%=Url.Content("~/Content/js/jquery-confirm.min.js")%>" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $('.datetimepicker2').datetimepicker({
            language: 'en',
            //pick12HourFormat: true
            pickTime: false
        });
    });

    containerModal = function (id, type) {
        $("#containerModal").unbind();
        // wire up the buttons to dismiss the modal when shown
        $("#containerModal").bind("show", function () {
            $(".modal-footer").hide();
            $("#containerModal").addClass("container");
            $("#containerModalProgress").show();
            $("#containerModalCreateContent").hide();
            $("#containerModalProgressTitle").show();
            $("#containerModal .btn:not(.btn-primary)").click(function (e) {
                // hide the dialog box
                $("#containerModal").modal('hide');
            });
            $("#printContainer").click(function (e) {
                $("#containerModalCreateContent").print();
                //$("#containerModal").modal('hide');
            });
//            $.get(baseurl+"/Container/Detail/" + id, function (data) { //old
            $.get(baseurl+"/Container/Detail/?id=" + id+"&type="+type+"&next="+0+"&prev="+0, function (data) {
            
                $("#containerModal").css("margin-top", "-282px");
                $(".modal-footer").show();
                $("#containerModalProgressTitle").hide();
                $("#containerModalProgress").hide();
                $("#containerModalCreateContent").html(data);
                $("#containerModalCreateContent").show();
            });
        });
        // remove the event listeners when the dialog is hidden
        $("#containerModal").bind("hide", function () {
            // remove event listeners on the buttons
            $("#containerModal .btn").unbind();
            $("#containerModal").removeClass("container");
        });
        // finally, wire up the actual modal functionality and show the dialog
        $("#containerModal").modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true // this parameter ensures the modal is shown immediately
        });
    }

    checkboxItem = function () {
        console.log("Valid Checkbox");

        let selected = [];
        $("input.duplicate-check").each(function () {
            if ($(this).prop("checked")) {
                let val = $(this).attr("data-conid");
                selected.push(val);
            }
            else {
                console.log(false);
            }
        });

        console.log(selected);
        if (selected.length > 0) {
            deleteModal(selected.toString());
        }
    }

    deleteModal = function (listConID) {
        $.confirm({
            title: 'Delete selected Container data?',
            boxWidth: '20%',
            useBootstrap: false,
            content:
                '<div class="form-group">' +
                '<label>Please input reason for delete!</label><br>' +
                '<input type="text" placeholder="Reason" class="input-reason form-control" required />' +
                '</div>',
            buttons: {
                formSubmit: {
                    text: 'Delete',
                    btnClass: 'btn-red',
                    action: function () {
                        let reason = this.$content.find('.input-reason').val();
                        if (!reason) {
                            $.alert('please input delete reason!!');
                            return false;
                        }

                        $.confirm({
                            boxWidth: '20%',
                            useBootstrap: false,
                            content: function () {
                                var self = this;
                                self.setTitle('Delete Response...');
                                return $.ajax({
                                    url: baseurl +'/ContainerDuplicate/Delete',
                                    type: "GET",
                                    datatype: "json",
                                    data: {
                                        conID: listConID,
                                        deleteNote: reason
                                    }
                                }).done(function (e) {
                                    if (e.message.toLowerCase() == "true")
                                        self.setContent("Delete Data Success...!");
                                    else
                                        self.setContent(e.message);
                                }).fail(function () {
                                    self.setContent('Something went wrong.');
                                    self.setContentAppend('Please try again later...');
                                });
                            },
                            buttons: {
                                close: function () {
                                    $("form > .control-group a.btn-search").trigger('click');
                                    //window.location.reload();
                                }
                            }
                        });
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    }

    function getDate(date){
        if(!date){
            date ="";
        }else{
            date = date.split('/');
            if ($.isArray(date)) {
                date = date[0] + '-' + date[1] + '-' + date[2];
            }
        }  
        return  date;
    }

    function OnDataBinding(e){
        $("#topProgress").show();
        var grid = $("#ContainerDuplicateList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
    }

    function OnDataBound(e){
        $("#topProgress").hide();
    }

    function getParamUrl(){
        fromDate = $("#fromDate").val();
        toDate = $("#toDate").val();
        fromDate = getDate(fromDate);
        toDate = getDate(toDate);

        return "/?from=" + fromDate + "&to=" + toDate;
    }

    function getAjaxUrl() {
        var selectUrl = "<%=Url.Action("_AjaxFilterBinding","ContainerDuplicate")%>" +getParamUrl();
        return selectUrl;
    }

    function filterGridData() {
        var grid = $("#ContainerDuplicateList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
        grid.ajaxRequest();
        return false;
    }

    <%--$("#exportCSV").click(function(){
        $(this).attr('target','_blank');
        window.open("<%=Url.Action("ExportCSV","ContainerDuplicate")%>"+getParamUrl());
    });

    $("#exportExcell").click(function(){
        $(this).attr('target','_blank');
        window.open("<%=Url.Action("ExportExcell","ContainerDuplicate")%>"+getParamUrl());
    });--%>

    function loadList() {
        $("#topProgress").show();
        $.get(baseurl +"/ContainerDuplicate/IndexAjax" + getParamUrl(), function (data) {
            $("#topProgress").hide();
            $("#arsipList").html(data);
        });
        return false;
    }
    
    $(document).ready(function(){
        loadList();
    });


</script>
</asp:Content>
