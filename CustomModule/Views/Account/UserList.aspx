﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.RegisterModel>" %>
		    <table class="table table-bordered table-striped">
				<thead>
                    <th>
                    <a href="javascript:void(0);" title="Add"  onclick="createModal('/Account/UserAdd','User','/Account/UserList');"  class="btn btn-mini">
                    <i class="icon-plus-sign"></i></a></th>
					<th>Nama Lengkap</th>
					<th>NIP</th>
					<th>Username</th>
					<th>Roles</th>
					<th>Aktif</th>
					<th>Action</th>
				</thead>
                <tbody>
		        
            <% int i = 1; %>
            <% foreach(MembershipUser user in (MembershipUserCollection) ViewData["Users"]){ %>
            <% CustomModule.Models.UserProfile p = CustomModule.Models.UserProfile.GetUserProfile(user.UserName); %>
            <%if(p!=null){ %>
			<tr>
                <th><%=i++ %></th>
				<td><%=(p.Name!=null ? p.Name : "")%></td>
				<td><%=(p.NIP!=null ?p.NIP:"")%></td>
				<td><%=user.UserName %></td>
				<td><%=String.Join(",",Roles.GetRolesForUser(user.UserName))%></td>
				<td><%=(user.IsApproved)?"Ya":"Tidak" %></td>
				<td class="center">
                    <%if(!user.IsApproved){ %>
					<a href="javascript:void(0);" title="Activate" onclick="confirmModal('/Account/UserActivate/<%=user.UserName%>','User','/Account/UserList','Activate','Aktifkan user : <%=user.UserName%>');"  class="btn btn-mini"><i class="icon-ok"></i></a>
					<% }%>
                    <%if(user.IsApproved){ %>
                    <a href="javascript:void(0);" title="Deactivate" onclick="confirmModal('/Account/UserDeactivate/<%=user.UserName%>','User','/Account/UserList','Deactivate','Nonaktifkan user : <%=user.UserName%>');"  class="btn btn-mini"><i class="icon-remove"></i></a>
                    <% }%>
                    <a href="javascript:void(0);" title="Edit"  onclick="createModal('/Account/UserEdit/<%=user.UserName%>','User','/Account/UserList');"  class="btn btn-mini"><i class="icon-edit"></i></a>
                    <a href="javascript:void(0);" title="Delete"  onclick="confirmModalUserDelete('/Account/UserDelete/<%=user.UserName%>','User','/Account/UserList','<%=user.UserName%>');"  class="btn btn-mini"><i class="icon-trash"></i></a>
				</td>
			</tr>
			<% }%>
			<% }%>
            </tbody>
			</table>
            <%if ((int)ViewData["TotalPages"] > 1)
              { %>
			<div class="pagination pagination-centered">
				<ul>
					<li><a href="javascript:void(0)" onclick="gotoPage(<%=(int)ViewData["Page"]-1%>)">Prev</a></li>
                    <%for (i = 1; i <= (int)ViewData["TotalPages"]; i++)
                      { %>
					<li><a href="javascript:void(0)" onclick="gotoPage(<%=i%>)"><%=i %></a></li>
                    <% }%>
					<li><a href="javascript:void(0)" onclick="gotoPage(<%=(int)ViewData["Page"]+1%>)">Next</a></li>
				</ul>
			</div>
            <% }%>