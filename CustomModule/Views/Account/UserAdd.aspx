﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.RegisterModel>" %>
    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		<h3>Tambah Data User</h3>
	</div>
    <div class="modal-body">
        <% using (Html.BeginForm("UserAdd", "Account", FormMethod.Post, new { @class = "form-horizontal" }))
           { %>
            <%= Html.ValidationSummary(true, "Account creation was unsuccessful. Please correct the errors and try again.",new { @class = "alert alert-error" }) %>
		    <div class="control-group">
			    <label class="control-label" for="namaLengkap">Nama Lengkap :</label>
			    <div class="controls">
			    	<%=Html.TextBoxFor(model => model.Name)%><br />
                    <%=Html.ValidationMessageFor(model => model.Name, null, new { @class = "alert alert-error" })%>
                </div>
		    </div>
		    <div class="control-group">
			    <label class="control-label" for="nip">NIP :</label>
			    <div class="controls">
			    	<%=Html.TextBoxFor(model => model.NIP)%><br />
                    <%=Html.ValidationMessageFor(model => model.NIP, null, new { @class = "alert alert-error" })%>
                </div>
		    </div>
		    <div class="control-group">
			    <label class="control-label" for="username">Username :</label>
			    <div class="controls">
			    <%=Html.TextBoxFor(model => model.UserName)%><br />
                <%=Html.ValidationMessageFor(model => model.UserName,null,new { @class = "alert alert-error" })%>
                </div>
		    </div>
		    <div class="control-group">
			    <label class="control-label" for="password">Password :</label>
			    <div class="controls">
			    	<%=Html.TextBoxFor(model => model.Password)%><br />
                    <%=Html.ValidationMessageFor(model => model.Password,null, new { @class = "alert alert-error" })%>
			    </div>
		    </div>
            <div class="control-group">
			    <label class="control-label" for="confirm password">Confirm Password :</label>
			    <div class="controls">
			    	<%=Html.TextBoxFor(model => model.ConfirmPassword)%><br />
                    <%=Html.ValidationMessageFor(model => model.ConfirmPassword, null, new { @class = "alert alert-error" })%>
			    </div>
		    </div>
		    <div class="control-group">
			    <label class="control-label" for="aktif">Aktif :</label>
			    <div class="controls">
					<div class="btn-group" data-toggle="buttons-radio" id="aktif">
						<button type="button" onClick="$('#Active').val('True');" class="btn btn-radio <%=(Model.Active)?"active":""%>" >Ya</button>
						<button type="button" onClick="$('#Active').val('False');" class="btn btn-radio <%=(!Model.Active)?"active":""%>">Tidak</button>
                        <input id="Active" type="hidden" name="Active" value="<%=(Model.Active)?"True":"False"%>">
					</div>
			    </div>
		    </div>
		    <div class="control-group">
			    <label class="control-label" for="hakAkses">Hak Akses :</label>
			    <div class="controls">    
                    <%foreach(string role in (string[])ViewData["Roles"]){ %>
                    <%
                      bool found;    
                      try
                      {
                          string s = string.Join(" ", Model.Roles).ToLower();
                          found = s.Contains(role.ToLower());
                      }catch{
                          found = false;  
                      }%>
                    <label class="checkbox">
						<input type="checkbox" name="Roles" value="<%=role %>" <%if(found){%>checked="checked"<%}%>/><%=role %>
					</label>
                    <%} %>
			    </div>
		    </div>
		<% } %>
	</div>