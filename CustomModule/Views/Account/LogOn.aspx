﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Logon.Master" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.LogOnModel>" %>

<asp:Content ID="loginTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Log On
</asp:Content>

<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id="login-box">
            <div class="container-fluid login-title">
				<span><%= System.Configuration.ConfigurationManager.AppSettings["ApplicationName"]%></span> <img src="<%=Url.Content("~/Content/img/logo.png")%>" alt="logo"/>
			</div>
            <% using (Html.BeginForm("LogOn", "Account", FormMethod.Post, new { @class = "form-horizontal"}) ) { %>
                <%= Html.ValidationSummary(true, "Login was unsuccessful. Please correct the errors and try again.", new { @class = "alert alert-error" }) %>
				<div class="control-group">
                    <label class="control-label" for="UserName">Username :</label>
					<div class="controls">
						<%= Html.TextBoxFor(m => m.UserName) %>
                        <%= Html.ValidationMessageFor(m => m.UserName) %>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Password">Password :</label>
					<div class="controls">
						<%= Html.PasswordFor(m => m.Password) %>
                        <%= Html.ValidationMessageFor(m => m.Password) %>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<label class="checkbox">
							<%= Html.CheckBoxFor(m => m.RememberMe) %>
                            <%= Html.LabelFor(m => m.RememberMe) %>
						</label>
						<button type="submit" class="btn">Sign in</button>
					</div>
				</div>
			<% } %>

	</div>
</asp:Content>
