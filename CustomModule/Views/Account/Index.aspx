﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.RegisterModel>" %>

<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    User List
</asp:Content>

<asp:Content ID="registerContent" ContentPlaceHolderID="MainContent" runat="server">

        <fieldset>
			<legend><i class="icon-user"></i> Administrasi User</legend>
			<form class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="inputEmail">Username :</label>
					<div class="controls">
						<input type="text" id="username" />
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button title="Cari" class="btn" onclick="return search()"><i class="icon-search"></i></button>
					</div>
				</div>
			</form>
		</fieldset>
        <div id="topProgress" class="progress progress-striped" style="display:none">
            <div  class="bar" style="width: 100%;"></div>
        </div>
        <div id="UserList">
        <%Html.RenderPartial("UserList"); %>
        </div>  
</asp:Content>

<asp:Content ID="script" ContentPlaceHolderID="ScriptContent" runat="server"> 

<script type="text/javascript">

    function search() {
        $("#topProgress").show();
        $.get(baseurl+"/Account/UserList/" + $("#username").val(), function (data) {
            $("#topProgress").hide();
            $("#UserList").html(data);
        });
        return false;
    }

    function gotoPage(page) {
        $("#topProgress").show();
        $.get(baseurl+"/Account/UserList/" + $("#username").val()+"?page="+page, function (data) {
            $("#topProgress").hide();
            $("#UserList").html(data);
        });
        return false;
    }

</script>
</asp:Content>