﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<CustomModule.Models.ChangePasswordModel>" %>

<asp:Content ID="changePasswordTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Change Password
</asp:Content>

<asp:Content ID="changePasswordContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Ubah Password</h2>

    <% using (Html.BeginForm()) { %>
        <%= Html.ValidationSummary(true, "Password change was unsuccessful. Please correct the errors and try again.",new { @class = "alert alert-error" }) %>
        <div>
            <fieldset>
                <legend>Account Information</legend>
                
                <div class="editor-label">
                    <%= Html.LabelFor(m => m.OldPassword) %>
                </div>
                <div class="editor-field">
                    <%= Html.PasswordFor(m => m.OldPassword) %><br />
                    <%= Html.ValidationMessageFor(m => m.OldPassword, null, new { @class = "alert alert-error" })%>
                </div>
                
                <div class="editor-label">
                    <%= Html.LabelFor(m => m.NewPassword) %>
                </div>
                <div class="editor-field">
                    <%= Html.PasswordFor(m => m.NewPassword) %><br />
                    <%= Html.ValidationMessageFor(m => m.NewPassword, null, new { @class = "alert alert-error" })%>
                </div>
                
                <div class="editor-label">
                    <%= Html.LabelFor(m => m.ConfirmPassword) %>
                </div>
                <div class="editor-field">
                    <%= Html.PasswordFor(m => m.ConfirmPassword) %><br />
                    <%= Html.ValidationMessageFor(m => m.ConfirmPassword, null, new { @class = "alert alert-error" })%>
                </div>
                
                
		        <div class="control-group">
			        <div class="controls">
					    <div class="btn-group" data-toggle="buttons-radio" id="aktif">
						    <button class="btn btn-primary" type="submit">Ubah password</button>
					    </div>
			        </div>
		        </div>
            </fieldset>
        </div>
    <% } %>
</asp:Content>
