﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Search
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="alert <%=((bool)ViewData["HasBroken"])?"":"hide"%> alert-error pull-right ">
	    <button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4>Warning!</h4>
		Ada kontainer yang Rusak.
	</div>
     <fieldset>
		<legend><i class="icon-search"></i> Search</legend>
	</fieldset>
    <div id="topProgress" class="progress progress-striped" style="display:none">
            <div  class="bar" style="width: 100%;"></div>
        </div>
    <div id="searchList">
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
<!--/ Modal Detail-->

<div id="containerModal" class="modal hide fade fullwidth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div id="containerModalProgress" class="modal-body progress progress-striped">
        <div  class="bar" style="width: 100%;"></div>
    </div>
    <div id="containerModalCreateContent" ></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-primary" id="printContainer">Cetak</button>
	</div>
</div>
<script type="text/javascript">
    containerModal = function (id, type) {
        $("#containerModal").unbind();
        // wire up the buttons to dismiss the modal when shown
        $("#containerModal").bind("show", function () {
            $(".modal-footer").hide();
            $("#containerModal").addClass("container");
            $("#containerModalProgress").show();
            $("#containerModalCreateContent").hide();
            $("#containerModalProgressTitle").show();
            $("#containerModal .btn:not(.btn-primary)").click(function (e) {
                // hide the dialog box
                $("#containerModal").modal('hide');
            });
            $("#printContainer").click(function (e) {
                $("#containerModalCreateContent").print();
                //$("#containerModal").modal('hide');
            });
            $.get(baseurl+"/Container/Detail/?id=" + id+"&type="+type+"&next="+0+"&prev="+0, function (data) {
                $("#containerModal").css("margin-top", "-282px");
                $(".modal-footer").show();
                $("#containerModalProgressTitle").hide();
                $("#containerModalProgress").hide();
                $("#containerModalCreateContent").html(data);
                $("#containerModalCreateContent").show();
            });
        });
        // remove the event listeners when the dialog is hidden
        $("#containerModal").bind("hide", function () {
            // remove event listeners on the buttons
            $("#containerModal .btn").unbind();
            $("#containerModal").removeClass("container");
        });
        // finally, wire up the actual modal functionality and show the dialog
        $("#containerModal").modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true // this parameter ensures the modal is shown immediately
        });
    }

    function OnDataBinding(e){
        $("#topProgress").show();
        var grid = $("#ContainerList").data('tGrid');
        grid.ajax.selectUrl = getAjaxUrl();
    }

    function OnDataBound(e){
        $("#topProgress").hide();
    }

    function getParamUrl()
    {
        search = $("#searchString").val();
        return "/?searchString=" + search;
    }

    function getAjaxUrl() {
        var selectUrl = "<%=Url.Action("_AjaxBinding","Search")%>" +getParamUrl();
        return selectUrl;
    }

    function loadList(search) {
        $("#searchString").val(search)
        $("#topProgress").show();
        $.get(baseurl+"/Search/IndexAjax/?searchString="+search, function (data) {
            $("#topProgress").hide();
            $("#searchList").html(data);
        });
        return false;
    }

    releaseModal = function (id, type) {
        title = (type == "E") ? "export" : "import";
        url = '/Container/Release/' + id;
        createModal(url, title, "/Container/Index/" + type, "Release");
    }

    
    releaseP2Modal = function (id, type) {
        title = (type == "E") ? "export" : "import";
        url = '/Container/ReleaseP2/' + id;
        createModal(url, title, "/Container/Index/" + type, "Release P2");
    };

    holdModal = function (id, type) {
        title = (type == "E") ? "export" : "import";
        url = '/Container/Hold/' + id;
        createModal(url, title, "/Container/Index/" + type, "Hold");
    }

    $(document).ready(function(){ 
        loadList('<%=ViewData["search"]%>');
    });

    confirmModalContainerDelete = function (url, title, listurl,cn) {
        confirmModal(url, title, listurl, "Delete", "Anda yakin akan menghapus container : " + cn + " ?")
    }
</script>
</asp:Content>
