﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<List<CustomModule.Models.ContainerModel>>" %>
                        <%
                        Html.Telerik()
                              .Grid(Model)
                              .Name("ContainerList")
                              .ClientEvents(events=>events.OnDataBinding("OnDataBinding").OnDataBound("OnDataBound"))
                              .Columns(columns =>
                              {
                                  columns.Bound(o => o.ContainerAndSize).Title("No. Container / Size");
                                  columns.Bound(o => o.TRUCKPOSITION).Title("Movement");
                                  columns.Bound(o => o.DOCUMENTNUMBER).Title("Dok. Pabean");
                                  columns.Bound(o => o.CONSIGNEE).Title("Consignee");
                                  columns.Bound(o => o.TAGNUMBER).Title("No. Tag");
                                  columns.Bound(o => o.TRUCKNUMBER).Title("No. Polisi");
                                  columns.Bound(o => o.GATENUMBER).Title("No. Gate in");
                                  columns.Bound(o => o.GATEINTIME).Title("Waktu");
                                  columns.Bound(o => o.SEALNOTE).Title("Keterangan");
                                  columns.Bound(o => o.EXPORTIMPORT).Title("Jenis Kegiatan");
                                  columns.Bound(o => o.Action).Title("Action").Encoded(false);
                              })
                              .Sortable(sorting => sorting
                                  .SortMode(GridSortMode.SingleColumn)
                                  .OrderBy(order =>
                                  {
                                      order.Add(o => o.GATEINTIME);
                                  }
                                  ))
                              .DataBinding(d => d.Ajax().Select("_AjaxBinding", "Search"))
                              .Pageable(paging=>paging.PageSize(10)
                                  .Style(GridPagerStyles.NextPreviousAndNumeric)
                                  .Position(GridPagerPosition.Bottom))
                              .Footer(true)
                              .Filterable().Render();
                      %>